
This directory contains any polyfills that are not provided by
`babel-polyfill` or by another node module.

In order to include such a polyfill, add the implementation file in the
`entry.main` array (in the webpack configuration) before the project's
entry point(s) (e.g. `/app/index.js`).
