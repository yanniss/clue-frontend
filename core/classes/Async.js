
/**
 *
 * @param {number} delay - Time to sleep in seconds. For higher precision use a float,
 *      e.g. sleep(.016); to sleep for 16 milliseconds.
 * @returns {Promise<any>}
 */
const sleep = delay => new Promise(resolve => {

    setTimeout(() => {
        resolve();
    }, delay * 1000);
});

const event = (eventType = 'click') => new Promise(resolve => {

    const listener = () => {
        resolve();
        document.removeEventListener(eventType, listener);
    };

    document.addEventListener(eventType, listener);
});

export { sleep, event };
