
/**
 *
 */
export default class ArrayHelper {

    static createNat(length) {

        return ArrayHelper._create(length, (_, i) => i);
    }

    static createRandomNum(length) {

        return ArrayHelper._create(length, _ => Math.random());
    }

    static _create(length, mapFn) {

        return Array.from({ length: length }, mapFn);
    }

};
