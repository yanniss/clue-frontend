import ko from 'knockout';
import str from './StringHelper.static';

/**
 * Each uri segment, optional or mandatory, is recognized by Route and a parameter is created which will be passed to
 * the relevant component. Each parameter is a knockout observable to which the component itself must subscribe to if
 * needed.
 *
 * For each one of the received patterns, Route automatically appends an optional query string for crossroads and
 * creates a parameter named "_query" to be passed to the relevant component. Therefore, the optional query string can
 * be omitted in the received patterns.
 */
export default class Route {

    /** @type {array<string>} - The patterns that route to this Route. */
    patterns;

    /** @type {string} - The name of this Route's component. */
    component;

    /** @type {function: promise} - A function that fetches the component dynamically when requested. Returns a promise
     * that gets fulfilled upon successful fetch. */
    fetch;

    /** @type {object} - A map containing the parameters to be passed to the component instance. */
    params;

    /**
     *
     * @param {string|array<string>} patterns
     * @param {string} componentName
     * @param {function} [fnFetchComponent]
     */
    constructor(
        patterns,
        componentName,
        fnFetchComponent,
    ) {

        Route._validateArguments(patterns, componentName, fnFetchComponent);

        patterns = Array.isArray(patterns) ? patterns : [ patterns ];

        this.patterns = Route._getPatterns(patterns);
        this.component = componentName;
        this.fetch = fnFetchComponent || (() => new Promise(resolve => resolve()));
        this.params = Route._getParams(patterns);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static _validateArguments(patterns, componentName, fnFetchComponent) {

        if(Array.isArray(patterns)) {
            const index = patterns.findIndex(pattern => !str.isString(pattern));
            if(index !== -1) {
                throw new Error(`Route: items in 'patterns' array must a string: ${patterns[index]}`);
            }
        }
        else if(!str.isString(patterns)) {
            throw new Error(`Route: 'patterns' must be a string: ${patterns}`);
        }

        if(!str.isString(componentName)) {
            throw new Error(`Route: 'componentName' must be a string: ${componentName}`);
        }

        if(fnFetchComponent && typeof fnFetchComponent !== 'function') {
            throw new Error(`Route: optional 'fnFetchComponent' must be a function: ${fnFetchComponent}`);
        }
    }

    static _getParams(patterns) {

        const regex1 = /{([a-zA-Z0-9_$]+)}/g;   // matches with crossroads mandatory segments.
        const regex2 = /:([a-zA-Z0-9_$]+):/g;   // matches with crossroads optional segments.

        return patterns.reduce((acc, pattern) => {
            let match;

            while((match = regex1.exec(pattern)) !== null) {
                acc[match[1]] = ko.observable();
            }
            while((match = regex2.exec(pattern)) !== null) {
                acc[match[1]] = ko.observable();
            }
            acc['_query'] = ko.observable();

            return acc;
        }, {});
    }

    static _getPatterns(patterns) {

        return patterns.map(pattern => pattern + '/:?query:');
    }

};
