import { isFunction } from './FunctionHelpers';

/**
 *
 */
export default class PromiseQueue {

    /**
     * Pushes a promise in the queue.
     *
     * Guarantees that the pushed promise will be handled (i.e. execute the given "thener" or "catcher") after all
     * previously pushed promises in the queue have been settled, unless "waitForPreviousPendingPromises" is set to
     * "true".
     *
     * @param {Promise} promise
     * @param {function} [thener]
     * @param {function} [catcher]
     * @param {boolean} [doNotWaitPreviousPendingPromises = true]
     * @param {boolean} [discardPreviousPendingHandlers = true]
     */
    push(
        promise,
        thener,
        catcher,
        doNotWaitPreviousPendingPromises = true,
        discardPreviousPendingHandlers = true,
    ) {

        if(!(promise instanceof Promise)) {
            throw new Error(`PromiseQueue.push: argument 'promise' must be a Promise if given: ${promise}`);
        }
        if(thener && !isFunction(thener, true)) {
            throw new Error(`PromiseQueue.push: optional argument 'thener' must be a function if given: ${thener}`);
        }
        if(catcher && !isFunction(catcher, true)) {
            throw new Error(`PromiseQueue.push: optional argument 'catcher' must be a function if given: ${catcher}`);
        }

        thener = thener || (data => data);
        catcher = catcher || (data => { throw data; });

        const pending = this._pending;

        if(discardPreviousPendingHandlers) {
            // Disable all previous promise settle handlers in the queue.
            pending.forEach(pending => { pending.handlerEnabled = false; });
        }

        let data, fulfilled;

        // Handle given promise and store its settled status and returned data.
        // This is done in order to catch the possible promise rejection.
        promise = promise.then(results => {
            data = results;
            fulfilled = true;
        }).catch(error => {
            data = error;
            fulfilled = false;
        });

        let promises = [ promise ];
        if(!doNotWaitPreviousPendingPromises && pending.length > 0) {
            promises.push(pending[pending.length - 1].promise);
        }

        const queueEntry = {
            promise: null,
            handlerEnabled: true,
        };
        pending.push(queueEntry);

        // Wait for the settling of the previously pushed promise and the currently given one.
        queueEntry.promise = Promise.all(promises).then(_ => {
            // Handle promise result, only if this promise's handler is still enabled (i.e. not disabled by a later
            // pushed promise).
            queueEntry.handlerEnabled && (fulfilled ? thener(data) : catcher(data));

            // The promise has been handled (if its handler was enabled), so remove it from the pending promises.
            pending.splice(pending.findIndex(item => item === queueEntry), 1);
        });
    }

    isEmpty() {

        return this._pending.length === 0;
    }

    clear() {

        this._pending.forEach(pending => { pending.handlerEnabled = false; });
        this._pending = [];
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /** @type {Array} - Queue of pending promises and their handlers' state. */
    _pending = [];

};
