
/**
 *
 */
export default class Dom {

    static fireCustomEvent(element, type, data, options = {}) {

        element.dispatchEvent(new CustomEvent(type, {
            bubbles: true,
            detail: data,
            ...options,
        }));
    }

    /**
     * Returns the fixed position based on `top` and `left` for `element`, so that `element` will fit whole inside the
     * viewport; with the exception of `element` plus the given `margin` being larger than the viewport, in which case
     * `element` will overflow the right and/or bottom part of the viewport.
     *
     * Typical usage would be for a right-click menu which should open at the cursor position, but the menu should not
     * overflow outside of the viewport area.
     *
     * @param {HTMLElement} element
     * @param {number} top - Pixels from the top of the viewport.
     * @param {number} left - Pixels from the left of the viewport.
     * @param {number} [margin = 5]
     * @returns {{top: number, left: number}} - Fixed position relative to the viewport.
     */
    static fixPositionInViewport(element, top, left, margin = 0) {

        const vw = window.innerWidth,
            vh = window.innerHeight,
            ew = element.offsetWidth,
            eh = element.offsetHeight;

        // The "5" value passed to "Math.max" is to allow a small gap for clicking outside the `element` in case it
        // covers the whole viewport.
        const fixedTop = Math.max(Math.min(vh - eh - margin, top), 5),
            fixedLeft = Math.max(Math.min(vw - ew - margin, left), 5);

        return { top: fixedTop, left: fixedLeft };
    }

    /**
     * @param {HTMLElement} element
     * @returns {number} - (content width + paddings + borders width + margins)
     */
    static width(element) {

        const width = element.offsetWidth,
            style = getComputedStyle(element);

        return width + parseInt(style.marginRight) + parseInt(style.marginLeft);
    }

    /**
     * @param {HTMLElement} element
     * @returns {number} - (content height + paddings + borders height + margins)
     */
    static height(element) {

        const height = element.offsetHeight,
            style = getComputedStyle(element);

        return height + parseInt(style.marginTop) + parseInt(style.marginBottom);
    }

};
