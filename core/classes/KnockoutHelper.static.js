import ko from 'knockout';
import str from './StringHelper.static';
import { assert } from 'core/classes/Assert';

/**
 *
 */
export default class KnockoutHelper {

    static observable(...args) {

        const observable = ko.observable.apply(ko, args);

        observable.equalityComparer = (oldValue, newValue) => newValue === oldValue;

        return observable;
    }

    static computed(...args) {

        const computed = ko.computed.apply(ko, args);

        computed.equalityComparer = (oldValue, newValue) => newValue === oldValue;

        return computed;
    }

    /**
     * The component may or may not have its own view (let's call it "ownView"). Also, the component may or may not use
     * any HTML provided on its instantiation inside the component tags (let's call it "argView").
     *
     *  <component-x>
     *      This text is part of argView.
     *      <p>Also, this paragraph is part of argView.</p>
     *      <!-- this comment and the following component instantiation is part of argView. -->
     *      <component-inner></component-inner>
     *      etc etc... actually, everything inside the "component-x" tags is part of argView.
     *  </component-x>
     *
     * If the component has an ownView, it uses it for sure.
     * If an argView is also given, the component may either use it or discard it, based on whether the component is
     * built to handle an argView. A component can handle an argView, iff its base element or an element in its ownView
     * has the class "__ARG_VIEW_CONTAINER__"; in which case the argView is inserted inside that element. If multiple
     * elements with the "__ARG_VIEW_CONTAINER__" class exist, then the first one found in the DOM is chosen.
     *
     * @param {string} name
     * @param {function|class} vm
     * @param {string} [ownView = '']
     * @param {boolean} [supportsArgView = false]
     * @param {boolean} [ownViewIsArgView = false]
     */
    static registerComponent(name, vm, ownView = '', supportsArgView = false, ownViewIsArgView = false) {

        assert(str.isString(name), `registerComponent: "name" must be a string: ${name}`);
        assert(vm instanceof Function, `registerComponent: "vm" must be a function/class: ${vm}`);
        assert(str.isString(ownView), `registerComponent: "view" must be a string: ${ownView}`);

        ko.components.register(name, {

            viewModel: {
                createViewModel: (params, { element, templateNodes }) => {

                    if(supportsArgView) {
                        const argViewContainer = ownViewIsArgView ? element : findArgViewContainer(element);

                        if(argViewContainer) {
                            argViewContainer.innerHTML = '';
                            templateNodes.forEach(node => argViewContainer.appendChild(node));
                        }
                    }

                    return new vm(element, params);
                },
            },

            // In case "ownView" is an empty string (e.g. "ownView" argument not provided, or empty "X.ownView.html" file), the
            // following comment is passed to component's template in order to avoid knockout's error
            // "Component ... has no template".
            template: ownView || '<!-- automatically inserted by `registerComponent` -->',
        });
    }

};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// private stuff
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const argViewContainerClassName = '__ARG_VIEW_CONTAINER__';

const findArgViewContainer = node => {

    if(node instanceof HTMLElement) {

        return node.classList.contains(argViewContainerClassName)
            ? node
            : node.getElementsByClassName(argViewContainerClassName)[0];
    }
    else if(node instanceof Comment) {

        const v = ko.virtualElements;

        for(let child = v.firstChild(node); child; child = v.nextSibling(child)) {
            const argViewContainer = findArgViewContainer(child);
            if(argViewContainer) {
                return argViewContainer;
            }
        }
    }
    // All other node types are irrelevant to knockout component instantiation.
};
