
export const isNatural = argument => Number.isInteger(argument) && argument >= 0;

export const isPositiveNatural = argument => Number.isInteger(argument) && argument > 0;
