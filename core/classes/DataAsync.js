
/**
 *
 */
export default class DataAsync {

    /**
     *
     * @param {function: Promise} loadMethod
     */
    constructor(loadMethod) {

        this.loadMethod = loadMethod;
    }

    get(thener = null, forceReload = false, suppressExceptionOnError = false) {

        if(forceReload || !this.data && !this.pending) {
            this.pending = true;
            this.promise = this.loadMethod()
                .finally(() => {
                    this.pending = false;
                })
                .then(data => {
                    this.data = data;
                    return data;
                })
                .catch(error => {
                    throw error;
                });
        }

        return this.promise
            .then(data => {
                return thener ? thener(data) : data;
            })
            .catch(error => {
                if(!suppressExceptionOnError) throw error;
            });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    loadMethod = null;
    promise = null;
    data = null;
    pending = false;

};
