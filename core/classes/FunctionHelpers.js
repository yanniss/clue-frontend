
export const isFunction = (arg, loose = false) => {

    const type = typeof arg;

    return type === 'function' || (loose && (type === 'undefined' || arg === null));
};

export const once = callback => {

    let called = false;

    return function() {
        if(!called) {
            called = true;
            callback.apply(this, arguments);
        }
    };
};

export const throttle = (callback, waitTime) => {

    let time = Date.now();

    return function() {
        if((time + waitTime - Date.now()) < 0) {
            callback.apply(this, arguments);
            time = Date.now();
        }
    };
};

export const debounce = (callback, waitTime, immediate = false) => {

    let timeout;

    if(immediate) {
        const clear = () => { clearTimeout(timeout); timeout = null; };

        return function() {
            timeout ? clear() : callback.apply(this, arguments);
            timeout = setTimeout(clear, waitTime);
        };
    }
    else {
        return function() {
            clearTimeout(timeout);
            timeout = setTimeout(() => callback.apply(this, arguments), waitTime);
        };
    }
};
