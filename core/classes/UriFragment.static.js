import obj from './ObjectHelper.static';
import str from './StringHelper.static';

/**
 *
 */
export default class UriFragment {

    /**
     *
     * @returns {string}
     */
    static getPath() {

        return location.hash.split('?')[0].substring(1);
    }

    /**
     *
     * @returns {string}
     */
    static getQuery() {

        return location.hash.split('?')[1] || '';
    }

    /**
     *
     * @param {boolean} asArray
     * @returns {array|object}
     */
    static getQueryParams(asArray = false) {

        return UriFragment.getQuery().split('&').reduce((acc, param) => {

            if(!str.isEmpty(param, true)) {
                let [ name, value ] = param.split('=');
                name = decodeURIComponent(name);
                value = decodeURIComponent(value) || undefined;

                asArray ? acc.push({ name: name, value: value }) : (acc[name] = value);
            }

            return acc;

        }, asArray ? [] : {});
    }

    /**
     *
     * @param {string} paramName
     * @returns {boolean}
     */
    static hasQueryParam(paramName) {

        return UriFragment.getQueryParams().hasOwnProperty(paramName);
    }

    /**
     *
     * @param {string} paramName
     * @returns {string}
     */
    static getQueryParamValue(paramName) {

        return UriFragment.getQueryParams()[paramName];
    }

    /**
     *
     * @param {string} queryString - Must be uri-encoded. Note that the query string does not contain the query's "?".
     */
    static setQueryString(queryString) {

        if(!str.isString(queryString)) {
            throw new Error(`UriFragment.setQueryString: 'query' must be a string: ${queryString}`);
        }
        else if(str.isEmpty(queryString)) {
            location.hash = `${UriFragment.getPath()}`;
        }
        else {
            location.hash = `${UriFragment.getPath()}?${queryString}`;
        }
    }

    /**
     *
     * @param {array|object} params
     */
    static setQueryParams(params) {

        let query = '';

        if(Array.isArray(params)) {
            params.forEach(({ name, value }, index) => {
                query += `${index === 0 ? '' : '&' }${encodeURIComponent(name)}=${encodeURIComponent(value)}`;
            })
        }
        else {
            Object.entries(params).forEach(([ name, value ], index) => {
                query += `${index === 0 ? '' : '&' }${encodeURIComponent(name)}=${encodeURIComponent(value)}`;
            });
        }

        UriFragment.setQueryString(query);
    }

    /**
     *
     * @param {object} paramsToBeAdded - params to be added or updated
     * @param {object} paramsToBeRemoved
     */
    static updateQueryParams(paramsToBeAdded = {}, paramsToBeRemoved = {}) {

        // Remove params
        const queryParams = UriFragment.getQueryParams(true).filter(
            param => !paramsToBeRemoved.hasOwnProperty(param.name)
        );

        // Update existing params
        paramsToBeAdded = obj.clone(paramsToBeAdded);
        queryParams.forEach(param => {
            if(paramsToBeAdded.hasOwnProperty(param.name)) {
                param.value = paramsToBeAdded[param.name];
                delete paramsToBeAdded[param.name];
            }
        });

        // Append new params
        Object.entries(paramsToBeAdded).forEach(([ name, value ]) => {
            if(paramsToBeRemoved.hasOwnProperty(name)) {
                throw new Error(`UriFragment.updateQueryParams: same parameter name to be both added and removed: ${name}`);
            }
            queryParams.push({ name: name, value: value });
        });

        UriFragment.setQueryParams(queryParams);
    }

    /**
     *
     * @param {string} paramNames
     */
    static removeQueryParams(...paramNames) {

        UriFragment.updateQueryParams({}, paramNames.reduce((acc, paramName) => {
            acc[paramName] = null;
            return acc;
        }, {}));
    }

};
