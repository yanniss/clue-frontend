
let counter = 0;

/**
 *
 */
export default class StringHelper {

    static isString(arg, loose = false) {

        const type = typeof arg;

        return type === 'string' || arg instanceof String || (loose && (type === 'undefined' || arg === null));
    }

    static isEmpty(string, trimWhitespaces = false) {

        return trimWhitespaces ? string.trim().length === 0 : string.length === 0;
    }

    static capitalize(string) {

        return string[0].toUpperCase() + string.substr(1);
    }

    static createUnique() {

        return '_UNIQUE_' + counter++;
    }

    static escapeHtml(htmlString) {

        const textarea = document.createElement('textarea');

        textarea.textContent = htmlString;

        return textarea.innerHTML;
    }

};
