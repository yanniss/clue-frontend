import str from 'core/classes/StringHelper.static';

const CTX_APP = str.createUnique(),
    CTX_PAGE = str.createUnique(),
    CTX_ASCENDANTS = str.createUnique(),
    CTX_DESCENDANTS = str.createUnique();

export {
    CTX_APP,
    CTX_PAGE,
    CTX_ASCENDANTS,
    CTX_DESCENDANTS,
};
