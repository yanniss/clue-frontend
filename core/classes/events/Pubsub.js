import { isFunction } from 'core/classes/FunctionHelpers';

/**
 *
 */
export default class Pubsub {

    events = {};

    /**
     *
     * @param {string} type
     * @param {*} data
     * @param {null|*} source - Set this value if you want to omit self notification. Requires that the subscriber has been set when called "on".
     */
    emit(type, data, source = null) {

        const events = this.events;
        let event = events[type];

        if(!event) {
            event = events[type] = new Event(type);
        }

        event.notify(data, source);
    }

    on(type, callback, subscriber = null, {
        synchronous = false,
        initialize = true,
        ignoreIfEquals = true,
    } = {
        synchronous: false,
        initialize: true,
        ignoreIfEquals: true,
    }) {

        const events = this.events;
        let event = events[type];

        if(!event) {
            event = events[type] = new Event(type);
        }

        event.push(subscriber, callback, synchronous, ignoreIfEquals);

        if(initialize && event.hasData) {
            callback.call(subscriber, event.data);
        }
    }

    off(type, callback, subscriber = null) {

        const event = this.events[type];

        event && event.remove(subscriber, callback);
    }

    clear() {

        this.events = {};
    }

};

/**
 *
 */
class Event {

    /** @type {string} type */
    type;

    /** @type {*} data */
    data;

    /** @type {boolean} hasData - Equals to "true" if "data" has been set at least once. */
    hasData = false;

    /** @type {array<object>} subscriptions */
    subscriptions = [];

    constructor(type) {

        this.type = type;
    }

    toString() {

        return this.type;
    }

    push(subscriber, callback, synchronous, ignoreIfEquals) {

        this.subscriptions.push({
            subscriber: subscriber,
            callback: callback,
            synchronous: synchronous,
            ignoreIfEquals: ignoreIfEquals,
        });
    }

    remove(subscriber, callback) {

        let fnFilterIn;

        if(subscriber && callback) {
            fnFilterIn = subscription => subscriber !== subscription.subscriber || callback !== subscription.callback;
        }
        else if(subscriber) {
            fnFilterIn = subscription => subscription.subscriber !== subscriber;
        }
        else if(callback) {
            fnFilterIn = subscription => subscription.callback !== callback;
        }
        else {
            throw new Error('Pubsub/Event.remove: cannot remove with both subscriber and callback undefined');
        }

        this.subscriptions = this.subscriptions.filter(fnFilterIn);
    }

    notify(data, source) {

        const subscriptions = this.subscriptions;

        for(let i = 0, n = subscriptions.length; i < n; i++) {
            const subscription = subscriptions[i],
                subscriber = subscription.subscriber,
                ignoreIfEquals = subscription.ignoreIfEquals;

            if(source && source === subscriber) { continue; }
            if(ignoreIfEquals === true && data === this.data) { continue; }
            if(isFunction(ignoreIfEquals) && ignoreIfEquals(data, this.data)) { continue; }

            if(subscription.synchronous) {
                subscription.callback.call(subscriber, data);
            }
            else {
                setTimeout(() => subscription.callback.call(subscriber, data));
            }
        }

        this.data = data;
        this.hasData = true;
    }

}
