import Base from './Base.vm';
import PromiseQueue from '../PromiseQueue';
import Route from '../Route';
import crossroads from 'crossroads/dist/crossroads.min';
import hasher from 'hasher/dist/js/hasher.min';
import ko from 'knockout';
import koh from '../KnockoutHelper.static';
import { CTX_DESCENDANTS } from 'core/classes/events/contexts';
import { COMPONENT_REQUIRES_RELOAD } from 'core/classes/events/types';

/**
 *
 */
export default class Router extends Base {

    /**
     * @public
     * @param {HTMLElement} element
     * @param {{pages:array<Route>,notfound:Route}} routes
     */
    constructor(element, routes) {

        super(element);

        if(!routes.hasOwnProperty('pages') || !Array.isArray(routes.pages)) {
            throw new Error(`Router: 'routes.pages' must exist and be an Array: ${routes}`);
        }
        else {
            const index = routes.pages.findIndex(route => !(route instanceof Route));
            if(index !== -1) {
                throw new Error(`Router: items in 'routes.pages' array must be a Route: ${routes[index]}`);
            }
        }
        if(!routes.hasOwnProperty('notfound') || !(routes.notfound instanceof Route)) {
            throw new Error(`Router: 'routes.notfound' must be of type Route: ${routes.notfound}`);
        }

        this.pageRoutes = routes.pages;
        this.notfoundRoute = routes.notfound;

        this.listenToChildSelfReloadRequest();
        this.subscribeToRouteChange();
        this.setCrossroads();
        this.setHasher();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @private
     * @type {array<Route>}
     */
    pageRoutes;

    /**
     * @private
     * @type {Route}
     */
    notfoundRoute;

    /**
     * @private
     * @type {PromiseQueue}
     */
    requests = new PromiseQueue();

    /**
     * @private
     * @type {ko.observable}
     */
    currentRoute = koh.observable();

    /**
     * @private
     */
    listenToChildSelfReloadRequest() {

        this.on(CTX_DESCENDANTS, COMPONENT_REQUIRES_RELOAD, () => this.currentRoute.valueHasMutated());
    }

    /**
     * @private
     */
    subscribeToRouteChange() {

        this.subscribe(this.currentRoute, route => {

            // The following conditional "comma" prepending keeps knockout from including the property-value pair
            // '"undefined":""' to the parameters that will be passed to the component.
            const params = Object.entries(route.params).reduce((acc, [ name, value ], index) =>
                `${acc}${index > 0 ? ',' : ''}${name}:currentRoute.peek().params['${name}']`
            , '');

            const html = route ? `<${route.component} params="${params}"></${route.component}>` : '';

            ko.utils.setHtml(this.element, html);
            ko.applyBindingsToDescendants(this, this.element);
        });
    }

    /**
     * Setup crossroads which manages the routing, based on URL's hash part and the given "routes".
     *
     * @private
     */
    setCrossroads() {

        // Add page routes to crossroads.
        this.pageRoutes.forEach(route => {
            route.patterns.forEach(pattern => {
                crossroads.addRoute(pattern, requestParams => {
                    this.requests.push(route.fetch(), () => this.handleMatchedPattern(route, requestParams));
                });
            });
        });

        // Filter argument passed to "addRoute" handler (i.e. onRoutePatternMatched).
        crossroads.normalizeFn = crossroads.NORM_AS_OBJECT;

        // Handle "on successful routing" event.
        crossroads.routed.add(hash => {
            console.debug(`valid route: *${hash}*`);
        });

        // Handle "on failed routing" event.
        // Another way to do it: https://github.com/millermedeiros/crossroads.js/issues/132
        crossroads.bypassed.add(hash => {
            console.debug(`invalid route: *${hash}*`);

            this.requests.push(Promise.resolve(), () => {
                this.currentRoute(this.notfoundRoute);
            });
        });

        // If "ignoreState" is not enabled, then crossroads will not trigger "routed" and "bypassed"
        // events when going from an invalid route to a valid route, and vice versa, respectively.
        crossroads.ignoreState = true;
    }

    /**
     * @private
     * @param {Route} route
     * @param {object|null} requestParams
     */
    handleMatchedPattern(route, requestParams) {

        Object.entries(route.params).forEach(([ name, value ]) => {
            if(requestParams.hasOwnProperty(name)) {
                // The parameter is a uri segment. Pass the uri-decoded value to the routed component.
                value(decodeURIComponent(requestParams[name]));
            }
            else if(name === '_query' && requestParams.hasOwnProperty('?query')) {
                // The parameter is the uri query string. Pass the deeply uri-decoded value to the routed component.
                const param = requestParams['?query'] || {};

                value(Object.entries(param).reduce((acc, [ name, value ]) => {
                    acc[decodeURIComponent(name)] = decodeURIComponent(value);
                    return acc;
                }, {}));
            }
        });

        this.currentRoute(route);
    }

    /**
     * Setup hasher, which listens to changes in the URL's hash part and calls crossroad's appropriate function.
     *
     * @private
     */
    setHasher() {

        function parseHash(newHash, oldHash) {
            crossroads.parse(newHash);
        }

        hasher.raw = true;                  // do not decode uri parts
        hasher.initialized.add(parseHash);  // parse initial hash
        hasher.changed.add(parseHash);      // parse hash changes
        hasher.init();                      // start listening for history change
    }

};
