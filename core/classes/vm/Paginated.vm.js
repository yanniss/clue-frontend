/**
 * todos:
 * - Use stricter parseInt: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/parseInt#A_stricter_parse_function
 * - Add a "has pending fetch items" requests function
 */

import Page from './Page.vm';
import PromiseQueue from '../PromiseQueue';
import ko from 'knockout';
import uri from '../UriFragment.static';

const defaultQuery = '';
const defaultOrderBy = 'asc';
const defaultItemsPerPage = 100;

/**
 *
 */
export default class Paginated extends Page {

    sortByOptions;
    query = ko.observable().extend({ deferred: true });
    sortBy = ko.observable().extend({ deferred: true });
    orderBy = ko.observable().extend({ deferred: true });
    itemsPerPage = ko.observable().extend({ deferred: true });
    totalPages = ko.observable(0).extend({ deferred: true });
    totalItems = ko.observable(0).extend({ deferred: true });
    page = ko.observable().extend({ deferred: true });
    items = ko.observableArray();

    /**
     *
     * @param {HTMLElement} element
     * @param {Array<ko.observable>} reloadParams
     * @param {ko.observable} queryParams
     * @param {Object} options - Map of options:
     *      {?} sortBy:
     *      {Array<?>} sortByOptions:
     *      {Function: Promise} fnFetchItems:
     *      [query]:
     *      [orderBy]:
     *      [page]:
     *      [itemsPerPage]:
     */
    constructor(element, reloadParams, queryParams, {
        fnFetchItems,
        sortBy,
        sortByOptions,
        query = defaultQuery,
        orderBy = defaultOrderBy,
        page = 1,
        itemsPerPage = defaultItemsPerPage,
    }) {

        super(element, reloadParams);

        Paginated._validateArguments(queryParams, sortBy, sortByOptions, fnFetchItems);

        this._fnFetchItems = fnFetchItems;
        this.sortByOptions = sortByOptions;

        // Set default values.
        this._defaultSortBy = Paginated._filterSortBy(sortBy, sortByOptions);
        this._defaultOrderBy = Paginated._filterOrderBy(orderBy);
        this._defaultItemsPerPage = Paginated._filterItemsPerPage(itemsPerPage);

        // Set initial values.
        this.query(Paginated._filterQuery(query));
        this.sortBy(this._defaultSortBy);
        this.orderBy(this._defaultOrderBy);
        this.itemsPerPage(this._defaultItemsPerPage);
        this.page(Paginated._filterPage(page, this.totalPages()));

        //this._setSanitizing();
        //this._setPageReset();
        //this._setUpdatingFromQueryParams(queryParams);
        //this._setQueryParamsUpdating(); // Also initializes the state based on query param values.
        //this._setItemsUpdating();
    }

    dispose() {

        super.dispose();
    }

    goToPreviousPage = () => {

        this.page(Math.max(this.page.peek() - 1, 1));
    };

    goToNextPage = () => {

        this.page(Math.min(this.page.peek() + 1, this.totalPages.peek()));
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    _fnFetchItems;
    _defaultSortBy;
    _defaultOrderBy;
    _defaultItemsPerPage;
    _pendingItemFetches = new PromiseQueue();

    static _validateArguments(queryParams, sortBy, sortByOptions, fnFetchItems) {

        if(!ko.isObservable(queryParams)) {
            throw new Error(`Paginated: 'queryParams' must be a knockout observable: ${queryParams}`);
        }

        // Validate required options.

        if(typeof sortBy === 'undefined') {
            throw new Error(`Paginated: 'options.sortBy' is a required option`);
        }
        if(!Array.isArray(sortByOptions)) {
            throw new Error(`Paginated: 'options.sortByOptions' must be of type Array: ${sortByOptions}`);
        }
        if(!sortByOptions.includes(sortBy)) {
            throw new Error(`Paginated: 'options.sortBy' must be included in 'options.sortByOptions'`);
        }
        if(typeof fnFetchItems !== 'function') {
            throw new Error(`Paginated: 'options.fnFetchItems' must be a function (required option)`);
        }
    }

    _setSanitizing() {

        // Whenever one of { query, sortBy, orderBy, itemsPerPage, totalPages } changes, then reset page to defaultPage.

        console.log('set sanitizing');

        this.subscribe(this.query, query => {
            const filtered = Paginated._filterQuery(query);
                console.log(`%cquery: ${query} -> ${filtered}`, 'color: red;');

            this.query(Paginated._filterQuery(query));
            this.page(this._filterPage(1));
        });

        this.subscribe(this.sortBy, sortBy => {
            const filtered = this._filterSortBy(sortBy);
                console.log(`%csortBy: ${sortBy} -> ${filtered}`, 'color: red;');
            this.sortBy(filtered);
            this.page(this._filterPage(1));
        });

        this.subscribe(this.orderBy, orderBy => {
            const filtered = this._filterOrderBy(orderBy);
                console.log(`%corderBy: ${orderBy} -> ${filtered}`, 'color: red;');
            this.orderBy(filtered);
            this.page(this._filterPage(1));
        });

        this.subscribe(this.itemsPerPage, itemsPerPage => {
            const filtered = this._filterItemsPerPage(itemsPerPage);
                console.log(`%citemsPerPage: ${itemsPerPage} -> ${filtered}`, 'color: red;');
            this.itemsPerPage(filtered);
            this.page(this._filterPage(1));
        });

        this.subscribe(this.totalPages, totalPages => {
            const filtered = Paginated._filterTotalPages(totalPages);
                console.log(`%ctotalPages: ${totalPages} -> ${filtered}`, 'color: red;');
            this.totalPages(filtered);
            this.page(this._filterPage(this.page.peek()));
        });

        this.subscribe(this.page, page => {
            const filtered = this._filterPage(page);
                console.log(`%cpage: ${page} -> ${filtered}`, 'color: red;');
            this.page(filtered);
        });
    }

    _setPageReset() {

        /*
        const fn = () => {
            this.page(this._filterPage(1));
        };

        let setDependenciesOnly = true;

        this.computed(() => { console.log(`%cCOMPUTED 0.1: page reset`, 'color: pink;');

            this.query(); this.sortBy(); this.orderBy(); this.itemsPerPage();

            setDependenciesOnly || fn();
            setDependenciesOnly = false;

        }, { deferred: true });

        /*
        this.computed(() => {

            console.log(`%cCOMPUTED 0.2: page reset`, 'color: pink;');

            this.totalPages();
            this.page(this._filterPage(this.page.peek()));

        }, { deferred: true });
        */
    }

    _setUpdatingFromQueryParams(queryParams) {

        this.subscribe(queryParams, queryParams => { console.log(`%cCOMPUTED 1: update from query params: `, 'color: blue;', queryParams);

            this.query(Paginated._filterQuery(queryParams.q));
            this.sortBy(this._filterSortBy(queryParams.s));
            this.orderBy(this._filterOrderBy(queryParams.o));
            this.itemsPerPage(this._filterItemsPerPage(queryParams.c));
            this.page(this._filterPage(queryParams.p));
        });
    }

    _setQueryParamsUpdating() {

        let setDependenciesOnly

        this.computed(() => {

            console.log('%cCOMPUTED 2: update URI', 'color:green');

            const q = this.query(),
                s = this.sortBy(),
                o = this.orderBy(),
                c = this.itemsPerPage(),
                p = this.page(),
                paramsToBeAdded = {},
                paramsToBeRemoved = {};

            q === defaultQuery              ? (paramsToBeRemoved.q = q) : (paramsToBeAdded.q = q);
            s === this._defaultSortBy       ? (paramsToBeRemoved.s = s) : (paramsToBeAdded.s = s);
            o === this._defaultOrderBy      ? (paramsToBeRemoved.o = o) : (paramsToBeAdded.o = o);
            c === this._defaultItemsPerPage ? (paramsToBeRemoved.c = c) : (paramsToBeAdded.c = c);
            p === 1                         ? (paramsToBeRemoved.p = p) : (paramsToBeAdded.p = p);

            uri.updateQueryParams(paramsToBeAdded, paramsToBeRemoved);

        }).extend({ deferred: true });
    }

    _fetchItems(q, s, o, c, p) {

        this._pendingItemFetches.push(this._fnFetchItems(q, s, o, c, p), ({ page, items, hits }) => {

            const totalPages = Math.ceil(hits / this.itemsPerPage());

            console.log(`%c  page: ${this.page.peek()} -> ${page} # %c${this.page.peek() === page}`, 'color: brown', 'color: blue;');
            console.log(`%c  total: ${this.totalPages.peek()} -> ${totalPages}`, 'color: brown');

            this.totalItems(hits);
            this.totalPages(Paginated._filterTotalPages(totalPages));
            this.items(items);
            page !== undefined && this.page(this._filterPage(page));

        });
    }

    _setItemsUpdating() {

        let setDependenciesOnly = true;

        this.computed(() => {

            console.log('%cCOMPUTED 3: fetch items', 'color:brown');

            const q = this.query(),
                s = this.sortBy(),
                o = this.orderBy(),
                c = this.itemsPerPage(),
                p = this.page();

            setDependenciesOnly || this._fetchItems(q, s, o, c, p);
            setDependenciesOnly = false;

        }).extend({ deferred: true });
    }

    static _filterQuery(query) {

        return query === undefined ? defaultQuery : query;
    }

    static _filterSortBy(sortBy, sortByOptions) {

        return sortByOptions.includes(sortBy) ? sortBy : sortByOptions[0];
    }

    _filterSortBy(sortBy) {

        return Paginated._filterSortBy(sortBy, this.sortByOptions);
    }

    static _filterOrderBy(orderBy, defaultValue = defaultOrderBy) {

        return ['asc', 'desc'].includes(orderBy) ? orderBy : defaultValue;
    }

    _filterOrderBy(orderBy) {

        return Paginated._filterOrderBy(orderBy, this._defaultOrderBy);
    }

    static _filterItemsPerPage(itemsPerPage, defaultValue = defaultItemsPerPage) {

        itemsPerPage = parseInt(itemsPerPage);

        if(!Number.isInteger(itemsPerPage) || itemsPerPage < 1) {
            itemsPerPage = defaultValue;
        }

        return itemsPerPage;
    }

    _filterItemsPerPage(itemsPerPage) {

        return Paginated._filterItemsPerPage(itemsPerPage, this._defaultItemsPerPage);
    }

    static _filterTotalPages(totalPages) {

        totalPages = parseInt(totalPages);

        if(!Number.isInteger(totalPages) || totalPages < 0) {
            return 0;
        }
        else {
            return totalPages;
        }
    }

    static _filterPage(page, totalPages) {

        page = parseInt(page);

        if(!Number.isInteger(page) || page < 1 || totalPages === 0) {
            return 1;
        }
        else if(page > totalPages) {
            return totalPages;
        }
        else {
            return page;
        }
    }

    _filterPage(page) {

        return Paginated._filterPage(page, this.totalPages.peek());
    }

};
