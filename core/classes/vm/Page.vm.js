import Base from './Base.vm';
import ko from 'knockout';
import {
    CTX_ASCENDANTS,
    CTX_PAGE
} from 'core/classes/events/contexts';
import { COMPONENT_REQUIRES_RELOAD } from 'core/classes/events/types';

/**
 *
 */
export default class Page extends Base {

    /**
     *
     * @param {HTMLElement} element
     * @param {ko.observable|array<ko.observable>|{}} [params = {}]
     */
    constructor(element, params = {}) {

        super(element);

        let reloadParams;

        if(ko.isObservable(params)) {
            reloadParams = [ params ];
        }
        else if(Array.isArray(params)) {
            reloadParams = params;
        }
        else if(params && typeof params === 'object') {
            reloadParams = [];
            for(const [ name, value ] of Object.entries(params)) {
                if(name === '$raw' || name === '_query') { continue; }
                reloadParams.push(value);
            }
        }

        for(let i = 0, n = reloadParams.length; i < n; i++) {
            this.subscribe(reloadParams[i], () => this.notify(CTX_ASCENDANTS, COMPONENT_REQUIRES_RELOAD));
        }

        this.createPubsubContext(CTX_PAGE);
    }

};
