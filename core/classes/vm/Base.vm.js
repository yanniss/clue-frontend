import Pubsub from 'core/classes/events/Pubsub';
import dom from 'core/classes/Dom.static';
import ko from 'knockout';
import koh from 'core/classes/KnockoutHelper.static';
import {
    CTX_ASCENDANTS,
    CTX_DESCENDANTS
} from 'core/classes/events/contexts';
import {
    COMPONENT_CREATION_FINISHED,
    COMPONENT_CREATION_STARTED,
    COMPONENT_DISPOSAL_FINISHED
} from 'core/classes/events/types';
import {assert} from 'core/classes/Assert';

/**
 *
 */
export default class Base {

    /**
     * @public
     * @type {string} - The view model's unique identifier.
     */
    id = '' + Base.instancesCounter++;

    /**
     * @public
     * @type {HTMLElement} - The DOM element this view model is bound to.
     * */
    element;

    /**
     * @public
     * @param {HTMLElement} element
     */
    constructor(element) {

        this.element = element;
        this.name = element.dataset.name;

        this.notifyAscendantsAboutMyCreation();
        this.createDisplayMutationObserver();
        this.listenToChildrenStateChanges();

        setTimeout(() => this.initAfterBinding());
    }

    /**
     * @public
     */
    dispose() {

        for(const subscription of Object.values(this.subscriptions)) {
            subscription.dispose();
        }

        this.destroyDisplayMutationObserver();
        this.notifyAscendantsAboutMyDisposal();
    }

    /**
     * @public
     * @returns {string}
     */
    toString() {

        return this.id;
    }

    /**
     * To be overridden by derived view model.
     *
     * @protected
     */
    initAfterBinding() {}

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Custom event mechanism
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @protected
     * @param {*} context
     * @param {*} type
     * @param {function} callback - first argument will be the event-related data.
     *      In the case of "CTX_DESCENDANTS", the component view model that fired the event will be provided as second
     *      argument.
     * @param [options = {}] - Since it depends on the "context" whether this function will use the dom event mechanism
     *      or a Pubsub for communicating the event between components, the "options" argument can have two different
     *      valid sets of properties, each for each case. Specifically:
     *      1) For Pubsub-based context, "options" is the same as the one "Pubsub.on" receives as its 4th argument.
     *      2) CTX_DESCENDANTS: "options" is the same as native "EventTarget.addEventListener" receives as its 3rd
     *         argument, plus custom option "allowPropagation" which allows bubbling the event above this component.
     * @returns {{dispose: function}}
     */
    on(context, type, callback, options = {}) {

        if(context === CTX_DESCENDANTS) {
            const internalCallback = event => {
                const { source, data } = event.detail;
                if(source !== this) {
                    callback(data, source);
                    options.allowPropagation || event.stopPropagation();
                }
            };

            return this.addEventListener(type, internalCallback, options);
        }

        const pubsub = this.contextPubsubs[context];
        if(pubsub) {
            const subscriptionId = '' + this.subscriptionCounter++,
                subscriptions = this.subscriptions;

            pubsub.on(type, callback, this, options);

            return (subscriptions[subscriptionId] = {
                dispose: () => {
                    delete subscriptions[subscriptionId];
                    pubsub.off(type, callback, this);
                },
            });
        }
        else {
            throw new Error(`Base.on: invalid context: ${context}`);
        }
    }

    /**
     * @protected
     * @param {*} context
     * @param {*} type
     * @param {*} [data = null]
     */
    notify(context, type, data = null) {

        if(context === CTX_ASCENDANTS) {
            dom.fireCustomEvent(this.element, type, { source: this, data: data }, this);
            return;
        }

        const pubsub = this.contextPubsubs[context];
        if(pubsub) {
            pubsub.emit(type, data, this);
        }
        else {
            throw new Error(`Base.notify: context does not exist: ${context}`);
        }
    }

    /**
     * @protected
     * @param {*} context
     */
    createPubsubContext(context) {

        if(this.contextPubsubs.hasOwnProperty(context)) {
            throw new Error(`Base.createPubsubContext: context already exists: ${context}`);
        }

        this.contextPubsubs[context] = new Pubsub();
    }

    /**
     * @protected
     * @param {*} context
     * @returns {Pubsub|undefined}
     */
    getPubsub(context) {

        return this.contextPubsubs[context];
    }

    /**
     * Pass context-based pubsubs to each child. A child belongs to all of the contexts its ancestors belong to.
     *
     * @private
     * @param {Base} child
     */
    passPubsubContextsToChild(child) {

        const childContextPubsubs = child.contextPubsubs;

        for(const [ name, value ] of Object.entries(this.contextPubsubs)) {
            if(!childContextPubsubs.hasOwnProperty(name)) {
                childContextPubsubs[name] = value;
            }
        }
    }

    /** @private */
    contextPubsubs = {};

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Knockout subscription & computed helpers
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @protected
     * @param {ko.observable} observable
     * @param {function} callback
     * @param {object} [target = this]
     * @returns {{dispose: dispose}}
     */
    subscribe(observable, callback, target = this) {

        const subscriptionId = '' + this.subscriptionCounter++,
            subscriptions = this.subscriptions,
            subscription = observable.subscribe(callback, target);

        return (subscriptions[subscriptionId] = {
            dispose: () => {
                delete subscriptions[subscriptionId];
                subscription.dispose();
            },
        });
    }

    /**
     * @protected
     * @param {function} func
     * @param {object} [target = this]
     * @returns {{dispose: dispose}}
     */
    computed(func, target = this) {

        const subscriptionId = '' + this.subscriptionCounter++,
            subscriptions = this.subscriptions,
            computed = koh.computed(func, target),
            originalDispose = computed.dispose;

        computed.dispose = () => {
            delete subscriptions[subscriptionId];
            originalDispose.call(computed);
        };

        return (subscriptions[subscriptionId] = computed);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // DOM Event handling helpers
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @protected
     * @param {*} type
     * @param {function} callback
     * @param {object} [options]
     * @param {Element|Window} [target = this.element]
     * @returns {{dispose: function}}
     */
    addEventListener(type, callback, options, target = this.element) {

        const subscriptionId = '' + this.subscriptionCounter++,
            subscriptions = this.subscriptions;

        target.addEventListener(type, callback, options);

        return (subscriptions[subscriptionId] = {
            dispose: () => {
                delete subscriptions[subscriptionId];
                target.removeEventListener(type, callback, options);
            },
        });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Visibility-related functionality.
    //
    // In the following context, the "visibility" term has to do with the element's "style.display" property.
    //
    // Handling changes in the visibility state works only when modifying it directly through the element's "style"
    // attribute. Working with css classes, e.g. bootstrap's "hide", will only confuse and possibly break the logic
    // below.
    //
    // If you want to declaratively make a component invisible through HTML, you can either do 'style="display:none;"',
    // or even 'data-bind="visible: false"'.
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @public
     */
    show() {

        this.element.style.display = '';
    }

    /**
     * @public
     */
    hide() {

        this.element.style.display = 'none';
    }

    /**
     * Will be called right after the component becomes visible. To be overridden by derived view model.
     *
     * @protected
     */
    onShown() {}

    /**
     * Will be called right after component becomes hidden. To be overridden by derived view model.
     *
     * @protected
     */
    onHidden() {}

    /**
     * @private
     * @type {MutationObserver}
     */
    displayMutationObserver;

    /**
     * @private
     */
    createDisplayMutationObserver() {

        const observer = new MutationObserver(mutations => {

            const styleMutation = mutations[0],
                wasHidden = styleMutation.oldValue === 'display: none;',
                isHidden = this.element.style.display === 'none';

            if(wasHidden === isHidden) {
                // nop
            }
            else if(wasHidden) {
                this.onShown();
            }
            else {
                this.onHidden();
            }
        });

        observer.observe(this.element, {
            attributes: true,
            attributeFilter: ['style'],
            attributeOldValue: true,
        });

        this.displayMutationObserver = observer;
    }

    /**
     * @private
     */
    destroyDisplayMutationObserver() {

        this.displayMutationObserver.disconnect();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Component family tree building functionality
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @public
     * @type {string|undefined} name - The view model's name given by its parent.
     * */
    name;

    /**
     * @public
     * @type {object} - Map containing the view models of the named children components.
     * */
    children = {};

    /**
     * @private
     */
    notifyAscendantsAboutMyCreation() {

        this.notify(CTX_ASCENDANTS, COMPONENT_CREATION_STARTED, this);

        setTimeout(() => {
            this.notify(CTX_ASCENDANTS, COMPONENT_CREATION_FINISHED, this);
        });
    }

    /**
     * @private
     */
    notifyAscendantsAboutMyDisposal() {

        setTimeout(() => {
            this.notify(CTX_ASCENDANTS, COMPONENT_DISPOSAL_FINISHED, this);
        });
    }

    /**
     * @private
     */
    listenToChildrenStateChanges() {

        this.on(CTX_DESCENDANTS, COMPONENT_CREATION_STARTED, child => {

            const name = child.name;
            name && (this.children[name] = child);

            this.passPubsubContextsToChild(child);
        });

        this.on(CTX_DESCENDANTS, COMPONENT_DISPOSAL_FINISHED, child => {

            const name = child.name;
            name && (delete this.children[name]);
        });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Various private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @private
     * @type {number} - Used for uniquely identifying each component instance in the whole application.
     */
    static instancesCounter = 0;

    /**
     * @private
     * @type {number}
     */
    subscriptionCounter = 0;

    /** @private */
    subscriptions = {};

    requirePropertyInAncestors(propertyName) {

        const koContext = ko.contextFor(this.element);
        const koNode = [ koContext.$data, ...koContext.$parents ].find(koNode =>
            koNode.hasOwnProperty(propertyName)
        );

        assert(koNode, `Did not find required property: ${propertyName}`);

        return koNode[propertyName];
    }

};
