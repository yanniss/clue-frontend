import Router from './Router.vm';
import { CTX_APP } from 'core/classes/events/contexts';

/**
 *
 */
export default class SPA extends Router {

    /**
     *
     * @param {HTMLElement} element
     * @param {{pages:array<Route>,notfound:Route}} routes
     */
    constructor(element, routes) {

        super(element, routes);

        this.createPubsubContext(CTX_APP);
    }

};
