import '@babel/polyfill';
import './styles/main.scss';
import App from 'app/App.vm';
import ko from 'knockout';
import koh from './classes/KnockoutHelper.static';

document.addEventListener('contextmenu', e => {
    (e.ctrlKey || e.altKey) ? e.stopPropagation() : e.preventDefault();
}, { capture: true });

koh.registerComponent('component-main', App);
ko.applyBindings({});
