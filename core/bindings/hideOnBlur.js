/**
 * todo: Implement hiding in a more accessibility-friendly way instead of "display: none;".
 */

import ko from 'knockout';

/**
 * The bound element is automatically hidden when:
 * - the focus is moved out of it (and its descendants).
 * - the escape key is pressed while it is focused.
 *
 * Usage: data-bind="hideOnBlur: switch"
 *
 *  "switch" must be a knockout observable that commands the bound element to show or hide:
 *  - "switch" has truthy value: show bound element.
 *  - "switch" has falsy value: hide bound element.
 *
 * Optional: data-focus
 *
 *  "data-focus" can be set as an attribute to a descendant element to automatically get focused whenever the bound
 *  element appears.
 *
 */
ko.bindingHandlers.hideOnBlur = {

    init: function(element, valueAccessor, allBindingsAccessor, data, context) {

        const switch_ = valueAccessor(),
            defaultToFocus = element.querySelector('[data-focus]');

        if(!ko.isObservable(switch_)) {
            console.error(element);
            throw new Error(`custom binding hideOnBlur: argument must be a knockout observable: ${switch_}`);
        }

        let outerLastFocused,
            innerLastFocused;

        const show = () => {

            outerLastFocused = document.activeElement;

            element.style.display = '';

            (defaultToFocus || innerLastFocused || element).focus();
        };

        const hide = () => {

            element.style.display = 'none';
        };

        const onFocusinHandler = event => {

            innerLastFocused = event.target;
        };

        const onFocusoutHandler = event => {

            // In the following commented logic, it is assumed that "FocusEvent.relatedTarget" contains the element that
            // the focus moved to, right after the bound element lost it. Therefore, if it was a descendant element, do
            // not hide the bound element:
            //
            //  const descendantGainedFocus = element.contains(event.relatedTarget);
            //  descendantGainedFocus || hide();
            //
            // Since "FocusEvent.relatedTarget" is not standardized yet:
            //  https://developer.mozilla.org/en-US/docs/Web/API/FocusEvent/relatedTarget
            // Use the following fallback logic instead:

            setTimeout(() => {
                const descendantGainedFocus = element.contains(document.activeElement);
                descendantGainedFocus || hide();
            });
        };

        const onKeydownHandler = event => {

            if(event.key === 'Escape') {
                hide();
                outerLastFocused && outerLastFocused.focus();
            }
        };

        hide();

        // tabindex="-1" is required in order to catch the "focusin" and "focusout" events.
        element.setAttribute('tabindex', '-1');

        element.addEventListener('keydown', onKeydownHandler);
        element.addEventListener('focusout', onFocusoutHandler);

        // Do not set "focusin" event listener if there is a default element to focus (listener will be redundant).
        defaultToFocus || element.addEventListener('focusin', onFocusinHandler);

        const subscription = switch_.subscribe(truthy => truthy ? show() : hide());

        ko.utils.domNodeDisposal.addDisposeCallback(element, () => {
            element.removeEventListener('keydown', onKeydownHandler);
            element.removeEventListener('focusout', onFocusoutHandler);
            element.removeEventListener('focusin', onFocusinHandler);

            subscription.dispose();
        });
    },

};
