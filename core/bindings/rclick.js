import ko from 'knockout';

/**
 * "contextmenu" event is being fired on "mouseup" on Windows, whereas in Linux & MacOS it is being fired on "mousedown".
 * This binding makes it consistent across platforms.
 *
 * Could use "auxclick" event but its spec is still in progress; plus it is not widely supported yet across browsers:
 *  https://developer.mozilla.org/en-US/docs/Web/API/Element/auxclick_event
 */
ko.bindingHandlers.rclick = {

    init: function(element, valueAccessor, allBindingsAccessor, data, context) {

        const callback = valueAccessor();
        let inProgress = false,
            onDocumentMouseupHandler = null;

        // Define element event handlers.

        const onMousedownHandler = event => {

            if(!isRightButtonEvent(event)) { return; }

            inProgress = true;
        };

        const onMouseupHandler = event => {

            if(!isRightButtonEvent(event) || !inProgress) { return; }

            inProgress = false;

            // Call `callback` through `.call` and with the following arguments in order to achieve the same behaviour
            // as knockout's builtin "click" binding.
            callback.call(data, data, event);
        };

        // Setting the document listener on "mouseup" may be a bit too much, but it helps to achieve the same triggering
        // behaviour as the builtin "click" event (either browser's or knockout's). An alternative but lighter solution
        // could be to simply set `inProgress` to "false" on "mouseleave".
        const onMouseleaveHandler = event => {

            if(!inProgress || onDocumentMouseupHandler) { return; }

            onDocumentMouseupHandler = event => {
                document.removeEventListener('mouseup', onDocumentMouseupHandler);
                onDocumentMouseupHandler = null;
                inProgress = false;
            };

            document.addEventListener('mouseup', onDocumentMouseupHandler);
        };

        element.addEventListener('contextmenu', onContextmenuHandler);
        element.addEventListener('mousedown', onMousedownHandler);
        element.addEventListener('mouseup', onMouseupHandler);
        element.addEventListener('mouseleave', onMouseleaveHandler);

        ko.utils.domNodeDisposal.addDisposeCallback(element, () => {
            element.removeEventListener('contextmenu', onContextmenuHandler);
            element.removeEventListener('mousedown', onMousedownHandler);
            element.removeEventListener('mouseup', onMouseupHandler);
            element.removeEventListener('mouseleave', onMouseleaveHandler);
            document.removeEventListener('mouseup', onDocumentMouseupHandler);
        });
    },

};

const isRightButtonEvent = event => parseInt(event.button) === 2;

const onContextmenuHandler = event => event.preventDefault();
