import ko from 'knockout';

/**
 * Could use "auxclick" event but its spec is still in progress; plus it is not widely supported yet across browsers:
 *  https://developer.mozilla.org/en-US/docs/Web/API/Element/auxclick_event
 */
ko.bindingHandlers.midclick = {

    init: function(element, valueAccessor, allBindingsAccessor, data, context) {

        const callback = valueAccessor();
        let inProgress = false,
            onDocumentMouseupHandler = null;

        // Define element event handlers.

        const onMousedownHandler = event => {

            if(!isMiddleButtonEvent(event)) { return; }

            // The following `preventDefault` disables probable middle click scrolling (either on element itself or on
            // one of its ascendants).
            event.preventDefault();

            inProgress = true;
        };

        const onMouseupHandler = event => {

            if(!isMiddleButtonEvent(event) || !inProgress) { return; }

            inProgress = false;

            // Call `callback` through `.call` and with the following arguments in order to achieve the same behaviour
            // as knockout's builtin "click" binding.
            callback.call(data, data, event);
        };

        // Setting the document listener on "mouseup" may be a bit too much, but it helps to achieve the same triggering
        // behaviour as the builtin "click" event (either browser's or knockout's). An alternative but lighter solution
        // could be to simply set `inProgress` to "false" on "mouseleave".
        const onMouseleaveHandler = event => {

            if(!inProgress || onDocumentMouseupHandler) { return; }

            onDocumentMouseupHandler = event => {
                document.removeEventListener('mouseup', onDocumentMouseupHandler);
                onDocumentMouseupHandler = null;
                inProgress = false;
            };

            document.addEventListener('mouseup', onDocumentMouseupHandler);
        };

        element.addEventListener('mousedown', onMousedownHandler);
        element.addEventListener('mouseup', onMouseupHandler);
        element.addEventListener('mouseleave', onMouseleaveHandler);

        ko.utils.domNodeDisposal.addDisposeCallback(element, () => {
            element.removeEventListener('mousedown', onMousedownHandler);
            element.removeEventListener('mouseup', onMouseupHandler);
            element.removeEventListener('mouseleave', onMouseleaveHandler);
            document.removeEventListener('mouseup', onDocumentMouseupHandler);
        });
    },

};

const isMiddleButtonEvent = event => parseInt(event.button) === 1;
