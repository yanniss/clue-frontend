import ko from 'knockout';

/**
 *
 */
ko.bindingHandlers.numberFormatted = {

    init: function(element, valueAccessor, allBindingsAccessor, data, context) {

        element.textContent = format(ko.unwrap(valueAccessor()));
    },

    update: function(element, valueAccessor, allBindingsAccessor, data, context) {

        element.textContent = format(ko.unwrap(valueAccessor()));
    },

};

function format(value) {

    if(window['Intl'] && Intl['NumberFormat']) {
        return Intl.NumberFormat('en').format(value);
    }
    else if(typeof value === 'number' || value instanceof Number) {
        return value.toLocaleString();
    }
    else {
        return Number(value).toLocaleString();
    }
}
