import 'jquery-ui/themes/base/core.css';
import 'jquery-ui/themes/base/resizable.css';
import 'jquery-ui/themes/base/theme.css';
import 'jquery-ui/ui/widgets/resizable';
import './resizable.scss';
import $ from 'jquery';
import ko from 'knockout';
import str from 'core/classes/StringHelper.static';

/**
 * Allows only one of these bindings applied to the same element. Applies the first, ignores the rest.
 * When initialized loads from localStorage any previous stored state (i.e. size).
 * When a resizing operation is completed, the new state is stored in localStorage.
 */

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// "resizable*" bindings
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ko.bindingHandlers.resizableTop = createResizableBinding('top');
ko.bindingHandlers.resizableRight = createResizableBinding('right');
ko.bindingHandlers.resizableBottom = createResizableBinding('bottom');
ko.bindingHandlers.resizableLeft = createResizableBinding('left');

function createResizableBinding(side) {

    return {
        init: function(element, valueAccessor, allBindingsAccessor, data, context) {

            const newValueAccessor = () => ({
                ...(valueAccessor() || {}),
                side: side,
            });

            ko.bindingHandlers.resizable.init(element, newValueAccessor, allBindingsAccessor, data, context);
        }
    };
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// "resizable" binding
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ko.bindingHandlers.resizable = {

    init: function(element, valueAccessor, allBindingsAccessor, data, context) {

        const { side, lsKey, lsValueId, onDrag } = valueAccessor(),
            warn = message => console.warn('knockout binding "resizable":', message, 'Element:', element);

        if(element.classList.contains('ko-resizable')) {
            warn(`Ignoring "resizable${str.capitalize(side)}" binding attempt. A resizable* binding has already been applied.`);
            return;
        }
        if(!['top', 'right', 'bottom', 'left'].includes(side)) {
            warn(`Ignoring "resizable" binding attempt. Invalid "side" argument: "${side}".`);
            return;
        }

        const $element = $(element),
            direction = translateSideToDirection(side);

        // todo: check if the following two (or any flag in general) is actually needed.
        let created = false,
            disposed = false;

        setTimeout(() => {

            if(disposed) { return; }

            addResizableClasses(element, direction, side);
            setResizableSize($element, direction, parseInt(loadSize(lsKey, lsValueId)));
            createResizeHandlerElement(element, side);

            const eventHandlers = getEventHandlers($element, direction, onDrag, size => storeSize(lsKey, lsValueId, size));

            $element.resizable({
                ...eventHandlers,
                handles: { side: $element.children('.ko-resizer')[0] },
            });

            created = true;
        });

        ko.utils.domNodeDisposal.addDisposeCallback(element, function() {

            if(created) {
                $element.resizable('destroy');
            }

            disposed = true;
        });
    },
};

function translateSideToDirection(side) {

    switch(side) {
        case 'top':
        case 'bottom':
            return 'height';
        case 'right':
        case 'left':
            return 'width';
    }
}

function translateSideToJQueryUi(side) {

    switch(side) {
        case 'top':     return 'n';
        case 'right':   return 'e';
        case 'bottom':  return 's';
        case 'left':    return 'w';
    }
}

function addResizableClasses(element, direction, side) {

    element.classList.add(
        // Add jQuery UI classes
        'ui-resizable',
        // Add ko binding "resizable" classes
        `ko-resizable`, `ko-resizable-${direction}`, `ko-resizable-${side}`,
    );
}

function setResizableSize($element, direction, size) {

    Number.isInteger(size) && $element[direction](size);
}

function createResizeHandlerElement(element, side) {

    const resizer = document.createElement('div');

    resizer.classList.add(
        // Add jQuery UI classes
        'ui-resizable-handle', `ui-resizable-${translateSideToJQueryUi(side)}`,
        // Add ko binding "resizable" classes
        'ko-resizer',
    );

    element.appendChild(resizer);
}

function getEventHandlers($element, direction, onDrag, storeSize) {

    const eventHandlers = {};

    if(onDrag instanceof Function) {
        let resizeTimerId = null;

        eventHandlers.start = () => {
            clearTimeout(resizeTimerId);
            resizeTimerId = setInterval(() => onDrag(), 100);
        };

        eventHandlers.stop = (event, ui) => {
            clearTimeout(resizeTimerId);
            resizeTimerId = null;
            storeSize(ui.size[direction]);
        };
    }
    else {
        eventHandlers.stop = (event, ui) => {
            storeSize(ui.size[direction]);
        };
    }

    return eventHandlers;
}

function loadSize(lsKey, lsValueId) {

    let size;

    if(lsKey) {
        let state = JSON.parse(localStorage.getItem(lsKey) || '{}');
        if(state.hasOwnProperty(lsValueId)) {
            size = state[lsValueId];
        }
    }

    return size;
}

function storeSize(lsKey, lsValueId, size) {

    if(lsKey) {
        let state = JSON.parse(localStorage.getItem(lsKey) || '{}');
        state[lsValueId] = size;
        localStorage.setItem(lsKey, JSON.stringify(state));
    }
}
