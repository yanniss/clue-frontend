import ko from 'knockout';

/**
 *
 */
ko.bindingHandlers.stemplate = {

    init: function(element, valueAccessor, allBindingsAccessor, data, context) {

        return { controlsDescendantBindings: true };
    },

    update: function(element, valueAccessor, allBindingsAccessor, data, context) {

        // `ko.utils.setHtml` will unwrap the value if needed.
        ko.utils.setHtml(element, valueAccessor());
        ko.applyBindingsToDescendants(context, element);
    },

};
