
import DataAsync from 'core/classes/DataAsync';
import { sleep } from 'core/classes/Async';

const successes = [ false, true ];
let calls = 0;

const fetchData = () => new Promise(async (resolve, reject) => {
    //const willSucceed = Math.random() >= .25;
    const willSucceed = successes[calls++ % 2];
    console.log('new fetch data', willSucceed);
    await sleep(1);
    willSucceed ? resolve(Math.random()) : reject('errorito');
});


(async () => {
    const data = new DataAsync(fetchData);

    data.then(data => { console.log('1', ...data); });
    data.then(data => { console.log('2', ...data); }, false);
    await sleep(2);
    data.then(data => { console.log('3', ...data); }, false);
    data.then(data => { console.log('4', ...data); });

})();
