import u from 'classes/UriFragment.static';

test('UriFragment', () => {
    expect(1+1).toBe(2);
});

/*
console.log('getPath():', u.getPath());
console.log('getQuery():', u.getQuery());
console.log('getQueryParams():', u.getQueryParams());
u.setQueryString('mike=2');
console.log('getQueryParamValue():', u.getQueryParamValue('mike'));
console.log('hasQueryParam():', u.hasQueryParam('mike'));
u.setQueryParams({ a: 1, b: 2, '/': '&' });
u.removeQueryParams('a');
u.removeQueryParams(['b','c']);
*/
