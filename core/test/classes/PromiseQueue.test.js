import PromiseQueue from 'classes/PromiseQueue';

describe('PromiseQueue', () => {

    const options = {
        promiseSucceed: [ true, false ],
        thener: [ 'omit', 'ok', 'throw '],
        catcher: [ 'omit', 'ok', 'throw '],
        keepHandlers: [ true, false ],
        waitPromises: [ true, false ],
        combos: [[1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,1,2], [3,2,1]],
    };

    const entries = Object.entries(options);
    let i = 0, j = 0;
    while(true) {
        if(i === entries.length) {
            break;
        }
        if(j === entries[i][1].length) {
            i++; i = 0;
            continue;
        }


    }


    function f(promiseSucceed, thener, catcher, keepHandlers, waitPromises, combos) {

    }


    function foo(queue, succeed, delay, passThener, passCatcher, ) {

        queue.push(
            new Promise((resolve, reject) => {
                setTimeout(() => {
                    succeed ? resolve('success') : reject('error');
                }, delay);
            }),
            passThener ? results => `thener: ${results}` : null,
        )
    }

    test('order of execution: A, B, C', () => {

        const pq = new PromiseQueue();
        expect(1).toBe(1);
    });

});



/*
let q = new PromiseQueue();

const pusher = (message, succeed, delay, truncate) => {
    q.push(
        new Promise((resolve, reject) => {
            setTimeout(() => {
                succeed ? resolve(Math.random()) : reject('ERROR');
            }, delay);
        }),
        results => {
            console.log(message, 'success', results);
        },
        error => {
            console.log(message, 'error', error);
            throw error;
        },
        truncate,
    );
};

pusher('first', false, 3500);
pusher('second', true, 2500, true);
pusher('third', true, 1500);
*/
