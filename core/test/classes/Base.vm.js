import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import koh from 'core/classes/KnockoutHelper.static';

class A extends Base {
    constructor(element) {
        super(element);

        let index = 0;
        const displays = [ true, false, false, true, false, true, true ];
        const intervalId = setInterval(() => {
            this.showChild(displays[index++]);
            if(index === displays.length) clearInterval(intervalId);
        }, 1000);
    }
    dispose() {
        super.dispose();
    }
    growChild() {
        const v = this.children.mike.value;
        v(v() + 1);
    }
    toggleChild() {
        //this.children.mike.toggle();
        this.showChild(!this.showChild());
    }
    showChild = ko.observable(true);
}

class B extends Base {
    constructor(element) {
        super(element);
    }
    dispose() {
        super.dispose();
    }
    toggle() {
        const display = this.element.style.display;
        this.element.style.display = display === 'none' ? 'block' : 'none';
    }
    value = ko.observable(0);
}

koh.registerComponent('component-main', A, `
<p>Hello I am A!</p>
<button data-bind="click: growChild">GROW</button>
<button data-bind="click: toggleChild">TOGGLE</button>
<component-b data-name="mike" data-bind="visible: showChild"></component-b>
...
`);

koh.registerComponent('component-b', B, `
<p>Hello I am B!</p>
<p data-bind="text: value"></p>
`);

ko.applyBindings({});
