import './Dropdown.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './Dropdown.view.html';
import vm from './Dropdown.vm';

koh.registerComponent('component-dropdown', vm, view);
