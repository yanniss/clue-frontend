import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import { CTX_ASCENDANTS } from 'core/classes/events/contexts';
import { DROPDOWN_OPTION_CHANGED } from './Dropdown.events';
import 'bootstrap.style';
import 'bootstrap.logic';

/**
 *
 */
export default class Dropdown extends Base {

    /**
     * @param element
     * @param {array<object|null>} options -
     *  `null` means separator.
     *  `object`:
     *      required properties: {string} text
     *      optional properties: {"asc"|"desc"} order
     * @param [defaultOption = null]
     */
    constructor(element, {
        options,
        defaultOption = null,
    }) {

        super(element);

        element.classList.add('dropdown');

        this.options = options;
        this.selected(defaultOption || options[0] || {});

        this.subscribe(this.selected, option =>
            this.notify(CTX_ASCENDANTS, DROPDOWN_OPTION_CHANGED, option)
        );
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    options;
    selected = ko.observable({});

    getOrderView = option => {

        if(!option) { return ''; }

        const { type, order } = option;

        if(type === 'quantity') {
            return `<span class="fa fa-sort-amount-${order}"></span>`;
        }
        else if(type === 'number') {
            return `<span class="fa fa-sort-numeric-${order}"></span>`;
        }
        else if(type === 'alpha') {
            const letters = order === 'asc' ? ['A','Z'] : ['Z','A'];
            return `<span class="order">${letters[0]}<span> ... ${letters[1]}</span></span>`;
        }
        else {
            return '';
        }
    };

    selectedView = this.computed(() => {

        const selected = this.selected();

        return `${selected.text} &nbsp; ${this.getOrderView(selected)}`;
    });

};
