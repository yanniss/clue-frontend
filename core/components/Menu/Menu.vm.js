import Base from 'core/classes/vm/Base.vm';
import dom from 'core/classes/Dom.static';

/**
 *
 */
export default class Menu extends Base {

    /**
     * @public
     * @param element
     */
    constructor(element) {

        super(element);

        element.setAttribute('tabindex', '-1');

        this.startListeningToLocalUiEvents();
    }

    /**
     * @public
     * @param clickEvent - click or contextmenu event
     */
    open(clickEvent) {

        const { top, left } = dom.fixPositionInViewport(this.element, clickEvent.clientY, clickEvent.clientX, 5),
            style = this.element.style;

        this.startListeningToGlobalUiEvents();

        style.top = `${top}px`;
        style.left = `${left}px`;

        this.storedFocusedElement = document.activeElement;
        this.element.focus();
    }

    /**
     * @public
     */
    close() {

        this.element.style.top = '-9999px';

        this.stopListeningToGlobalUiEvents();
    }

    /** @private */
    storedFocusedElement;

    /** @private */
    mousedownSubscription;

    /**
     * @private
     */
    startListeningToLocalUiEvents() {

        this.addEventListener('keydown', event => {
            if(event.key === 'Escape') {
                this.storedFocusedElement && this.storedFocusedElement.focus();
                this.close();
            }
        });

        this.addEventListener('click', event => {
            const element = this.element;
            let node = event.target;
            while(node !== element) {
                const classList = node.classList;
                if(node.tagName === 'BUTTON' && node.parentNode === element || classList.contains('item')) {
                    if(!classList.contains('disabled')) { this.close(); }
                    break;
                }
                node = node.parentNode;
            }
        });
    }

    /**
     * @private
     * @param event
     */
    mousedownHandler = event => {

        if(this.element.contains(event.target)) { return; }

        this.close(event);
    };

    /**
     * @private
     */
    startListeningToGlobalUiEvents() {

        this.mousedownSubscription =
            this.addEventListener('mousedown', this.mousedownHandler, { capture: true }, document);
    }

    /**
     * @private
     */
    stopListeningToGlobalUiEvents() {

        if(this.mousedownSubscription) {
            this.mousedownSubscription.dispose();
            this.mousedownSubscription = null;
        }
    }

};
