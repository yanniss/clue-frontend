import str from 'core/classes/StringHelper.static';

export const PAGINATION_PAGE_CHANGED = str.createUnique();
