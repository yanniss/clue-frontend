/**
 * todo: Add constructor option to use anchors instead of buttons for page navigation.
 */

import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import { assert } from 'core/classes/Assert';
import { isNatural } from 'core/classes/NumberHelpers';
import { CTX_ASCENDANTS } from 'core/classes/events/contexts';
import { PAGINATION_PAGE_CHANGED } from './Pagination.events';
import 'core/bindings/numberFormatted';
import 'core/bindings/hideOnBlur';
import 'bootstrap.style';

/**
 *
 */
export default class Pagination extends Base {

    /**
     * @param element
     * @param [total = 0]
     * @param [page = 0]
     */
    constructor(element, {
        total = 0,
        page = 0,
    }) {

        super(element);

        this.setTotal(total);
        this.goto(page);
    }

    gotoFirst() {

        this.goto(1);
    }

    gotoPrevious() {

        this.goto(this.page.peek() - 1);
    }

    gotoNext() {

        this.goto(this.page.peek() + 1);
    }

    gotoLast() {

        this.goto(this.total.peek());
    }

    /**
     * @param {number|string} page
     */
    goto(page) {

        page = parseInt(page);

        assert(!Number.isNaN(page), `Paginator.goto: 'page' must be integer: ${page}`);

        page = Math.max(page, 1);
        page = Math.min(page, this.total.peek());

        this.page(page);
        this.pageSelectorValue(page);

        this.notify(CTX_ASCENDANTS, PAGINATION_PAGE_CHANGED, page);
    }

    /**
     * @param {number|string} total
     */
    setTotal(total) {

        total = parseInt(total);

        assert(isNatural(total), `Paginator.setTotal: 'total' must be non-negative integer: ${total}`);

        if(this.total.peek() !== total) {
            this.total(total);
            this.gotoFirst();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    page = ko.observable(0);
    total = ko.observable(0);

    pageSelectorSwitch = ko.observable().extend({ notify: 'always' });
    pageSelectorValue = ko.observable();
    pageSelectorInputWidth = ko.computed(() => `${this.total().toString().length}ch`);

    pageDescriptionWidth = ko.computed(() => {

        let charCount = this.total().toString().length; // Number of digits in max page number.
        charCount += Math.floor((charCount-1)/3);       // Number of 1000-separator commas in max page number.
        charCount *= 2;                                 // Two page number appearances.
        charCount += 10;                                // "Page  of ".length

        return `${charCount}ch`;
    });

    submit() {

        this.goto(this.pageSelectorValue.peek());

        this.pageSelectorSwitch(false);
    }

};
