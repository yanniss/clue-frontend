import './Pagination.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './Pagination.view.html';
import vm from './Pagination.vm';

koh.registerComponent('component-pagination', vm, view);
