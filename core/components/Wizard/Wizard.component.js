import './Wizard.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './Wizard.view.html';
import vm from './Wizard.vm';

koh.registerComponent('component-wizard', vm, view, true);
