import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import $ from 'jquery';
import { CTX_DESCENDANTS } from 'core/classes/events/contexts';
import { COMPONENT_CREATION_FINISHED } from 'core/classes/events/types';
import 'bootstrap.style';
import 'bootstrap.logic';

/**
 * todo: extend component to accept raw html as wizard steps (not only components with class "wizard-step").
 * todo: now each wizard-step component vm must have a clear() method to clear its state.
 */
export default class Wizard extends Base {

    constructor(element) {

        super(element);

        this.addEventListener('keypress', e => {

            if(e.key === 'Enter' && this.currentStep().vm.ready()) {
                this.goNext();
            }
        });

        this.on(CTX_DESCENDANTS, COMPONENT_CREATION_FINISHED, childVm => {

            if(childVm.element.classList.contains('wizard-step')) {
                childVm.element.classList.add('hide');
                this.steps.push({
                    vm: childVm,
                    functional: ko.observable(false),
                });
            }
        });

        let prevStep = this.currentStep();
        this.subscribe(this.currentStep, step => {
            step && step.vm.element.classList.toggle('hide');
            prevStep && prevStep.vm.element.classList.toggle('hide');
            prevStep = step;
        });
    }

    dispose() {

        this.$modal.removeClass('fade');
        this.$modal.modal('hide');
        super.dispose();
    }

    show() {

        this.goTo(0, true, true);
        this.$modal.modal('show');
    }

    hide() {

        this.$modal.modal('hide');
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    steps = ko.observableArray();
    currentStep = ko.observable();
    pendingNext = ko.observable(false);
    $modal = $(this.element.getElementsByClassName('modal')[0]);

    initAfterBinding() {

        setTimeout(() => {
            // At this point, all wizard steps have been collected;

            this.goTo(0);
        });
    }

    goTo = (step, clearAllAfter = false, clearSelf = false) => {

        const steps = this.steps(),
            index = parseInt(Number.isInteger(step) ? step : steps.findIndex(s => s === step));

        if(index < 0 || index >= steps.length) { return; }

        if(clearSelf) {
            steps[index].vm.clear();
        }

        steps[index].functional(true);
        this.currentStep(steps[index]);

        if(clearAllAfter) {
            for(let i = index + 1, n = steps.length; i < n; i++) {
                steps[i].functional(false);
                steps[i].vm.clear();
            }
        }
    };

    goBack = () => {

        const currentStep = this.currentStep(),
            index = this.steps().findIndex(step => step === currentStep);

        this.goTo(index - 1);
    };

    goNext = () => {

        const steps = this.steps(),
            currentStep = this.currentStep(),
            index = steps.findIndex(step => step === currentStep);

        this.pendingNext(true);

        currentStep.vm.deliver()
            .then(response => {
                if(index === steps.length - 1) {
                    this.hide();
                    // Notifier.success(this.element.dataset.finalMessage || 'Completed successfully');
                }
                else {
                    steps[index + 1].vm.receive(response);
                    this.goTo(index + 1, true);
                }
            })
            .finally(() =>
                this.pendingNext(false)
            );
    };

};
