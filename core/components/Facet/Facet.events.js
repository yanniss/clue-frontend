import str from 'core/classes/StringHelper.static';

export const FACET_ENABLED_OPTIONS_CHANGED = str.createUnique();
