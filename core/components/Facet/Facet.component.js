import './Facet.style.scss'
import koh from 'core/classes/KnockoutHelper.static';
import vm from './Facet.vm';
import view from './Facet.view.html';

koh.registerComponent('component-facet', vm, view);
