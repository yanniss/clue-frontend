/**
 * todo: feature: boolean flag to move "just checked" option to top.
 * todo: feature: store initial/total options; introduce boolean flag on how to handle zero-count options (completely
 *                hide them or show them disabled).
 * todo: optimize: store initial/total options (and their views); an update should use from the existing/constructed ones.
 */

import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import Category from './classes/Category';
import Option from './classes/Option';
import { assert } from 'core/classes/Assert';
import { CTX_ASCENDANTS } from 'core/classes/events/contexts';
import { FACET_ENABLED_OPTIONS_CHANGED } from './Facet.events';
import 'core/bindings/numberFormatted';

/**
 *
 */
export default class Facet extends Base {

    /**
     * @param element
     * @param categories
     */
    constructor(element, { categories = {} }) {

        super(element);

        this.update(categories, {});
    }

    /**
     * @param categories
     * @param activeCategoriesInfo
     */
    update(categories, activeCategoriesInfo) {

        // Collect categories' expansion state before updating them with the new data.
        const isCategoryExpanded = this.categories.peek().reduce((acc, category) => ({
            ...acc,
            [category.name]: category.expanded.peek(),
        }), {});

        // Create the array of categories with their options.
        let newCategories = Object.entries(categories)
            .filter(([ categoryName, _ ]) => activeCategoriesInfo.hasOwnProperty(categoryName))
            .map(([ categoryName, options ]) => {

                const enabledOptions = this.enabledOptions[categoryName] || {},
                    category = new Category(
                        categoryName,
                        activeCategoriesInfo[categoryName].label,
                        !!isCategoryExpanded[categoryName]
                    );

                for(const [ optionName, count ] of Object.entries(options)) {
                    const isOptionEnabled = enabledOptions.hasOwnProperty(optionName);

                    !isOptionEnabled && category.disabledOptions.push(new Option(optionName, count));
                }

                return category;
            });

        // The following logic ensures that the categories remain the same and in the same order, between different
        // calls to `update`.
        if(newCategories.length < this.categories.peek().length) {

            const enhancedNewCategories = [];

            for(let i = 0, categories = this.categories.peek(); i < categories.length; i++) {

                const category = categories[i],
                    categoryName = category.name;

                enhancedNewCategories.push(
                    newCategories.find(category => category.name === categoryName) ||
                    new Category(categoryName, category.expanded.peek())
                );
            }

            newCategories = enhancedNewCategories;
        }

        // Enhance new categories/options with enabled options.
        for(const [ categoryName, enabledOptions ] of Object.entries(this.enabledOptions)) {

            let category = newCategories.find(category => category.name === categoryName);

            for(const optionName of Object.keys(enabledOptions)) {
                const count = (categories[categoryName] || {})[optionName] || 0;

                category.enabledOptions.push(new Option(optionName, count));
            }
        }

        newCategories.forEach(category => category.prepare());

        this.categories(newCategories);
    }

    clearEnabledOptions() {

        this.enabledOptions = {};
    }

    getEnabledOptions() {

        return this.enabledOptions;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    categories = ko.observableArray();

    enabledOptions = {};

    toggleCategoryOption = (option, event) => {

        const category = ko.contextFor(event.target).$parent,
            enabled = event.target.checked;

        enabled
            ? this.enableCategoryOption(category.name, option.name)
            : this.disableCategoryOption(category.name, option.name);

        return true;
    };

    clearCategory = (category, event) => {

        const enabledOptions = this.enabledOptions;

        assert(enabledOptions.hasOwnProperty(category.name));

        delete enabledOptions[category.name];

        this.notify(CTX_ASCENDANTS, FACET_ENABLED_OPTIONS_CHANGED, enabledOptions);
    };

    enableCategoryOption(categoryName, optionName) {

        const enabledOptions = this.enabledOptions;

        if(!enabledOptions.hasOwnProperty(categoryName)) {
            enabledOptions[categoryName] = {};
        }
        enabledOptions[categoryName][optionName] = true;

        this.notify(CTX_ASCENDANTS, FACET_ENABLED_OPTIONS_CHANGED, enabledOptions);
    }

    disableCategoryOption(categoryName, optionName) {

        const enabledOptions = this.enabledOptions;

        assert(enabledOptions.hasOwnProperty(categoryName));
        assert(enabledOptions[categoryName].hasOwnProperty(optionName));

        delete enabledOptions[categoryName][optionName];

        if(Object.keys(enabledOptions[categoryName]).length === 0) {
            delete enabledOptions[categoryName];
        }

        this.notify(CTX_ASCENDANTS, FACET_ENABLED_OPTIONS_CHANGED, enabledOptions);
    }

};
