
/**
 *
 */
export default class Option {

    name;
    count;

    /**
     * @param {string} name
     * @param {number} count
     */
    constructor(name, count) {

        this.name = name;
        this.count = count;
    }

    toString() {

        return this.name;
    }

};
