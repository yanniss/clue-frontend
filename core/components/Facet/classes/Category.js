import ko from 'knockout';

/**
 *
 */
export default class Category {

    name;
    label;
    expanded = ko.observable();
    enabledOptions = [];
    disabledOptions = [];
    disabledOptionsLimit = 5;
    visibleDisabledOptions;
    hiddenDisabledOptionsLength;
    toggleExpansionText;
    toggleExpansionTitle;

    /**
     * @param {string} name
     * @param {boolean} [expanded = false]
     */
    constructor(name, label, expanded = false) {

        this.name = name;
        this.label = label;
        this.expanded(expanded);
    }

    prepare() {

        this.visibleDisabledOptions = ko.computed(() =>
            this.expanded() ? this.disabledOptions : this.disabledOptions.slice(0, this.disabledOptionsLimit)
        );

        this.hiddenDisabledOptionsLength = ko.computed(() =>
            this.expanded() ? 0 : Math.max(this.disabledOptions.length - this.disabledOptionsLimit, 0)
        );

        this.toggleExpansionText = ko.computed(() =>
            this.expanded() ? 'show less' : 'show all'
        );

        this.toggleExpansionTitle = ko.computed(() =>
            this.expanded() ? false : `show all ${this.enabledOptions.length + this.disabledOptions.length} options`
        );

        this.enabledOptions.sort();
    }

    toggleExpansion(self, event) {

        this.expanded(!this.expanded());
    };

    hasEnabledOptions() {

        return this.enabledOptions.length > 0;
    }

    hasManyDisabledOptions() {

        return this.disabledOptions.length > this.disabledOptionsLimit;
    }

};
