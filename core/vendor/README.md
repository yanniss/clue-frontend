
This directory contains any 3rd-party modules that are not installed
through npm.

In order to include such a module, import it as any other module but
also use the `vendor` prefix. For example:

    import 'vendor/custom-lib.js';

The `vendor` prefix is simply an alias defined in the webpack
configuration.