const Path = require('path');
const Webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = (env, argv) => {

    // The following line is required only in order for this config file to be successfully parsed by WebStorm.
    argv = argv || {};

    /***************************************************************
     * Parse npm scripts and command line arguments.
     **************************************************************/

    // Webpack v4's default mode is production.
    const mode = ['development', 'production'].includes(argv.mode) ? argv.mode : 'production';

    // All "options" properties must contain a boolean value.
    const options = {
        minify:     env && env.hasOwnProperty('minify')     ? env.minify !== 'false'     : mode === 'production',
        sourceMaps: env && env.hasOwnProperty('sourceMaps') ? env.sourceMaps !== 'false' : mode === 'development',
        analyze:    env && env.hasOwnProperty('analyze')    ? env.analyze !== 'false'    : false,
        // The ".js" extension is required for Windows.
        devServer:  process.argv.some(arg => /webpack-dev-server(\.js)?$/.test(arg)),
    };

    const dirs = {
        babel: path('build/babel'),
        build: path(`build/${mode}`),
    };

    const ports = {
        apiPort: getPort(argv, 'apiPort', 13000),
        analyzerPort: getPort(argv, 'analyzerPort', 13100),
    };

    /***************************************************************
     * Produce webpack configuration
     **************************************************************/

    return {
        target: 'web',
        mode: mode,
        entry: {
            main: [
                'whatwg-fetch',
                path('core/polyfills/window.CustomEvent.js'),
                path('core/polyfills/String.prototype.padStart.js'),
                path('core/index.js'),
            ],
        },
        output: {
            path: dirs.build,
            filename: '[name].bundle.js',
        },
        plugins: [
            // https://github.com/johnagan/clean-webpack-plugin
            new CleanWebpackPlugin([
                ...insertIf(options.devServer, `${dirs.babel}/*`),
                ...insertIf(!options.devServer, `${dirs.build}/*`),
            ]),
            // https://webpack.js.org/plugins/uglifyjs-webpack-plugin/
            ...insertIf(options.minify,
                new UglifyJsPlugin({
                    sourceMap: options.sourceMaps,
                }),
            ),
            // https://github.com/jantimon/html-webpack-plugin#options
            new HtmlWebpackPlugin({
                favicon: path('app/images/favicon.ico'),
                template: path('core/index.html'),
                inject: true,
            }),
            // https://webpack.js.org/plugins/provide-plugin/
            new Webpack.ProvidePlugin({
                $:               'jquery',
                jQuery:          'jquery',
                'window.$':      'jquery',
                'window.jQuery': 'jquery',
            }),
            // https://webpack.js.org/plugins/define-plugin/
            new Webpack.DefinePlugin({
                __API_END_POINT__: options.devServer ? `"//${argv.host || 'localhost'}:${ports.apiPort}/clue"` : '"/clue"',
            }),
            // https://github.com/webpack-contrib/webpack-bundle-analyzer
            ...insertIf(options.analyze,
                new BundleAnalyzerPlugin({
                    openAnalyzer: false,
                    analyzerPort: ports.analyzerPort,
                }),
            ),
        ],
        resolve: {
            alias: {
                app: path('app'),
                'bootstrap.style': 'bootstrap/dist/css/bootstrap.min.css',
                'bootstrap.logic': 'bootstrap/dist/js/bootstrap.min',
                codebrowser: path('app/pages/CodeBrowser'),
                core: path('core'),
                fontawesome: 'font-awesome/css/font-awesome.min.css',
            },
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /(node_modules|vendor)/,
                    use: [{
                        // https://github.com/babel/babel-loader#options
                        loader: 'babel-loader',
                        options: {
                            // more options in .babelrc
                            minified: options.minify,
                            sourceMaps: options.sourceMaps,
                            cacheDirectory: options.devServer && dirs.babel,
                        },
                    }],
                },
                {
                    test: /\.html$/,
                    use: [{
                        // https://github.com/webpack-contrib/html-loader
                        loader: 'html-loader',
                        options: {
                            // https://github.com/kangax/html-minifier#options-quick-reference
                            minimize: options.minify,
                            ignoreCustomComments: [/^\s*ko\s/, /^\s*\/ko\s*$/], // ignore knockout comments
                        },
                    }],
                },
                {
                    test: /\.(css|sass|scss)$/,
                    use: [
                        {
                            // https://github.com/webpack-contrib/style-loader#options
                            loader: 'style-loader',
                            options: {
                                hmr: true,
                                sourceMap: options.sourceMaps,
                            },
                        },
                        {
                            // https://github.com/webpack-contrib/css-loader#options
                            loader: 'css-loader',
                            options: {
                                minimize: options.minify,
                                sourceMap: options.sourceMaps,
                                camelCase: true,
                            },
                        },
                        {
                            // https://webpack.js.org/loaders/sass-loader/
                            loader: 'sass-loader',
                        },
                    ],
                },
                {
                    test: /\.(jpg|png|ttf|eot|svg|woff|woff2|gif)$/,
                    use: [{
                        // https://github.com/webpack-contrib/file-loader#options
                        loader: 'file-loader',
                    }],
                },
            ],
        },
        devtool: (() => {
            // https://webpack.js.org/configuration/devtool/#devtool
            if(!options.sourceMaps) {
                return false;
            }
            else if(options.devServer) {
                return 'eval-source-map';
            }
            else {
                return 'source-map';
            }
        })(),
        devServer: {
            // https://webpack.js.org/configuration/dev-server/
            compress: true,
            overlay: {
                warnings: true,
                errors: true,
            },
            stats: 'minimal',
            watchOptions: {
                // https://webpack.js.org/configuration/watch/
                ignored: /node_modules/,
            },
        },
    };
};

/***************************************************************
 * Helper functions.
 **************************************************************/

const path = path => Path.join(__dirname, path);

const insertIf = (condition, ...elements) => condition ? elements : [];

const getPort = (argv, argName, defaultValue) => {
    let port;
    if(!argv.hasOwnProperty(argName)) {
        port = parseInt(defaultValue);
    }
    else {
        port = parseInt(argv[argName]);
        if(isNaN(port)) {
            throw new Error(`Invalid port number: ${argName}=${argv[argName]}. Must be number.`);
        }
    }
    return port;
};
