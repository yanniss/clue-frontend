import File from 'codebrowser/classes/File';

const file = new File(
    '2684eba8-6cfb-44ad-b74e-dce6e7d36298',
    null,
    'com/facebook/appupdate/AppUpdateService.shimple',
    '20181017_12.12.30_context-insensitive-plus_edc702b2-2a15-4bb2-9352-42cd1d4d996a',
);

file.methods.get(items => { console.log('methods:'); items.forEach(i => console.log(i)); });
file.types.get(items => { console.log('types:'); items.forEach(i => console.log(i)); });

Promise.all([ file.methods.get(), file.types.get() ]).then(results => {
    console.log('total');
    console.log(results);
});
