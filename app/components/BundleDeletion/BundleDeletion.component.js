import './BundleDeletion.style.scss'
import koh from 'core/classes/KnockoutHelper.static';
import view from './BundleDeletion.view.html';
import vm from './BundleDeletion.vm';

koh.registerComponent('component-bundle-deletion', vm, view);
