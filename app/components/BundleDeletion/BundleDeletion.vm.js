import $ from 'jquery';
import Notifier from 'app/classes/Notifier.static';
import Api from 'app/classes/Api.static';
import Base from 'core/classes/vm/Base.vm';
import { CTX_ASCENDANTS } from 'core/classes/events/contexts';
import { BUNDLE_DELETE } from 'app/classes/eventTypes';
import koh from 'core/classes/KnockoutHelper.static';

/**
 *
 */
export default class BundleDeletion extends Base {

    $modal = $(this.element).find('.modal');
    bundle = koh.observable();

    /**
     * @public
     * @override
     * @param {Bundle} bundle
     */
    show(bundle) {

        this.bundle(bundle);
        this.$modal.modal('show');
    }

    /**
     * @public
     * @override
     */
    hide() {

        this.$modal.modal('hide');
        this.bundle(null);
    }

    /**
     * @private
     */
    submit() {

        Api.deleteBundle(this.bundle().id)
            .then(() => {
                this.hide();
                this.notify(CTX_ASCENDANTS, BUNDLE_DELETE, this.bundle());
                Notifier.success(`Project successfully deleted.`);
            })
            .catch(() => Notifier.error(`Failed to delete bundle.`));
    }

};
