import $ from 'jquery';
import Notifier from 'app/classes/Notifier.static';
import Api from 'app/classes/Api.static';
import Base from 'core/classes/vm/Base.vm';
import { CTX_ASCENDANTS } from 'core/classes/events/contexts';
import { PROJECT_DELETE } from 'app/classes/eventTypes';
import koh from 'core/classes/KnockoutHelper.static';

/**
 *
 */
export default class ProjectDeletion extends Base {

    $modal = $(this.element).find('.modal');
    project = koh.observable();

    /**
     * @public
     * @override
     * @param {Project} project
     */
    show(project) {

        this.project(project);
        this.$modal.modal('show');
    }

    /**
     * @public
     * @override
     */
    hide() {

        this.$modal.modal('hide');
        this.project(null);
    }

    /**
     * @private
     */
    submit() {

        Api.deleteProject(this.project().id)
            .then(() => {
                this.hide();
                this.notify(CTX_ASCENDANTS, PROJECT_DELETE, this.project());
                Notifier.success(`Project successfully deleted.`);
            })
            .catch(() => Notifier.error(`Failed to delete project.`));
    }

};
