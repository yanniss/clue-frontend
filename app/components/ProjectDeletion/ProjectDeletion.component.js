import './ProjectDeletion.style.scss'
import koh from 'core/classes/KnockoutHelper.static';
import view from './ProjectDeletion.view.html';
import vm from './ProjectDeletion.vm';

koh.registerComponent('component-project-deletion', vm, view);
