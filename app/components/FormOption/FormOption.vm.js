import ko from 'knockout';
import viewCheckbox from './views/FormOption.view.checkbox.html';
import viewFile from './views/FormOption.view.file.html';
import viewSelect from './views/FormOption.view.select.html';
import viewText from './views/FormOption.view.text.html';
import 'core/bindings/stemplate';

/**
 *
 */
export default class FormOption {

    constructor(element) {

        const option = ko.contextFor(element).$data;

        this.option = option;

        if(option.isBoolean) {
            this.view = viewCheckbox;
        }
        else if(option.isFile) {
            this.valueObservables = [ ko.observable(), ko.observable(), ko.observable() ];
            this.required = ko.computed(() =>
                option.isMandatory && this.valueObservables.every(valueObservable => !valueObservable())
            );
            this.view = viewFile;
        }
        else if(this.isSelectDropdown()) {
            this.view = viewSelect;
        }
        else {
            this.view = viewText;
        }
    }

    dispose() {

        this.required && this.required.dispose();
    }

    isLocalOrZipFile() {

        return this.option.fileTypes.some(type => ['LOCAL_FILE', 'ZIP'].includes(type));
    }

    isMavenArtifact() {

        return this.option.fileTypes.includes('MAVEN_ARTIFACT');
    }

    isUrl() {

        return this.option.fileTypes.includes('URL');
    }

    isSelectDropdown() {

        return ko.unwrap(this.option.validValues).length > 0;
    }

};
