import './FormOption.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './FormOption.view.html';
import vm from './FormOption.vm';

koh.registerComponent('component-form-option', vm, view);
