import './UserDeletion.style.scss'
import koh from 'core/classes/KnockoutHelper.static';
import vm from './UserDeletion.vm';
import view from './UserDeletion.view.html';

koh.registerComponent('component-user-deletion', vm, view);
