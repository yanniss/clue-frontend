import ko from 'knockout';
import $ from 'jquery';
import Notifier from 'app/classes/Notifier.static';
import Api from 'app/classes/Api.static';
import Base from 'core/classes/vm/Base.vm';

/**
 *
 */
export default class UserDeletion extends Base {

    $element = $(this.element).find('.modal');
    user = ko.observable();
    callbacks;

    constructor(element, {
        userId,
        config = {},
        observables = {},
        callbacks = {},
    }) {

        super(element);

        this.callbacks = callbacks;

        this.subscribe(observables.openUser, user => {
            this.user(user);
            this.open();
        });
    }

    open() {

        this.$element.modal('show');
    }

    close() {

        this.$element.modal('hide');
    }

    submit() {

        const userId = this.user.peek();

        Api.deleteUser(userId)
            .then(() => {
                this.close();
                this.$element.on('hidden.bs.modal', () => {
                    this.user(null);
                    this.callbacks.onDeleted(); // Must call it AFTER hide animation has finished
                    Notifier.success(`User ${userId} has been deleted.`);
                });
            })
            .catch(() => {
                Notifier.error(`Failed to delete analysis ${userId}.`);
            });

        return false;
    }

};
