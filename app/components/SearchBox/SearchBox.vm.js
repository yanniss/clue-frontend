import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';

/**
 *
 */
export default class SearchBox extends Base {

    text;
    placeholder;
    keypressFilter;

    constructor(element, {
        text = ko.observable(''),
        placeholder = '',
        keypressFilter = (() => true),
    }) {

        super(element);

        this.text = text;
        this.placeholder = placeholder;
        this.keypressFilter = keypressFilter;
    }

    keypress(self, event) {

        if(event.key === 'Enter') {
            this.text.valueHasMutated();
        }
        else {
            return this.keypressFilter(this, event);
        }
    }

    clear(self, event) {

        self.text('');
    }

};
