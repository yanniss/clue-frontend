import './SearchBox.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './SearchBox.vm';
import view from './SearchBox.view.html';

koh.registerComponent('search-box', vm, view);
