import 'core/bindings/stemplate';
import ko from 'knockout';
import Api from 'app/classes/Api.static';
import Base from 'core/classes/vm/Base.vm';

/**
 *
 */
export default class Navbar extends Base {

    constructor(element, { content }) {

        super(element);

        this.viewContent = ko.observable(content || null);
    }

    logout() {

        Api.logout().then(() => {
            window.location.reload();
        });
    }

    isOnAdminPage() {

        return /^#\/admin\/.*/.test(location.hash);
    }

};
