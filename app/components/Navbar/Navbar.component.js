import './Navbar.style.scss'
import koh from 'core/classes/KnockoutHelper.static';
import vm from './Navbar.vm';
import view from './Navbar.view.html';

koh.registerComponent('navbar', vm, view);
