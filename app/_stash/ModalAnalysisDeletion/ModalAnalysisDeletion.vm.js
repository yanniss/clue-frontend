import ko from 'knockout';
import $ from 'jquery';
import Notifier from 'app/classes/Notifier.static';
import Api from 'app/classes/Api.static';
import Base from 'core/classes/vm/Base.vm';

/**
 *
 */
export default class ModalAnalysisDeletion extends Base {

    $element = $(this.element).find('.modal');
    analysis = ko.observable();
    callbacks;

    constructor(element, {
        analysis,
        config = {},
        observables = {},
        callbacks = {},
    }) {

        super(element);

        this.callbacks = callbacks;

        this.subscribe(observables.openAnalysis, analysis => {
            this.analysis(analysis);
            this.open();
        });
    }

    open() {

        this.$element.modal('show');
    }

    close() {

        this.$element.modal('hide');
    }

    submit() {

        const analysisId = this.analysis.peek().id;

        Api.deleteAnalysis(analysisId)
            .then(() => {
                this.close();
                this.$element.on('hidden.bs.modal', () => {
                    this.analysis(null);
                    this.callbacks.onDeleted(); // Must call it AFTER hide animation has finished
                    Notifier.success(`Analysis ${analysisId} has been deleted.`);
                });
            })
            .catch(() => {
                Notifier.error(`Failed to delete analysis ${analysisId}.`);
            });

        return false;
    }

};
