import './ModalAnalysisDeletion.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './ModalAnalysisDeletion.view.html';
import vm from './ModalAnalysisDeletion.vm';

koh.registerComponent('modal-analysis-deletion', vm, view);
