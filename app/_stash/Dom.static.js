
/**
 * @param {HTMLElement} element
 * @returns {number} - (content width + paddings + borders width + margins)
 */
function width(element) {

    const width = element.offsetWidth,
        style = getComputedStyle(element);

    return width + parseInt(style.marginRight) + parseInt(style.marginLeft);
}

/**
 * @param {HTMLElement} element
 * @returns {number} - (content height + paddings + borders height + margins)
 */
function height(element) {

    const height = element.offsetHeight,
        style = getComputedStyle(element);

    return height + parseInt(style.marginTop) + parseInt(style.marginBottom);
}

function fixPosition(element, top, left, {
    containerElement = window,
    ignoreContainerPaddings = false,
    margin = 0,
} = {
    containerElement: window,
    ignoreContainerPaddings: false,
    margin: 0,
}) {

    console.log(containerElement, ignoreContainerPaddings, margin);

    let cw, ch;
    if(containerElement === window) {
        cw = window.innerWidth;
        ch = window.innerHeight;
    }
    else if(containerElement instanceof HTMLElement) {
        const style = getComputedStyle(containerElement);
        cw = style.width;
        ch = style.height;
        if(ignoreContainerPaddings && style.boxSizing === 'content-box') {
            cw += style.paddingRight + style.paddingLeft;
            ch += style.paddingTop + style.paddingBottom;
        }
        else if(!ignoreContainerPaddings && style.boxSizing === 'border-box') {
            cw -= style.paddingRight + style.paddingLeft;
            ch -= style.paddingTop + style.paddingBottom;
        }
    }

    const ew = element.offsetWidth,
        eh = element.offsetHeight;

    const fixedTop = Math.max(Math.min(ch - eh, top) - margin, margin),
        fixedLeft = Math.max(Math.min(cw - ew, left) - margin, margin);

    return { top: fixedTop, left: fixedLeft };
}
