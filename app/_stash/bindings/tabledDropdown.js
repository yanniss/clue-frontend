import $ from 'jquery';
import ko from 'knockout';

ko.bindingHandlers.tabledDropdown = {

    init: function(element, valueAccessor, bindings, vm, bindingContext) {

        const $dropdown = $(element),
            $dropdownMenu = $dropdown.find('.dropdown-menu');

        $dropdown.on('show.bs.dropdown', () => {
            $dropdownMenu.addClass('tabled');
        }).on('hidden.bs.dropdown', () => {
            $dropdownMenu.removeClass('tabled');
        });

        ko.utils.domNodeDisposal.addDisposeCallback(element, () => {

        });
    },

};
