import 'rateyo/min/jquery.rateyo.min.css';
import 'rateyo/min/jquery.rateyo.min';
import $ from 'jquery';
import ko from 'knockout';

/**
 * Apply as:
 *
 *  data-bind="
 *   starated: <real number in [0,5]>,
 *   size: <star size in pixels>
 *  "
 *
 * Doc: http://rateyo.fundoocode.ninja/
 */

ko.bindingHandlers.starated = {

    init: function(element, valueAccessor, bindings, vm, bindingContext) {

        const $element = $(element),
            rating = valueAccessor(),
            size = (bindings.size || '15') + 'px';

        $element.rateYo({
            starWidth: size,
            normalFill: '#ccc',
            ratedFill: '#00a9ff',
            //multiColor: {
            //    startColor: '#cc4444',
            //    endColor: '#44cc44'
            //},
            numStars: 5,
            maxValue: 5,
            precision: 1,
            halfStar: false,
            fullStar: false,
            rating: rating,
            readOnly: true,
            spacing: '0px',
            rtl: false
        });

        ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
            $element.rateYo('destroy');
        });
    },

};
