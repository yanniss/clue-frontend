import $ from 'jquery';
import ko from 'knockout';

/**
 * Apply as:
 *
 *  data-bind="
 *   boxrated: <real number in [0,5]>,
 *   size: <star size in pixels>
 *  "
 *
 * Doc: http://antenna.io/demo/jquery-bar-rating/examples/
 */

ko.bindingHandlers.boxrated = {

    init: function(element, valueAccessor, bindings, vm, bindingContext) {

        const $element = $(element),
            rating = valueAccessor();

        let $boxContainer = $('<div class="boxrating"></div>');
        for(let i = 1; i <= 5; i++) {
            $boxContainer.append($('<span class="box"></span>').addClass(i <= rating ? 'filled' : ''));
        }
        $element.append($boxContainer);

        ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
            $boxContainer.remove();
        });
    },

};
