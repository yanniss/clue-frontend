import 'jquery-ui/themes/base/core.css';
import 'jquery-ui/themes/base/resizable.css.css';
import 'jquery-ui/themes/base/theme.css';
import 'jquery-ui/ui/widgets/resizable';
import $ from 'jquery';
import ko from 'knockout';

/**
 * Apply as:
 *
 *  data-bind="splitter: {
 *      side: 'e' OR 'w',
 *      onDrag: <callback> [OPTIONAL]
 *      resizerElement: <DOM or Jquery element> [OPTIONAL]
 *  }"
 *
 */

ko.bindingHandlers.splitter = {

    init: function(element, valueAccessor, bindings, vm, bindingContext) {

        const args = valueAccessor();

        // ...
        const side = args.direction === 'h' ? 'e' : 's';

        // ...
        const cssClass = args.direction === 'h' ? 'fcr' : 'fcc';
        element.classList.add(cssClass, 'splitter');

        // ...
        const bar = document.createElement('div');
        bar.classList.add('bar', 'ui-resizable-handle', 'ui-resizable-' + side);
        Array.from(element.children).forEach(function(childElement) {

            childElement.classList.add('fif');

            if(childElement.nextElementSibling) {
                const barClone = bar.cloneNode(true);

                barClone.addEventListener('mousedown', function() {
                    this.classList.add('fis');
                    this.classList.remove('fif');
                });

                //element.insertBefore(barClone, childElement.nextElementSibling);
                childElement.appendChild(barClone);

                const handles = {};
                handles[side] = barClone;
                $(childElement).resizable({ handles: handles });
            }
        });
    },

};
