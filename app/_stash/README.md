
This directory can be used for storing anything that is not included in
the release version of the project. That could be unfinished components,
experiments, etc.
