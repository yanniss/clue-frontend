import './BuiltInFormFeatures.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './BuiltInFormFeatures.view.html';
import vm from './BuiltInFormFeatures.vm';

koh.registerComponent('builtin-form-features', vm, view);
