import ko from 'knockout';

/**
 * Apply as:
 *  data-bind="validationMessage: 'the message you want to show to the user on wrong input format'"
 */

ko.bindingHandlers.validationMessage = {

    init: function(element, valueAccessor, bindings, vm, bindingContext) {

        const message = valueAccessor() || element.dataset.message;

        element.addEventListener('input', event => {

            // Clear error state.
            element.setCustomValidity('');

            if(element.checkValidity()) {
                // nop
            }
            else if(element.validity.valid) {

            }
            else {
                console.log(false);
                element.setCustomValidity(message);
            }
        });
    },

};
