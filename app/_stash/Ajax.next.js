
const ContentTypes = {
    html: 'text/html; charset=utf-8',
    text: 'text/plain; charset=utf-8',
    json: 'application/json; charset=utf-8',
    form: 'application/x-www-form-urlencoded; charset=utf-8',
    fileForm: 'multipart/form-data; charset=utf-8',
};

/**
 *
 */
export default class Ajax {

    constructor(options = {}) {

        this.options = {
            beforeSend: () => {},
            complete: () => {},
            error: () => {},
            headers: {},
            ...options,
        };
    }

    jsonGet(url, data = {}) {

        return this._fetch(url, 'GET', ContentTypes.json, data);
    }

    jsonPost(url, data = {}) {

        return this._fetch(url, 'POST', ContentTypes.json, data);
    }

    jsonDelete(url, data = {}) {

        return this._fetch(url, 'DELETE', ContentTypes.json, data);
    }

    jsonPut(url, data = {}) {

        return this._fetch(url, 'PUT', ContentTypes.json, data);
    }

    formPost(url, $form) {

        return this._fetchForm(url, $form, 'POST');
    }

    /**
     * Get the error message from the jqXhr response. Expects a json object with key "msg".
     *
     * @param jqXhr
     * @param errorThrown
     * @returns {*}
     */
    getErrorMessage(jqXhr, errorThrown) {

        try {
            let message = JSON.parse(jqXhr.responseText).msg;
            if(message) {
                return errorThrown + ': ' + message;
            }
            else {
                return errorThrown + ': ' + jqXhr.responseText;
            }
        }
        catch(e) {
            return errorThrown;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    _fetch(url, httpMethod, contentType, data) {

        const options = {
            method: httpMethod,
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'include',
            referrer: 'no-referrer',
            headers: {},
        };

        if(contentType) {
            options.headers['Content-Type'] = contentType;
        }
        if(data) {
            switch(httpMethod) {
                case 'GET':
                case 'DELETE':
                    const index = url.indexOf('?');
                    if(index === -1) {
                        url += `?${Ajax._asUrlEncoded(data)}`
                    }
                    else if(index === url.length - 1) {
                        url += `${Ajax._asUrlEncoded(data)}`;
                    }
                    else {
                        url += `&${Ajax._asUrlEncoded(data)}`;
                    }
                    break;
                case 'POST':
                case 'PUT':
                    options.body = data;
                    break;
                default:
                    break;
            }
        }

        this.options.beforeSend();

        return fetch(url, options)
            .then(response =>
                response.json()
            )
            .catch((...args) => {
                this.options.error.apply(null, args);
                throw args;
            })
            .finally(() => {
                this.options.complete();
            });
    }

    _fetchForm(url, $form, httpMethod) {

        const enctype = $form.attr('enctype');
        const formData = new FormData($form[0]);
        let contentType;
        let body = '';

        if(enctype.startsWith('application/x-www-form-urlencoded')) {
            body = Ajax._asUrlEncoded(formData.entries());
            contentType = ContentTypes.form;
        }
        else if(enctype.startsWith('multipart/form-data')) {
            body = formData;
        }
        else if(enctype.startsWith('text/plain')) {
            // to do ...
        }
        else {
            throw new Error(`Ajax.fetchForm: invalid enctype: ${enctype}`);
        }

        return this._fetch(url, httpMethod, contentType, body);
    }

    static _asEntries(data) {

        
    }

    static _asUrlEncoded(entries) {

        let data = '';

        for(const [ name, value ] of entries) {
            data += (data.length > 0 ? '&' : '') + encodeURIComponent(name) + '=' + encodeURIComponent(value);
        }

        return data;
    }

};
