import {STORAGE_MANAGER_GET, STORAGE_MANAGER_REMOVE, STORAGE_MANAGER_SET} from 'app/classes/StorageManager.events';

/**
 *
 */
export default class StorageManager {

    constructor(bundle, pubsub) {

        this.bundle = pubsub;
        this.pubsub = pubsub;
    }

    dispose() {

        this.pubsub.off(STORAGE_MANAGER_SET, null, this);
        this.pubsub.off(STORAGE_MANAGER_GET, null, this);
        this.pubsub.off(STORAGE_MANAGER_REMOVE, null, this);
    }

    setItem(key, value) {

        if(!key) {
            this.removeI
        }
    }

    getItem(key) {

    }

    removeItem(key) {

    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bundle;
    pubsub;

    startListeningToPageEvents() {

        this.pubsub.on(STORAGE_MANAGER_GET, callback => {
            const
        });

        this.pubsub.on(STORAGE_MANAGER_SET, () => {});

        this.pubsub.on(STORAGE_MANAGER_REMOVE, () => {});
    }

};
