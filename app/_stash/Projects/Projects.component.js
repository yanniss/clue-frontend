import './Projects.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './Projects.vm';
import view from './Projects.view.html';

koh.registerComponent('component-projects', vm, view);
