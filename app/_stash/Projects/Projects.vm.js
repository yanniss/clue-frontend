import 'app/bindings/boxrated';
import 'app/bindings/starated';
import 'app/components/Navbar/Navbar.component';
import 'app/components/ProjectDeletion/ProjectDeletion.component';
import 'app/components/ModalProjectCreation/ModalProjectCreation.component';
import 'app/components/SearchBox/SearchBox.component';
import 'app/vendor/simplePagination/simplePagination.css';
import 'app/vendor/simplePagination/jquery.simplePagination';
import $ from 'jquery';
import Api from 'app/classes/Api.static';
import Paginated from 'core/classes/vm/Paginated.vm';
import ko from 'knockout';
import navbar from './Projects.view.navbar.html';
import {CTX_DESCENDANTS} from "core/classes/events/contexts";
import {PROJECT_DELETE, PROJECT_NEW} from "app/classes/eventTypes";

const searchTextThrottle = 400;
const sortByOptions = [ // first entry is also the default one
    {
        type:    'option',
        id:      '+name',
        label:   'Name (a-z)',
        icon:    '',
        request: 'lowercase_id|ASC',
        allow:   'ALL'
    },
    {
        type:    'option',
        id:      '-name',
        label:   'Name (z-a)',
        icon:    '',
        request: 'lowercase_id|DESC',
        allow:   'ALL'
    },
    {
        type:    'option',
        id:      '-rating',
        label:   'JCenter rating',
        icon:    'fa fa-signal fa-flip-horizontal',
        request: 'rating|DESC',
        allow:   'ALL'
    },
    {
        type:    'option',
        id:      '+rating',
        label:   'JCenter rating',
        icon:    'fa fa-signal',
        request: 'rating|ASC',
        allow:   'ALL'
    }
    // { id: 'size',   label: 'Size',   request: 'size|ASC' } // size not supported yet
];

/**
 *
 */
export default class Projects extends Paginated {

    navbarContent = navbar;
    $paginator = $('.paginator');
    totalItemsNoSearch = ko.observable();

    /**
     *
     * @param element
     * @param _query
     */
    constructor(element, { _query }) {

        super(element, [], _query, {
            fnFetchItems: Projects._fetchItems,
            sortBy: sortByOptions[0],
            sortByOptions: sortByOptions,
        });

        Api.fetchProjects({ _start: 0, _count: 0, _sort: sortByOptions[0].request, text: '' }, true)
            .then(response => response.hits)
            .then(this.totalItemsNoSearch);

        Api.fetchProjects({ _start: 0, _count: 100, _sort: sortByOptions[0].request, text: '' }, true)
            .then(response => response.projects)
            .then(this.items);

        this._initPaginator();
        this._setPaginatorUpdating();

        this.startListeningToChildren();
    }

    openProjectInEditor = (project, event) => {

        location.hash = `/projects/${project.id}/codebrowser`;
    };

    requiresPagination() {

        return this.itemsPerPage() < this.totalItems();
    }

    sortOptionClicked = (sortBy, event) => {

        if(sortBy !== this.sortBy.peek()) {
            this.sortBy(sortBy);
        }
    };

    filterKeypress(self, event) {

        return /[a-zA-Z0-9 ]/.test(event.key);
    }

    getProjectUrl(project) {

        console.log(project);
        return `#/projects/${encodeURIComponent(project.id)}`;
    }

    reloadItems = () => {

        Projects._fetchItems(this.query(), this.sortBy(), this.orderBy(), this.itemsPerPage(), this.page())
            .then(({ items, hits }) => {
                this.totalItems(hits);
                this.items(items);
            });
    };

    openProjectCreationModal() {

        this.children.projectCreationModal.show();
    }

    openProjectDeletionModal = project => {

        this.children.projectDeletionModal.show(project);
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    startListeningToChildren() {

        this.on(CTX_DESCENDANTS, PROJECT_NEW, () => {
            this.reloadItems();
        });

        this.on(CTX_DESCENDANTS, PROJECT_DELETE, () => {
            this.reloadItems();
        });
    }

    static _fetchItems(q, s, o, c, p) {

        const searchWords = q.match(/([\S]+)/g),
            params = {
                _start: (p - 1) * c,
                _count: c,
                _sort:  s.request,
                text:   searchWords ? searchWords.map(word => word + '*').join(' ') : ''
            };

        return Api.fetchProjects(params, true)
            .then(response => ({
                items: response.projects,
                hits: response.hits,
            }));
    }

    _initPaginator() {

        this.$paginator.pagination({
            items:       this.totalItems.peek(),
            itemsOnPage: this.itemsPerPage.peek(),
            currentPage: this.page.peek(),
            cssStyle:    'light-theme',
            onPageClick: (page, event) => {
                this.page(page);
                return false;
            }
        });
    }

    _setPaginatorUpdating() {

        this.subscribe(this.itemsPerPage, itemsPerPage => {
            // "itemsPerPage" must be filtered
            this.$paginator.pagination('updateItemsOnPage', itemsPerPage);
        });

        this.subscribe(this.totalItems, totalItems => {
            // "totalItems" must be filtered
            this.$paginator.pagination('updateItems', totalItems);
        });

        this.subscribe(this.page, page => {
            // "page" must be filtered
            this.$paginator.pagination('drawPage', page);
        });
    }

};
