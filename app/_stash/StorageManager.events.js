import str from 'core/classes/StringHelper.static';

export const STORAGE_MANAGER_SET = str.createUnique();
export const STORAGE_MANAGER_GET = str.createUnique();
export const STORAGE_MANAGER_REMOVE = str.createUnique();
