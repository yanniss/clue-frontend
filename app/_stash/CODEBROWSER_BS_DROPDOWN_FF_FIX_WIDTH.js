import ko from 'knockout';

/**
 * Fix positioning and sizing issue in Firefox.
 *
 * Apply as:
 *
 *  data-bind="CODEBROWSER_BS_DROPDOWN_FF_FIX_WIDTH"
 */

ko.bindingHandlers.CODEBROWSER_BS_DROPDOWN_FF_FIX_WIDTH = {

    init: function(element, valueAccessor, bindings, vm, bindingContext) {

        const $dropdown = $(element),
            $menu = $dropdown.find('.dropdown-menu');

        $dropdown.on('shown.bs.dropdown', () => {
            $menu.width($menu.width() + 20);
        });

        ko.utils.domNodeDisposal.addDisposeCallback(element, () => {
            $dropdown.off();
        });
    },

};
