import {assert} from 'core/classes/Assert';
import str from 'core/classes/StringHelper.static';
import ko from 'knockout';

function registerHybridViewComponent(name, vm, viewHTML = '') {

    assert(str.isString(name), '');
    assert(vm instanceof Function, '');
    assert(str.isString(viewHTML), '');

    ko.components.register(name, {

        viewModel: {
            createViewModel: (params, { element, templateNodes }) => {

                const getNodeOuterHtml = node => {

                    switch(node.nodeType) {
                        case Node.ELEMENT_NODE:
                            return node.outerHTML;
                        case Node.TEXT_NODE:
                            return node.textContent;
                        case Node.COMMENT_NODE:
                            if(node.textContent.startsWith('[CDATA')) {
                                throw new Error(`KnockoutHelper.getNodeHtml: node type not supported: CDATA`);
                            }
                            return `<!--${node.textContent}-->`;
                        default:
                            throw new Error(`KnockoutHelper.getNodeHtml: node type not supported: ${node.nodeName}`);
                    }
                };
                insertTemplateNodesToElement(
                    templateNodes,
                    viewHTML ? element.getElementsByClassName('HYBRID')[0] : element,
                );


                /*
                const templateNodesHTML = componentInfo.templateNodes.reduce(
                    (acc, node) => acc + getNodeOuterHtml(node)
                , '').trim();

                const hasTemplateNodes = true;

                if(templateNodesHTML && viewHTML) {
                    console.log(element.getElementsByClassName('HYBRID'));
                    element.innerHTML = viewHTML.replace('<!--%VIEW_ARG%-->', templateNodesHTML);
                }
                else if(templateNodesHTML) {
                    element.innerHTML = '';
                    componentInfo.templateNodes.forEach(node => element.appendChild(node));
                }*/

                return new vm(element, params);
            },
        },

        // In case "viewHTML" is an empty string (e.g. empty "X.view.html" file), the following comment is passed to
        // component's template in order to avoid knockout's error "Component ... has no template".
        template: viewHTML || '<!-- automatically inserted by `registerComponent` -->',
    });
}


const insertTemplateNodesToElement = (templateNodes, element) => {

    if(!element) { return; }

    element.innerHTML = '';

    templateNodes.forEach(node => element.appendChild(node));
};

