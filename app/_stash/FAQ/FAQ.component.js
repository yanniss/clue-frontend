import './FAQ.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './FAQ.view.html';
import vm from './FAQ.vm';

koh.registerComponent('faq', vm, view);
