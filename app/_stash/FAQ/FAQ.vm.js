import 'app/components/Navbar/Navbar.component';
import $ from 'jquery';
import Page from 'core/classes/vm/Page.vm';

/**
 *
 */
export default class FAQ extends Page {

    constructor(element) {

        super(element);

        const h2 = $('h2')[0];

        if(h2 && h2.nextElementSibling.tagName.toLowerCase() !== 'hr') {
            h2.parentNode.insertBefore(document.createElement('hr'), h2.nextElementSibling);
        }
    }

};
