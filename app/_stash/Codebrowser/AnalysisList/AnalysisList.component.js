import './AnalysisList.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './AnalysisList.vm';
import view from './AnalysisList.view.html';

koh.registerComponent('component-analysis-list', vm, view);
