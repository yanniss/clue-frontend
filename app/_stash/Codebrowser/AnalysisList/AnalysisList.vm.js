import '../ResourceMonitor/ResourceMonitor.component';
import 'core/bindings/stemplate';
import $ from 'jquery';
import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';

/**
 *
 */
export default class AnalysisList extends Base {

    bundleId;
    analyses;
    activeAnalysis;
    activateAnalysis;
    selectedAnalysis = ko.observable();

    constructor(element, {
        bundle,
        analyses,
        analysis,
        open,
        activate,
    }) {

        super(element);

        this.bundleId = bundle.id;
        this.analyses = analyses;
        this.activeAnalysis = analysis;
        this.activateAnalysis = activate;

        this.subscribe(open, selectedAnalysis => {
            // todo: scroll to selected analysis.

            this.selectedAnalysis(selectedAnalysis);
            $(element).find('.modal').modal('show');
        });
    }

    dispose() {

        $(this.element).find('.modal').modal('hide');
        super.dispose();
    }

    activate(self, event) {

        if(this.cannotBeActivated(this.selectedAnalysis())) {
            event.stopPropagation();
        }
        else {
            this.activateAnalysis(this.selectedAnalysis());
        }
    };

    cannotBeActivated(analysis) {

        return !analysis || analysis === this.activeAnalysis() || !analysis.hasFinished();
    }

};
