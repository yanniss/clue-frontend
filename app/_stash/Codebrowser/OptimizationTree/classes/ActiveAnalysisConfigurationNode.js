import Node from '../../Tree/classes/Node';
import Notifier from 'app/classes/Notifier.static';

/**
 *
 */
export default class ActiveAnalysisConfigurationNode extends Node {

    static typesOptions = {
        activeAnalysisConfiguration: {
            icon: 'active-analysis-configuration-node fa fa-cogs',
            sorting_weight: 1,
        },
    };

    constructor(tree, activeBundleConfigurationNode) {

        super(`${tree._nodeIdPrefix}/activeAnalysisConfiguration`, 'activeAnalysisConfiguration', 'Active analysis configuration', false, false, {
            title: 'The set of directives produced by currently active analysis.',
            class: 'inactive-node',
        });

        this.tree = tree;
        this.activeBundleConfigurationNode = activeBundleConfigurationNode;
    }

    canBeMerged() {

        return this.activeAnalysis !== null && this.activeBundleConfigurationNode.hasActive();
    }

    mergeToActive() {

        if(!this.canBeMerged()) { return; }

        this.tree.mergeConfiguration(this.activeAnalysis);
    }

    canBeExported() {

        return this.activeAnalysis !== null;
    }

    export() {

        if(!this.canBeExported()) { return; }

        this.tree.downloadDirectivesFile(this.activeAnalysis);
    }

    canBeCloned() {

        return this.activeAnalysis !== null;
    }

    clone() {

        if(!this.canBeCloned()) { return; }

        this.tree.cloneConfiguration(this.activeAnalysis);
    }

    canBeSaved() {

        return this.activeAnalysis !== null;
    }

    saveInBundle() {

        if(!this.canBeSaved()) { return; }

        Notifier.info('<strong>Current analysis configuration save as bundle configuration</strong><br><i>~~~Under construction ~~~</i>');
    }

    ////////////////////////////////////////////////////////////////

    tree = null;
    activeAnalysis = null;
    activeBundleConfigurationNode = null;

    getConfigurationName() {

        return this.activeAnalysis && this.activeAnalysis.configurationSetName;
    }

    setAnalysis(analysis) {

        this.activeAnalysis = analysis.id ? analysis : null;
        this.updateView();
    }

    updateView() {

        const hasAnalysis = this.activeAnalysis !== null;

        const element = document.getElementById(`${this.id}_anchor`);
        element && element.classList[hasAnalysis ? 'remove' : 'add']('inactive-node');

        const cssClass = hasAnalysis ? '' : 'inactive-node';
        this.a_attr.class = cssClass;

        const jstreeNode = this.tree._jstree && this.tree._jstree.get_node(this.id);
        jstreeNode && (jstreeNode.a_attr.class = cssClass);
    }

};
