import Node from '../../Tree/classes/Node';

/**
 *
 */
export default class ActiveBundleConfigurationNode extends Node {

    static typesOptions = {
        activeBundleConfiguration: {
            icon: 'active-bundle-configuration-node fa fa-wrench',
            sorting_weight: 0,
        },
    };

    constructor(tree) {

        super(`${tree._nodeIdPrefix}/activeBundleConfiguration`, 'activeBundleConfiguration', 'Active bundle configuration', false, false, {
            title: 'The set of directives to use as seeds when creating a new analysis or as rules when optimizing your apk.',
            class: 'inactive-node',
        });

        this.tree = tree;
    }

    clear() {

        if(!this.hasActive()) { return; }

        const previous = this.active;
        this.active = null;
        previous.updateView();
        this.updateView();
        this.tree.updateActiveConfigurationName(this.getConfigurationName());
    }

    hasActive() {

        return this.active !== null;
    }

    canBeExported() {

        return this.hasActive();
    }

    export() {

        if(!this.canBeExported()) { return; }

        this.tree.downloadDirectivesFile(this.active.configuration);
    }

    canBeCloned() {

        return this.hasActive();
    }

    clone() {

        if(!this.canBeCloned()) { return; }

        this.tree.cloneConfiguration(this.active.configuration);
    }

    ////////////////////////////////////////////////////////////////

    tree = null;
    active = null;

    getConfigurationName() {

        return this.active && this.active.getConfigurationName();
    }

    setActive(bundleConfigurationNode) {

        if(bundleConfigurationNode === this.active) { return; }

        const previous = this.active;
        this.active = bundleConfigurationNode;
        previous && previous.updateView();
        this.updateView();
        this.tree.updateActiveConfigurationName(this.getConfigurationName());
    }

    updateView() {

        const element = document.getElementById(`${this.id}_anchor`);
        element && element.classList[this.hasActive() ? 'remove' : 'add']('inactive-node');

        const cssClass = this.hasActive() ? '' : 'inactive-node';
        this.a_attr.class = cssClass;

        const jstreeNode = this.tree._jstree && this.tree._jstree.get_node(this.id);
        jstreeNode && (jstreeNode.a_attr.class = cssClass);

        const newText = `Current bundle configuration${ this.hasActive() ? `: ${this.active.configuration.name}` : ''}`;
        this.text = newText;
        jstreeNode && this.tree._jstree && this.tree._jstree.set_text(jstreeNode, newText);
    }

};
