import Node from '../../Tree/classes/Node';

/**
 *
 */
export default class ProguardFileNode extends Node {

    static typesOptions = {
        proguardFile: {
            icon: 'proguard-file-node fa fa-plug',
            sorting_weight: -1,
        },
    };

    constructor(tree) {

        super(`${tree._nodeIdPrefix}/proguardFile`, 'proguardFile', 'Proguard File', true, true);

        this.tree = tree;
    }

    ////////////////////////////////////////////////////////////////

    tree = null;

};
