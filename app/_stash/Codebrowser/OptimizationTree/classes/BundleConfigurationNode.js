import './BundleConfigurationNode.scss';

/**
 *
 */
const BundleConfigurationNode = function(parentNode, configuration, activeBundleConfigurationNode, storedConfigurationName) {

    this.id = `${parentNode.id}/bundle/${configuration.name}`;
    this.type = 'bundleConfiguration';
    this.text = configuration.name;
    this.children = false;
    this.state = { opened: false };
    this.a_attr = {
        title: `Created:\n${configuration.createdAt}\n\nModified:\n${configuration.modifiedAt}`,
        class: '',
    };

    this.tree = parentNode.tree;
    this.configuration = configuration;
    this.activeBundleConfigurationNode = activeBundleConfigurationNode;

    if(configuration.name === storedConfigurationName) {
        this.makeActive();
    }
};

BundleConfigurationNode.typesOptions = {
    bundleConfiguration: {
        icon: 'bundle-configuration-node fa fa-circle',
        comparator: (a,b) => b.configuration.createdAt - a.configuration.createdAt,
    },
};

BundleConfigurationNode.prototype.isActive = function() {

    return this.activeBundleConfigurationNode.active === this;
};

BundleConfigurationNode.prototype.makeActive = function() {

    if(this.isActive()) { return; }

    this.activeBundleConfigurationNode.setActive(this);
    this.updateView();
};

BundleConfigurationNode.prototype.canBeMerged = function() {

    return this.activeBundleConfigurationNode.hasActive() && !this.isActive();
};

BundleConfigurationNode.prototype.mergeToActive = function() {

    if(!this.canBeMerged()) { return; }

    this.tree.mergeConfiguration(this.configuration);
};

BundleConfigurationNode.prototype.canBeCloned = function() {

    return true;
};

BundleConfigurationNode.prototype.clone = function() {

    this.tree.cloneConfiguration(this.configuration);
};

BundleConfigurationNode.prototype.canBeDeleted = function() {

    return true;
};

BundleConfigurationNode.prototype.delete = function() {

    this.tree.deleteConfiguration(this.configuration, this.id);
};

BundleConfigurationNode.prototype.canBeExported = function() {

    return true;
};

BundleConfigurationNode.prototype.export = function() {

    this.tree.downloadDirectivesFile(this.configuration);
};

BundleConfigurationNode.prototype.rename = function() {

    console.log('BundleConfigurationNode.rename');

    this.tree.renameConfiguration(this.configuration, this.id);
};

////////////////////////////////////////////////////////////////

BundleConfigurationNode.prototype.getConfigurationName = function() {

    return this.configuration.name;
};

BundleConfigurationNode.prototype.updateView = function() {

    const element = document.getElementById(`${this.id}_anchor`);
    element && element.classList[this.isActive() ? 'add' : 'remove']('active-node');

    const cssClass = this.isActive() ? 'active-node' : '';
    this.a_attr.class = cssClass;

    const jstreeNode = this.tree._jstree && this.tree._jstree.get_node(this.id);
    jstreeNode && (jstreeNode.a_attr.class = cssClass);
};

export default BundleConfigurationNode;

/*
 * For now, use the old way of JS OOP (the above implementation). Using the ES6 classes, jstree does not work correctly.
 * In "create_node" jstree uses "$.extend" to clone each passed node instance. But "$.extend" does not clone the whole
 * object deeply (misses prototype properties etc). Therefore, I re-implemented "BundleConfiguration" "class" in the ES5-way
 * and it works...
 *
class BundleConfiguration extends JstreeNode {

    constructor(parentNode, configuration, currentBundleConfigurationNode, storedConfigurationName) {

        super(`${parentNode.id}/bundle/${configuration.name}`, 'bundle_config_set', configuration.name, false, false, {
            title: `Created:\n${configuration.createdAt}\n\nModified:\n${configuration.modifiedAt}`,
            class: '',
        });

        this.tree = parentNode.tree;
        this.configuration = configuration;
        this.currentBundleConfigurationNode = currentBundleConfigurationNode;

        if(configuration.name === storedConfigurationName) {
            this.makeCurrent();
        }
    }

    isCurrent() {

        return this.currentBundleConfigurationNode.current === this;
    }

    makeCurrent() {

        if(this.isCurrent()) { return; }

        this.currentBundleConfigurationNode.setCurrent(this);
        this.updateView();
    }

    canBeMerged() {

        return this.currentBundleConfigurationNode.hasCurrent() && !this.isCurrent();
    }

    mergeToCurrent() {

        if(!this.canBeMerged()) { return; }

        this.tree.mergeConfiguration(this.configuration);
    }

    canBeCloned() {

        return true;
    }

    clone() {

        this.tree.cloneConfiguration(this.configuration);
    }

    canBeDeleted() {

        return true;
    }

    deleteConfig() {

        this.tree.deleteConfiguration(this.configuration, this.id);
    }

    canBeExported() {

        return true;
    }

    exportConfiguration() {

        this.tree.downloadDirectivesFile(this.configuration);
    }

    rename() {

        this.tree.renameConfiguration(this.configuration, this.id);
    }

    ////////////////////////////////////////////////////////////////

    tree = null;
    configuration = null;
    currentBundleConfigurationNode = null;

    getConfigurationName() {

        return this.configuration.name;
    }

    updateView() {

        const element = document.getElementById(`${this.id}_anchor`);
        element && element.classList[this.isCurrent() ? 'add' : 'remove']('current');

        const cssClass = this.isCurrent() ? 'current' : '';
        this.a_attr.class = cssClass;

        const jstreeNode = this.tree._jstree && this.tree._jstree.get_node(this.id);
        jstreeNode && (jstreeNode.a_attr.class = cssClass);
    }
}
*/
