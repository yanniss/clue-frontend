import Node from '../../Tree/classes/Node';
import BundleConfigurationNode from './BundleConfigurationNode';

/**
 *
 */
export default class BundleConfigurationsNode extends Node {

    static typesOptions = {
        bundleConfigurations: {
            icon: 'bundle-configurations-node fa fa-list-ul',
            sorting_weight: 2,
        },
    };

    constructor(tree) {

        super(`${tree._nodeIdPrefix}/bundleConfigurations`, 'bundleConfigurations', 'Bundle configurations', true, true, {
            title: 'The list of bundle-specific configurations.\nEach one can be set as the current bundle configuration.',
        });

        this.tree = tree;
    }

    create() {

        this.tree.createConfiguration();
    }

    ////////////////////////////////////////////////////////////////

    prependConfiguration(configuration, activeBundleConfigurationNode) {

        const jstreeNode = this.tree._jstree.get_node(this.id),
            bundleConfigurationNode = new BundleConfigurationNode(this, configuration, activeBundleConfigurationNode, null);

        this.tree._jstree.create_node(jstreeNode, bundleConfigurationNode, 0);
    }

    tree = null;

};
