import './OptimizationTree.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './OptimizationTree.view.html';
import vm from './OptimizationTree.vm';

koh.registerComponent('component-optimization-tree', vm, view);
