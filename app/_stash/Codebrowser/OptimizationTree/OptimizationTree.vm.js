import 'app/components/FormOption/FormOption.component';
import 'core/components/Menu/Menu.component';
import ko from 'knockout';
import Api from 'app/classes/Api.static';
import Tree from '../Tree/Tree.vm';
import BundleConfigurationNode from './classes/BundleConfigurationNode';
import ActiveAnalysisConfigurationNode from './classes/ActiveAnalysisConfigurationNode';
import ActiveBundleConfigurationNode from './classes/ActiveBundleConfigurationNode';
import BundleConfigurationsNode from './classes/BundleConfigurationsNode';
import ProguardFileNode from './classes/ProguardFileNode';
import { CTX_PAGE } from 'core/classes/events/contexts';
import {
    ANALYSIS_ACTIVATED,
    CONFIGURATION_ACTIVATED, CONFIGURATION_CREATE, CONFIGURATION_CREATED,
    CONFIGURATION_OPEN, CONFIGURATION_RENAME, CONFIGURATION_RENAMED,
    PROGUARD_FILE_OPEN,
} from 'app/classes/eventTypes';
import Notifier from 'app/classes/Notifier.static';

const typesOptions = {
    ...BundleConfigurationNode.typesOptions,
    ...ActiveAnalysisConfigurationNode.typesOptions,
    ...ActiveBundleConfigurationNode.typesOptions,
    ...BundleConfigurationsNode.typesOptions,
    ...ProguardFileNode.typesOptions,
};

/**
 *
 */
export default class OptimizationTree extends Tree {

    bundle;
    lsKey;
    activeBundleConfigurationNode;
    activeAnalysisConfigurationNode;
    bundleConfigurationsNode;
    rightClickedNode = ko.observable();

    constructor(element, { bundle }) {

        super(element, bundle, {
            treeName: 'OptimizationTree',
            stateKeyName: 'OptimizationTree',
            jstreeOptions: {
                core: { check_callback: true },
                types: typesOptions,
            },
        });

        this.bundle = bundle;
        this.lsKey = `CodeBrowser.activeBundleConfiguration:${this.bundle.id}`;

        this.setLoadDataFn(this._loadNodeChildren.bind(this));

        this.setUiEventHandlers('click', [
            {
                nodeTypes: ['activeBundleConfiguration'],
                handler: (e, node) => {
                    if(node.hasActive()) {
                        this.notify(CTX_PAGE, CONFIGURATION_OPEN, node.active.configuration);
                    }
                    else {
                        Notifier.warning('You must first select a configuration from <strong>Bundle configurations</strong>');
                    }
                },
            },
            {
                nodeTypes: ['activeAnalysisConfiguration'],
                handler: (e, node) => {
                    if(node.activeAnalysis !== null) {
                        this.notify(CTX_PAGE, CONFIGURATION_OPEN, node.activeAnalysis);
                    }
                    else {
                        Notifier.warning('You must first activate an analysis');
                    }
                },
            },
            {
                nodeTypes: ['bundleConfiguration'],
                handler: (e, node) => {
                    this.notify(CTX_PAGE, CONFIGURATION_OPEN, node.configuration);
                },
            },
            {
                nodeTypes: ['proguardFile'],
                handler: (e, node) => {
                    this.notify(CTX_PAGE, PROGUARD_FILE_OPEN);
                },
            },
        ]);

        this.setUiEventHandlers('contextmenu', [
            {
                nodeTypes: ['activeBundleConfiguration', 'activeAnalysisConfiguration', 'bundleConfigurations', 'bundleConfiguration'],
                handler: (e, node) => {
                    this.rightClickedNode(node);
                    setTimeout(() => this.children.menu.open(e), 100);
                },
            },
        ]);

        this.draw();

        this.startListeningToPageEvents();
    }

    _loadNodeChildren(node, setChildrenFn) {

        if(!node) {
            this._loadRootChildren(setChildrenFn);
        }
        else if('bundleConfigurations' === node.type) {
            this._loadBundleConfigurations(node, setChildrenFn);
        }
        else {
            setChildrenFn([]);
        }
    }

    _loadRootChildren(setChildrenFn) {

        this.proguardFileNode = new ProguardFileNode(this);
        this.activeBundleConfigurationNode = new ActiveBundleConfigurationNode(this);
        this.activeAnalysisConfigurationNode = new ActiveAnalysisConfigurationNode(this, this.activeBundleConfigurationNode);
        this.bundleConfigurationsNode = new BundleConfigurationsNode(this);

        setChildrenFn([
            ...(this.bundle.hasProguard ? [ this.proguardFileNode ] : []),
            this.activeBundleConfigurationNode,
            this.activeAnalysisConfigurationNode,
            this.bundleConfigurationsNode,
        ]);
    }

    _loadBundleConfigurations(parentNode, setChildrenFn) {

        Api.fetchConfigurations(this.bundle.id)
            .then(configurations => {
                const storedConfigurationName = this.getStoredConfigurationName();

                setChildrenFn(configurations.map(configuration => new BundleConfigurationNode(
                    parentNode,
                    configuration,
                    this.activeBundleConfigurationNode,
                    storedConfigurationName,
                )));
            })
            .catch(() => {
                setChildrenFn(false);
            });
    }

    startListeningToPageEvents() {

        this.on(CTX_PAGE, ANALYSIS_ACTIVATED, analysis => {
            this.activeAnalysisConfigurationNode.setAnalysis(analysis);
        });

        this.on(CTX_PAGE, CONFIGURATION_RENAMED, configurationName => {
            const jstreeNode = this._jstree.get_node(this.configurationUnderRenamingNodeId);
            this._jstree.rename_node(jstreeNode, configurationName);
            jstreeNode.original.configuration.name = configurationName;
        });

        this.on(CTX_PAGE, CONFIGURATION_CREATED, configuration => {
            this.bundleConfigurationsNode.prependJstreeNodeFor(configuration, this.activeBundleConfigurationNode);
        });
    }

    getStoredConfigurationName() {

        return localStorage.getItem(this.lsKey);
    }

    storeConfigurationName(configurationName) {

        localStorage.setItem(this.lsKey, configurationName);
    }

    removeStoredConfigurationName() {

        localStorage.removeItem(this.lsKey);
    }

    updateActiveConfigurationName(configurationName) {

        if(configurationName) {
            this.storeConfigurationName(configurationName);
        }
        else {
            this.removeStoredConfigurationName();
        }

        this.notify(CTX_PAGE, CONFIGURATION_ACTIVATED, configurationName);
    }

    downloadDirectivesFile(configuration) {

        Api.downloadDirectivesFile(this.bundle.id, configuration);
    }

    cloneConfiguration(configuration) {

        Api.cloneConfiguration(this.bundle.id, configuration)
            .then(clonedConfiguration => {
                this.bundleConfigurationsNode.prependJstreeNodeFor(clonedConfiguration, this.activeBundleConfigurationNode);
            })
            .catch(() => Notifier.error('Cannot clone same configuration twice'));
    }

    mergeConfiguration(configuration) {

        Api.mergeConfigurationTo(this.bundle.id, configuration, this.activeBundleConfigurationNode.active.configuration)
            .then(() => {
                location.reload(); //todo: do not reload;
            })
            .catch(() => Notifier.error('Configurations merging failed'));
    }

    deleteConfiguration(configuration, nodeId) {

        Api.deleteConfiguration(this.bundle.id, configuration)
            .then(() => {
                const jstree = this._jstree;
                jstree.delete_node(jstree.get_node(nodeId));
            })
            .catch(() => Notifier.error('Configuration deletion failed'));
    }

    configurationUnderRenamingNodeId;

    renameConfiguration(configuration, nodeId) {

        this.configurationUnderRenamingNodeId = nodeId;
        this.notify(CTX_PAGE, CONFIGURATION_RENAME, configuration);
    }

    createConfiguration() {

        this.notify(CTX_PAGE, CONFIGURATION_CREATE);
    }

};
