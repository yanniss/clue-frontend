import './Configuration.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './Configuration.vm';
import view from './Configuration.view.html';

koh.registerComponent('component-configuration', vm, view);
