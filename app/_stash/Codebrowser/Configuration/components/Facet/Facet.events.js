import str from 'core/classes/StringHelper.static';

const FACET_VALUE_ENABLED = str.createUnique();
const FACET_VALUE_DISABLED = str.createUnique();
const FACET_FILTERS_CATEGORY_CLEAR = str.createUnique();
const FACET_FILTERS_ALL_CLEAR = str.createUnique();

export {
    FACET_VALUE_ENABLED,
    FACET_VALUE_DISABLED,
    FACET_FILTERS_CATEGORY_CLEAR,
    FACET_FILTERS_ALL_CLEAR,
};
