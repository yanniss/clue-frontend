import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import { CTX_ASCENDANTS } from 'core/classes/events/contexts';
import { FACET_VALUE_ENABLED, FACET_VALUE_DISABLED, FACET_FILTERS_CATEGORY_CLEAR } from './Facet.events';

/**
 *
 */
export default class Facet extends Base {

    constructor(element, { category, values }) {

        super(element);

        this.category = category;
        this.values = values;
        this.hasChecked = values.some(v => v.checked);
    }

    toggle = (value, event) => {

        const checked = event.target.checked;

        this.notify(CTX_ASCENDANTS, checked ? FACET_VALUE_ENABLED : FACET_VALUE_DISABLED, {
            category: this.category,
            value: value.name,
        });

        return true;
    };

    clearFilters = () => {

        this.notify(CTX_ASCENDANTS, FACET_FILTERS_CATEGORY_CLEAR, this.category);
    };

};
