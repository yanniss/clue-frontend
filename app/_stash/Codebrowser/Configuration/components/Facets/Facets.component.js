import './Facets.style.scss'
import koh from 'core/classes/KnockoutHelper.static';
import vm from './Facets.vm';
import view from './Facets.view.html';

koh.registerComponent('component-facet', vm, view);
