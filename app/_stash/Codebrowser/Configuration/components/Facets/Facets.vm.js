import '../Facet/Facet.component';
import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import { FACET_FILTERS_ALL_CLEAR } from '../Facet/Facet.events';
import { CTX_ASCENDANTS } from 'core/classes/events/contexts';

/**
 *
 */
export default class Facets extends Base {

    hasChecked = false;

    constructor(element, { facets, filters, textFilter }) {

        super(element);
        this.textFilter = textFilter;
        this.updateFacets(facets, filters);
    }

    facets = ko.observableArray();

    updateFacets(facets, filters) {

        let hasChecked = false;

        this.facets(Object.entries(facets).map(([ category, values ]) => ({
            category: category,
            values: Object.entries(values).map(([ value, count ]) => {
                const checked = filters[category].hasOwnProperty(value);
                hasChecked = hasChecked || checked;
                return {
                    name: value,
                    count: count,
                    checked: checked,
                };
            }),
        })));

        this.hasChecked = hasChecked;
    }

    clearFilters() {

        this.notify(CTX_ASCENDANTS, FACET_FILTERS_ALL_CLEAR);
    }

};
