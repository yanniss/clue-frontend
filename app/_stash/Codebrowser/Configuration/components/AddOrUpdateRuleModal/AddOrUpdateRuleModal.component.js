import './AddOrUpdateRuleModal.style.scss'
import koh from 'core/classes/KnockoutHelper.static';
import view from './AddOrUpdateRuleModal.view.html';
import vm from './AddOrUpdateRuleModal.vm';

koh.registerComponent('modal-for-rule', vm, view);
