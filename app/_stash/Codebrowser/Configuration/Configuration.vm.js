import str from 'core/classes/StringHelper.static';
import 'core/bindings/rclick';
import './components/Facet/Facet.component';
import Api from 'app/classes/Api.static';
import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import Configuration from 'codebrowser/classes/Configuration';
import Analysis from 'codebrowser/classes/Analysis';
import './components/AddOrUpdateRuleModal/AddOrUpdateRuleModal.component';
import { CTX_DESCENDANTS, CTX_PAGE } from 'core/classes/events/contexts';
import {
    CONFIGURATION_CREATED,
    DIRECTIVE_DELETE,
    DIRECTIVE_KEEP_METHOD,
    DIRECTIVE_MENU_OPEN,
    DIRECTIVE_REMOVE_METHOD,
    FILE_RAW_OPEN
} from 'app/classes/eventTypes';
import {
    FACET_FILTERS_ALL_CLEAR,
    FACET_FILTERS_CATEGORY_CLEAR,
    FACET_VALUE_DISABLED,
    FACET_VALUE_ENABLED
} from './components/Facet/Facet.events';

const resultsPerPage = 20;

class TextFilter {

    //user input
    text = ko.observable(null);
    //type of input
    textType = 'W'; //Wildcard (also supported: R-Regex,FT-FullText, but not tested)
    //where to look it up
    lookupFields = ko.observableArray(['package']); //allowed values: 'package', 'class', 'method'

    clear() {
        this.text(null);
        this.lookupFields(['package']);
    }
}

class ActionOnFilteredDirectives {

    constructor(label, description, action) {
        this.label = label;
        this.description = description;
        this.execute = action;
    }
}

/**
 *
 */
export default class Directives extends Base {

    configuration;
    facets = ko.observable();
    directives = ko.observableArray();
    count = ko.observable(0).extend({ notify: 'always' });

    atPage = ko.observable(0).extend({ notify: 'always' });
    totalPages = ko.observable(0);

    hasChanged = false;
    loadFacets = false;

    filters;

    activeConfigurationName;

    textFilter = new TextFilter();

    rules = ko.observableArray();
    activeRule = ko.observable(null);

    showFilters = ko.observable(false);

    openRuleModal = ko.observable(false);

    isAnalysis = ko.observable(false);

    actions = [
        /*
        new ActionOnFilteredDirectives("Toggle", 'Convert "Keep" to "Remove" and vice versa', () => {
            alert("Under construction")
        }),
        new ActionOnFilteredDirectives('Convert to "Keep"', 'Convert all directives to "Keep"', () => {
            alert("Under construction")
        }),
        new ActionOnFilteredDirectives('Convert to "Remove"', 'Convert all directives to "Remove"', () => {
            alert("Under construction")
        }),
        */
        new ActionOnFilteredDirectives("Merge", "Merge all directives to the current bundle configuration", () => {
            const activeConfigurationName = this.activeConfigurationName();
            if (activeConfigurationName) {
                this.mergeDirectives(activeConfigurationName);
            }
            else {
                alert("There is no active bundle configuration");
            }
        }),
        new ActionOnFilteredDirectives("Clone", "Clone all directives to a new configuration", () => {
            this.cloneDirectives();
        }),
        new ActionOnFilteredDirectives("Export", "Export all directives", () => {
            this.downloadDirectives();
        }),
        new ActionOnFilteredDirectives("Delete", "Delete all directives", () => {
            //do we need a confirmation here?
            this.deleteDirectives();
        }),
    ]

    /**
     * @public
     * @param {HTMLElement} element
     * @param {Bundle} bundle
     */
    constructor(element, { bundle, activeConfigurationName }) {

        super(element);

        this.bundle = bundle;
        this.activeConfigurationName = activeConfigurationName;

        this.addOrUpdateRule = this.addOrUpdateRule.bind(this);

        this.computed(() => {
            const count = this.count();
            this.totalPages(Math.ceil(count / resultsPerPage));
        }).extend({ deferred: true });

        this.subscribe(this.atPage, atPage => {
            this.loadDirectives();
        });

        this.subscribe(this.textFilter.text, txt => {
            this.loadFacets = true;
            this.atPage(1);
        });

        this.subscribe(this.textFilter.lookupFields, fields => {
            if (this.textFilter.text()) {
                this.loadFacets = true;
                this.atPage(1);
            }
        });

        this.on(CTX_PAGE, DIRECTIVE_KEEP_METHOD, method => {
            this.hasChanged = true;
            this.loadDirectives();
        }, { ignoreIfEquals: false });

        this.on(CTX_PAGE, DIRECTIVE_REMOVE_METHOD, method => {
            this.hasChanged = true;
            this.loadDirectives();
        }, { ignoreIfEquals: false });

        this.on(CTX_PAGE, DIRECTIVE_DELETE, method => {
            this.hasChanged = true;
            this.loadFacets = true;
            this.updateFilters(this.facets);
            this.loadDirectives();
        }, { ignoreIfEquals: false });

        this.on(CTX_DESCENDANTS, FACET_VALUE_DISABLED, ({ category, value }) => {
            delete this.filters[category][value];
            this.loadFacets = true;
            this.atPage(1);
        });
        this.on(CTX_DESCENDANTS, FACET_VALUE_ENABLED, ({ category, value }) => {
            this.filters[category][value] = true;
            this.loadFacets = true;
            this.atPage(1);
        });
        this.on(CTX_DESCENDANTS, FACET_FILTERS_CATEGORY_CLEAR, category => {
            this.filters[category] = {};
            this.loadFacets = true;
            this.atPage(1);
        }, { ignoreIfEquals: false });
        this.on(CTX_DESCENDANTS, FACET_FILTERS_ALL_CLEAR, () => {
            this.clearFilters();
            this.text(null);
            this.loadFacets = true;
            this.atPage(1);
        }, { ignoreIfEquals: false });

        this.clearFilters();
        /*
        this.clearFilters();
        this.loadFacets = true;
        this.loadDirectives();
        */
    }

    /**
     * @public
     */
    openConfiguration(configuration) {

        if(configuration === this.configuration && !this.hasChanged) { return; }
        this.configuration = configuration;
        this.hasChanged = false;

        if (this.configuration instanceof Configuration) {
            //config set
            this.loadRules();
            this.isAnalysis(false);
        }
        else {
            //analysis directives
            this.activeRule(null);
            this.resetDirectives();
            this.isAnalysis(true);
        }
    }

    /**
     * @private
     */
    resetDirectives() {
        this.clearFilters();
        this.loadFacets = true;
        this.atPage(1);
    }

    /**
     * @private
     */
    openSymbol = directive => {

        this.notify(CTX_PAGE, FILE_RAW_OPEN, {
            bundleId: this.bundle.id,
            filepath: directive.filepath,
            line: directive.line,
            column: directive.column,
        });
    };

    openMenu = (directive, e) => {

        this.notify(CTX_PAGE, DIRECTIVE_MENU_OPEN, {
            method: {
                ...directive,
                bundleDirective: directive,
            },
            event: e,
        });
    };

    /**
     * @private
     */
    activateRule(rule) {
        this.activeRule(rule);
        this.resetDirectives();
    }

    /**
     * @private
     */
    addRuleWithModal() {
        console.log("Adding rule with modal");
        this.activeRule(null);
        this.openRuleModal(true);
    }

    /**
     * @private
     */
    editRuleWithModal() {
        //there is always an active rule when invoked
        console.log("Updating rule with modal ", this.activeRule());
        this.openRuleModal(true);
    }

    /**
     * @private
     */
    deleteRuleWithModal() {
        //there is always an active rule when invoked
        const rule = this.activeRule();
        console.log("Deleting rule with modal ", rule);
        if (window.confirm("Really delete the selected rule?")) {
            Api.deleteRule(this.bundle.id, this.configuration.name, rule.id).then(() => {
                this.activeRule(null);
                this.rules.remove(rule);
            });
        }
    }

    /**
     * @public
     */
    addOrUpdateRule(ruleId, ruleBody) {
        const promise = ruleId === null ?
            Api.addRule(this.bundle.id, this.configuration.name, ruleBody) :
            Api.updateRule(this.bundle.id, this.configuration.name, ruleId, ruleBody);

        let serverRuleId = null;
        promise.then( id => {
            serverRuleId = id;
            return this.loadRules();
        }).then( () => {
            const rule = this.rules().find( r => r.id === serverRuleId );
            if (rule) {
                this.activateRule(rule);
            }
        });

        return promise;
    }

    /**
     * @private
     */
    loadRules() {
        if(!this.configuration || !this.configuration instanceof Configuration) { return; }
        Api.fetchRules(this.bundle.id, this.configuration.name).then( rules => this.rules(rules) );
    }

    /**
     * @private
     */
    loadDirectives() {

        if(!this.configuration) { return; }

        const loadFacets = this.loadFacets;
        const params = {
            _start: (this.atPage.peek() - 1) * resultsPerPage,
            _count: resultsPerPage,
            //_text: '',
            _sort: 'doopId|ASC',
            ...(loadFacets ? { _facets: true } : {}),
            ...this.convertTextAndFiltersToParams(),
        };

        if(this.configuration instanceof Configuration) {
            params.set = this.configuration.name;
        }
        else if(this.configuration instanceof Analysis) {
            params.analysis = this.configuration.id;
        }

        const rule = this.activeRule();
        if (rule !== null) {
            params.ruleId = rule.id;
        }

        Api.fetchDirectives(this.bundle.id, params)
            .then(({ hits, facets, directives }) => {
                if(loadFacets) {
                    this.updateFilters(facets);
                    this.facets(facets);
                }
                this.count(hits);
                this.directives(directives);
            });

        this.loadFacets = false;
    }

    mergeDirectives(target) {
        Api.mergeDirectivesOfConfigurationTo(this.bundle.id, this.configuration, this.convertTextAndFiltersToParams(), target)
           .then(() => {
               window.location.reload(); //TODO
           });
    }

    cloneDirectives() {
        Api.cloneDirectivesOfConfiguration(this.bundle.id, this.configuration, this.convertTextAndFiltersToParams()).then( configuration => {
            this.notify(CTX_PAGE, CONFIGURATION_CREATED, configuration)
        });
    }

    downloadDirectives() {
        Api.downloadFilteredDirectivesFile(this.bundle.id, this.configuration, this.convertTextAndFiltersToParams());
    }

    deleteDirectives() {

        const configurationName = (this.configuration instanceof Configuration) && this.configuration.name
            || (this.configuration instanceof Analysis) && this.configuration.id;

        Api.deleteDirectives(this.bundle.id, configurationName, this.convertTextAndFiltersToParams())
            .then(() => {
                this.loadFacets = true;
                this.clearFilters();
                this.loadDirectives();
            });
    }

    convertTextAndFiltersToParams() {

        const params = {};

        for(let [ category, enabledValues ] of Object.entries(this.filters)) {
            enabledValues = Object.keys(enabledValues);

            if(enabledValues.length === 0) { continue; }

            if(category === 'directiveTypes') {
                category = 'typeOfDirective';
            }
            params[category] = Array.from(enabledValues);
        }

        params.textType = this.textFilter.textType;
        params.text = this.textFilter.text();
        params.textField = this.textFilter.lookupFields();

        return params;
    }

    /**
     * Updates filters based on new facets. Actually removes any filters that do not exist in the new facets.
     *
     * @private
     * @param facets
     */
    updateFilters(facets) {

        for(const [ category, enabledValues ] of Object.entries(this.filters)) {
            if(Object.keys(facets[category]).length === 0) {
                this.filters[category] = {};
                continue;
            }

            const facet = facets[category];
            for(const name of Object.keys(enabledValues)) {
                if(!facet.hasOwnProperty(name)) {
                    delete enabledValues[name];
                }
            }
        }
    }

    clearFilters() {

        this.filters = {
            directiveTypes: {},
            origin: {},
            package: {},
            class: {},
        };
        this.textFilter.clear();
    }

};
