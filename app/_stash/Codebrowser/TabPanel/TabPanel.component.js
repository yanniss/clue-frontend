import './TabPanel.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './TabPanel.view.html';
import vm from './TabPanel.vm';

koh.registerComponent('component-tab-panel', vm, view);
