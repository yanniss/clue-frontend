import './components/Tabs/Tabs.component';
import '../Overview/Overview.component';
import '../Editor/Editor.component';
import '../Analysis/Analysis.component';
import '../Directives/Directives.component';
import '../ProguardManager/ProguardManager.component';
import Base from 'core/classes/vm/Base.vm';
import { CTX_DESCENDANTS } from 'core/classes/events/contexts';
import { FILE_SAVE_STATE} from 'app/classes/eventTypes';
import * as tabEvents from './TabPanel.events';

/**
 *
 */
export default class TabPanel extends Base {

    /**
     *
     * @param {HTMLElement} element
     * @param {Bundle} bundle
     */
    constructor(element, { bundle }) {

        super(element);

        this.bundle = bundle;

        this.startListeningToTabs();
        this.startListeningToEditor();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bundle;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view model members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    activeChild;

    startListeningToTabs() {

        this.on(CTX_DESCENDANTS, tabEvents.TAB_OVERVIEW_ACTIVATED, () => {
            this.activateChild(this.children.overview);
        });

        this.on(CTX_DESCENDANTS, tabEvents.TAB_FILE_ACTIVATED, ({ file, line, column, scrollTop, scrollLeft }) => {
            const editor = this.children.editor;
            this.activateChild(editor);
            editor.openFile(file, line, column, scrollTop, scrollLeft);
        });

        this.on(CTX_DESCENDANTS, tabEvents.TAB_ANALYSIS_ACTIVATED, analysis => {
            const analysisPanel = this.children.analysis;
            this.activateChild(analysisPanel);
            analysisPanel.openAnalysis(analysis);
        });

        this.on(CTX_DESCENDANTS, tabEvents.TAB_CONFIGURATION_ACTIVATED, configuration => {
            const configurationPanel = this.children.configuration;
            this.activateChild(configurationPanel);
            //configurationPanel.openConfiguration(configuration);
        });

        this.on(CTX_DESCENDANTS, tabEvents.TAB_PROGUARD_FILE_ACTIVATED, () => {
            const proguardManager = this.children.proguardManager;
            this.activateChild(proguardManager);
        });

        this.on(CTX_DESCENDANTS, tabEvents.TAB_NO_ACTIVE, () => {
            this.activateChild(null);
        });
    }

    startListeningToEditor() {

        this.on(CTX_DESCENDANTS, FILE_SAVE_STATE, ({ file, line, column, scrollTop, scrollLeft }) => {
            const tabs = this.children.tabs;
            tabs.saveState(file, line, column, scrollTop, scrollLeft);
        });
    }

    activateChild(childToActivate) {

        const activeChild = this.activeChild;

        if(childToActivate !== activeChild) {
            activeChild && activeChild.hide();
            childToActivate && childToActivate.show();
            this.activeChild = childToActivate;
        }
    }

};
