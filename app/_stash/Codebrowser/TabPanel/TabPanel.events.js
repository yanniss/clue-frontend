import str from 'core/classes/StringHelper.static';

/**
 * Tab-related events
 */

/**
 * undefined
 */
export const TAB_FILE_ACTIVATED = str.createUnique();

/**
 * undefined
 */
export const TAB_OVERVIEW_ACTIVATED = str.createUnique();

/**
 * undefined
 */
export const TAB_CONFIGURATION_ACTIVATED = str.createUnique();

/**
 * undefined
 */
export const TAB_PROGUARD_FILE_ACTIVATED = str.createUnique();

/**
 * undefined
 */
export const TAB_ANALYSIS_ACTIVATED = str.createUnique();

/**
 * undefined
 */
export const TAB_NO_ACTIVE = str.createUnique();
