import File from 'codebrowser/classes/File';
import Tab from './Tab';

/**
 *
 */
export default class FileTab extends Tab {

    file;
    line;
    column;
    scrollTop;
    scrollLeft;

    /**
     *
     * @param {File} file
     * @param {number} line
     * @param {number} column
     */
    constructor(file, line, column) {

        if(!(file instanceof File)) {
            throw new Error(`FileTab.constructor: file must be of type File: ${file}`);
        }

        super(file.name, file.path);

        this.file = file;
        this.line = line;
        this.column = column;
    }

    dispose() {

        this.file.dispose();

        super.dispose();
    }

    toString() {

        return this.file.toString();
    }

};
