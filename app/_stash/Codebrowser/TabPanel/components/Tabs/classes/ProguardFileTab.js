import Tab from './Tab';

/**
 *
 */
export default class ProguardFileTab extends Tab {

    constructor() {

        super('Proguard', 'Proguard File Rules');
    }

    toString() {

        return 'proguard file tab';
    }

};
