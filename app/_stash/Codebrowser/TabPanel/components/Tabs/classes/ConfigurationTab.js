import Tab from './Tab';

/**
 *
 */
export default class ConfigurationTab extends Tab {

    constructor(configuration) {

        super(configuration.name, configuration.name);

        this.configuration = configuration;
    }

    toString() {

        return `configuration_name:${this.configuration.name}`;
    }

};
