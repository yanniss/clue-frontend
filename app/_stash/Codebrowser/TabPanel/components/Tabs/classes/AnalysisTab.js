import Tab from './Tab';

/**
 *
 */
export default class AnalysisTab extends Tab {

    constructor(analysis) {

        super(analysis.configurationName, `${analysis.configurationName}\n${analysis.createdAt}`);

        this.analysis = analysis;
    }

    toString() {

        return `analysis_name:${this.analysis.name}`;
    }

};
