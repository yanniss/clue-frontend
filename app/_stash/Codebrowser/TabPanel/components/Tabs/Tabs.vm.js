import Base from 'core/classes/vm/Base.vm';
import AnalysisTab from './classes/AnalysisTab';
import ConfigurationTab from './classes/ConfigurationTab';
import File from 'codebrowser/classes/File';
import FileTab from './classes/FileTab';
import OverviewTab from './classes/OverviewTab';
import $ from 'jquery';
import ko from 'knockout';
import { isPositiveNatural } from 'core/classes/NumberHelpers';
import { CTX_PAGE, CTX_ASCENDANTS } from 'core/classes/events/contexts';
import { FILE_RAW_OPEN, CONFIGURATION_OPEN, PROGUARD_FILE_OPEN, ANALYSIS_OPEN } from 'app/classes/eventTypes';
import * as tabEvents from '../../TabPanel.events';
import ProguardFileTab from './classes/ProguardFileTab';

/**
 *
 */
export default class Tabs extends Base {

    /**
     * @param {HTMLElement} element
     * @param {Bundle} bundle
     */
    constructor(element, { bundle}) {

        super(element);

        this.bundle = bundle;
    }

    dispose() {

        const tabs = this.tabs();
        for(let i = 0, n = tabs.length; i < n; i++) {
            tabs[i].dispose();
        }

        super.dispose();
    }

    /**
     * @public
     * @param {string} [artifactId]
     * @param {string} filepath
     * @param {number} [line]
     * @param {number} [column]
     */
    openFile(artifactId, filepath, line, column) {

        const fileId = File.createId(this.bundle.id, artifactId, filepath);
        let tab = this.tabs().find(tab => tab.file && tab.file.toString() === fileId);

        if(tab) {
            if(isPositiveNatural(line)) {
                tab.line = line;
                tab.column = isPositiveNatural(column) ? column : 1;
            }
        }
        else {
            const file = new File(this.bundle.id, artifactId, filepath, this.getPubsub(CTX_PAGE));
            tab = new FileTab(file, line, column);
            this.tabs.push(tab);
        }

        this.selectTab(tab);
    }

    /**
     * @public
     * @param {File} file
     * @param {number} [line]
     * @param {number} [column]
     * @param {number} [scrollTop]
     * @param {number} [scrollLeft]
     */
    saveState(file, line, column, scrollTop, scrollLeft) {

        if(!(file instanceof File)) {
            throw new Error(`Tabs.saveState: file must be of type File: ${file}`);
        }

        const tab = this.tabs().find(tab => tab.file === file);
        // If tab does not exist, it is not necessarily an error, since the tab might have just been removed.
        if(tab) {
            tab.line = line;
            tab.column = column;
            tab.scrollTop = scrollTop;
            tab.scrollLeft = scrollLeft;
        }
    }

    /**
     * @public
     * @param {*} configuration
     */
    openConfiguration(configuration) {

        let tab = this.tabs().find(tab => tab instanceof ConfigurationTab && tab.configuration.id === configuration.id);

        if(!tab) {
            tab = new ConfigurationTab(configuration);
            this.tabs.push(tab);
        }

        this.selectTab(tab);
    }

    /**
     * @public
     * @param {*} analysis
     */
    openAnalysis(analysis) {

        let tab = this.tabs().find(tab => tab instanceof AnalysisTab && tab.analysis.id === analysis.id);

        if(!tab) {
            tab = new AnalysisTab(analysis);
            this.tabs.push(tab);
        }

        this.selectTab(tab);
    }

    /**
     * @public
     */
    openProguardFile() {

        let tab = this.tabs().find(tab => tab instanceof ProguardFileTab);

        if(!tab) {
            tab = new ProguardFileTab();
            this.tabs.push(tab);
        }

        this.selectTab(tab);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    tabs = ko.observableArray([ new OverviewTab() ]);
    activeTab = ko.observable();

    selectTab = tab => {

        this.activateTab(tab);

        if(tab instanceof FileTab) {
            this.notify(CTX_ASCENDANTS, tabEvents.TAB_FILE_ACTIVATED, {
                file: tab.file,
                line: tab.line,
                column: tab.column,
                scrollTop: tab.scrollTop,
                scrollLeft: tab.scrollLeft,
            });
        }
        else if(tab instanceof OverviewTab) {
            this.notify(CTX_ASCENDANTS, tabEvents.TAB_OVERVIEW_ACTIVATED);
        }
        else if(tab instanceof AnalysisTab) {
            this.notify(CTX_ASCENDANTS, tabEvents.TAB_ANALYSIS_ACTIVATED, tab.analysis);
        }
        else if(tab instanceof ConfigurationTab) {
            this.notify(CTX_ASCENDANTS, tabEvents.TAB_CONFIGURATION_ACTIVATED, tab.configuration);
        }
        else if(tab instanceof ProguardFileTab) {
            this.notify(CTX_ASCENDANTS, tabEvents.TAB_PROGUARD_FILE_ACTIVATED);
        }
        else {
            this.notify(CTX_ASCENDANTS, tabEvents.TAB_NO_ACTIVE);
        }
    };

    closeTab = (tab, event) => {

        if(!tab.closable) { return; }

        // Remove tab.
        const index = this.tabs.indexOf(tab);
        this.tabs.splice(index, 1);

        // Select another tab after removing the current one.
        if(tab === this.activeTab()) {
            const tabs = this.tabs();

            if(index < tabs.length) {
                this.selectTab(tabs[index]);
            }
            else if(index > 0) {
                this.selectTab(tabs[index - 1]);
            }
            else {
                this.selectTab();
            }
        }

        tab.dispose();

        // Stop event bubbling for the case where the tab was closed by clicking the "close" button. If bubbling was
        // allowed, then a "click" event would fire for the tab itself, which would result to a "selectTab" call for
        // the just removed tab.
        event.stopPropagation();
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view model members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bundle;

    /**
     * @override
     */
    initAfterBinding() {

        this.selectTab(this.tabs()[0]);
        this.startListeningToPageEvents();
    }

    startListeningToPageEvents() {

        this.on(CTX_PAGE, FILE_RAW_OPEN, ({ artifactId, filepath, line, column }) => {
            this.openFile(artifactId, filepath, line, column);
        });

        this.on(CTX_PAGE, CONFIGURATION_OPEN, configuration => {
            this.openConfiguration(configuration);
        }, { ignoreIfEquals: false });

        this.on(CTX_PAGE, ANALYSIS_OPEN, analysis => {
            this.openAnalysis(analysis);
        }, { ignoreIfEquals: false });

        this.on(CTX_PAGE, PROGUARD_FILE_OPEN, () => {
            this.openProguardFile();
        }, { ignoreIfEquals: false });
    }

    activateTab = (tab = null) => {

        this.activeTab(tab);
        tab && this.scrollToTab(tab);
    };

    scrollToTab(tab) {

        const element = this.element,
            li = document.getElementById(tab.domId),
            rangeBegin = element.scrollLeft,
            rangeEnd = rangeBegin + $(element).width(),
            tabBegin = li.offsetLeft - element.offsetLeft,
            tabEnd = tabBegin + $(li).outerWidth();

        if(tabBegin < rangeBegin) {
            element.scrollLeft = tabBegin;
        }
        else if(tabEnd > rangeEnd) {
            element.scrollLeft = element.scrollLeft + tabEnd - rangeEnd;
        }
    }

};
