import _ from 'underscore';
import Api from '../../../../classes/Api.static';
import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';

const count = 10;

/**
 *
 */
export default class OptimizationDirectiveList extends Base {

    constructor(element, {
        project,
        userOptimizationDirectives,
        analysis,
    }) {

        super(element);

        this.project = project;
        this.analysis = analysis;

        this._lsKey = `OptimizationDirectiveList:${project.id}`;
        let state   = JSON.parse(localStorage.getItem(this._lsKey) || '{}'),
            origin  = state.origin || 'USER',
            typeOD  = state.typeOfDirective || 'REMOVE',
            typeOE  = state.typeOfElement || 'Method';

        this.origin = ko.observable(origin);
        this.typeOfDirective = ko.observable(typeOD);
        this.typeOfElement = ko.observable(typeOE);

        this.directives = ko.observableArray();
        this.start = 0;
        this.hasMore = ko.observable(true);

        this._userOptimizationDirectives = userOptimizationDirectives;

        this.sub = ko.computed(() => {
            this._load(0, count, false);
        }).extend({ deferred: true });

        this._setupKoSubscriptions();
    }

    dispose() {

        this.sub.dispose();
        super.dispose();
    }

    _setupKoSubscriptions() {

        this.subscribe(this._userOptimizationDirectives.directiveAdded, () => {
            this.origin() === 'USER' && this._loadUserDirectives();
        });

        this.subscribe(this._userOptimizationDirectives.directiveRemoved, () => {
            this.origin() === 'USER' && this._loadUserDirectives();
        });
    }

    _loadUserDirectives() {

        const typeOfDirective = this.typeOfDirective();

        this.directives(this._userOptimizationDirectives.getAllSorted().filter(d =>
            d.typeOfDirective === typeOfDirective
        ));
    }

    _load(start, count, append) {

        const projId = this.project.id,
            analId = this.analysis.peek().id,
            origin = this.origin(),
            typeOD = this.typeOfDirective();

        localStorage.setItem(this._lsKey, JSON.stringify({
            origin: origin,
            typeOfDirective: typeOD,
        }));

        if(origin === 'USER') {
            this._loadUserDirectives();
            this.hasMore(false);
        }
        else {
            Api.getOptimizationDirectives(projId, analId, origin, typeOD, start, count)
                .then(results => {
                    const directives = [];
                    results.results.forEach(result => {
                        const directive = result.directives[origin === 'USER' && 'definedByUser' || origin === 'ANALYSIS' && 'generatedByAnalysis'];
                        directive.element = result.element;
                        directives.push(directive);
                    });

                    this.start += results.results.length;
                    this.hasMore(results.hits > this.start);
                    this.directives(append ? this.directives.peek().concat(directives) : directives);
                })
                .catch(() => {});
        }
    }

    loadMore() {

        this._load(this.start, count, true);
    }

    open = (directive, event) => {

        this.notify('request open file', {
            file:            directive.element.sourceFileName,
            line:            directive.element.position.startLine,
            column:          directive.element.position.startColumn,
            triggerPosition: true,
        });
    };

    openMenu = (directive, event) => {

        this.notify('open optimization directive menu', {
            element: directive.element,
            event:   event,
        });
    };

    getDeclaringClassHtml(directive) {

        const declaringClass = _.escape(directive.element.declaringClassDoopId);

        return `<span style="font-size: 80%; color: #888;">${declaringClass}</span>`;
    }

    getSymbolNameHtml(directive) {

        const method = directive.element,
            parts = method.declaringClassDoopId.split('.'),
            declaringClassName = parts[parts.length - 1].split('$'),
            methodName = (method.name === '<init>' && method.sourceFileName.endsWith('.java') ? declaringClassName : method.name),
            params = method.paramTypes.join(', '),
            paramsShortened = params.substr(0, 30 - methodName.length);

        return `
            <span style="font-weight: bold;">${_.escape(methodName)}</span>
            <span>(${_.escape(paramsShortened)}${params.length !== paramsShortened.length ? ' ...' : ''}): ${_.escape(method.returnType)}</span>
        `;
    }

};
