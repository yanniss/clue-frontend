import {
    ANALYSIS_ACTIVATED,
    CONFIGURATION_ACTIVATED, DIRECTIVE_DELETE,
    DIRECTIVE_KEEP_METHOD,
    DIRECTIVE_REMOVE_METHOD
} from 'app/classes/eventTypes';
import DataAsync from 'core/classes/DataAsync';

export default class FileManager {

    files = {};
    pubsub;
    activeAnalysis;

    constructor() {

    }

    dispose() {

    }

    startListeningToPageEvents() {


        this.pubsub.on(ANALYSIS_ACTIVATED, analysis => {

            this.activeAnalysis = analysis;

            this.analysisId = analysis.id;
            this.analysisDirectives = new DataAsync(this.load('analysisDirectives'));

        }, this, { synchronous: true });

        this.pubsub.on(CONFIGURATION_ACTIVATED, configurationName => {

            this.bundleConfigurationName = configurationName;
            this.bundleDirectives = new DataAsync(this.load('bundleDirectives'));

        }, this, { synchronous: true });

        const clear = directive => {
            if(directive.sourceFileName !== this.path) { return; }
            this.bundleDirectives = new DataAsync(this.load('bundleDirectives'));
        };
        this.pubsub.on(DIRECTIVE_KEEP_METHOD, clear, this, { synchronous: true });
        this.pubsub.on(DIRECTIVE_REMOVE_METHOD, clear, this, { synchronous: true });
        this.pubsub.on(DIRECTIVE_DELETE, clear, this, { synchronous: true });
    }

};
