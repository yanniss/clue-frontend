import 'core/bindings/numberFormatted';
import Base from 'core/classes/vm/Base.vm';
import Api from 'app/classes/Api.static';
import ko from 'knockout';
import PromiseQueue from 'core/classes/PromiseQueue';

/**
 *
 */
export default class ProguardManager extends Base {

    /**
     * @public
     * @param {HTMLElement} element
     * @param {Bundle} bundle
     */
    constructor(element, { bundle }) {

        super(element);

        this.rules = ko.observableArray();
        this.activeRule = ko.observable();
        this.matches = ko.observableArray();

        Api.fetchProguardRules(bundle.id)
            .then(this.rules);

        const requests = new PromiseQueue();
        this.subscribe(this.activeRule, rule => {
            requests.push(Api.fetchProguardRuleMatches(bundle.id), this.matches, () => this.matches([]));
        });
    }

};
