import './ProguardManager.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './ProguardManager.vm';
import view from './ProguardManager.view.html';

koh.registerComponent('component-proguard-manager', vm, view);
