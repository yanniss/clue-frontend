import ko from 'knockout';

/**
 *
 */
export default class UserOptimizationDirectives {

    constructor() {

        this.directives       = {};
        this.directiveAdded   = ko.observable();
        this.directiveRemoved = ko.observable();
    }

    add(directive) {

        this.directives[directive.doopId] = directive;
        this.directiveAdded(directive);
    }

    remove(directive) {

        delete this.directives[directive.doopId];
        this.directiveRemoved(directive);
    }

    getAllSorted() {

        return Object.values(this.directives).sort((a,b) => a.doopId.localeCompare(b.doopId));
    }

};
