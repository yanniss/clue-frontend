import $ from 'jquery';
import Api from '../../../../classes/Api.static';
import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';

const options = [
    {
        option: 'final:remove',
        text: 'Final: Remove',
    },
    {
        option: 'analysis:keep',
        text: 'Analysis: Keep',
    },
    {
        option: 'analysis:remove',
        text: 'Analysis: Remove',
    },
    {
        option: 'user:keep',
        text: 'User: Keep',
    },
    {
        option: 'user:remove',
        text: 'User: Remove',
    },
];

const AN_KEEP   = 0b0001;
const AN_REMOVE = 0b0010;
const U_KEEP    = 0b0100;
const U_REMOVE  = 0b1000;

/**
 *
 */
export default class OptimizationDirectiveList extends Base {

    constructor(element, {
        project,
        userOptimizationDirectives,
        analysis,
    }) {

        super(element);

        this.project = project;
        this.analysis = analysis;

        this.options = ko.observableArray(options);
        this.selectedOption = ko.observable(options[0]);
        this.directives = ko.observableArray();

        this.m = {};

        this._analysisKeeps = null;
        this._analysisRemoves = null;
        this._userKeeps = null;
        this._userRemoves = null;
        this._finalRemoves = null;

        this.pending = ko.observable(true);
        Promise.all([ this._fetchAnalysisDirectives('KEEP'), this._fetchAnalysisDirectives('REMOVE') ])
            .then(([ analysisKeeps, analysisRemoves ]) => {
                this._analysisKeeps = analysisKeeps.sort(directiveComparator);
                this._analysisRemoves = analysisRemoves.sort(directiveComparator);
                this._setUserDirectives(userOptimizationDirectives);
                this._setFinalDirectives();

                const m = this.m;
                this._analysisKeeps.forEach(({ doopId }) =>   { if(!m[doopId])m[doopId]=0; m[doopId]|=AN_KEEP; });
                this._analysisRemoves.forEach(({ doopId }) => { if(!m[doopId])m[doopId]=0; m[doopId]|=AN_REMOVE; });
                this._userKeeps.forEach(({ doopId }) =>       { if(!m[doopId])m[doopId]=0; m[doopId]|=U_KEEP; });
                this._userRemoves.forEach(({ doopId }) =>     { if(!m[doopId])m[doopId]=0; m[doopId]|=U_REMOVE; });

                this._subscribeToUserDirectiveChange(userOptimizationDirectives);
                this._subscribeToSelectedOptionChange();

                this._showDirectives();

                this.pending(false);
            });
    }

    dispose() {

        super.dispose();
    }

    openSymbol = (directive, event) => {

        this.notify('request open file', {
            file: directive.element.sourceFileName,
            line: directive.element.position.startLine,
            column: directive.element.position.startColumn,
            triggerPosition: true,
        });
    };

    openMenu = (directive, event) => {

        this.notify('open optimization directive menu', {
            element: directive.element,
            event: event,
        });
    };

    _setUserDirectives(userDirectives) {

        const directivesMap = userDirectives.directives,
            keeps = [],
            removes = [];

        Object.keys(directivesMap).forEach(property => {
            const directive = directivesMap[property];
            if(directive.typeOfDirective === 'KEEP') {
                directive.__viewable = getViewableForm(directive.element);
                keeps.push(directive);
            }
            else if(directive.typeOfDirective === 'REMOVE') {
                directive.__viewable = getViewableForm(directive.element);
                removes.push(directive);
            }
        });

        this._userKeeps = keeps.sort(directiveComparator);
        this._userRemoves = removes.sort(directiveComparator);
    }

    _setFinalDirectives() {

        const analysisRemoves = this._analysisRemoves;

        // Calculate: "analysis removes" - "user keeps"
        const userKeeps = this._userKeeps;
        const userKeepsMap = userKeeps.reduce((acc,v) => { acc[v.doopId] = v; return acc; }, {});
        const removes1 = analysisRemoves.filter(v => !userKeepsMap[v.doopId]);

        // Calculate: ("analysis removes" - "user keeps") + "user removes"
        const userRemoves = this._userRemoves;
        const analysisRemovesMap = analysisRemoves.reduce((acc,v) => { acc[v.doopId] = v; return acc; }, {});
        const removes2 = userRemoves.filter(v => !analysisRemovesMap[v.doopId]);

        this._finalRemoves = removes1.concat(removes2).sort(directiveComparator);
    }

    _subscribeToUserDirectiveChange(userDirectives) {

        this.subscribe(userDirectives.directiveAdded, directive => {
            const doopId = directive.doopId;
            const type = directive.typeOfDirective;
            const userKeeps = this._userKeeps;
            const userRemoves = this._userRemoves;

            if(type === 'KEEP') {
                let index = userRemoves.findIndex(directive => directive.doopId === doopId);
                if(index !== -1) {
                    userRemoves.splice(index, 1);

                    const $e = getUD(doopId); $e.removeClass('remove');
                }

                const found = userKeeps.some(directive => directive.doopId === doopId);
                if(!found) {
                    directive.__viewable = getViewableForm(directive.element);
                    insertIntoSorted(userKeeps, directive, directiveComparator);

                    const $e = getUD(doopId); $e.addClass('keep');
                }

                const m = this.m; if(!m[doopId])m[doopId]=0; m[doopId]=(m[doopId]&(AN_KEEP|AN_REMOVE))|U_KEEP;
            }
            else if(type === 'REMOVE') {
                let index = userKeeps.findIndex(directive => directive.doopId === doopId);
                if(index !== -1) {
                    userKeeps.splice(index, 1);

                    const $e = getUD(doopId); $e.removeClass('keep');
                }

                const found = userRemoves.some(directive => directive.doopId === doopId);
                if(!found) {
                    directive.__viewable = getViewableForm(directive.element);
                    insertIntoSorted(userRemoves, directive, directiveComparator);

                    const $e = getUD(doopId); $e.addClass('remove');
                }

                const m = this.m; if(!m[doopId])m[doopId]=0; m[doopId]=(m[doopId]&(AN_KEEP|AN_REMOVE))|U_REMOVE;
            }


            {
                const flags = this.m[doopId] || 0;
                if((flags&AN_REMOVE) !== 0 && (flags&U_KEEP) === 0 || (flags&U_REMOVE) !== 0) {
                    const found = this._finalRemoves.some(d => d.doopId === doopId);
                    if(!found) insertIntoSorted(this._finalRemoves, directive, directiveComparator);
                }
                else {
                    const index = this._finalRemoves.findIndex(d => d.doopId === doopId);
                    if(index !== -1) this._finalRemoves.splice(index, 1);
                }
            }


            this._showDirectives();
        });

        this.subscribe(userDirectives.directiveRemoved, directive => {
            const doopId = directive.doopId;
            const kind = directive.typeOfDirective === 'KEEP' && '_userKeeps' || directive.typeOfDirective === 'REMOVE' && '_userRemoves';
            const directives = this[kind];

            let index = directives.findIndex(directive => directive.doopId === doopId);
            if(index !== -1) {
                directives.splice(index, 1);

                const $e = getUD(doopId); $e.removeClass('keep'); $e.removeClass('remove');
            }

            const m = this.m; if(!m[doopId])m[doopId]=0; m[doopId]&=(AN_KEEP|AN_REMOVE);


            {
                const flags = this.m[doopId] || 0;
                if((flags&AN_REMOVE) !== 0 && (flags&U_KEEP) === 0 || (flags&U_REMOVE) !== 0) {
                    const found = this._finalRemoves.some(d => d.doopId === doopId);
                    if(!found) insertIntoSorted(this._finalRemoves, directive, directiveComparator);
                }
                else {
                    const index = this._finalRemoves.findIndex(d => d.doopId === doopId);
                    if(index !== -1) this._finalRemoves.splice(index, 1);
                }
            }


            this._showDirectives();
        });
    }

    _subscribeToSelectedOptionChange() {

        this.subscribe(this.selectedOption, option => {
            this._showDirectives(option);
        });
    }

    _showDirectives({ option } = this.selectedOption()) {

        switch(option) {
            case 'analysis:keep':
                this.directives(this._analysisKeeps);
                break;
            case 'analysis:remove':
                this.directives(this._analysisRemoves);
                break;
            case 'user:keep':
                this.directives(this._userKeeps);
                break;
            case 'user:remove':
                this.directives(this._userRemoves);
                break;
            case 'final:remove':
                this.directives(this._finalRemoves);
                break;
            default:
                throw new Error('OptimizationDirectiveList._subscribeToSelectedOptionChange');
        }
    }

    _fetchAnalysisDirectives(typeOfDirective) {

        return Api.getOptimizationDirectives(
            this.project.id, this.analysis.id, 'ANALYSIS', typeOfDirective, 0, 10000
        ).then(results => results.results.map(result => ({
            ...result.directives.generatedByAnalysis,
            element: result.element,
            __viewable: getViewableForm(result.element),
        }))).catch(() => []);
    }

    is_a_keep = doopId => {
        return (this.m[doopId] & AN_KEEP) !== 0
    };
    is_a_remove = doopId => {
        return (this.m[doopId] & AN_REMOVE) !== 0
    };
    is_u_keep = doopId => {
        return (this.m[doopId] & U_KEEP) !== 0
    };
    is_u_remove = doopId => {
        return (this.m[doopId] & U_REMOVE) !== 0
    };

};

function directiveComparator(a, b) {

    a = a.element;
    b = b.element;

    const result = a.declaringClassDoopId.localeCompare(b.declaringClassDoopId);
    return result ? result : a.name.localeCompare(b.name);
}

function insertIntoSorted(array, element, comparator) {

    const index = array.findIndex(e => comparator(e, element) >= 0);
    array.splice(index, 0, element);
}

const regex = /[<>]/g;
const replacer = c => c === '<' && '&lt;' || c === '>' && '&gt' || c;
function getViewableForm(method) {

    let name = method.name.replace(regex, replacer);
    return `<span class="text-muted">${simplifyType(method.declaringClassDoopId)}</span>`
        + `.<b>${name}</b>`
        + ` (${method.paramTypes.map(type => simplifyType(type)).join(', ')}): `
        + simplifyType(method.returnType);
}

function simplifyType(type) {

    if(type.length < 5) {
        return type;
    }
    else {
        let parts = type.split('.');
        parts = parts[parts.length - 1].split('$');
        return parts[parts.length - 1];
    }
}

function getUD(doopId) {

    return $(document.getElementById(`odl:${doopId}`)).find('.u-d');
}
