import './OptimizationDirectiveList.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './OptimizationDirectiveList.vm';
import view from './OptimizationDirectiveList.view.html';

koh.registerComponent('optimization-directive-list', vm, view);
