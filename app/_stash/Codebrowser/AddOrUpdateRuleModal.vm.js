import 'app/components/FormOption/FormOption.component';
import $ from 'jquery';
import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';

/**
 *
 */
export default class AddOrUpdateRuleModal extends Base {

    $modal = $(this.element).find('.modal');
    $form;
    $submit;
    visible;//observable
    addOrUpdateRule;//function
    ruleId;//string
    title = ko.observable();
    ruleBody = ko.observable();

    /**
     * @param {HTMLElement} element
     */
    constructor(element, { visible, addOrUpdateRule, activeRule }) {

        super(element);

        this.visible = visible;
        this.addOrUpdateRule = addOrUpdateRule;

        this.subscribe(this.visible, (isVisible) => {
            console.log("Modal visible=", isVisible);
            if (isVisible) {

                const ruleObj = ko.unwrap(activeRule);
                console.log(ruleObj);
                if (ruleObj === null) {
                    this.ruleId = null;
                    this.ruleBody(null);
                    this.title("Add rule");
                }
                else {
                    this.ruleId = ruleObj.id;
                    this.ruleBody(ruleObj.ruleBody);
                    this.title("Edit rule");
                }

                this.show();
            }
        });


        this.$modal.on('hidden.bs.modal', () => {
            this.visible(false);
        });

    }

    dispose() {

        this.$modal.off();
        super.dispose();
    }

    /**
     * @public
     */
    show() {

        this.$form = this.$modal.find('form');
        this.$submit = this.$form.find('button[type=submit]');
        this.$modal.modal('show');
    }

    /**
     * @private
     */
    submit() {

        this.$submit.button('loading');
        this.addOrUpdateRule(this.ruleId, this.ruleBody()).then(() => {
            this.$modal.modal('hide');
            this.visible(false);
        }).finally(() => {
            this.$submit.button('reset');
        });
    }

};
