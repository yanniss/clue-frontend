import './Project.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './Project.vm';
import view from './Project.view.html';

koh.registerComponent('component-project', vm, view);
