import 'app/bindings/boxrated';
import 'app/bindings/starated';
import 'app/components/Navbar/Navbar.component';
import 'app/components/ProjectDeletion/ProjectDeletion.component';
import 'app/components/SearchBox/SearchBox.component';
import 'app/vendor/simplePagination/simplePagination.css';
import 'app/vendor/simplePagination/jquery.simplePagination';
import $ from 'jquery';
import Api from 'app/classes/Api.static';
import Paginated from 'core/classes/vm/Paginated.vm';
import ko from 'knockout';
import navbar from './Project.view.navbar.html';

const searchTextThrottle = 400;
const sortByOptions = [ // first entry is also the default one
    {
        type:    'option',
        id:      '+name',
        label:   'Name (a-z)',
        icon:    '',
        request: 'lowercase_id|ASC',
        allow:   'ALL'
    },
    {
        type:    'option',
        id:      '-name',
        label:   'Name (z-a)',
        icon:    '',
        request: 'lowercase_id|DESC',
        allow:   'ALL'
    },
    {
        type:    'option',
        id:      '-rating',
        label:   'JCenter rating',
        icon:    'fa fa-signal fa-flip-horizontal',
        request: 'rating|DESC',
        allow:   'ALL'
    },
    {
        type:    'option',
        id:      '+rating',
        label:   'JCenter rating',
        icon:    'fa fa-signal',
        request: 'rating|ASC',
        allow:   'ALL'
    }
    // { id: 'size',   label: 'Size',   request: 'size|ASC' } // size not supported yet
];

/**
 *
 */
export default class Project extends Paginated {

    navbarContent = navbar;
    $paginator = $('.paginator');
    openDeleteProjectModal = ko.observable();
    totalItemsNoSearch = ko.observable();

    /**
     *
     * @param element
     * @param _query
     */
    constructor(element, { _query }) {

        super(element, [], _query, {
            itemsPerPage: 2,
            fnFetchItems: ProjectList._fetchItems,
            sortBy: sortByOptions[0],
            sortByOptions: sortByOptions,
        });

        Api.fetchBundles({ _start: 0, _count: 0, _sort: sortByOptions[0].request, text: '' }, true)
            .then(response => response.hits)
            .then(this.totalItemsNoSearch);

        Api.fetchBundles({ _start: 0, _count: 100, _sort: sortByOptions[0].request, text: '' }, true)
            .then(response => response.projects)
            .then(this.items);

        this._initPaginator();
        this._setPaginatorUpdating();
    }

    openProjectInEditor = (project, event) => {

        location.hash = `/projects/${project.id}/codebrowser`;
    };

    requiresPagination() {

        return this.itemsPerPage() < this.totalItems();
    }

    /**
     *
     * @param project
     * @returns {number}    {1,2,3,4,5}
     */
    getCoverage(project) {

        const p = project.metrics.appMethodsPercent;

        if(p < 0.1) {
            return 1;
        }
        else if(p < 0.2) {
            return 2;
        }
        else if(p < 0.3) {
            return 3;
        }
        else if(p < 0.4) {
            return 4;
        }
        else {
            return 5;
        }
    }

    sortOptionClicked = (sortBy, event) => {

        if(sortBy !== this.sortBy.peek()) {
            this.sortBy(sortBy);
        }
    };

    filterKeypress(self, event) {

        return /[a-zA-Z0-9 ]/.test(event.key);
    }

    formatNumber(number) {

        /* Referring to "Intl", instead of "window['Intl']" should be enough, but Safari 7.1 crashes/mishandles the
         * former notation. To be checked; also check later versions of Safari, if 7.1 is not in our supported browsers
         * list.
         */
        return window['Intl'] && Intl['NumberFormat']
            ? new Intl.NumberFormat('en').format(number)
            : number.toLocaleString();
    }

    getCodeBrowserUrlForProject(project) {

        return `#/projects/${encodeURIComponent(project.id)}/codebrowser`;
    }

    getProjectTitle(project) {

        return 'Application artifacts:\n' + project.appArtifacts.map(input => '\n- ' + input.name).join('')
            + '\n\n'
            + 'Dependency artifacts:\n' + project.depArtifacts.map(input => '\n- ' + input.name ).join('');
    }

    getJoinedArtifactNames(artifacts) {

        return artifacts.map(artifact => artifact.name).join(', ');
    }

    reloadItems = () => {

        ProjectList._fetchItems(this.query(), this.sortBy(), this.orderBy(), this.itemsPerPage(), this.page());
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    static _fetchItems(q, s, o, c, p) {

        const searchWords = q.match(/([\S]+)/g),
            params = {
                _start: (p - 1) * c,
                _count: c,
                _sort:  s.request,
                text:   searchWords ? searchWords.map(word => word + '*').join(' ') : ''
            };

        return Api.fetchBundles(params, true)
            .then(response => ({
                items: response.projects,
                hits: response.hits,
            }));
    }

    _initPaginator() {

        this.$paginator.pagination({
            items:       this.totalItems.peek(),
            itemsOnPage: this.itemsPerPage.peek(),
            currentPage: this.page.peek(),
            cssStyle:    'light-theme',
            onPageClick: (page, event) => {
                this.page(page);
                return false;
            }
        });
    }

    _setPaginatorUpdating() {

        this.subscribe(this.itemsPerPage, itemsPerPage => {
            // "itemsPerPage" must be filtered
            this.$paginator.pagination('updateItemsOnPage', itemsPerPage);
        });

        this.subscribe(this.totalItems, totalItems => {
            // "totalItems" must be filtered
            this.$paginator.pagination('updateItems', totalItems);
        });

        this.subscribe(this.page, page => {
            // "page" must be filtered
            this.$paginator.pagination('drawPage', page);
        });
    }

};
