import ajax from './base/ajax';
import { bundles as pathnameBundles } from './base/pathnames';
import encodeParamsAsQueryString from './base/encodeParamsAsQueryString';
import Configuration from 'codebrowser/classes/Configuration';
import Analysis from 'codebrowser/classes/Analysis';
import Directive from 'codebrowser/classes/Directive';

export const addOptimizationDirective = (bundleId, $form) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/directives`;

    return ajax.formPost(uri, $form);
};

export const editOptimizationDirective = (bundleId, directiveId, typeOfDirective) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/directives/${ENC(directiveId)}`
        + `?typeOfDirective=${ENC(typeOfDirective)}`;

    return ajax.jsonPut(uri);
};

export const deleteDirective = (bundleId, directiveId) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/directives/${ENC(directiveId)}`;

    return ajax.jsonDelete(uri);
};

export const cloneDirectivesOfConfiguration = (bundleId, configuration, params) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/sets/${configuration.name}`;

    //(configuration instanceof Configuration)
    let part = ``;

    let queryStr = encodeParamsAsQueryString(params);

    return ajax.jsonGet(`${makeBundleUrl(bundleId)}/${part}?action=CLONE_FILTERED&${queryStr}`)
        .then(results => new Configuration(results));
};

static mergeDirectivesOfConfigurationTo(bundleId, source, params, targetName) {

    //(configuration instanceof Configuration)
    let part = `sets/${source.name}`;

    let queryStr = Api.encodeParamsAsQueryString(params);

    return ajax.jsonGet(`${makeBundleUrl(bundleId)}/${part}?action=MERGE_FILTERED&target=${encodeURIComponent(targetName)}&${queryStr}`);
}

static fetchDirectives(bundleId, {
    source,
    page,
    resultsPerPage = 15,
    sortBy = 'doopId|ASC',
    facetOptions = {},
    facets = false,
    textFilters = {},
}) {

    const params = {
        _start: (page - 1) * resultsPerPage,
        _count: resultsPerPage,
        _sort: sortBy,
        _facets: facets,
        ...facetOptions,
        ...textFilters,
    };

    if(source instanceof Configuration) {
        params.set = source.name;
    }
    else if(source instanceof Analysis) {
        params.analysis = source.id;
    }
    else {
        throw new Error(`Api.fetchDirectives: source must be either a Configuration or an Analysis: ${source}`);
    }

    const url = `${makeBundleUrl(bundleId)}/directives`;

    return ajax.jsonGet(url, params)
        .then(results => {
            const directives = results.results.map(result => new Directive(result));
            return { hits: results.hits, facets: results.facets, directives: directives };
        });
}

static deleteDirectives(bundleId, configurationName, params) {

    const url = `${makeBundleUrl(bundleId)}/sets/${encodeURIComponent(configurationName)}`;

    return ajax.jsonGet(url, { ...params, action: 'DELETE_FILTERED' });
}

static getOptimizationDirectives(projectId, analysisId, origin, typeOfDirective, start, count) {
    let query = '';

    if(analysisId) {
        query += `${query ? '&' : '?'}analysis=${encodeURIComponent(analysisId)}`;
    }
    if(origin) {
        query += `${query ? '&' : '?'}origin=${encodeURIComponent(origin)}`;
    }
    if(typeOfDirective) {
        query += `${query ? '&' : '?'}typeOfDirective=${encodeURIComponent(typeOfDirective)}`;
    }
    if(start || start === 0) {
        query += `${query ? '&' : '?'}_start=${encodeURIComponent(start)}`;
    }
    if(count || count === 0) {
        query += `${query ? '&' : '?'}_count=${encodeURIComponent(count)}`;
    }

    return ajax.jsonGet(`${makeBundleUrl(projectId)}/directives${query}`).then(results => {
        return results;
    });
}




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// private stuff
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const ENC = encodeURIComponent;
