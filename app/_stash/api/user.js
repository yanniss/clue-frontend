import ajax from './base/ajax';
import { auth as pathnameAuth, users as pathnameUsers } from './base/pathnames';
import tokenName from './base/token';
import Cookies from 'js-cookie';

export const createUser = $form => {

    return ajax.formPost(pathnameUsers, $form)
        .then(data => data.id);
};

export const deleteUser = userId => {

    const uri = `${pathnameUsers}/${ENC(userId)}`;

    return ajax.jsonDelete(uri);
};

export const fetchUsers = () => {

    return ajax.jsonGet(pathnameUsers)
        .then(data => data.list);
};

// todo: rename to "fetchCurrentUserInfo"
export const fetchUserInfo = () => {

    return wrapPromiseToUpdateCustomHeaderOption(
        ajax.jsonGet(pathnameAuth)
    );
};

export const updateUserPassword = (userId, oldPassword, newPassword) => {

    const uri = `${pathnameUsers}/${ENC(userId)}/password`;
    const params = {
        oldPassword: oldPassword,
        newPassword: newPassword
    };

    return ajax.jsonPut(uri, params);
};

export const login = (username, password) => {

    const params = {
        username: username,
        password: password,
    };

    return wrapPromiseToUpdateCustomHeaderOption(
        ajax.jsonPost(pathnameAuth, params)
    );
};

export const logout = () => {

    return wrapPromiseToUpdateCustomHeaderOption(
        ajax.jsonDelete(pathnameAuth)
    );
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// private stuff
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const ENC = encodeURIComponent;

const wrapPromiseToUpdateCustomHeaderOption = promise => {

    return promise
        .then((...args) => {
            ajax.options.headers[tokenName] = Cookies.get('SESSION_ID');
            return args;
        })
        .catch((...args) => {
            ajax.options.headers[tokenName] = Cookies.get('SESSION_ID');
            throw args;
        });
};
