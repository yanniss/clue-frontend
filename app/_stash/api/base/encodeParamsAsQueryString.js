
export default params => {

    return Object.entries(params)
        .map(([key, value]) => {
            if(!value) {
                return null;
            }
            else if(Array.isArray(value)) {
                value.map(v => `${key}=${encodeURIComponent(v)}`).join('&');
            }
            else {
                return `${key}=${encodeURIComponent(value)}`;
            }
        })
        .filter(s => s != null)
        .join('&');
};
