import $ from 'jquery';
import Analysis from 'codebrowser/classes/Analysis';
import Bundle from 'app/classes/Bundle';
import Directive from 'codebrowser/classes/Directive';
import Rule from 'codebrowser/classes/Rule';
import Project from 'app/classes/Project';
import Configuration from 'codebrowser/classes/Configuration';
import ajax from 'app/api/base/ajax';

// todo: fetch*Info ... only one filter applies for possibly many returned values ...



function makeProjectUrl(id) {
    return paths.projects + (id ? '/' + encodeURIComponent(id) : '');
}
function makeBundleUrl(id) {
    return paths.bundles + (id ? '/' + encodeURIComponent(id) : '');
}

function makeAnalysisUrl(projectId, analysisId) {
    let lastPart = analysisId ? `/${encodeURIComponent(analysisId)}` : '';
    return `${makeBundleUrl(projectId)}/analyses${lastPart}`;
}

/**
 *
 */
export default class Api {

    static dispose() {
        subscription.dispose();
    }

    static getDownloadOptimizedApkUrl(projectId, analysisId) {
        return `${makeBundleUrl(projectId)}/optimize`
            + (analysisId ? `?analysis=${encodeURIComponent(analysisId)}` : '');
    }


    static fetchSymbolsThatStartWith(projectId, text, symbolTypes) {
        return ajax.jsonPost(`${makeBundleUrl(projectId)}/symbols/startWith`, {
            prefix: text,
            types:  symbolTypes,
        });
    }

    static encodeParamsAsQueryString(params) {
        return Object.entries(params).map(([key, value]) => {
            if (value) {
                if (Array.isArray(value)) {
                    value.map( v => {
                        return `${key}=${encodeURIComponent(v)}`;
                    }).join('&');
                }
                else {
                    return `${key}=${encodeURIComponent(value)}`;
                }
            }
            else {
                return null;
            }
        }).filter( s => {
            return (s != null);
        }).join('&');
    }

    static downloadFilteredDirectivesFile(bundleId, configuration, params) {

        //console.log(bundleId, configuration, params);

        //(configuration instanceof Configuration)
        let part = `sets/${configuration.name}`;

        let queryStr = Api.encodeParamsAsQueryString(params);

        window.location = `${makeBundleUrl(bundleId)}/${part}?action=EXPORT_FILTERED&${queryStr}`;
    }




    static fetchProguardRules(bundleId) {

        return ajax.jsonGet(`${makeBundleUrl(bundleId)}/proguard/report`)
            .then(results => results.results);
    }

    static fetchProguardRuleMatches(bundleId, ruleId) {

        return ajax.jsonGet(`${makeBundleUrl(bundleId)}/proguard/expand`)
            .then(results => results.results);
    }

    //////////////////////////////////////////////////////////////////
    // Files, packages & hierarchies of those
    //////////////////////////////////////////////////////////////////

    static fetchDatastoreQuery(analysisId, query) {
        return ajax.jsonPost(`${paths.analyses}/${encodeURIComponent(analysisId)}/query/datastore`, query);
    }

    static fetchArtifacts(projectId) {
        return ajax.jsonGet(`${makeBundleUrl(projectId)}/artifacts`);
    }

    /**
     * todo: update documentation
     * Fetches information on the requested file or directory
     *
     * @param projectId
     * @param artifactId
     * @param path
     *      "/"
     *      in "path" starting slash is optional
     * @param analysisId
     * @param options
     * @returns {*}
     */
    static fetchFile(projectId, artifactId, path, options) {
        let url = makeBundleUrl(projectId)
                + (artifactId ? `/artifacts/${encodeURIComponent(artifactId)}` : '')
                + `/files/${encodeURIComponent(path || '')}`,
            queryString = '';

        if(options) {
            if(options.classes) {
                queryString += `${queryString ? '&' : '?'}classes=`;
            }
            if(options.fields) {
                queryString += `${queryString ? '&' : '?'}fields=`;
            }
            if(options.methods) {
                queryString += `${queryString ? '&' : '?'}methods=`;
            }
            if(!options.content) {
                queryString += `${queryString ? '&' : '?'}noContent=`;
            }
            if(options.set) {
                queryString += `${queryString ? '&' : '?'}set=${encodeURIComponent(options.set)}`;
            }
            if(options.analysis) {
                queryString += `${queryString ? '&' : '?'}analysis=${encodeURIComponent(options.analysis)}`;
            }
        }

        return ajax.jsonGet(url + queryString).then(results => results);
    }

    static fetchPackage(projectId, context, packge, analysisId) {
        let url = makeBundleUrl(projectId)
                + (context ? `/contexts/${encodeURIComponent(context)}` : '')
                + `/packages/${encodeURIComponent(packge)}`,
            queryString = '';

        if(analysisId) {
            queryString += `${queryString ? '&' : '?'}analysis=${encodeURIComponent(analysisId)}`;
        }

        return ajax.jsonGet(url + queryString);
    }

    static fetchQueries(projectId, analysisId) {
        let url = makeBundleUrl(projectId)
                + '/queries',
            queryString = analysisId ? `?analysis=${encodeURIComponent(analysisId)}` : '';

        return ajax.jsonGet(url + queryString).then(results => results.results);
    }

    static fetchQuery(projectId, analysisId, queryId) {
        let url = makeBundleUrl(projectId)
                + `/queries/${encodeURIComponent(queryId)}`,
            queryString = analysisId ? `?analysis=${encodeURIComponent(analysisId)}` : '';

        return ajax.jsonGet(url + queryString).then(results => results.results);
    }

    static fetchPackageGraph(projectId, analysisId) {
        let url = makeBundleUrl(projectId)
                + '/packageGraph',
            queryString = analysisId ? `?analysis=${encodeURIComponent(analysisId)}` : '';

        return ajax.jsonGet(url + queryString).then(results => results.list);
    }

    static fetchPackageHierarchy(projectId, analysisId) {
        let url = makeBundleUrl(projectId)
                + '/packageHierarchy',
            queryString = analysisId ? `?analysis=${encodeURIComponent(analysisId)}` : '';

        return ajax.jsonGet(url + queryString).then(results => results.results);
    }

};
