import ko from 'knockout';
import np from 'nprogress';
import Ajax from 'app/classes/Ajax';
import Cookies from 'js-cookie';
import Notifier from 'app/classes/Notifier.static';
import tokenName from './token';

let pendingRequests = 0;

const hasPendingRequests = ko.observable(false);
const subscription = hasPendingRequests.subscribe(value => {
    value ? np.start() : np.done();
});

np.configure({ trickleSpeed: 100 });

function requestInitiated() {
    pendingRequests++;
    hasPendingRequests(true);
}
function requestCompleted() {
    if(--pendingRequests === 0) {
        hasPendingRequests(false);
    }
}

export default new Ajax({
    headers: {
        [tokenName]: Cookies.get('SESSION_ID')
    },
    beforeSend: () => { requestInitiated(); },
    complete: () => { requestCompleted(); },
    error: (jqXhr, textStatus, errorThrown) => {
        if(textStatus === 'error') {
            Notifier.error(errorThrown || 'Cannot connect to the server');
        }
        else if(textStatus === 'abort') {
            Notifier.warning('Aborted');
        }
        else {
            Notifier.error(Ajax.getErrorMessage(jqXhr, errorThrown)); // this.getErrorMessage refers to "Ajax"
        }
    }
});
