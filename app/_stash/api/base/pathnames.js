
const apiEntry = __API_END_POINT__; // Defined through webpack.DefinePlugin

/**
 * Pathname prefixes.
 */

export const auth       = encodeURIComponent(`${apiEntry}/auth/v1/session`);
export const users      = encodeURIComponent(`${apiEntry}/api/v1/users`);
export const projects   = encodeURIComponent(`${apiEntry}/api/v1/projects`);
export const bundles    = encodeURIComponent(`${apiEntry}/api/v1/bundles`);
export const families   = encodeURIComponent(`${apiEntry}/api/v1/families`);
export const options    = encodeURIComponent(`${apiEntry}/api/v1/options`);
export const analyses   = encodeURIComponent(`${apiEntry}/api/v1/analyses`);
