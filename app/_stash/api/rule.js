import ajax from './base/ajax';
import { bundles as pathnameBundles } from './base/pathnames';
import Rule from 'codebrowser/classes/Rule';

export const addRule = (bundleId, configurationName, ruleBody) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/configs/${ENC(configurationName)}`;
    const params = { body: ruleBody };

    return ajax.jsonPost(uri, params)
        .then(results => {
            console.log("Added new rule ", results);
            return results;
        });
};

export const deleteRule = (bundleId, configurationName, ruleId) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/configs/${ENC(configurationName)}/rules/${ENC(ruleId)}`;

    return ajax.jsonDelete(uri)
        .then(results => {
            console.log("Deleted rule ", results);
            return results;
        });
};

export const fetchRules = (bundleId, configurationName) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/configs/${ENC(configurationName)}`;

    return ajax.jsonGet(uri)
        .then( results => {
            console.log(results);
            return results.rules.map(rule => new Rule(rule));
        });
};

export const updateRule = (bundleId, configurationName, ruleId, ruleBody) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/configs/${ENC(configurationName)}/rules/${ENC(ruleId)}`;
    const params = { body: ruleBody };

    return ajax.jsonPut(uri, params)
        .then(results => {
            console.log("Updated rule ", results);
            return results;
        });
};

export const addRuleFromDirective = (bundleId, configurationName, doopId, typeOfDirective) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/configs/${ENC(configurationName)}`;
    const params = {
        doopId: doopId,
        type: typeOfDirective
    };

    return ajax.jsonPost(uri, params);
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// private stuff
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const ENC = encodeURIComponent;
