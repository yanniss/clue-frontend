import ajax from './base/ajax';
import { bundles as pathnameBundles } from './base/pathnames';

export const fetchProgramElement = (bundleId, analysisId, filepath, line, column, noValues) => {

    let queryString = '';

    if(analysisId) {
        queryString += `${queryString ? '&' : '?'}analysis=${ENC(analysisId)}`;
    }
    if(noValues) {
        queryString += `${queryString ? '&' : '?'}noExpansion=`;
    }

    const uri = `${pathnameBundles}/${ENC(bundleId)}/symbols/${ENC(filepath)}/${ENC(line)}/${ENC(column)}`
        + queryString;

    return ajax.jsonGet(uri)
        .then(results => results.symbols);
};

export const fetchTypeInfo = (bundleId, analysisId, classId, options) => {

    let queryString = '',
        filter = results => results;

    if(analysisId) {
        queryString += `${queryString ? '&' : '?'}analysis=${ENC(analysisId)}`;
    }
    if(options) {
        if(!options.self) {
            queryString += `${queryString ? '&' : '?'}noDef=`;
        }
        if(options.allocatedAt) {
            queryString += `${queryString ? '&' : '?'}allocationsOfType=`;
            filter = results => results.allocationsOfType;
        }
        if(options.superTypes) {
            queryString += `${queryString ? '&' : '?'}hierarchy=superTypes`;
            filter = results => Object.keys(results.hierarchy).reduce(
                (p1, p2) => results.hierarchy[p1].concat(results.hierarchy[p2])
            );
        }
        if(options.subTypes) {
            queryString += `${queryString ? '&' : '?'}hierarchy=subTypes`;
            filter = results => Object.keys(results.hierarchy).reduce(
                (p1, p2) => results.hierarchy[p1].concat(results.hierarchy[p2])
            );
        }
        if(options.fields) {
            queryString += `${queryString ? '&' : '?'}fields=`;
            filter = results => results.fields;
        }
        if(options.fieldsOfType) {
            queryString += `${queryString ? '&' : '?'}fieldsOfType=`;
            filter = results => results.fieldsOfType;
        }
        if(options.varsOfType) {
            queryString += `${queryString ? '&' : '?'}varsOfType=`;
            filter = results => results.varsOfType;
        }
    }

    const uri = `${pathnameBundles}/${ENC(bundleId)}/classes/${ENC(classId)}`
        + queryString;

    return ajax.jsonGet(uri)
        .then(filter);
};

export const fetchFieldInfo = (bundleId, analysisId, fieldId, options) => {

    let queryString = '',
        filter = results => results;

    if(analysisId) {
        queryString += `${queryString ? '&' : '?'}analysis=${ENC(analysisId)}`;
    }
    if(options) {
        if(!options.self) {
            queryString += `${queryString ? '&' : '?'}noDef=`;
        }
        if(options.shadowedBy) {
            queryString += `${queryString ? '&' : '?'}shadowedBy=`;
            filter = results => results.shadowedBy;
        }
        if(options.readAccesses) {
            queryString += `${queryString ? '&' : '?'}accessedBy=R`;
            filter = results => results.accessedBy;
        }
        if(options.writeAccesses) {
            // 3.b) baseValue=<baseValue> -- optional
            queryString += `${queryString ? '&' : '?'}accessedBy=W`;
            filter = results => results.accessedBy;
        }
        if(options.values) {
            queryString += `${queryString ? '&' : '?'}values=`;
            filter = results => results.values;
            if(options.baseValue) {
                queryString += `${queryString ? '&' : '?'}baseValue=${options.baseValue}`;
            }
        }
    }

    const uri = `${pathnameBundles}/${ENC(bundleId)}/fields/${ENC(fieldId)}`
        + queryString;

    return ajax.jsonGet(uri)
        .then(filter);
};

export const fetchMethodInfo = (bundleId, analysisId, methodId, options) => {

    let queryString = '',
        filter = results => results;

    if(analysisId) {
        queryString += `${queryString ? '&' : '?'}analysis=${ENC(analysisId)}`;
    }
    if(options) {
        if(!options.self) {
            queryString += `${queryString ? '&' : '?'}noDef=`;
        }
        if(options.overrides) {
            queryString += `${queryString ? '&' : '?'}hierarchy=superTypes`;
            filter = results => results.hierarchy;
        }
        if(options.overriddenBy) {
            queryString += `${queryString ? '&' : '?'}hierarchy=subTypes`;
            filter = results => results.hierarchy;
        }
        if(options.invokedBy) {
            queryString += `${queryString ? '&' : '?'}reverseInvocations=`;
            filter = results => results.reverseInvocations;
        }
        if(options.invokedByNaive) {
            queryString += `${queryString ? '&' : '?'}reverseChaInvocations=`;
            filter = results => results.reverseChaInvocations;
        }
        if(options.thisValues) {
            queryString += `${queryString ? '&' : '?'}this=`;
            filter = results => results.this;
        }
        if(options.returnValues) {
            queryString += `${queryString ? '&' : '?'}returnValues=`;
            filter = results => results.returnValues;
        }
    }

    const uri = `${pathnameBundles}/${ENC(bundleId)}/methods/${ENC(methodId)}`
        + queryString;

    return ajax.jsonGet(uri)
        .then(filter);
};

export const fetchVariableInfo = (bundleId, analysisId, variableId, options) => {

    let queryString = '',
        filter = results => results;

    if(analysisId) {
        queryString += `${queryString ? '&' : '?'}analysis=${ENC(analysisId)}`;
    }
    if(options) {
        if(!options.self) {
            queryString += `${queryString ? '&' : '?'}noDef=`;
        }
        if(options.readAccesses) {
            queryString += `${queryString ? '&' : '?'}accessedBy=R`;
            filter = results => results.accessedBy;
        }
        if(options.writeAccesses) {
            queryString += `${queryString ? '&' : '?'}accessedBy=W`;
            filter = results => results.accessedBy;
        }
        if(options.values) {
            queryString += `${queryString ? '&' : '?'}values=`;
            if(options.line) {
                queryString += `${queryString ? '&' : '?'}line=${options.line}`;
            }
            filter = results => results.values;
        }
    }

    const uri = `${pathnameBundles}/${ENC(bundleId)}/vars/${ENC(variableId)}`
        + queryString;

    return ajax.jsonGet(uri)
        .then(filter);
};

export const fetchMethodInvocationInfo = (bundleId, analysisId, methodId, options) => {

    let queryString = '',
        filter = results => results;

    if(analysisId) {
        queryString += `${queryString ? '&' : '?'}analysis=${ENC(analysisId)}`;
    }
    if(options) {
        if(!options.self) {
            queryString += `${queryString ? '&' : '?'}noDef=`;
        }
        if(options.methods) {
            queryString += `${queryString ? '&' : '?'}values=`;
            filter = results => results.values;
        }
        if(options.methodsNaive) {
            queryString += `${queryString ? '&' : '?'}cha=`;
            filter = results => results.cha;
        }
    }

    const uri = `${pathnameBundles}/${ENC(bundleId)}/invocations/${ENC(methodId)}`
        + queryString;

    return ajax.jsonGet(uri)
        .then(filter);
};

export const fetchValueInfo = (bundleId, analysisId, valueId, options) => {

    let queryString = '',
        filter = results => results;

    if(analysisId) {
        queryString += `${queryString ? '&' : '?'}analysis=${ENC(analysisId)}`;
    }
    if(options) {
        if(!options.self) {
            queryString += `${queryString ? '&' : '?'}noDef=`;
        }
        if(options.fields) {
            queryString += `${queryString ? '&' : '?'}fields=`;
            filter = results => results.fields;
        }
        if(options.arrayValues) {
            queryString += `${queryString ? '&' : '?'}arrayValues=`;
            filter = results => results.arrayValues;
        }
    }

    const uri = `${pathnameBundles}/${ENC(bundleId)}/values/${ENC(valueId)}`
        + queryString;

    return ajax.jsonGet(uri)
        .then(filter);
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// private stuff
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const ENC = encodeURIComponent;
