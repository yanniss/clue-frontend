import ajax from './base/ajax';
import { projects as pathname } from './base/pathnames';
import Project from 'app/classes/Project';

export const createProject = $form => {

    return ajax.formPost(pathname, $form)
        .then(results => new Project(results));
};

export const deleteProject = projectId => {

    const uri = `${pathname}/${ENC(projectId)}`;

    return ajax.jsonDelete(uri);
};

export const fetchProject = projectId => {

    const uri = `${pathname}/${ENC(projectId)}`;

    return ajax.jsonGet(uri)
        .then(results => new Project(results));
};

export const fetchProjects = (params, returnHits = false) => {

    return ajax.jsonGet(pathname, params)
        .then(results => {
            const projects = results.results.map(result => new Project(result));
            return returnHits ? { hits: results.hits, projects: projects } : projects;
        })
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// private stuff
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const ENC = encodeURIComponent;
