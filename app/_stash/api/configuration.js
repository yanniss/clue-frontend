import ajax from './base/ajax';
import { bundles as pathnameBundles } from './base/pathnames';
import Configuration from 'codebrowser/classes/Configuration';
import Analysis from 'codebrowser/classes/Analysis';

export const createConfiguration = (bundleId, configurationName) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/sets`
        + `?name=${ENC(configurationName)}`;

    return ajax.jsonPost(uri)
        .then(results => new Configuration(results));
};

export const deleteConfiguration = (bundleId, configuration) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/sets/${ENC(configuration.name)}`;

    return ajax.jsonDelete(uri);
};

export const fetchConfigurations = bundleId => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/sets`;

    return ajax.jsonGet(uri)
        .then(results => results.results.map(data => new Configuration(data)));
};

export const renameConfiguration = (bundleId, configuration, newName) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/configs/${ENC(configuration.name)}/actions/rename`
        + `?target=${ENC(newName)}`;

    return ajax.jsonGet(uri)
        .then(results => results.nameOfSet);
};

export const cloneConfiguration = (bundleId, configuration) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/configs/${ENC(configuration.name)}/actions/clone`;

    return ajax.jsonGet(uri)
        .then(results => new Configuration(results));
};

export const mergeConfigurationTo = (bundleId, source, target) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/sets/${ENC(source.name)}`
        + `?action=MERGE&target=${ENC(target.name)}`;

    return ajax.jsonGet(uri);
};

export const downloadDirectivesFile = (bundleId, configuration)  => {

    window.location = `${pathnameBundles}/${ENC(bundleId)}/configs/${ENC(configuration.name)}/actions/export`;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// private stuff
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const ENC = encodeURIComponent;
