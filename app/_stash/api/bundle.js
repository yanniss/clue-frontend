import ajax from './base/ajax';
import { bundles as pathnameBundles, options as pathnameOptions } from './base/pathnames';
import Bundle from 'app/classes/Bundle';

export const fetchBundleOptions = () => {

    const uri = pathnameOptions
        + `?what=bundle`;

    return ajax.jsonGet(uri)
        .then(results => results.options);
};

export const createBundle = (projectId, $form) => {

    const uri = pathnameBundles
        + `?family=doop&project=${ENC(projectId)}`;

    return ajax.formPost(uri, $form)
        .then(data => data.id);
};

export const deleteBundle = bundleId => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}`;

    return ajax.jsonDelete(uri);
};

export const fetchBundle = bundleId => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}`;

    return ajax.jsonGet(uri)
        .then(data => new Bundle(data.inputBundle));
};

export const fetchBundles = (projectId, params, returnHits = false) => {

    params = {
        project: projectId,
        ...params,
    };

    return ajax.jsonGet(pathnameBundles, params)
        .then(results => {
            if(!results || !Array.isArray(results.results)) {
                throw new Error('Api.fetchBundles: malformed data');
            }
            const bundles = results.results.map(bundle => new Bundle(bundle));

            return returnHits ? { hits: results.hits, bundles: bundles } : bundles;
        });
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// private stuff
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const ENC = encodeURIComponent;
