import ajax from 'app/api/base/ajax';
import { bundles as pathnameBundles, options as pathnameOptions } from './base/pathnames';
import Analysis from 'codebrowser/classes/Analysis';

export const createAnalysis = (bundleId, $form) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/analyses`;

    return ajax.formPost(uri, $form)
        .then(data => data.id);
};

export const deleteAnalysis = (bundleId, analysisId) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/analyses/${ENC(analysisId)}`;

    return ajax.jsonDelete(uri);
};

export const fetchAnalysis = (bundleId, analysisId, noProgressBar = false) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/analyses/${ENC(analysisId)}`;
    const additionalOptions = noProgressBar ? { beforeSend: null, complete: null } : {};

    return ajax.jsonGet(uri, null, additionalOptions)
        .then(data => new Analysis(bundleId, data.analysis));
};

export const fetchAnalyses = bundleId => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/analyses`;

    return ajax.jsonGet(uri)
        .then(results => results.results
            .map(analysisData => new Analysis(bundleId, analysisData))
            .sort((a, b) => {
                a = a.createdAt.getTime();
                b = b.createdAt.getTime();
                if(b < a) return -1;
                if(b > a) return 1;
                return 0;
            })
        );
};

export const fetchAnalysisRuntime = (bundleId, analysisId) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/analyses/${ENC(analysisId)}/runtime`;
    const additionalOptions = { beforeSend: null, complete: null };

    return ajax.jsonGet(uri, null, additionalOptions);
};

export const executeAnalysisAction = (bundleId, analysisId, action) => {

    const uri = `${pathnameBundles}/${ENC(bundleId)}/analyses/${ENC(analysisId)}/action/${ENC(action)}`;

    return ajax.jsonPut(uri);
};

export const fetchAnalysisOptions = () => {

    const uri = pathnameOptions
        + `?what=analysis`;

    return ajax.jsonGet(uri)
        .then(results => results.options);
};

export const downloadDirectivesFile = (bundleId, analysis) => {

    window.location = `${pathnameBundles}/${ENC(bundleId)}/analyses/${analysis.id}/directives?action=EXPORT`;
};

/*
export const renameConfiguration = (bundleId, configuration, newName) => {

    let url = '';

    if(configuration instanceof Configuration) {
        url = `${makeBundleUrl(bundleId)}/configs/${encodeURIComponent(configuration.name)}/actions/rename`
            + `?target=${encodeURIComponent(newName)}`;
    }
    else if(configuration instanceof Analysis) {
        url = `${makeBundleUrl(bundleId)}/sets/${encodeURIComponent(configuration.name)}`
            + '?action=RENAME'
            + `&target=${encodeURIComponent(newName)}`;
    }

    return ajax.jsonGet(url)
        .then(results => results.nameOfSet);
}

static cloneConfiguration(bundleId, configuration) {

    let url = '';

    if(configuration instanceof Configuration) {
        url = `${makeBundleUrl(bundleId)}/configs/${configuration.name}/actions/clone`;
    }
    else if(configuration instanceof Analysis) {
        url = `${makeBundleUrl(bundleId)}/analyses/${configuration.id}/directives?action=CLONE`;
    }

    return ajax.jsonGet(url)
        .then(results => new Configuration(results));
}

static mergeConfigurationTo(bundleId, source, target) {

    let part = '';

    if(source instanceof Configuration) {
        part = `sets/${source.name}`;
    }
    else if(source instanceof Analysis) {
        part = `analyses/${source.id}/directives`;
    }

    return ajax.jsonGet(`${makeBundleUrl(bundleId)}/${part}?action=MERGE&target=${encodeURIComponent(target.name)}`);
}
*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// private stuff
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const ENC = encodeURIComponent;
