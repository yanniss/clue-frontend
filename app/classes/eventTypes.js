import Directive from 'codebrowser/classes/Directive';
import File from 'codebrowser/classes/File';
import Project from 'app/classes/Project';
import Selection from 'codebrowser/classes/Selection';
import str from 'core/classes/StringHelper.static';

let spec;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Project-related events
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const PROJECT_NEW = str.createUnique();
spec = Project;

export const PROJECT_DELETE = str.createUnique();
spec = Project;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Bundle-related events
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const BUNDLE_NEW = str.createUnique();
spec = Project;

export const BUNDLE_DELETE = str.createUnique();
spec = Project;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// File-related events
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const FILE_RAW_OPEN = str.createUnique();
spec = {
    artifactId: String || undefined,
    filepath: String,
    line: Number /* [1, oo+) */ || undefined,
    column: Number /* [1, oo+) */ || undefined,
};

export const FILE_OPEN = str.createUnique();
spec = {
    file: File,
    line: Number /* [1, oo+) */ || undefined,
    column: Number /* [1, oo+) */ || undefined,
    scrollTop: Number /* [0, oo+) */ || undefined,
    scrollLeft: Number /* [0, oo+) */ || undefined,
};

export const FILE_CLOSE = str.createUnique();
spec = File;

export const FILE_SAVE_STATE = str.createUnique();
spec = {
    file: File,
    line: Number /* [1, oo+) */ || undefined,
    column: Number /* [1, oo+) */ || undefined,
    scrollTop: Number /* [0, oo+) */ || undefined,
    scrollLeft: Number /* [0, oo+) */ || undefined,
};

export const FILE_TRIGGER_POSITION = str.createUnique();
spec = {
    file: File,
    line: Number /* [1, oo+) */,
    column: Number /* [1, oo+) */,
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Selection-related events
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const SELECTION_NEW = str.createUnique();
spec = Selection;

export const SELECTION_ACTIVATED = str.createUnique();
spec = Selection || null;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Various events
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

export const RESIZE_X = str.createUnique();
spec = undefined;

export const DIRECTIVE_MENU_OPEN = str.createUnique();
spec = {
    method: Object,
    event: Event,
};

export const DIRECTIVE_KEEP_METHOD = str.createUnique();
spec = Directive;

export const DIRECTIVE_REMOVE_METHOD = str.createUnique();
spec = Directive;

export const DIRECTIVE_DELETE = str.createUnique();
spec = Directive;

export const PROGUARD_FILE_OPEN = str.createUnique();
spec = String;

// todo: remove this
export const ANALYSIS_ACTIVATED = str.createUnique();
