
export default class Project {

    constructor(data) {

        this.id = data.id;
        this.name = data.name;
        this.createdAt = new Date(data.created);
        this.owner = data.owner;
    }

}
