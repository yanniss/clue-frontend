import 'nprogress/nprogress.css';
import Cookies  from 'js-cookie';
import np from 'nprogress/nprogress';
import ko from 'knockout';
import $ from 'jquery';
import Notifier from 'app/classes/Notifier.static';
import Ajax from 'app/classes/Ajax';
import Analysis from 'codebrowser/classes/analysis/Analysis';
import Bundle from 'app/classes/Bundle';
import Directive from 'codebrowser/classes/Directive';
import Rule from 'codebrowser/classes/Rule';
import Project from 'app/classes/Project';
import Configuration from 'codebrowser/classes/configuration/Configuration';

// todo: fetch*Info ... only one filter applies for possibly many returned values ...

const apiEntry = __API_END_POINT__; // Defined through webpack.DefinePlugin
const tokenName = 'x-clue-token';
const paths = {
    auth:       `${apiEntry}/auth/v1/session`,
    users:      `${apiEntry}/api/v1/users`,
    projects:   `${apiEntry}/api/v1/projects`,
    bundles:    `${apiEntry}/api/v1/bundles`,
    families:   `${apiEntry}/api/v1/families`,
    options:    `${apiEntry}/api/v1/options`,
    analyses:   `${apiEntry}/api/v1/analyses`,
    proguard:   `${apiEntry}/api/v1/proguard`,
};

let pendingRequests = 0;
const hasPendingRequests = ko.observable(false);
const subscription = hasPendingRequests.subscribe(value => {
    value ? np.start() : np.done();
});
np.configure({ trickleSpeed: 100 });

const ajax = new Ajax({
    headers: {
        [tokenName]: Cookies.get('SESSION_ID')
    },
    beforeSend: () => { requestInitiated(); },
    complete: () => { requestCompleted(); },
    error: (jqXhr, textStatus, errorThrown) => {
        if(textStatus === 'error') {
            Notifier.error(errorThrown || 'Cannot connect to the server');
        }
        else if(textStatus === 'abort') {
            Notifier.warning('Aborted');
        }
        else {
            Notifier.error(Ajax.getErrorMessage(jqXhr, errorThrown)); // this.getErrorMessage refers to "Ajax"
        }
    }
});

function requestInitiated() {
    pendingRequests++;
    hasPendingRequests(true);
}
function requestCompleted() {
    if(--pendingRequests === 0) {
        hasPendingRequests(false);
    }
}

function makeProjectUrl(id) {
    return paths.projects + (id ? '/' + encodeURIComponent(id) : '');
}
function makeBundleUrl(id) {
    return paths.bundles + (id ? '/' + encodeURIComponent(id) : '');
}

function makeAnalysisUrl(projectId, analysisId) {
    let lastPart = analysisId ? `/${encodeURIComponent(analysisId)}` : '';
    return `${makeBundleUrl(projectId)}/analyses${lastPart}`;
}

/**
 *
 */
export default class Api {

    static dispose() {
        subscription.dispose();
    }

    static downloadOptimizedApk(projectId, configuration) {

        const url = `${makeBundleUrl(projectId)}/optimize?set=${encodeURIComponent(configuration.name())}`,
            anchor = window.document.createElement('a');

        anchor.setAttribute('href', url);
        anchor.setAttribute('download', 'true');
        anchor.style.display = 'none';
        document.body.appendChild(anchor);
        anchor.click();
        document.body.removeChild(anchor);

        return Promise.resolve();
    }

    static getBundleUrl(bundleId) {

        return makeBundleUrl(bundleId);
    }

    //////////////////////////////////////////////////////////////////
    // User(s)
    //////////////////////////////////////////////////////////////////

    static fetchUserInfo() {

        return ajax.jsonGet(paths.auth)
            .finally(() => {
                ajax.options.headers[tokenName] = Cookies.get('SESSION_ID');
            });
    }

    static login(username, password) {

        return ajax.jsonPost(paths.auth, {
                username: username,
                password: password
            })
            .finally(() => {
                ajax.options.headers[tokenName] = Cookies.get('SESSION_ID');
            });
    }

    static logout() {

        return ajax.jsonDelete(paths.auth)
            .finally(() => {
                ajax.options.headers[tokenName] = Cookies.get('SESSION_ID');
            });
    }

    static updateUserPassword(userId, oldPassword, newPassword) {

        let params = {
            oldPassword: oldPassword,
            newPassword: newPassword
        };

        return ajax.jsonPut(`${paths.users}/${encodeURIComponent(userId)}/password`, params);
    }

    static fetchUsers() {
        return ajax.jsonGet(paths.users).then(data => data.list);
    }

    static createUser($form) {

        return ajax.formPost(paths.users, $form)
            .then(data => data.id);
    }

    static deleteUser(userId) {
        return ajax.jsonDelete(`${paths.users}/${encodeURIComponent(userId)}`);
    }

    //////////////////////////////////////////////////////////////////
    // Project(s)
    //////////////////////////////////////////////////////////////////

    static createProject($form) {

        const url = paths.projects;

        return ajax.formPost(url, $form)
            .then(results => new Project(results));
    }

    static deleteProject(projectId) {

        const url = `${paths.projects}/${projectId}`;

        return ajax.jsonDelete(url);
    }

    static fetchProject(projectId) {

        const url = `${paths.projects}/${projectId}`;

        return ajax.jsonGet(url)
            .then(results => new Project(results));
    }

    static fetchProjects(params, returnHits) {

        return ajax.jsonGet(paths.projects, params)
            .then(results => {
                const projects = results.results.map(result => new Project(result));
                return returnHits ? { hits: results.hits, projects: projects } : projects;
            })
    }

    //////////////////////////////////////////////////////////////////
    // Bundle(s)
    //////////////////////////////////////////////////////////////////

    static fetchBundleOptions() {
        return ajax.jsonGet(`${paths.options}?what=bundle`).then(results => results.options);
    }


    static createBundle(projectId, $form) {

        const url = `${makeBundleUrl()}?family=doop&project=${projectId}`;

        return ajax.formPost(url, $form)
            .then(data => data.id);
    }

    static fetchBundles(projectId, params, returnHits) {

        return ajax.jsonGet(paths.bundles, { project: projectId, ...params })
            .then(results => {
                if(!results || !Array.isArray(results.results)) {
                    return $.Deferred().reject('Api.fetchBundles: malformed data');
                }
                let bundles = results.results.map(bundle => new Bundle(bundle));

                return returnHits ? { hits: results.hits, bundles: bundles } : bundles;
            });
    }

    static fetchBundle(bundleId) {
        return ajax.jsonGet(makeBundleUrl(bundleId)).then(data => new Bundle(data.inputBundle));
    }

    static deleteBundle(bundleId) {
        return ajax.jsonDelete(makeBundleUrl(bundleId));
    }

    /**
     * Fetches bundle config sets. Not analyses'.
     *
     * @param bundleId
     * @returns {*}
     */
    static fetchConfigurations(bundleId) {

        return ajax.jsonGet(`${makeBundleUrl(bundleId)}/configs`)
            .then(results => results.results);
    }

    static fetchConfiguration(bundleId, configurationName) {

        return ajax.jsonGet(`${makeBundleUrl(bundleId)}/configs/${encodeURIComponent(configurationName)}`);
    }

    static fetchRules(bundleId, configurationName, {
        page,
        resultsPerPage = 15,
        sortBy,
        facetOptions = {},
        facets = false,
    }) {

        const params = {
            _start: (page - 1) * resultsPerPage,
            _count: resultsPerPage,
            _sort: sortBy,
            _facets: facets,
            ...facetOptions,
        };

        const url = `${makeBundleUrl(bundleId)}/configs/${encodeURIComponent(configurationName)}/rules`;

        return ajax.jsonGet(url, params)
            .then(results => {
                const rules = results.results.map(result => new Rule(result));
                return { hits: results.hits, facets: results.facets, rules: rules };
            });
    }

    static fetchRuleContents(bundleId, configurationName, ruleId, packge, clazz) {

        let queryString = '';
        if(packge) {
            queryString += `${queryString ? '&' : '?'}package=${packge}`;
        }
        if(clazz) {
            queryString += `${queryString ? '&' : '?'}class=${clazz}`;
        }

        const url = makeBundleUrl(bundleId)
            + `/configs/${encodeURIComponent(configurationName)}`
            + `/matches/${encodeURIComponent(ruleId)}`
            + queryString;

        return ajax.jsonGet(url)
            .then(results => results.results);
    }

    static addRule(bundleId, configurationName, ruleBody) {
        const url = `${makeBundleUrl(bundleId)}/configs/${encodeURIComponent(configurationName)}/rules`;

        // todo: change "body" to "ruleBody".
        return ajax.jsonPost(url, {body: ruleBody});
    }

    static updateRule(bundleId, configurationName, ruleId, ruleBody, comment) {

        const url = `${makeBundleUrl(bundleId)}/configs/${encodeURIComponent(configurationName)}/rules/${encodeURIComponent(ruleId)}`,
            params = {};

        if(ruleBody !== null) params.ruleBody = ruleBody;
        if(comment !== null) params.comment = comment;

        return ajax.jsonPut(url, params);
    }

    static deleteRule(bundleId, configurationName, ruleId) {
        const url = `${makeBundleUrl(bundleId)}/configs/${encodeURIComponent(configurationName)}/rules/${encodeURIComponent(ruleId)}`;

        return ajax.jsonDelete(url);
    }

    static fetchDirectives(bundleId, {
        source,
        page,
        resultsPerPage = 15,
        sortBy = 'doopId|ASC',
        facetOptions = {},
        facets = false,
        textFilters = {},
    }) {

        const params = {
            _start: (page - 1) * resultsPerPage,
            _count: resultsPerPage,
            _sort: sortBy,
            _facets: facets,
            ...facetOptions,
            ...textFilters,
        };

        let url = makeBundleUrl(bundleId);

        if(source instanceof Configuration) {
            url += `/configs/${encodeURIComponent(source.name())}/directives`;
        }
        else if(source instanceof Analysis) {
            url += '/directives';
            params.analysis = source.id;
        }
        else {
            throw new Error(`Api.fetchDirectives: source must be either a Configuration or an Analysis: ${source}`);
        }

        return ajax.jsonGet(url, params)
            .then(results => {
                const directives = results.results.map(result => new Directive(result));
                return { hits: results.hits, facets: results.facets, directives: directives };
            });
    }

    static deleteDirectives(bundleId, configurationName, params) {

        const url = `${makeBundleUrl(bundleId)}/sets/${encodeURIComponent(configurationName)}`;

        return ajax.jsonGet(url, { ...params, action: 'DELETE_FILTERED' });
    }

    static getOptimizationDirectives(projectId, analysisId, origin, typeOfDirective, start, count) {
        let query = '';

        if(analysisId) {
            query += `${query ? '&' : '?'}analysis=${encodeURIComponent(analysisId)}`;
        }
        if(origin) {
            query += `${query ? '&' : '?'}origin=${encodeURIComponent(origin)}`;
        }
        if(typeOfDirective) {
            query += `${query ? '&' : '?'}typeOfDirective=${encodeURIComponent(typeOfDirective)}`;
        }
        if(start || start === 0) {
            query += `${query ? '&' : '?'}_start=${encodeURIComponent(start)}`;
        }
        if(count || count === 0) {
            query += `${query ? '&' : '?'}_count=${encodeURIComponent(count)}`;
        }

        return ajax.jsonGet(`${makeBundleUrl(projectId)}/directives${query}`).then(results => {
            return results;
        });
    }

    static addOptimizationDirective(projectId, $form) {

        const url = `${makeBundleUrl(projectId)}/directives`;

        return ajax.formPost(url, $form);
    }

    static addRuleFromDirective(bundleId, configuration, doopId, typeOfDirective) {
        const url = `${makeBundleUrl(bundleId)}/configs/${encodeURIComponent(configuration.name())}`

        return ajax.jsonPost(url, {
            doopId: doopId,
            type: typeOfDirective
        });
    }

    static editOptimizationDirective(projectId, directiveId, typeOfDirective) {
        directiveId = encodeURIComponent(directiveId);
        typeOfDirective = encodeURIComponent(typeOfDirective);

        return ajax.jsonPut(`${makeBundleUrl(projectId)}/directives/${directiveId}?typeOfDirective=${typeOfDirective}`);
    }

    static deleteDirective(bundleId, directiveId) {

        return ajax.jsonDelete(`${makeBundleUrl(bundleId)}/directives/${encodeURIComponent(directiveId)}`);
    }

    static downloadDirectivesFile(bundleId, configuration) {

        if(configuration instanceof Configuration) {
            window.location = `${makeBundleUrl(bundleId)}/configs/${configuration.name()}/actions/export`;
        }
        else if(configuration instanceof Analysis) {
            window.location = `${makeBundleUrl(bundleId)}/analyses/${configuration.id}/directives?action=EXPORT`;
        }

        // todo: somehow check if request was successful and return result.
        return Promise.resolve(true);
    }

    static encodeParamsAsQueryString(params) {
        return Object.entries(params).map(([key, value]) => {
            if (value) {
                if (Array.isArray(value)) {
                    value.map( v => {
                        return `${key}=${encodeURIComponent(v)}`;
                    }).join('&');
                }
                else {
                    return `${key}=${encodeURIComponent(value)}`;
                }
            }
            else {
                return null;
            }
        }).filter( s => {
            return (s != null);
        }).join('&');
    }

    static downloadFilteredDirectivesFile(bundleId, configuration, params) {

        //console.log(bundleId, configuration, params);

        //(configuration instanceof Configuration)
        let part = `sets/${configuration.name()}`;

        let queryStr = Api.encodeParamsAsQueryString(params);

        window.location = `${makeBundleUrl(bundleId)}/${part}?action=EXPORT_FILTERED&${queryStr}`;
    }

    static cloneConfiguration(bundleId, configuration) {

        let url = '';

        if(configuration instanceof Configuration) {
            url = `${makeBundleUrl(bundleId)}/configs/${configuration.name()}/actions/clone`;
        }
        else if(configuration instanceof Analysis) {
            url = `${makeBundleUrl(bundleId)}/analyses/${configuration.id}/directives?action=CLONE`;
        }

        return ajax.jsonGet(url);
    }

    static importAnalysis(bundleId, configuration, analysis) {

        let url = `${makeBundleUrl(bundleId)}/configs/${configuration.name()}/actions/import_directives?target=${encodeURIComponent(analysis)}`;

        return ajax.jsonGet(url);
    }

    static cloneDirectivesOfConfiguration(bundleId, configuration, params) {

        //console.log(bundleId, configuration, params);

        //(configuration instanceof Configuration)
        let part = `sets/${configuration.name()}`;

        let queryStr = Api.encodeParamsAsQueryString(params);

        return ajax.jsonGet(`${makeBundleUrl(bundleId)}/${part}?action=CLONE_FILTERED&${queryStr}`);
    }

    static mergeConfigurationTo(bundleId, source, target) {

        let part = '';

        if(source instanceof Configuration) {
            part = `sets/${source.name()}`;
        }
        else if(source instanceof Analysis) {
            part = `analyses/${source.id}/directives`;
        }

        return ajax.jsonGet(`${makeBundleUrl(bundleId)}/${part}?action=MERGE&target=${encodeURIComponent(target.name())}`);
    }

    static mergeDirectivesOfConfigurationTo(bundleId, source, params, targetName) {

        //(configuration instanceof Configuration)
        let part = `sets/${source.name()}`;

        let queryStr = Api.encodeParamsAsQueryString(params);

        return ajax.jsonGet(`${makeBundleUrl(bundleId)}/${part}?action=MERGE_FILTERED&target=${encodeURIComponent(targetName)}&${queryStr}`);
    }

    static deleteConfiguration(bundleId, configurationName) {

        return ajax.jsonDelete(`${makeBundleUrl(bundleId)}/sets/${encodeURIComponent(configurationName)}`);
    }

    static renameConfiguration(bundleId, configuration, newName) {

        let url = '';

        if(configuration instanceof Configuration) {
            url = `${makeBundleUrl(bundleId)}/configs/${encodeURIComponent(configuration.name())}/actions/rename`
                + `?target=${encodeURIComponent(newName)}`;
        }
        else if(configuration instanceof Analysis) {
            url = `${makeBundleUrl(bundleId)}/sets/${encodeURIComponent(configuration.name)}`
                + '?action=RENAME'
                + `&target=${encodeURIComponent(newName)}`;
        }

        return ajax.jsonGet(url)
            .then(results => results.nameOfSet);
    }

    static createConfiguration(bundleId, configurationName) {

        return ajax.jsonPost(`${makeBundleUrl(bundleId)}/configs?name=${encodeURIComponent(configurationName)}`);
    }

    static fetchProguardRules(bundleId) {

        return ajax.jsonGet(`${makeBundleUrl(bundleId)}/proguard/report`)
            .then(results => results.results);
    }

    static fetchProguardRuleMatches(bundleId, ruleId) {

        return ajax.jsonGet(`${makeBundleUrl(bundleId)}/proguard/expand`)
            .then(results => results.results);
    }

    static parseProguardFile($form) {

        return ajax.formPost(paths.proguard, $form);
    }

    static postBatchRules(bundleId, configurationName, $form) {

        return ajax.formPost(`${makeBundleUrl(bundleId)}/configs/${encodeURIComponent(configurationName)}/rulesBatch`, $form);
    }

    //////////////////////////////////////////////////////////////////
    // Analysis/es
    //////////////////////////////////////////////////////////////////

    static fetchAnalyses(projectId) {
        return ajax.jsonGet(makeAnalysisUrl(projectId))
            .then(results => results.results);
    }

    /**
     * Requests the analysis based on the given id.
     * Returns a jQuery promise which on success returns an "Analysis" object.
     *
     * @param projectId
     * @param analysisId
     * @param noProgressBar
     * @returns {*}
     */
    static fetchAnalysis(projectId, analysisId, noProgressBar) {
        let additionalOptions = noProgressBar ? { beforeSend: null, complete: null } : {};

        return ajax.jsonGet(makeAnalysisUrl(projectId, analysisId), null, additionalOptions)
            .then(data => data.analysis);
    }

    static fetchAnalysisRuntime(projectId, analysisId) {
        let additionalOptions = { beforeSend: null, complete: null };

        return ajax.jsonGet(`${makeAnalysisUrl(projectId, analysisId)}/runtime`, null, additionalOptions);
    }

    static executeAnalysisAction(projectId, analysisId, action) {
        return ajax.jsonPut(`${makeAnalysisUrl(projectId, analysisId)}/action/${action}`);
    }

    static fetchAnalysisOptions() {
        return ajax.jsonGet(`${paths.options}?what=analysis`).then(results => results.options);
    }

    static createAnalysis(projectId, configurationName, $form) {

        const url = `${makeBundleUrl(projectId)}/configs/${encodeURIComponent(configurationName)}/analyze`;

        return ajax.formPost(url, $form)
            .then(data => data.id);
    }

    static deleteAnalysis(projectId, analysisId) {
        return ajax.jsonDelete(makeAnalysisUrl(projectId, analysisId));
    }

    static fetchSymbolsThatStartWith(projectId, text, symbolTypes) {
        return ajax.jsonPost(`${makeBundleUrl(projectId)}/symbols/startWith`, {
            prefix: text,
            types:  symbolTypes,
        });
    }

    //////////////////////////////////////////////////////////////////
    // Files, packages & hierarchies of those
    //////////////////////////////////////////////////////////////////

    static fetchDatastoreQuery(analysisId, query) {
        return ajax.jsonPost(`${paths.analyses}/${encodeURIComponent(analysisId)}/query/datastore`, query);
    }

    static fetchArtifacts(projectId) {
        return ajax.jsonGet(`${makeBundleUrl(projectId)}/artifacts`);
    }

    /**
     * todo: update documentation
     * Fetches information on the requested file or directory
     *
     * @param projectId
     * @param artifactId
     * @param path
     *      "/"
     *      in "path" starting slash is optional
     * @param analysisId
     * @param options
     * @returns {*}
     */
    static fetchFile(projectId, artifactId, path, options) {
        let url = makeBundleUrl(projectId)
                + (artifactId ? `/artifacts/${encodeURIComponent(artifactId)}` : '')
                + `/files/${encodeURIComponent(path || '')}`,
            queryString = '';

        if(options) {
            if(options.classes) {
                queryString += `${queryString ? '&' : '?'}classes=`;
            }
            if(options.fields) {
                queryString += `${queryString ? '&' : '?'}fields=`;
            }
            if(options.methods) {
                queryString += `${queryString ? '&' : '?'}methods=`;
            }
            if(!options.content) {
                queryString += `${queryString ? '&' : '?'}noContent=`;
            }
            if(options.set) {
                queryString += `${queryString ? '&' : '?'}set=${encodeURIComponent(options.set)}`;
            }
            if(options.analysis) {
                queryString += `${queryString ? '&' : '?'}analysis=${encodeURIComponent(options.analysis)}`;
            }
        }

        return ajax.jsonGet(url + queryString).then(results => results);
    }

    static fetchPackage(projectId, context, packge, analysisId) {
        let url = makeBundleUrl(projectId)
                + (context ? `/contexts/${encodeURIComponent(context)}` : '')
                + `/packages/${encodeURIComponent(packge)}`,
            queryString = '';

        if(analysisId) {
            queryString += `${queryString ? '&' : '?'}analysis=${encodeURIComponent(analysisId)}`;
        }

        return ajax.jsonGet(url + queryString);
    }

    //////////////////////////////////////////////////////////////////
    // Program elements
    //////////////////////////////////////////////////////////////////

    static fetchProgramElement(projectId, analysisId, filepath, line, column, noValues) {
        let url = makeBundleUrl(projectId)
                + `/symbols/${encodeURIComponent(filepath)}`
                + `/${line}`
                + `/${column}`,
            queryString = '';

        if(analysisId) {
            queryString += `${queryString ? '&' : '?'}analysis=${encodeURIComponent(analysisId)}`;
        }
        if(noValues) {
            queryString += `${queryString ? '&' : '?'}noExpansion=`;
        }

        return ajax.jsonGet(url + queryString).then(results => results.symbols);
    }

    /**
     *
     * @param projectId
     * @param analysisId
     * @param classId
     * @param options
     *      { self, allocatedAt, superTypes, subTypes }
     * @returns {*}
     */
    static fetchTypeInfo(projectId, analysisId, classId, options) {
        let url = makeBundleUrl(projectId)
                + `/classes/${encodeURIComponent(classId)}`,
            queryString = '',
            filter = results => results;

        if(analysisId) {
            queryString += `${queryString ? '&' : '?'}analysis=${encodeURIComponent(analysisId)}`;
        }
        if(options) {
            if(!options.self) {
                queryString += `${queryString ? '&' : '?'}noDef=`;
            }
            if(options.allocatedAt) {
                queryString += `${queryString ? '&' : '?'}allocationsOfType=`;
                filter = results => results.allocationsOfType;
            }
            if(options.superTypes) {
                queryString += `${queryString ? '&' : '?'}hierarchy=superTypes`;
                filter = results => Object.keys(results.hierarchy).reduce(
                    (p1, p2) => results.hierarchy[p1].concat(results.hierarchy[p2])
                );
            }
            if(options.subTypes) {
                queryString += `${queryString ? '&' : '?'}hierarchy=subTypes`;
                filter = results => Object.keys(results.hierarchy).reduce(
                    (p1, p2) => results.hierarchy[p1].concat(results.hierarchy[p2])
                );
            }
            if(options.fields) {
                queryString += `${queryString ? '&' : '?'}fields=`;
                filter = results => results.fields;
            }
            if(options.fieldsOfType) {
                queryString += `${queryString ? '&' : '?'}fieldsOfType=`;
                filter = results => results.fieldsOfType;
            }
            if(options.varsOfType) {
                queryString += `${queryString ? '&' : '?'}varsOfType=`;
                filter = results => results.varsOfType;
            }
        }

        return ajax.jsonGet(url + queryString).then(filter);
    }

    /**
     *
     * @param projectId
     * @param analysisId
     * @param fieldId
     * @param options
     *      { self, shadowedBy, readAccesses, writeAccesses, values }
     * @returns {*}
     */
    static fetchFieldInfo(projectId, analysisId, fieldId, options) {
        let url = makeBundleUrl(projectId)
                + `/fields/${encodeURIComponent(fieldId)}`,
            queryString = '',
            filter = results => results;

        if(analysisId) {
            queryString += `${queryString ? '&' : '?'}analysis=${encodeURIComponent(analysisId)}`;
        }
        if(options) {
            if(!options.self) {
                queryString += `${queryString ? '&' : '?'}noDef=`;
            }
            if(options.shadowedBy) {
                queryString += `${queryString ? '&' : '?'}shadowedBy=`;
                filter = results => results.shadowedBy;
            }
            if(options.readAccesses) {
                queryString += `${queryString ? '&' : '?'}accessedBy=R`;
                filter = results => results.accessedBy;
            }
            if(options.writeAccesses) {
                // 3.b) baseValue=<baseValue> -- optional
                queryString += `${queryString ? '&' : '?'}accessedBy=W`;
                filter = results => results.accessedBy;
            }
            if(options.values) {
                queryString += `${queryString ? '&' : '?'}values=`;
                filter = results => results.values;
                if(options.baseValue) {
                    queryString += `${queryString ? '&' : '?'}baseValue=${options.baseValue}`;
                }
            }
        }

        return ajax.jsonGet(url + queryString).then(filter);
    }

    /**
     *
     * @param projectId
     * @param analysisId
     * @param methodId
     * @param options
     *      { self, overrides, overriddenBy, invokedBy, thisValues }
     * @returns {*}
     */
    static fetchMethodInfo(projectId, analysisId, methodId, options) {
        let url = makeBundleUrl(projectId)
                + `/methods/${encodeURIComponent(methodId)}`,
            queryString = '',
            filter = results => results;

        if(analysisId) {
            queryString += `${queryString ? '&' : '?'}analysis=${encodeURIComponent(analysisId)}`;
        }
        if(options) {
            if(!options.self) {
                queryString += `${queryString ? '&' : '?'}noDef=`;
            }
            if(options.overrides) {
                queryString += `${queryString ? '&' : '?'}hierarchy=superTypes`;
                filter = results => results.hierarchy;
            }
            if(options.overriddenBy) {
                queryString += `${queryString ? '&' : '?'}hierarchy=subTypes`;
                filter = results => results.hierarchy;
            }
            if(options.invokedBy) {
                queryString += `${queryString ? '&' : '?'}reverseInvocations=`;
                filter = results => results.reverseInvocations;
            }
            if(options.invokedByNaive) {
                queryString += `${queryString ? '&' : '?'}reverseChaInvocations=`;
                filter = results => results.reverseChaInvocations;
            }
            if(options.thisValues) {
                queryString += `${queryString ? '&' : '?'}this=`;
                filter = results => results.this;
            }
            if(options.returnValues) {
                queryString += `${queryString ? '&' : '?'}returnValues=`;
                filter = results => results.returnValues;
            }
        }

        return ajax.jsonGet(url + queryString).then(filter);
    }

    /**
     *
     * @param projectId
     * @param analysisId
     * @param variableId
     * @param options
     *      { self, readAccesses, writeAccesses, values, line:<int> }
     * @returns {*}
     */
    static fetchVariableInfo(projectId, analysisId, variableId, options) {
        let url = makeBundleUrl(projectId)
                + `/vars/${encodeURIComponent(variableId)}`,
            queryString = '',
            filter = results => results;

        if(analysisId) {
            queryString += `${queryString ? '&' : '?'}analysis=${encodeURIComponent(analysisId)}`;
        }
        if(options) {
            if(!options.self) {
                queryString += `${queryString ? '&' : '?'}noDef=`;
            }
            if(options.readAccesses) {
                queryString += `${queryString ? '&' : '?'}accessedBy=R`;
                filter = results => results.accessedBy;
            }
            if(options.writeAccesses) {
                queryString += `${queryString ? '&' : '?'}accessedBy=W`;
                filter = results => results.accessedBy;
            }
            if(options.values) {
                queryString += `${queryString ? '&' : '?'}values=`;
                if(options.line) {
                    queryString += `${queryString ? '&' : '?'}line=${options.line}`;
                }
                filter = results => results.values;
            }
        }

        return ajax.jsonGet(url + queryString).then(filter);
    }

    /**
     *
     * @param projectId
     * @param analysisId
     * @param methodId
     * @param options
     *      { self, values }
     * @returns {*}
     */
    static fetchMethodInvocationInfo(projectId, analysisId, methodId, options) {
        let url = makeBundleUrl(projectId)
                + `/invocations/${encodeURIComponent(methodId)}`,
            queryString = '',
            filter = results => results;

        if(analysisId) {
            queryString += `${queryString ? '&' : '?'}analysis=${encodeURIComponent(analysisId)}`;
        }
        if(options) {
            if(!options.self) {
                queryString += `${queryString ? '&' : '?'}noDef=`;
            }
            if(options.methods) {
                queryString += `${queryString ? '&' : '?'}values=`;
                filter = results => results.values;
            }
            if(options.methodsNaive) {
                queryString += `${queryString ? '&' : '?'}cha=`;
                filter = results => results.cha;
            }
        }

        return ajax.jsonGet(url + queryString).then(filter);
    }

    /**
     *
     * @param projectId
     * @param analysisId
     * @param valueId
     * @param options
     * @returns {*}
     */
    static fetchValueInfo(projectId, analysisId, valueId, options) {
        let url = makeBundleUrl(projectId)
                + `/values/${encodeURIComponent(valueId)}`,
            queryString = '',
            filter = results => results;

        if(analysisId) {
            queryString += `${queryString ? '&' : '?'}analysis=${encodeURIComponent(analysisId)}`;
        }
        if(options) {
            if(!options.self) {
                queryString += `${queryString ? '&' : '?'}noDef=`;
            }
            if(options.fields) {
                queryString += `${queryString ? '&' : '?'}fields=`;
                filter = results => results.fields;
            }
            if(options.arrayValues) {
                queryString += `${queryString ? '&' : '?'}arrayValues=`;
                filter = results => results.arrayValues;
            }
        }

        return ajax.jsonGet(url + queryString).then(filter);
    }

    static fetchQueries(projectId, analysisId) {
        let url = makeBundleUrl(projectId)
                + '/queries',
            queryString = analysisId ? `?analysis=${encodeURIComponent(analysisId)}` : '';

        return ajax.jsonGet(url + queryString).then(results => results.results);
    }

    static fetchQuery(projectId, analysisId, queryId) {
        let url = makeBundleUrl(projectId)
                + `/queries/${encodeURIComponent(queryId)}`,
            queryString = analysisId ? `?analysis=${encodeURIComponent(analysisId)}` : '';

        return ajax.jsonGet(url + queryString).then(results => results.results);
    }

    static fetchPackageGraph(projectId, analysisId) {
        let url = makeBundleUrl(projectId)
                + '/packageGraph',
            queryString = analysisId ? `?analysis=${encodeURIComponent(analysisId)}` : '';

        return ajax.jsonGet(url + queryString).then(results => results.list);
    }

    static fetchPackageHierarchy(projectId, analysisId) {
        let url = makeBundleUrl(projectId)
                + '/packageHierarchy',
            queryString = analysisId ? `?analysis=${encodeURIComponent(analysisId)}` : '';

        return ajax.jsonGet(url + queryString).then(results => results.results);
    }

};
