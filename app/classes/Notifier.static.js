import 'toastr/build/toastr.min.css';
import toastr from 'toastr';

toastr.options.escapeHTML = true;
toastr.options.progressBar = true;
toastr.options.positionClass = 'toast-bottom-right';
toastr.options.extendedTimeOut = 1000 * 2; // hide after on hover

/**
 *
 */
export default class Notifier {

    static success() {
        toastr.options.timeOut = 1000 * 5;
        toastr.success.apply(null, arguments);
    }

    static info() {
        toastr.options.timeOut = 1000 * 10;
        toastr.info.apply(null, arguments);
    }

    static warning() {
        toastr.options.timeOut = 1000 * 5;
        toastr.warning.apply(null, arguments);
    }

    static error() {
        toastr.options.timeOut = 1000 * 10;
        toastr.error.apply(null, arguments);
    }

    static custom(message, callback, timeout) {
        toastr.options.timeOut = isNaN(parseInt(timeout)) ? 10000 : parseInt(timeout);
        toastr.options.extendedTimeOut = toastr.options.timeOut;
        toastr.options.onclick = callback instanceof Function ? callback : null;
        toastr.options.closeButton = true;

        toastr.info(message);

        toastr.options.extendedTimeOut = 1000 * 10;
        toastr.options.onclick = null;
        toastr.options.closeButton = false;
    }

    static clear() {
        toastr.remove();
    }

    static clearAnimation() {
        toastr.clear();
    }

};
