
/**
 *
 */
export default class Bundle {

    constructor(data) {

        this.id = data.id;
        this.name = data.displayName;
        this.createdAt = new Date(data.created);
        this.projectId = data.projectId;
        this.options = data.options;
        this.appArtifacts = data.appArtifacts;
        this.depArtifacts = data.depArtifacts;
        this.platform = this.getOptionValue(data.options, 'Platform');
        this.hasProguard = data.hasProguard;
    }

    isAndroid() {

        return this.platform.toLowerCase().startsWith('android_');
    }

    /**
     * @private
     * @param options
     * @param label
     * @returns {*}
     */
    getOptionValue(options, label) {

        return (options.find(option => option.label === label) || {}).value;
    }

};
