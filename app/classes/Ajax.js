import $ from 'jquery';
import 'app/vendor/jquery.form.min';

/**
 *
 */
export default class Ajax {

    constructor(options) {

        this.options = options || {};
    }

    textGet(url, data, additionalOptions) {

        return this._fetch(url, 'GET', 'text', data, additionalOptions);
    }

    htmlGet(url, data, additionalOptions) {

        return this._fetch(url, 'GET', 'html', data, additionalOptions);
    }

    htmlPost(url, data, additionalOptions) {

        return this._fetch(url, 'POST', 'html', data, additionalOptions);
    }

    jsonGet(url, data, additionalOptions) {

        return this._fetch(url, 'GET', 'json', data, additionalOptions);
    }

    jsonPost(url, data, additionalOptions) {

        return this._fetch(url, 'POST', 'json', data, additionalOptions);
    }

    jsonDelete(url, data, additionalOptions) {

        return this._fetch(url, 'DELETE', 'json', data, additionalOptions);
    }

    jsonPut(url, data, additionalOptions) {

        return this._fetch(url, 'PUT', 'json', data, additionalOptions);
    }

    formPost(url, $form, additionalOptions) {

        return this._fetchForm(url, $form, 'POST', 'json', additionalOptions);
    }

    /**
     * Get the error message from the jqXhr response. Expects a json object with key "msg".
     *
     * @param jqXhr
     * @param errorThrown
     * @returns {*}
     */
    static getErrorMessage(jqXhr, errorThrown) {

        try {
            let message = JSON.parse(jqXhr.responseText).msg;
            if(message) {
                return errorThrown + ': ' + message;
            }
            else {
                return errorThrown + ': ' + jqXhr.responseText;
            }
        }
        catch(e) {
            return errorThrown;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    _fetch(url, httpMethod, dataType, data, additionalOptions = {}) {

        return new Promise((resolve, reject) => {
            $.ajax({
                ...this.options,
                ...{
                    url: url,
                    type: httpMethod,
                    dataType: dataType,
                    data: data,
                    traditional: true,
                    xhrFields: {withCredentials: true},
                },
                ...additionalOptions,
            }).always(() => {
                //console.warn(arguments[2].getAllResponseHeaders());
            }).done((...args) => {
                resolve.apply(null, args);
            }).fail((...args) => {
                reject.apply(null, args);
            });
        });
    }

    _fetchForm(url, $form, httpMethod, dataType, additionalOptions = {}) {

        const self = this;

        return new Promise((resolve, reject) => {
            $form.ajaxSubmit({
                ...this.options,
                ...{
                    url: url,
                    type: httpMethod,
                    dataType: dataType, // todo: is this option redundant/wrong for form submission?
                    traditional: true,
                    xhrFields: { withCredentials: true },
                    contentType: 'application/x-www-form-urlencoded', // todo: remove?
                },
                ...additionalOptions,
                ...{
                    uploadProgress: (...args) => {
                        const callback = additionalOptions.uploadProgress || self.options.uploadProgress;
                        callback && callback.apply(null, args);
                    },
                    success: (...args) => {
                        const callback = additionalOptions.success || self.options.success;
                        callback && callback.apply(null, args);
                        resolve.apply(null, args);
                    },
                    error: (...args) => {
                        const callback = additionalOptions.error || self.options.error;
                        callback.apply(null, args);
                        reject.apply(null, args);
                    },
                },
            });
        });
    }

};
