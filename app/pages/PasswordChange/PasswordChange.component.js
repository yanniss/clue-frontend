import './PasswordChange.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './PasswordChange.vm';
import view from './PasswordChange.view.html';

koh.registerComponent('password-change', vm, view);
