import 'app/components/Navbar/Navbar.component';
import $ from 'jquery';
import Api from 'app/classes/Api.static';
import Notifier from 'app/classes/Notifier.static';
import Page from 'core/classes/vm/Page.vm';
import ko from 'knockout';
import navbar from './PasswordChange.view.navbar.html';

/**
 *
 */
export default class PasswordChange extends Page {

    app = ko.contextFor(this.element).$root;

    navbarContent = navbar;

    oldPassword = ko.observable('');
    newPassword = ko.observable('');
    newPasswordRepeat = ko.observable('');
    newPasswordRepeatHasBeenFocused = ko.observable(false); // If it has been focused at least once.
    newPasswordRepeatFocused = () => { this.newPasswordRepeatHasBeenFocused(true); };

    submit(form) {

        const btn = $(form).find('button[type=submit].btn-success');

        btn.button('loading');

        Api.updateUserPassword(this.app.getUsername(), this.oldPassword(), this.newPassword())
            .then(() => {
                Notifier.success('Changed password successfully. Please wait ...');
                setTimeout(() => {
                    location.hash = '/';
                }, 2000);
            })
            .finally(() => {
                setTimeout(() => {
                    btn.button('reset');
                }, 750);
            });

        return false;
    }

};
