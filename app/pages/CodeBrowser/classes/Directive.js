
export default class Directive {

    constructor(data) {

        this.id = data.id;
        this.origin = data.origin;
        this.doopId = data.doopId;
        this.packageName = data.packageName;
        this.className = data.className;
        this.methodName = data.methodName;
        this.paramTypes = data.paramTypes;
        this.type = data.typeOfDirective;
        this.filepath = data.sourceFileName;
        this.line = data.position.startLine;
        this.column = data.position.startColumn;
        this.position = data.position; // needed for editor markers
    }

    getCommaSeparatedParamTypes() {

        return this.paramTypes.join(', ');
    }

};
