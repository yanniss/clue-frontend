import Api from 'app/classes/Api.static';
import Filters from './Filters.static';
import ProgramElement from './ProgramElement';

/**
 *
 */
export default class Method extends ProgramElement {

    constructor(bundleId, analysisId, data) {

        if(data.kind !== 'Method') {
            throw new Error(`Method::constructor(): Invalid argument: ${data}`);
        }

        data = Object.assign({}, data); // Do not override data

        data.icon = 'm<sub class="pe-definition">def</sub>';
        data.iconSmall = data.icon;
        data.kind = getKind(data);
        data.cssClass = getCssClass(data);

        data.propertyConfigurations = getPropertyConfigurations(data);

        super(bundleId, analysisId, data, Api.fetchMethodInfo);
    }

};

/* ******************************************* */

function getPropertyConfigurations(data) {

    const configs = [
        {
            name:         'invokedBy',
            initValue:    [],
            apiOptions:   { invokedBy: true },
            isAnalysisBased: true,
            resultFilter: results => {
                results.forEach(item => {
                    const matches = item.doopId.match(/^<([^:]*): [^\s]+ ([^(]+).*$/),
                        clazz = matches[1] && matches[1].split('.').pop(),
                        method = matches[2];
                    item.viewBody = {
                        main:       clazz + '.' + method,
                        secondary:  item.position.startLine,
                    };
                });
                return results.sort((a, b) => a.sourceFileName.localeCompare(b.sourceFileName));
            },
            title:        'Invocation locations',
            viewHeader:   '<div style="font-size: 14px; margin: 2px 0 0 -4px;">m( )</div>',
        },
        {
            name:         'invokedByNaive',
            initValue:    [],
            apiOptions:   { invokedByNaive: true },
            isAnalysisBased: false,
            resultFilter: results => {
                results.forEach(item => {
                    const matches = item.doopId.match(/^<([^:]*): [^\s]+ ([^(]+).*$/),
                        clazz = matches[1] && matches[1].split('.').pop(),
                        method = matches[2];
                    item.viewBody = {
                        main:       clazz + '.' + method,
                        secondary:  item.position.startLine,
                    };
                });
                return results.sort((a, b) => a.sourceFileName.localeCompare(b.sourceFileName));
            },
            title:        'Invocation locations (naive)',
            viewHeader:   '<div style="font-size: 14px; margin: 2px 0 0 -5px;">m(<small>?</small>)</div>',
        },
        {
            name:         'overrides',
            initValue:    [],
            apiOptions:   { overrides: true },
            isAnalysisBased: true,
            resultFilter: results => {
                results.forEach(item => {
                    const matches = item.doopId.match(/^<([^:]*).*$/),
                        clazz = matches[1] && matches[1].split('.').pop();
                    item.viewBody = {
                        main:       clazz,
                        secondary:  item.position.startLine,
                    };
                });
                return results.sort((a, b) => a.sourceFileName.localeCompare(b.sourceFileName));
            },
            title:        'Overrides methods',
            viewHeader:   '<div style="font-size: 14px; margin: -4px 0 0 0">m<span style="font-size: 20px;">&#x2191;</span></div>',
        },
        {
            name:         'overriddenBy',
            initValue:    [],
            apiOptions:   { overriddenBy: true },
            isAnalysisBased: true,
            resultFilter: results => {
                results.forEach(item => {
                    const matches = item.doopId.match(/^<([^:]*).*$/),
                        clazz = matches[1] && matches[1].split('.').pop();
                    item.viewBody = {
                        main:       clazz,
                        secondary:  item.position.startLine,
                    };
                });
                return results.sort((a, b) => a.sourceFileName.localeCompare(b.sourceFileName));
            },
            title:        'Overridden by methods',
            viewHeader:   '<div style="font-size: 14px; margin: -4px 0 0 -1px">m<span style="font-size: 20px;">&#x2193;</span></div>',
        },
        {
            name:         'returnValues',
            isAnalysisBased: true,
            initValue:    [],
            apiOptions:   { returnValues: true },
            resultFilter: Filters.filterValues,
            title:        'Possible return values',
            viewHeader:   '<div style="font-size: 13px; font-weight: bold; margin: 4px 0 0 1px">ret</div>',
        },
    ];

    if(!data.isStatic) {
        configs.push({
            name:         'thisValues',
            isAnalysisBased: true,
            initValue:    [],
            apiOptions:   { thisValues: true },
            resultFilter: Filters.filterValues,
            title:        'Possible values for "this" in method',
            viewHeader:   '<div style="font-size: 13px; font-weight: bold; margin: 4px 0 0 -2px;">this</div>',
        });
    }

    return configs;
}

function getKind(data) {

    return 'Method definition';
}

function getCssClass(data) {

    return 'pe-icon-method-def';
}
