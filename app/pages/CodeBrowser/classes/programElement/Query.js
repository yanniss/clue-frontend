import Api from 'app/classes/Api.static';
import Filters from './Filters.static';
import ProgramElement from './ProgramElement';

/**
 * Due to lack of dev time, I chose to disguise "Query" as a "ProgramElement",
 * although it clearly is not one, in order to avoid modifying all the other
 * parts(*) of the application to also support "Query".
 *
 * (*) These parts are the components <editor>, <selection> and <selection-history>.
 */
export default class Query extends ProgramElement {

    constructor(bundleId, analysisId, data) {

        if(data.kind !== 'Query') {
            throw new Error(`Query::constructor(): Invalid argument: ${data}`);
        }

        data.doopId     = data.id;
        data.position   = {};
        data.icon       = 'Q';
        data.iconSmall  = 'Q';
        data.cssClass   = 'pe-icon-query';
        data.propertyConfigurations = getPropertyConfigurations(data);

        super(bundleId, analysisId, data, Api.fetchQuery);
    }

};

/* ******************************************* */

function getPropertyConfigurations(data) {

    return [
        {
            name:         'irrelevant',
            initValue:    [],
            apiOptions:   { methods: true },
            resultFilter: results => {
                let { values, other } = results.reduce((acc, result) => {
                    switch(result.kind) {
                        case 'Class':
                            result.viewBody = {
                                main:       result.doopId,
                                secondary:  result.count,
                            };
                            acc.other.push(result);
                            break;
                        case 'Field':
                        case 'Method':
                            result.viewBody = {
                                main:       `${result.declaringClassDoopId}.${result.name}`,
                                secondary:  result.count,
                            };
                            acc.other.push(result);
                            break;
                        case 'MethodInvocation': {
                            const matches = result.doopId.match(/^<.*>\/(.*)\/.*$/),
                                main = matches[1] || result.name;
                            result.viewBody = {
                                main:       main,
                                secondary:  result.count,
                            };
                            acc.other.push(result);
                            break;
                        }
                        case 'Value': {
                            const filtered = Filters.filterValues([result])[0];
                            filtered.viewBody.secondary = result.count;
                            acc.values.push(filtered);
                            break;
                        }
                    }
                    return acc;
                }, { values: [], other: [] });

                return values.concat(other);
            },
            title:      null,
            viewHeader: null,
        },
    ];
}
