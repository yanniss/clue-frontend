import Api from 'app/classes/Api.static';
import ProgramElement from './ProgramElement';

// todo: provide "name" in server's response

/**
 *
 */
export default class MethodInvocation extends ProgramElement {

    constructor(bundleId, analysisId, data) {

        if(data.kind !== 'MethodInvocation') {
            throw new Error(`MethodInvocation::constructor(): Invalid argument: ${data}`);
        }

        data = Object.assign({}, data); // Do not override data

        data.icon = '<div style="margin-top: 4px; font-size: 22px;">m()</div>';
        data.iconSmall = 'm()';
        data.kind = getKind(data);
        data.cssClass = getCssClass(data);
        data.propertyConfigurations = getPropertyConfigurations(data);

        super(bundleId, analysisId, data, Api.fetchMethodInvocationInfo);
    }

};

/* ******************************************* */

function getPropertyConfigurations(data) {

    return [
        {
            name:         'methods',
            initValue:    [],
            apiOptions:   { methods: true },
            isAnalysisBased: true,
            resultFilter: results => {
                results.forEach(item => {
                    const clazz = item.declaringClassDoopId.split('.').pop();
                    item.viewBody = {
                        main:       clazz,
                        secondary:  item.position.startLine,
                    };
                });
                return results.sort((a, b) => a.sourceFileName.localeCompare(b.sourceFileName));
            },
            title:      'Possible definitions',
            viewHeader: '<div style="margin-left: -5px;">m<sub style="font-size: 9px; font-weight: bold; margin-left: 0px;"><i>def</i></sub></div>',
        },
        {
            name:         'cha',
            initValue:    [],
            apiOptions:   { methodsNaive: true },
            isAnalysisBased: false,
            resultFilter: results => {
                results.forEach(item => {
                    const clazz = item.declaringClassDoopId.split('.').pop();
                    item.viewBody = {
                        main:       clazz,
                        secondary:  item.position.startLine,
                    };
                });
                return results.sort((a, b) => a.sourceFileName.localeCompare(b.sourceFileName));
            },
            title:      'Possible definitions (naive)',
            viewHeader: `<div style="margin-left: -5px; position: relative;">m<sub style="font-size: 9px; font-weight: bold; margin-left: 0px;"><i>def</i></sub>
<sup style="position: absolute; top: 7px; right: 8px;">?</sup></div>`,
        },
    ];
}

function getKind(data) {

    return 'Method invocation';
}

function getCssClass(data) {

    return 'pe-icon-method-invocation';
}
