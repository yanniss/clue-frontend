import Api from 'app/classes/Api.static';

// todo: move subtype property creations in subtype

/**
 *
 */
export default class ProgramElement {

    constructor(bundleId, analysisId, data, apiMethod) {

        /** The id of the bundle that contains this program element */
        this.bundleId = bundleId;

        /** The id of the currently active analysis */
        this.analysisId = analysisId;

        /** A presentable description of the program element name */
        this.name = data.name;

        /** A presentable description of the program element kind */
        this.kind = data.kind;

        /** ... */
        this.icon = data.icon || data.kind[0].toUpperCase();

        /** ... */
        this.iconSmall = data.iconSmall;

        /** The file location of the program element */
        this.filepath = (data.usage || data).sourceFileName;

        /** The in file position of the program element */
        this.position = (data.usage || data).position;

        /** Information/Data sets of the program element */
        this.props = data.propertyConfigurations || [];

        /** The program element id; used for communicating with the server */
        this.id = data.doopId;

        /** The program element augmented id; used locally in the front-end */
        this.augId = `${this.id}#[(${this.position.startLine},${this.position.startColumn}),(${this.position.endLine},${this.position.endColumn}}`;

        /** ... */
        this.cssClass = data.cssClass;

        /** ... */
        this._apiMethod = apiMethod;

        this._propertyConfigurations = data.propertyConfigurations || [];

        setProperties(this);
    }

    toString() {

        return this.augId;
    }

    $prepare(propNames) {

        const promises = (Array.isArray(propNames) ? propNames : [propNames]).map(propName => {
            return this.$prepareSingle(propName);
        });

        return Promise.all(promises);
    }

    $prepareSingle(propName) {

        const _propName = `_${propName}`;

        if(!this.hasOwnProperty(propName)) {
            // There is no such property.
            throw new Error(`ProgramElement::$prepare(): Invalid argument: ${propName}`);
        }
        else if(!this.hasOwnProperty(_propName)) {
            // Property exists and is simple.
            return Promise.resolve(this[propName]);
        }
        else if(this[_propName].promise !== null) {
            // Property exists and has already been requested.
            return this[_propName].promise;
        }

        // Property exists and has not been requested yet.

        this[_propName].promise = new Promise((resolve, reject) => {
            this._apiMethod.call(Api, this.bundleId, this.analysisId, this.id, this[_propName].apiOptions)
                .then(results => {
                    results = results || [];
                    if(this[_propName].resultFilter) {
                        results = this[_propName].resultFilter(results, this);
                    }
                    this[propName] = results;
                    this[_propName].raw = results;
                    this[_propName].groupedByFile = results.reduce((acc, item) => {
                        if(!item.sourceFileName || !item.position) {
                            // nop
                        }
                        else if(!acc[item.sourceFileName]) {
                            acc[item.sourceFileName] = [item];
                        }
                        else {
                            acc[item.sourceFileName].push(item);
                        }
                        return acc;
                    }, {});
                    resolve(results);
                })
                .catch(() => {
                    this[_propName].promise = null;
                    reject();
                });
        });

        return this[_propName].promise;
    }

    isFunctional(prop) {

        //const prop = this.props.find(prop => prop.name === propId);

        return this.analysisId || !prop.isAnalysisBased;
    }

}

function setProperties(m) {

    m._propertyConfigurations.forEach(config => {
        // Create the property.
        m[config.name] = config.initValue;
        // Create the property's internal to "ProgramElement" data structures.
        m['_' + config.name] = {
            resultFilter:  config.resultFilter,
            apiOptions:    config.apiOptions,
            promise:       config.ready ? Promise.resolve(config.initValue) : null,
            raw:           config.initValue, // same as "m[config.name]"
            sortedByFile:  [],
            groupedByFile: config.initValue.reduce((acc, item) => {
                if(!item.sourceFileName || !item.position) {
                    // nop
                }
                else if(!acc[item.sourceFileName]) {
                    acc[item.sourceFileName] = [item];
                }
                else {
                    acc[item.sourceFileName].push(item);
                }
                return acc;
            }, {})
        };
    });
}
