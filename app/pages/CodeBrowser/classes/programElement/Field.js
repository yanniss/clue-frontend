import Api from 'app/classes/Api.static';
import Filters from './Filters.static';
import ProgramElement from './ProgramElement';

/**
 *
 */
export default class Field extends ProgramElement {

    constructor(bundleId, analysisId, data, baseValue) {

        if(data.kind !== 'Field') {
            throw new Error(`Field::constructor(): Invalid argument: ${data}`);
        }

        const d = Object.assign({}, data); // Do not override data

        d.icon = getIcon(data);
        d.iconSmall = d.icon;
        d.kind = getKind(data);
        d.cssClass = getCssClass(data);
        d.propertyConfigurations = getPropertyConfigurations(data, baseValue);

        super(bundleId, analysisId, d, Api.fetchFieldInfo);

        this.baseValue = baseValue;
    }

};

/* ******************************************* */

function getPropertyConfigurations(data, baseValue) {

    function filterOutSelf(results, programElement) {

        const pos1 = programElement.position;
        return results.filter(item => {
            const pos2 = item.position;
            return pos1.startLine   !== pos2.startLine
                || pos1.endLine     !== pos2.endLine
                || pos1.startColumn !== pos2.startColumn
                || pos1.endColumn   !== pos2.endColumn;
        });
    }

    const configs = [
        {
            default:      true,
            name:         'values',
            initValue:    [],
            apiOptions:   { values: true, baseValue: baseValue },
            isAnalysisBased: true,
            resultFilter: Filters.filterValues,
            title:        'Possible values',
            viewHeader:   '<div class="fa fa-sign-in fa-rotate-90" style="margin-left: 1px;"></div>',
        },
        {
            name:         'writeAccesses',
            initValue:    [],
            apiOptions:   { writeAccesses: true },
            resultFilter: (results, programElement) => {
                results = filterOutSelf(results, programElement);
                results.forEach(item => {
                    item.viewBody = {
                        main:       `position: (${item.position.startLine}, ${item.position.startColumn})`,
                        secondary:  '',
                    };
                });
                return results.sort((a, b) => a.sourceFileName.localeCompare(b.sourceFileName));
            },
            title:        'Write accesses',
            viewHeader:   '<div style="margin-left: -1px;">F<sub style="font-size: 11px; font-weight: bold;">W</sub></div>',
        },
        {
            name:         'readAccesses',
            initValue:    [],
            apiOptions:   { readAccesses: true },
            resultFilter: (results, programElement) => {
                results = filterOutSelf(results, programElement);
                results.forEach(item => {
                    item.viewBody = {
                        main:       `position: (${item.position.startLine}, ${item.position.startColumn})`,
                        secondary:  '',
                    };
                });
                return results.sort((a, b) => a.sourceFileName.localeCompare(b.sourceFileName));
            },
            title:        'Read accesses',
            viewHeader:   '<div style="margin-left: -1px;">F<sub style="font-size: 11px; font-weight: bold;">R</sub></div>',
        },
        {
            name:         'shadowedBy',
            initValue:    [],
            apiOptions:   { shadowedBy: true },
            isAnalysisBased: true,
            resultFilter: results => {
                results.forEach(item => {
                    const clazz = item.declaringClassDoopId.split('.');
                    item.viewBody = {
                        main:       clazz,
                        secondary:  item.position.startLine,
                    };
                });
                return results.sort((a, b) => a.sourceFileName.localeCompare(b.sourceFileName));
            },
            title:      'Shadowed by fields',
            viewHeader: '<div style="margin: 1px 0 0 1px;">F&#x2193;</div>',
        },
    ];

    if(data.usage) {
        const d = Object.assign({}, data);
        delete d.usage;
        d.viewBody = {
            main:       d.name,
            secondary:  d.sourceFileName.split('/').pop(),
        };
        configs.unshift({
            name:       'definition',
            initValue:  [d],
            ready:      true,
            title:      'Definition',
            viewHeader: '<div style="font-size: 13px; font-weight: bold; margin: 4px 0 0 -1px;">def</div>',
        });
    }

    return configs;
}


function getIcon(data) {

    return data.usage ? 'F' : 'F<sub class="pe-definition">def</sub>';
}


function getKind(data) {

    let kind;

    if(data.usage) {
        if(data.usage.usageKind === 'DATA_READ') {
            kind = 'Field access (read)';
        }
        else if(data.usage.usageKind === 'DATA_WRITE') {
            kind = 'Field access (write)';
        }
        else {
            kind = 'Field access (???)';
        }
    }
    else {
        kind = 'Field definition';
    }

    return kind;
}

function getCssClass(data) {

    let cssClass = 'pe-icon-field';

    if(data.usage) {
        if(data.usage.usageKind === 'DATA_READ') {
            cssClass += '-read';
        }
        else if(data.usage.usageKind === 'DATA_WRITE') {
            cssClass += '-write';
        }
    }
    else {
        cssClass += '-def';
    }

    return cssClass;
}
