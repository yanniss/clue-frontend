import Api from 'app/classes/Api.static';
import Filters from './Filters.static';
import ProgramElement from './ProgramElement';

/**
 *
 */
export default class Value extends ProgramElement {

    constructor(bundleId, analysisId, data) {

        if(data.kind !== 'Value') {
            throw new Error(`Value::constructor(): Invalid argument: ${data}`);
        }

        data = Object.assign({}, data); // Do not override data

        data.name = data.allocatedTypeDoopId;
        data.icon = '<div style="margin-top: 6px; font-size: 20px;">new</div>';
        data.iconSmall = 'new';
        data.kind = getKind(data);
        data.cssClass = getCssClass(data);
        data.propertyConfigurations = getPropertyConfigurations(data);

        super(bundleId, analysisId, data, Api.fetchValueInfo);
    }

};

/* ******************************************* */

function getPropertyConfigurations(data) {

    const config = [
        {
            name:         'fields',
            initValue:    [],
            apiOptions:   { fields: true },
            isAnalysisBased: true,
            resultFilter: results => {
                results.forEach(item => {
                    let clazz = item.declaringClassDoopId.split('.').pop();
                    item.viewBody = {
                        main:       item.name,
                        secondary:  clazz + ': ' + item.position.startLine,
                    };
                });
                return results.sort((a, b) => a.sourceFileName.localeCompare(b.sourceFileName));
            },
            title:        'Fields of instantiated type',
            viewHeader:   '<div style="margin: 1px 0 0 3px;">F</div>',
        },
    ];

    if(data.isArray) {
        config.push({
            name:         'arrayValues',
            initValue:    [],
            apiOptions:   { arrayValues: true },
            isAnalysisBased: true,
            resultFilter: Filters.filterValues,
            title:        'Possible values in array',
            viewHeader:   '<div style="font-size: 14px; margin: 2px 0 0 0;">[&nbsp;<i style="font-family: Serif">i</i>&nbsp;]</div>',
        });
    }

    return config;
}

function getKind(data) {

    return 'Object instantiation';
}

function getCssClass(data) {

    return '';
}
