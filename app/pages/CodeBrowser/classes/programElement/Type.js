import Api from 'app/classes/Api.static';
import Filters from './Filters.static';
import ProgramElement from './ProgramElement';

/**
 *
 */
export default class Type extends ProgramElement {

    constructor(bundleId, analysisId, data) {

        if(data.kind !== 'Class') {
            throw new Error(`Type::constructor(): Invalid argument: ${data}`);
        }

        let d = Object.assign({}, data); // Do not override data

        d.icon = getIcon(data);
        d.iconSmall = d.icon;
        d.kind = getKind(data);
        d.cssClass = getCssClass(data);
        d.propertyConfigurations = getPropertyConfigurations(data);

        super(bundleId, analysisId, d, Api.fetchTypeInfo);
    }

};

/* ******************************************* */

function getPropertyConfigurations(data) {

    const configs = [
        {
            name:         'fieldsOfType',
            initValue:    [],
            apiOptions:   { fieldsOfType: true },
            isAnalysisBased: true,
            resultFilter: results => {
                results.forEach(item => {
                    const clazz = item.declaringClassDoopId.split('.').pop();
                    item.viewBody = {
                        main:       item.name,
                        secondary:  `${clazz}: ${item.position.startLine}`,
                    };
                });
                return results.sort((a, b) => a.sourceFileName.localeCompare(b.sourceFileName));
            },
            title:        'Declared fields of this type',
            viewHeader:   '<div style="margin-left: -2px;">F<sub style="font-size: 10px; font-weight: bold; margin-left: -3px;"><i>def</i></sub></div>',
        },
        {
            name:         'varsOfType',
            initValue:    [],
            apiOptions:   { varsOfType: true },
            isAnalysisBased: true,
            resultFilter: results => {
                results.forEach(item => {
                    const matches = item.doopId.match(/^<([^:]*): [^\s]+ ([^(]+).*$/),
                        clazz = matches[1] && matches[1].split('.').pop(),
                        method = matches[2];
                    item.viewBody = {
                        main:       item.name,
                        secondary:  `${clazz}.${method}: ${item.position.startLine}`,
                    };
                });
                return results.sort((a, b) => a.sourceFileName.localeCompare(b.sourceFileName));
            },
            title:        'Declared variables of this type',
            viewHeader:   '<div style="margin-left: -2px;">V<sub style="font-size: 10px; font-weight: bold; margin-left: -3px;"><i>def</i></sub></div>',
        },
        {
            name:         'superTypes',
            initValue:    [],
            apiOptions:   { superTypes: true },
            isAnalysisBased: false,
            resultFilter: results => {
                results.forEach(item => {
                    const clazz = item.name || item.doopId.split('.').pop();
                    item.viewBody = {
                        main:       clazz,
                        secondary:  item.packageName,
                    };
                });
                return results.sort((a, b) => a.name.localeCompare(b.name));
            },
            title:        'Super types',
            viewHeader:   '<div style="margin: 1px 0 0 1px;">T&#x2191;</div>'
        },
        {
            name:         'subTypes',
            initValue:    [],
            apiOptions:   { subTypes: true },
            isAnalysisBased: false,
            resultFilter: results => {
                results.forEach(item => {
                    const clazz = item.name || item.doopId.split('.').pop();
                    item.viewBody = {
                        main:       clazz,
                        secondary:  item.packageName,
                    };
                });
                return results.sort((a, b) => a.name.localeCompare(b.name));
            },
            title:        'Sub types',
            viewHeader:   '<div style="margin: 1px 0 0 1px;">T&#x2193;</div>',
        },
    ];

    if(!data.isInterface && !data.isAbstract && !data.isEnum) {
        configs.unshift({
            name:         'allocatedAt',
            initValue:    [],
            apiOptions:   { allocatedAt: true },
            isAnalysisBased: true,
            resultFilter: Filters.filterValues,
            title:        'Instantiations',
            viewHeader:   '<div style="font-size: 13px; font-weight: bold; margin: 4px 0 0 -4px;">new</div>',
        });
    }

    if(data.usage) {
        const d = Object.assign({}, data);
        delete d.usage;
        d.viewBody = {
            main:       d.name,
            secondary:  d.sourceFileName.split('/').pop(),
        };
        configs.unshift({
            name:       'definition',
            initValue:  [d],
            ready:      true,
            title:      'Definition',
            viewHeader: '<div style="font-size: 13px; font-weight: bold; margin: 4px 0 0 -1px;">def</div>',
        });
    }

    return configs;
}

function getIcon(data) {

    let icon;

    if(data.isInterface) {
        icon = 'I';
    }
    else if(data.isAbstract) {
        icon = 'C';
    }
    else if(data.isEnum) {
        icon = 'E';
    }
    else {
        icon = 'C';
    }

    return data.usage ? icon : `${icon}<sub class="pe-definition">def</sub>`;
}

function getKind(data) {

    let kind;

    if(data.isInterface) {
        kind = 'Interface';
    }
    else if(data.isAbstract) {
        kind = 'Abstract Class';
    }
    else if(data.isEnum) {
        kind = 'Enum';
    }
    else {
        kind = 'Class';
    }

    return kind + (data.usage ? ' use' : ' definition');
}

function getCssClass(data) {

    let cssClass = '';

    if(data.isInterface) {
        cssClass = 'pe-icon-interface';
    }
    else if(data.isAbstract) {
        cssClass = 'pe-icon-abstract-class';
    }
    else if(data.isEnum) {
        cssClass = 'pe-icon-enum';
    }
    else {
        cssClass = 'pe-icon-class';
    }

    if(!data.usage) {
        cssClass += '-def';
    }

    return cssClass;
}
