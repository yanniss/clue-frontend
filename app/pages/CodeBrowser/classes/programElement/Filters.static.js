
/**
 *
 */
export default class Filters {

    /**
     * Assumptions being made when managing a value's properties:
     * 1) If "!!value.position == true" then "position" has to be a valid position
     *    object, that is having properties "startLine", "startColumn", etc.
     * 2) If !!value.sourceFileName == true" then "sourceFileName" must be a string.
     *
     * @param results
     * @returns {*}
     */
    static filterValues(results) {

        // Value kinds in the order they will appear in the view.
        const kinds = [
            'NORMAL_ALLOCATION',//pos
            'TAINT',//desc & pos
            'MOCK',//desc & pos
            'METAOBJECT',//desc & pos
            'MERGED_ALLOCATION',//desc
            'PRIMITIVE_VALUE',//desc
            'UNKNOWN'//desc
        ];

        // Group values by kind and set "viewBody" property for each value.
        const valuesPerKind = results.reduce((acc, item) => {

            const matches = item.doopId.match(/^<([^:]*): [^\s]+ ([^(]+).*>\/new ([^/]*).*$/);
            let main = '',
                secondary = '';

            if(matches) {
                const inClass = matches[1] ? matches[1].split('.').pop() : '',
                    inMethod = matches[2] || '',
                    allocType = matches[3] || item.allocatedTypeDoopId;
                main = allocType ? `new ${allocType}` : '<unknown:1>';
                secondary = inClass;
                if(inClass && inMethod) {
                    secondary += `.${inMethod}`;
                }
                if(secondary && item.position) {
                    secondary += `: ${item.position.startLine}`;
                }
            }
            else if(item.allocatedTypeDoopId) {
                main = item.valueKind === 'MOCK' ? item.description : `new ${item.allocatedTypeDoopId}`;
                if(item.sourceFileName && item.position) {
                    secondary = item.position.startLine;
                }
            }
            else {
                main = item.description || '<unknown:2>';
                if(item.sourceFileName && item.position) {
                    secondary = item.position.startLine;
                }
            }

            item.viewBody = { main: main, secondary: secondary };
            acc[item.valueKind].push(item);

            return acc;

        }, kinds.reduce((acc, kind) => { acc[kind] = []; return acc; }, {}));

        // Merge the per-kind value arrays into one.
        return kinds.reduce((acc, kind) => {
            if(valuesPerKind[kind].length > 0) {
                // If the array has values, then sort it and append it to the
                // already processed values.

                return acc.concat(valuesPerKind[kind].sort((a, b) => {
                    if(!a.sourceFileName !== !b.sourceFileName) {
                        // Exactly one item has "sourceFileName".
                        return a.sourceFileName ? -1 : 1;
                    }
                    // Both either have or don't have "sourceFileName".
                    else if(!a.sourceFileName) {
                        // Neither has "sourceFileName".
                        return (a.description || '').localeCompare(b.description);
                    }
                    // Both have "sourceFileName".
                    else if(a.sourceFileName !== b.sourceFileName) {
                        return a.sourceFileName.localeCompare(b.sourceFileName);
                    }
                    // Both have the same "sourceFileName".
                    else if(a.position && b.position) {
                        // Both have "position".
                        return a.position.startLine - b.position.startLine;
                    }
                    // Only one of them has "position".
                    else {
                        return a.position ? -1 : 1;
                    }
                }));
            }
            return acc;
        }, []);
    }

};
