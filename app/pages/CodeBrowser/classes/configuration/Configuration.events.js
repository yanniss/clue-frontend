import str from 'core/classes/StringHelper.static';

export const CONFIGURATION_CREATED = str.createUnique();
export const CONFIGURATION_RENAMED = str.createUnique();
export const CONFIGURATION_DELETED = str.createUnique();
export const CONFIGURATION_ACTIVATED = str.createUnique();

export const CONFIGURATION_OPEN = str.createUnique();
export const CONFIGURATION_OPEN_CREATION_FORM = str.createUnique();
export const CONFIGURATION_OPEN_RENAME_FORM = str.createUnique();
export const CONFIGURATION_OPEN_DELETION_VERIFICATION = str.createUnique();
export const CONFIGURATION_OPEN_ANALYSIS_FORM = str.createUnique();
