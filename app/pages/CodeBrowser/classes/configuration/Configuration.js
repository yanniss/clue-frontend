import ko from 'knockout';
import Api from 'app/classes/Api.static';
import Notifier from 'app/classes/Notifier.static';
import { createActions, orderActions } from './Configuration.view';
import {
    CONFIGURATION_OPEN_RENAME_FORM,
    CONFIGURATION_OPEN_DELETION_VERIFICATION,
    CONFIGURATION_OPEN_ANALYSIS_FORM,
    CONFIGURATION_CREATED,
    CONFIGURATION_RENAMED,
    CONFIGURATION_DELETED,
} from './Configuration.events';
import Analysis from 'codebrowser/classes/analysis/Analysis';
import $ from 'jquery';
import {RULE_CREATED, RULE_DELETED, RULE_EDITTED} from 'codebrowser/components/Configuration/Configuration.events';
import Rule from 'codebrowser/classes/Rule';

/**
 *
 */
export default class Configuration {

    id;
    created;
    createdDate;
    name = ko.observable();
    modified = ko.observable();
    modifiedDate = ko.observable();

    actions = createActions(this);
    orderedActions = orderActions(this.actions);

    analysis = ko.observable();
    dirty = ko.observable(false);
    hasPendingRequest = ko.observable(false);

    constructor(data, manager) {

        this.bundleId = data.bundleId;
        this.id = data.id;
        this.created = data.created.replace('T', ' ').replace(/\+[0-9]{4}/, '');
        this.createdDate = new Date(data.created);
        this.update(data);

        this.manager = manager;
    }

    dispose() {

    }

    toString() {

        return this.id;
    }

    refresh() {

        this.hasPendingRequest(true);

        return Api.fetchConfiguration(this.bundleId, this.name())
            .then(configurationData => {
                this.update(configurationData);
                return this;
            })
            .finally(() =>
                this.hasPendingRequest(false)
            );
    }

    ////////////////////////////////////////////////////////////////

    hasRunningAnalysis() {

        const analysis = this.analysis();

        return analysis ? analysis.isRunning() : false;
    }

    hasFinishedAnalysis() {

        const analysis = this.analysis();

        return analysis ? analysis.isFinished() : false;
    }

    ////////////////////////////////////////////////////////////////

    canRunAnalysis() {

        return !this.hasPendingRequest() && !this.hasRunningAnalysis() && this.dirty();
    }

    openAnalysisForm() {

        if(!this.canRunAnalysis()) { return; }

        this.manager.pubsub.emit(CONFIGURATION_OPEN_ANALYSIS_FORM, this);
    }

    runAnalysis($form) {

        if(!this.canRunAnalysis()) { return Promise.reject(); }

        this.hasPendingRequest(true);

        return Api.createAnalysis(this.bundleId, this.name(), $form)
            .then(async analysisId => {
                Notifier.success('Analysis started successfully');
                const analysis = new Analysis(await Api.fetchAnalysis(this.bundleId, analysisId), this);
                this.analysis(analysis);
                return analysis;
            })
            .catch(error => {
                Notifier.error('Failed to create analysis');
                throw error;
            })
            .finally(() =>
                this.hasPendingRequest(false)
            );
    }

    ////////////////////////////////////////////////////////////////

    canStopAnalysis() {

        return this.hasRunningAnalysis();
    }

    stopAnalysis() {

        if(!this.canStopAnalysis()) { return; }

        this.hasPendingRequest(true);

        return this.analysis().stop()
            .finally(() =>
                this.hasPendingRequest(false)
            );
    }

    ////////////////////////////////////////////////////////////////

    canRepackage() {

        return !this.hasPendingRequest() && !this.hasRunningAnalysis();
    }

    /**
     * todo: promise returned should fulfill only after the file download has finished successfully; not when it starts.
     *
     * check alternatives to download file:
     * https://github.com/eligrey/FileSaver.js/tree/master/src
     * https://stackoverflow.com/a/33542499/4546394
     * https://stackoverflow.com/a/41804224/4546394
     *
     */
    repackage() {

        if(!this.canRepackage()) { return; }

        this.hasPendingRequest(true);

        return Api.downloadOptimizedApk(this.bundleId, this)
            .then(() =>
                '<< todo ... >>'
            )
            .catch(error => {
                Notifier.error('Failed to repackage APK');
                throw error;
            })
            .finally(() =>
                this.hasPendingRequest(false)
            );
    }

    ////////////////////////////////////////////////////////////////

    canBeRenamed() {

        return !this.hasPendingRequest() && !this.hasRunningAnalysis();
    }

    openRenameForm() {

        if(!this.canBeRenamed()) { return; }

        this.manager.pubsub.emit(CONFIGURATION_OPEN_RENAME_FORM, this);
    }

    rename(newName) {

        if(!this.canBeRenamed()) { return Promise.reject(); }

        this.hasPendingRequest(true);

        return Api.renameConfiguration(this.bundleId, this, newName)
            .then(newName => {
                Notifier.success(`Configuration renamed successfully from "${this.name()}" to "${newName}"`);
                this.name(newName);
                this.manager.pubsub.emit(CONFIGURATION_RENAMED, this);
                return newName;
            })
            .catch(error => {
                Notifier.error(`Failed to rename configuration "${this.name()}" to "${newName}"`);
                throw error;
            })
            .finally(() =>
                this.hasPendingRequest(false)
            );
    }

    ////////////////////////////////////////////////////////////////

    canBeExported() {

        return !this.hasPendingRequest() && !this.hasRunningAnalysis();
    }

    export() {

        if(!this.canBeExported()) { return; }

        this.hasPendingRequest(true);

        return Api.downloadDirectivesFile(this.bundleId, this)
            .then(() => {
                Notifier.success(`Configuration exported successfully: ${this.name()}`);
                return '<< todo ... >>';
            })
            .catch(error => {
                Notifier.error('Failed to download clue file');
                throw error;
            })
            .finally(() =>
                this.hasPendingRequest(false)
            );
    }

    ////////////////////////////////////////////////////////////////

    canBeCloned() {

        return !this.hasPendingRequest();
    }

    clone() {

        if(!this.canBeCloned()) { return; }

        this.hasPendingRequest(true);

        return Api.cloneConfiguration(this.bundleId, this)
            .then(configurationData => {
                const configuration = new Configuration(configurationData, this.manager);
                Notifier.success(`Configuration cloned successfully to: ${configuration.name()}`);
                this.manager.pubsub.emit(CONFIGURATION_CREATED, configuration);
                return configuration;
            })
            .catch(error => {
                Notifier.error(`Failed to clone configuration: ${this.name()}. Cannot have multiple configurations with the same name`);
                throw error;
            })
            .finally(() =>
                this.hasPendingRequest(false)
            );
    }

    ////////////////////////////////////////////////////////////////

    canBeDeleted() {

        return !this.hasPendingRequest() && !this.hasRunningAnalysis() &&
            this.manager.getConfigurationsAsObservableArray()().length > 1;
    }

    openDeleteVerification() {

        if(!this.canBeDeleted()) { return; }

        this.manager.pubsub.emit(CONFIGURATION_OPEN_DELETION_VERIFICATION, this);
    }

    delete() {

        if(!this.canBeDeleted()) { return; }

        this.hasPendingRequest(true);

        return Api.deleteConfiguration(this.bundleId, this.name())
            .then(() => {
                Notifier.success(`Configuration deleted successfully: ${this.name()}`);
                this.manager.pubsub.emit(CONFIGURATION_DELETED, this.id);
                return this.id;
            })
            .catch(error => {
                Notifier.error(`Failed to delete configuration: ${this.name()}`);
                throw error;
            })
            .finally(() =>
                this.hasPendingRequest(false)
            );
    }

    ////////////////////////////////////////////////////////////////

    canModifyRuleSet() {

        return !this.hasPendingRequest() && !this.hasRunningAnalysis();
    }

    addRule(ruleBody, comment) {

        if(!this.canModifyRuleSet()) { return; }

        this.hasPendingRequest(true);

        return Api.addRule(this.bundleId, this.name(), ruleBody, comment)
            .then(ruleData => {
                const rule = new Rule(ruleData);
                Notifier.success('Rule added successfully');
                this.manager.pubsub.emit(RULE_CREATED, rule);
                this.refresh();
                return rule;
            })
            .catch(error => {
                Notifier.error('Failed to add rule');
                throw error;
            })
            .finally(() =>
                this.hasPendingRequest(false)
            );
    }

    addRules(origin, ruleBodies) {

        if(!this.canModifyRuleSet()) { return; }

        const $form = $(`<form>
            <input type="text" name="origin" value="${origin}">
            ${ruleBodies.reduce((acc, ruleBody) =>
                acc + `<input type="text" name="rules" value="${ruleBody.rule}">\n`
            , '')}
        </form>`);

        this.hasPendingRequest(true);

        return Api.postBatchRules(this.bundleId, this.name(), $form)
            .then(rulesData => {
                const rules = rulesData.rules.map(ruleData => new Rule(ruleData));
                Notifier.success(`Rules added successfully to configuration: ${this.name()}`);
                // We do not call "this.hasPendingRequest(false);" since it is managed inside "this.refresh();"
                this.manager.pubsub.emit(RULE_CREATED, rules);
                this.refresh();
                return rules;
            })
            .catch(error => {
                Notifier.error(`Failed to add rules to configuration: ${this.name()}`);
                this.hasPendingRequest(false);
                throw error;
            });
    }

    editRule(rule, newRuleBody, newComment) {

        if(!this.canModifyRuleSet()) { return; }

        this.hasPendingRequest(true);

        return rule.update(this, newRuleBody, newComment)
            .then(() => {
                this.manager.pubsub.emit(RULE_EDITTED, rule);
                this.refresh();
                return rule;
            })
            .finally(() =>
                this.hasPendingRequest(false)
            );
    }

    deleteRule(rule) {

        if(!this.canModifyRuleSet()) { return; }

        this.hasPendingRequest(true);

        return rule.delete(this)
            .then(() => {
                this.manager.pubsub.emit(RULE_DELETED, rule.id);
                this.refresh();
                return rule.id;
            })
            .finally(() =>
                this.hasPendingRequest(false)
            );
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bundleId;
    manager;

    update({ nameOfSet, currentAnalysis, modified, dirty }) {

        this.name(nameOfSet);
        this.modified(modified.replace('T', ' ').replace(/\+[0-9]{4}/, ''));
        this.modifiedDate(new Date(modified));
        this.dirty(dirty);

        if(currentAnalysis && (!this.analysis() || this.analysis().id !== currentAnalysis.id)) {
            this.analysis(new Analysis(currentAnalysis, this));
        }
    }

};
