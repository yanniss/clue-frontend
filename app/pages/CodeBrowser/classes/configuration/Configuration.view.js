
// Uses: fontawesome v4 icons
// Uses: bootstrap 3 "hide" class

export const createActions = configuration => ({

    runAnalysis: {
        name: 'runAnalysis',
        execute: () => configuration.openAnalysisForm(),
        text: () => 'Analyze',
        title: () => "Analyze bundle based on configuration's user defined rules",
        disabled: () => !configuration.canRunAnalysis(),
        cssIcon: () => ' fa fa-search ',
        cssDisabled: function() { return this.disabled()  ? ' disabled ' : ''; },
        cssActive: () => '',
    },

    stopAnalysis: {
        name: 'stopAnalysis',
        execute: () => configuration.stopAnalysis(),
        text: () => 'Stop analysis',
        title: () => 'Stop analysis',
        disabled: () => !configuration.canStopAnalysis(),
        cssIcon: () => ' fa fa-stop ',
        cssDisabled: function() { return this.disabled()  ? ' disabled ' : ''; },
        cssActive: () => '',
    },

    repackage: {
        name: 'repackage',
        execute: () => configuration.repackage(),
        text: () => 'Repackage',
        title: () => "Repackage bundle based on configuration's effective directives and download it",
        disabled: () => configuration.hasPendingRequest() || configuration.hasRunningAnalysis(),
        cssIcon: () => ' fa fa-magic ', // plug, magic, gavel, fire, flask, cart-arrow-down
        cssDisabled: function() { return this.disabled() ? ' disabled ' : ''; },
        cssActive: () => '',
    },

    rename: {
        name: 'rename',
        execute: () => configuration.openRenameForm(),
        text: () => 'Rename',
        title: () => 'Rename configuration',
        disabled: () => configuration.hasPendingRequest() || configuration.hasRunningAnalysis(),
        cssIcon: () => ' fa fa-edit ',
        cssDisabled: function() { return this.disabled() ? ' disabled ' : ''; },
        cssActive: () => '',
    },

    export: {
        name: 'export',
        execute: () => configuration.export(),
        text: () => 'Export',
        title: () => "Export configuration's user defined rules",
        disabled: () => configuration.hasPendingRequest() || configuration.hasRunningAnalysis(),
        cssIcon: () => ' fa fa-download ',
        cssDisabled: function() { return this.disabled() ? ' disabled ' : ''; },
        cssActive: () => '',
    },

    clone: {
        name: 'clone',
        execute: () => configuration.clone(),
        text: () => 'Clone',
        title: () => 'Clone configuration',
        disabled: () => configuration.hasPendingRequest(),
        cssIcon: () => ' fa fa-clone ',
        cssDisabled: function() { return this.disabled() ? ' disabled ' : ''; },
        cssActive: () => '',
    },

    delete: {
        name: 'delete',
        execute: () => configuration.openDeleteVerification(),
        text: () => 'Delete',
        title: () => 'Delete configuration',
        disabled: () => configuration.hasPendingRequest() || configuration.hasRunningAnalysis(),
        cssIcon: () => ' fa fa-trash ',
        cssDisabled: function() { return this.disabled() ? ' disabled ' : ''; },
        cssActive: () => '',
    },

});

export const orderActions = actions => [
    actions.runAnalysis,
    actions.repackage,
    actions.export,
    actions.rename,
    actions.clone,
    actions.delete,
];
