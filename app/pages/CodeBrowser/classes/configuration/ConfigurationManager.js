import Api from 'app/classes/Api.static';
import Configuration from './Configuration';
import Notifier from 'app/classes/Notifier.static';
import ko from 'knockout';
import { assert } from 'core/classes/Assert';
import {
    CONFIGURATION_CREATED,
    CONFIGURATION_DELETED,
    CONFIGURATION_ACTIVATED,
    CONFIGURATION_OPEN
} from './Configuration.events';

/**
 *
 */
export default class ConfigurationManager {

    constructor(bundleId, pubsub) {

        this.bundleId = bundleId;
        this.pubsub = pubsub;

        this.LS_KEY_ACTIVE_CONFIGURATION = this.LS_KEY_ACTIVE_CONFIGURATION.replace('<bundle_id>', bundleId);

        this.startListeningToPageEvents();
    }

    dispose() {

        this.stopListeningToPageEvents();

        this.configurationsObsArray().forEach(configuration => configuration.dispose());
    }

    ////////////////////////////////////////////////////////////////

    getConfigurations() {

        return this.fetchConfigurations();
    }

    getConfigurationsAsObservableArray() {

        this.fetchConfigurations()
            .catch(() => {});

        return this.configurationsObsArray;
    }

    ////////////////////////////////////////////////////////////////

    activateConfiguration(configuration) {

        if(configuration === this.activeConfiguration) {
            this.pubsub.emit(CONFIGURATION_OPEN, configuration);
            return;
        }

        this.activeConfiguration = configuration;
        localStorage.setItem(this.LS_KEY_ACTIVE_CONFIGURATION, configuration.id);

        configuration.refresh()
            .then(() => {
                this.pubsub.emit(CONFIGURATION_ACTIVATED, configuration);
                this.pubsub.emit(CONFIGURATION_OPEN, configuration);
            });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    LS_KEY_ACTIVE_CONFIGURATION = 'codebrowser:<bundle_id>:active_configuration_id';
    bundleId;
    pubsub;
    fetchRequest;
    configurationsMap = {};
    configurationsObsArray = ko.observableArray();
    activeConfiguration;

    startListeningToPageEvents() {

        ////////////////////////////////////////////////////////////////

        this.pubsub.on(CONFIGURATION_CREATED, configuration => {

            assert(!this.configurationsMap.hasOwnProperty(configuration),
                `ConfigurationManager: CONFIGURATION_CREATED handler: configuration already registered: ${configuration.name()}`);

            this.configurationsMap[configuration] = configuration;
            this.configurationsObsArray.unshift(configuration);
            this.activateConfiguration(configuration);

        }, this);

        ////////////////////////////////////////////////////////////////

        this.pubsub.on(CONFIGURATION_DELETED, configurationId => {

            const [ configuration ] = this.configurationsObsArray.remove(configuration =>
                configuration.id === configurationId
            );

            assert(configuration,
                `ConfigurationManager: CONFIGURATION_DELETED handler: configuration not registered: ${configurationId}`);

            if(this.activeConfiguration.id === configurationId) {
                this.activateConfiguration(this.configurationsObsArray()[0]);
            }

            delete this.configurationsMap[configurationId];
            configuration.dispose();

        }, this);
    }

    stopListeningToPageEvents() {

        this.pubsub.off(CONFIGURATION_CREATED, null, this);
        this.pubsub.off(CONFIGURATION_DELETED, null, this);
    }

    fetchConfigurations() {

        if(this.fetchRequest === 'completed') {
            return Promise.resolve(this.configurationsObsArray());
        }

        const storedConfigurationId = localStorage.getItem(this.LS_KEY_ACTIVE_CONFIGURATION);

        this.fetchRequest = this.fetchRequest || Api.fetchConfigurations(this.bundleId)
            .then(configurationsData => configurationsData
                .map(configurationData => new Configuration(configurationData, this))
                .sort((a, b) => b.createdDate - a.createdDate)
            )
            .then(configurations => {

                assert(configurations.length > 0, 'Requirement "there is always at least one configuration" not satisfied');

                for(const configuration of configurations) {
                    this.configurationsMap[configuration] = configuration;
                    if(configuration.id === storedConfigurationId) {
                        this.activateConfiguration(configuration);
                    }
                }

                // If there was no (matching) configuration id stored in local storage, activate the most recent
                // configuration.
                if(!this.activeConfiguration) {
                    this.activateConfiguration(configurations[0]);
                }

                this.configurationsObsArray(configurations);
                this.fetchRequest = 'completed';
                return configurations;
            })
            .catch(error => {
                this.fetchRequest = null;
                Notifier.error('Failed to fetch configurations');
                throw error;
            });

        return this.fetchRequest;
    }

};
