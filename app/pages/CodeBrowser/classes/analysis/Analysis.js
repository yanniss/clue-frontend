import ko from 'knockout';
import Api from 'app/classes/Api.static';
import Notifier from 'app/classes/Notifier.static';
import PromiseQueue from 'core/classes/PromiseQueue';
import { createActions, orderActions } from './Analysis.view';
import { ANALYSIS_DELETED, ANALYSIS_OPEN_DELETION_VERIFICATION } from './Analysis.events';
import {CONFIGURATION_ACTIVATED} from "codebrowser/classes/configuration/Configuration.events";

/**
 * Disable analysis monitoring and local storage.
 */
const ENABLE_UI_BUTTON_TOGGLE_ANALYSIS_MONITORING = (() => {

    const key = '__enable_ui_button_toggle_analysis_monitoring',
        value = localStorage.getItem(key);

    // Set the following local storage value, simply in order to present the option to the user.
    value === null && localStorage.setItem(key, 'HINT: set value to "true" and refresh to show ui button');

    return value === 'true';
})();

/**
 * Analysis monitoring polling interval and local storage.
 */
const POLLING_INTERVAL = (() => {

    const key = '__analysis_monitoring_polling_interval_in_seconds',
        value = localStorage.getItem(key);

    // Set the following local storage value, simply in order to present the option to the user.
    value === null && localStorage.setItem(key, 'HINT: set a value and refresh to update');

    return (parseInt(value) || 10) * 1000;
})();

/**
 * Possible analysis states: [ ERROR, READY, RUNNING, CANCELLED, TIMED_OUT, COMPLETED, POST_PROCESSING, FINISHED ]
 */
export default class Analysis {

    id;
    name;
    options;
    created;
    createdDate;
    configurationName;
    state = ko.observable();
    cpu = ko.observable('-');
    memory = ko.observable('-');
    hasPendingRequest = ko.observable(false);

    constructor(data, configuration) {

        this.bundleId = data.bundleId;
        this.id = data.id;
        this.name = data.displayName || data.anName;
        this.options = data.anOptions;
        this.created = data.created;
        this.createdDate = new Date(data.created);
        this.configurationName = `${this.name}.clue`;
        this.state(data.state);

        this.configuration = configuration;

        this.startMonitoring();
    }

    dispose() {

        this.stopMonitoring();
    }

    toString() {

        return this.id;
    }

    ////////////////////////////////////////////////////////////////

    isRunning() {

        return ['RUNNING', 'COMPLETED', 'POST_PROCESSING'].includes(this.state());
    }

    isFinished() {

        return this.state() === 'FINISHED';
    }

    ////////////////////////////////////////////////////////////////

    canBeStopped() {

        return this.isRunning();
    }

    stop() {

        if(this.hasPendingRequest() || !this.canBeStopped()) { return; }

        this.hasPendingRequest(true);

        return Api.executeAnalysisAction(this.bundleId, this.id, 'stop')
            .then(() => {
                this.stopMonitoring(true);
                this.hasPendingRequest(false);
                return true;
            })
            .catch(error => {
                Notifier.error('Failed to stop analysis');
                this.hasPendingRequest(false);
                throw error;
            });
    }

    ////////////////////////////////////////////////////////////////

    startMonitoring(immediately = false, maxSteps = Number.MAX_SAFE_INTEGER) {

        if(!this.canBeMonitored() || this.isMonitored()) { return; }

        let steps = 0;

        const request = () => {

            if(++steps > maxSteps) {
                this.stopMonitoring();
                return;
            }

            const promise = this.updateStateWithUserReport(),
                thener = () => {
                    if(this.isFinished() || !this.isRunning()) {
                        this.stopMonitoring();
                    }
                },
                catcher = () => {
                    this.stopMonitoring();
                    Notifier.error(`Stopped monitoring analysis running state: ${this.name}. Please refresh the page.`);
                };

            this.requests.push(promise, thener, catcher);
        };

        immediately && request();
        this.intervalId(setInterval(request, POLLING_INTERVAL));
    }

    stopMonitoring(updateBeforeStop = false) {

        const wasUnderMonitoring = this.isMonitored();

        clearInterval(this.intervalId());
        this.requests.clear();
        this.intervalId(0);

        if(wasUnderMonitoring && updateBeforeStop) {
            this.startMonitoring(true, 1);
            clearInterval(this.intervalId());
            this.requests.clear();
            this.intervalId(0);
        }
    }

    toggleMonitoring() {

        this.isMonitored() ? this.stopMonitoring(true) : this.startMonitoring(true);
    }

    canBeMonitored() {

        return this.isRunning();
    }

    isMonitored() {

        return this.intervalId() !== 0;
    }

    isToggleMonitoringUiButtonEnabled() {

        return ENABLE_UI_BUTTON_TOGGLE_ANALYSIS_MONITORING && this.canBeMonitored();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bundleId;
    requests = new PromiseQueue();
    intervalId = ko.observable(0);

    async updateStateWithUserReport() {

        await this.updateState();

        if(this.isFinished()) {
            Notifier.success(`Analysis has finished: ${this.name}`);
            this.configuration.refresh();
        }
        else if(!this.isRunning()) {
            switch(this.state()) {
                case 'ERROR':
                    Notifier.error(`Analysis has failed: ${this.name}`);
                    break;
                case 'CANCELLED':
                    Notifier.warning(`Analysis was cancelled: ${this.name}`);
                    break;
                case 'TIMED_OUT':
                    Notifier.warning(`Analysis has timed out: ${this.name}`);
                    break;
                default:
                    throw new Error(`${this.name}: impossible analysis state: ${this.state()}`);
            }
        }
    };

    async updateState() {

        const { state } = await Api.fetchAnalysis(this.bundleId, this.id, true);
        this.state(state);

        if(this.isRunning()) {
            const { CPU_PER, RES } = await Api.fetchAnalysisRuntime(this.bundleId, this.id);
            this.cpu(Analysis.formatCpuUsage(CPU_PER));
            this.memory(Analysis.formatMemoryUsage(RES));
        }
    };

    static formatCpuUsage(percentage) {

        percentage = parseInt(percentage);

        return Number.isNaN(percentage) ? '-' : `${percentage}%`;
    }

    static formatMemoryUsage(MBs) {

        MBs = parseInt(MBs);

        if(Number.isNaN(MBs)) {
            return '-';
        }
        else if(MBs < 1024) {
            return `${MBs}MB`;
        }
        else if(MBs < 1048576) { // 1024*1024
            return `${(MBs/1024).toFixed(1)}GB`;
        }
        else {
            return `${(MBs/1048576).toFixed(1)}TB`;
        }
    }

};
