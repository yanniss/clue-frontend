import Api from 'app/classes/Api.static';
import Analysis from './Analysis';
import Notifier from 'app/classes/Notifier.static';
import ko from 'knockout';
import { assert } from 'core/classes/Assert';
import { ANALYSIS_CREATED, ANALYSIS_DELETED } from './Analysis.events';

/**
 *
 */
export default class AnalysisManager {

    constructor(bundleId, pubsub) {

        this.bundleId = bundleId;
        this.pubsub = pubsub;

        this.startListeningToPageEvents();
    }

    dispose() {

        this.stopListeningToPageEvents();

        this.analysesObsArray().forEach(analysis => analysis.dispose());
    }

    ////////////////////////////////////////////////////////////////

    getAnalyses() {

        return this.fetchAnalyses();
    }

    getAnalysesAsObservableArray() {

        this.fetchAnalyses()
            .catch(() => {});

        return this.analysesObsArray;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bundle;
    pubsub;
    fetchRequest;
    analysisMap = {};
    analysesObsArray = ko.observableArray();

    startListeningToPageEvents() {

        ////////////////////////////////////////////////////////////////

        this.pubsub.on(ANALYSIS_CREATED, analysis => {

            assert(!this.analysisMap.hasOwnProperty(analysis),
                `AnalysisManager: ANALYSIS_CREATED handler: analysis already registered: ${analysis.name}`);

            this.analysisMap[analysis] = analysis;
            this.analysesObsArray.unshift(analysis);

        }, this);

        ////////////////////////////////////////////////////////////////

        this.pubsub.on(ANALYSIS_DELETED, analysisId => {

            const [ analysis ] = this.analysesObsArray.remove(analysis =>
                analysis.id === analysisId
            );

            assert(analysis,
                `AnalysisManager: ANALYSIS_DELETED handler: analysis not registered: ${analysisId}`);

            delete this.analysisMap[analysisId];
            analysis.dispose();

        }, this);
    }

    stopListeningToPageEvents() {

        this.pubsub.off(ANALYSIS_CREATED, null, this);
        this.pubsub.off(ANALYSIS_DELETED, null, this);
    }

    fetchAnalyses() {

        if(this.fetchRequest === 'completed') {
            return Promise.resolve(this.analysesObsArray());
        }

        this.fetchRequest = this.fetchRequest || Api.fetchAnalyses(this.bundleId)
            .then(analysesData => analysesData
                .map(analysisData => new Analysis(analysisData, this))
                .sort((a, b) => b.createdDate - a.createdDate)
            )
            .then(analyses => {
                for(const analysis of analyses) {
                    this.analysisMap[analysis] = analysis;
                }
                this.analysesObsArray(analyses);
                this.fetchRequest = 'completed';
                return analyses;
            })
            .catch(error => {
                this.fetchRequest = null;
                Notifier.error('Failed to fetch analyses');
                throw error;
            });

        return this.fetchRequest;
    }

};
