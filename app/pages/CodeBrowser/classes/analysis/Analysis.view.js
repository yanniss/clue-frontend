
// Uses: fontawesome v4 icons
// Uses: bootstrap 3 "hide" class

export const createActions = analysis => ({

    toggleMonitoring: {
        name: 'toggleMonitoring',
        execute: () => analysis.toggleMonitoring(),
        text: () => analysis.isMonitored() ? 'Stop monitoring' : 'Start monitoring',
        title: () => analysis.isMonitored() ? 'Stop monitoring analysis' : 'Start monitoring analysis',
        cssIcon: () => ' fa fa-eye ',
        cssHidden: () => analysis.isToggleMonitoringUiButtonEnabled() ? '' : ' hide ',
        cssDisabled: () => analysis.hasPendingRequest() ? ' disabled ' : '',
        cssActive: () => analysis.isMonitored() ? ' active ' : '',
    },

    stop: {
        name: 'stop',
        execute: () => analysis.stop(),
        text: () => 'Stop',
        title: () => 'Stop analysis',
        cssIcon: () => ' fa fa-stop ',
        cssHidden: () => analysis.canBeStopped() ? '' : ' hide ',
        cssDisabled: () => analysis.hasPendingRequest() ? ' disabled ' : '',
        cssActive: () => '',
    },

    rerun: {
        name: 'rerun',
        execute: () => analysis.rerun(),
        text: () => 'Rerun',
        title: () => 'Rerun analysis',
        cssIcon: () => ' fa fa-rotate-left ',
        cssHidden: () => analysis.canBeRerun() ? '' : ' hide ',
        cssDisabled: () => analysis.hasPendingRequest() ? ' disabled ' : '',
        cssActive: () => '',
    },

    delete: {
        name: 'delete',
        execute: () => analysis.delete(),
        text: () => 'Delete',
        title: () => 'Delete analysis',
        cssIcon: () => ' fa fa-trash ',
        cssHidden: () => analysis.canBeDeleted() ? '' : ' hide ',
        cssDisabled: () => analysis.hasPendingRequest() ? ' disabled ' : '',
        cssActive: () => '',
    },

});

export const orderActions = actions => [
    actions.toggleMonitoring,
    actions.stop,
    actions.rerun,
    //actions.delete,
];
