import str from 'core/classes/StringHelper.static';

export const ANALYSIS_CREATED = str.createUnique();
export const ANALYSIS_DELETED = str.createUnique();

export const ANALYSIS_OPEN = str.createUnique();
export const ANALYSIS_OPEN_CREATION_FORM = str.createUnique();
export const ANALYSIS_OPEN_DELETION_VERIFICATION = str.createUnique();
