import Field from './programElement/Field';
import Method from './programElement/Method';
import MethodInvocation from './programElement/MethodInvocation';
import ProgramElement from './programElement/ProgramElement';
import Query from './programElement/Query';
import Type from './programElement/Type';
import Value from './programElement/Value';
import Variable from './programElement/Variable';
import ko from 'knockout';

/**
 *
 */
export default class Selection {

    programElement;
    activeProp = ko.observable();

    /**
     * @param {string|null} bundleId
     * @param {string|null} analysisId
     * @param {object} data
     */
    constructor(bundleId, analysisId, data) {

        this.programElement = data instanceof ProgramElement
            ? data
            : Selection.createProgramElement(bundleId, analysisId, data);

        this.setDefaultActiveProp();
    }

    toString() {

        return this.programElement.toString();
    }

    activateProp(prop) {

        const programElement = this.programElement,
            activeProp = this.activeProp();

        if(!prop) {
            this.activeProp(null);
        }
        else if(!activeProp || prop.name !== activeProp.prop.name) {
            const propName = prop.name;

            if(activeProp) {
                activeProp.prop = programElement.props.find(prop => prop.name === propName);
            }
            else {
                this.activeProp({
                    prop: programElement.props.find(prop => prop.name === propName),
                    values: programElement[`_${prop.name}`],
                });
            }

            programElement.$prepare([ propName ]).then(() => {
                this.activeProp().values = programElement[`_${prop.name}`];
                this.activeProp.valueHasMutated();
            });
        }
    }

    copyActivePropFrom(selection) {

        const prop = selection.activeProp();

        if(!prop) { return; }

        this.activateProp(prop.prop);
    }

    containsQuery() {

        return this.programElement instanceof Query;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    setDefaultActiveProp() {

        const props = this.programElement.props.filter(prop => this.programElement.isFunctional(prop)),
            prop = props.find(prop => prop.default) || props[0];

        this.activateProp(prop);
    }

    static createProgramElement(bundleId, analysisId, data) {

        switch(data.kind) {
            case 'Class':
                return new Type(bundleId, analysisId, data);
            case 'Field':
                return new Field(bundleId, analysisId, data);
            case 'Method':
                return new Method(bundleId, analysisId, data);
            case 'Variable':
                return new Variable(bundleId, analysisId, data);
            case 'MethodInvocation':
                return new MethodInvocation(bundleId, analysisId, data);
            case 'Value':
                return new Value(bundleId, analysisId, data);
            case 'Query':
                return new Query(bundleId, analysisId, data);
            default:
                throw new Error(`Selection.createProgramElement: Invalid argument: ${data}`);
        }
    }

};
