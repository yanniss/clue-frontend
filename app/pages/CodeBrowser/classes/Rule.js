import ko from 'knockout';
import Api from 'app/classes/Api.static';
import Notifier from 'app/classes/Notifier.static';

/**
 *
 */
export default class Rule {

    bundleId;
    id;
    ruleBody = ko.observable('');
    ruleType;
    comment = ko.observable('');
    origin;
    originType;
    userIndex;
    matchingMethods = ko.observable(0);
    hasPendingOperation = ko.observable(false);

    constructor({ bundleId, id, ruleBody, ruleType, comment, origin, originType, userIndex, matchingMethods }) {

        this.bundleId = bundleId;
        this.id = id;
        this.ruleBody(ruleBody || '');
        this.ruleType = ruleType;
        this.comment(comment || '');
        this.origin = origin;
        this.originType = originType;
        this.userIndex = userIndex;
        this.matchingMethods(matchingMethods);
    }

    update(configuration, ruleBody, comment) {

        this.hasPendingOperation(true);

        return Api.updateRule(this.bundleId, configuration.name(), this.id, ruleBody, comment)
            .then(({ ruleBody, comment, matchingMethods }) => {
                Notifier.success('Rule updated successfully');
                this.ruleBody(ruleBody || '');
                this.comment(comment || '');
                this.matchingMethods(matchingMethods || 0);
                return this;
            })
            .catch(error => {
                Notifier.error('Failed to update rule');
                throw error;
            })
            .finally(() =>
                this.hasPendingOperation(false)
            );
    }

    delete(configuration) {

        this.hasPendingOperation(true);

        return Api.deleteRule(this.bundleId, configuration.name(), this.id)
            .then(() => {
                Notifier.success('Rule deleted successfully');
                return this.id;
            })
            .catch(error => {
                Notifier.error('Failed to delete rule');
                throw error;
            })
            .finally(() =>
                this.hasPendingOperation(false)
            );
    }

};
