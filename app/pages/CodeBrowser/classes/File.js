import Api from 'app/classes/Api.static';
import DataAsync from 'core/classes/DataAsync';
import Pubsub from 'core/classes/events/Pubsub';
import {
    ANALYSIS_ACTIVATED,
    DIRECTIVE_DELETE,
    DIRECTIVE_KEEP_METHOD,
    DIRECTIVE_REMOVE_METHOD
} from 'app/classes/eventTypes';
import { CONFIGURATION_ACTIVATED } from './configuration/Configuration.events';
import Directive from 'codebrowser/classes/Directive';

/**
 * This map should contain the languages for which we want syntax highlighting, but they are not supported by Ace.
 * The idea is to enable syntax highlighting for an unsupported-by-Ace language based on the rules of a
 * supported-by-Ace language of our choice.
 *
 * todo: document case insensitivity
 */
const extensionLanguageMap = {
    'jimple':   'java',
    'shimple':  'java'
};

/**
 *
 */
export default class File {

    bundleId;
    artifactId;
    path;
    name;

    content = new DataAsync(this.load('content'));
    types = new DataAsync(this.load('classes'));
    fields = new DataAsync(this.load('fields'));
    methods = new DataAsync(this.load('methods'));
    bundleDirectives = new DataAsync(this.load('bundleDirectives'));
    analysisDirectives = new DataAsync(this.load('analysisDirectives'));

    /**
     *
     * @param {string} bundleId
     * @param {string} artifactId
     * @param {string} path
     * @param {Pubsub} pubsub
     */
    constructor(bundleId, artifactId, path, pubsub) {

        if(!(pubsub instanceof Pubsub)) {
            throw new Error(`File.constructor: pubsub must be of type Pubsub: ${pubsub}`);
        }

        this.bundleId = bundleId;
        this.artifactId = artifactId;
        this.path = path;
        this.name = path.split('/').pop();

        this.pubsub = pubsub;

        this.startListeningToPageEvents();
    }

    dispose() {

        this.stopListeningToPageEvents();
    }

    toString() {

        return File.createId(this.bundleId, this.artifactId, this.path);
    }

    static createId(bundleId, artifactId, filepath) {

        return `${bundleId} ${artifactId} ${filepath}`;
    }

    getExtension() {

        return this.name.split('.').pop();
    }

    getLanguage() {

        const extension = this.getExtension().toLowerCase();

        return extensionLanguageMap[extension] || extension;
    }

    hasMetadata() {

        return ['java', 'shimple'].includes(this.getExtension());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    pubsub;
    analysisId;
    bundleConfigurationName;

    load(property) {

        return () => {

            let apiPropertyName, apiPropertyValue, propertyNameInResults, morph;

            const morphDirectives = results =>
                (results || []).reduce((acc, d) => {
                    acc[d.doopId] = new Directive(d);
                    return acc;
                }, {});

            if(property === 'bundleDirectives') {
                apiPropertyName = 'set';
                apiPropertyValue = this.bundleConfigurationName;
                propertyNameInResults = 'directives';
                morph = morphDirectives;
            }
            else if(property === 'analysisDirectives') {
                apiPropertyName = 'analysis';
                apiPropertyValue = this.analysisId;
                propertyNameInResults = 'directives';
                morph = morphDirectives;
            }
            else {
                apiPropertyName = property;
                apiPropertyValue = true;
                propertyNameInResults = property;
                morph = x => x;
            }

            return Api.fetchFile(this.bundleId, this.artifactId, this.path, { [apiPropertyName]: apiPropertyValue })
                .then(results => morph(results[propertyNameInResults]));
        };
    }

    startListeningToPageEvents() {

        this.pubsub.on(ANALYSIS_ACTIVATED, analysis => {

            this.analysisId = analysis.id;
            this.analysisDirectives = new DataAsync(this.load('analysisDirectives'));

        }, this, { synchronous: true });

        this.pubsub.on(CONFIGURATION_ACTIVATED, configuration => {

            this.bundleConfigurationName = configuration.name();
            this.bundleDirectives = new DataAsync(this.load('bundleDirectives'));

        }, this, { synchronous: true });

        const clear = directive => {
            if(directive.sourceFileName !== this.path) { return; }
            this.bundleDirectives = new DataAsync(this.load('bundleDirectives'));
        };
        this.pubsub.on(DIRECTIVE_KEEP_METHOD, clear, this, { synchronous: true });
        this.pubsub.on(DIRECTIVE_REMOVE_METHOD, clear, this, { synchronous: true });
        this.pubsub.on(DIRECTIVE_DELETE, clear, this, { synchronous: true });
    }

    stopListeningToPageEvents() {

        this.pubsub.off(ANALYSIS_ACTIVATED, null, this);
        this.pubsub.off(CONFIGURATION_ACTIVATED, null, this);
    }

};
