import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import $ from 'jquery';
import Api from 'app/classes/Api.static';
import Notifier from 'app/classes/Notifier.static';
import Configuration from 'codebrowser/classes/configuration/Configuration';
import { CTX_PAGE } from 'core/classes/events/contexts';
import { CONFIGURATION_CREATED, CONFIGURATION_OPEN_CREATION_FORM } from 'codebrowser/classes/configuration/Configuration.events';
import 'app/components/FormOption/FormOption.component';

/**
 *
 */
export default class ConfigurationCreation extends Base {

    constructor(element) {

        super(element);

        this.startListeningToPageEvents();
        this.startListeningToUiEvents();
    }

    dispose() {

        this.stopListeningToUiEvents();
        this.$modal.removeClass('fade');
        this.$modal.modal('hide');
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    configurationManager = this.requirePropertyInAncestors('configurationManager');
    bundle = ko.observable({});
    options = [{
        id: 'name',
        description: '',
        name: 'Name',
        isMandatory: true,
        isFile: false,
        isBoolean: false,
        fileTypes: [],
        validValues: [],
        defaultValue: null,
    }];
    $modal = $(this.element.getElementsByClassName('modal')[0]);
    $submitButton = $(this.element.querySelector('button[type="submit"]'));
    $nameInput;

    initAfterBinding() {

        this.$nameInput = $(this.element.querySelector(`input[name="${this.options[0].id}"]`));
    }

    startListeningToPageEvents() {

        this.on(CTX_PAGE, CONFIGURATION_OPEN_CREATION_FORM, bundle => {
            this.bundle(bundle);
            this.$modal.modal('show');
        }, { ignoreIfEquals: false });
    }

    startListeningToUiEvents() {

        this.$modal.on('shown.bs.modal', () =>
            this.$nameInput.focus()
        );

        this.$modal.on('hidden.bs.modal', () =>
            this.$nameInput.val('')
        );
    }

    stopListeningToUiEvents() {

        this.$modal.off();
    }

    submit() {

        const bundleId = this.bundle().id,
            configurationName = this.$nameInput.val();

        this.$submitButton.button('loading');

        return Api.createConfiguration(bundleId, configurationName)
            .then(configurationData => {
                this.notify(CTX_PAGE, CONFIGURATION_CREATED, new Configuration(configurationData, this.configurationManager));
                Notifier.success(`Configuration was created successfully: ${configurationName}`);
            })
            .catch(() =>
                Notifier.error(`Failed to create configuration: ${configurationName}`)
            )
            .then(() => {
                this.$submitButton.button('reset');
                this.$modal.modal('hide');
            });
    }

};
