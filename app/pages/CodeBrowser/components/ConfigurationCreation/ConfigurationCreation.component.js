import './ConfigurationCreation.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './ConfigurationCreation.vm';
import view from './ConfigurationCreation.view.html';

koh.registerComponent('component-configuration-creation', vm, view);
