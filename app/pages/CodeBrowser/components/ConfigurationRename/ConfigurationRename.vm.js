import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import $ from 'jquery';
import { CTX_PAGE } from 'core/classes/events/contexts';
import { CONFIGURATION_OPEN_RENAME_FORM } from 'codebrowser/classes/configuration/Configuration.events';
import 'app/components/FormOption/FormOption.component';

/**
 *
 */
export default class ConfigurationRename extends Base {


    constructor(element) {

        super(element);

        this.startListeningToPageEvents();
        this.startListeningToUiEvents();
    }

    dispose() {

        this.stopListeningToUiEvents();
        this.$modal.removeClass('fade');
        this.$modal.modal('hide');
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    configuration = ko.observable({});
    name = ko.observable('');
    options = [{
        id: 'name',
        description: '',
        name: 'New name',
        isMandatory: true,
        isFile: false,
        isBoolean: false,
        fileTypes: [],
        validValues: [],
        defaultValue: null,
        observableToUse: this.name,
    }];
    $modal = $(this.element.getElementsByClassName('modal')[0]);
    $submitButton = $(this.element.querySelector('button[type="submit"]'));
    $nameInput;

    initAfterBinding() {

        this.$nameInput = $(this.element.querySelector(`input[name="${this.options[0].id}"]`));
    }

    startListeningToPageEvents() {

        this.on(CTX_PAGE, CONFIGURATION_OPEN_RENAME_FORM, configuration => {
            this.configuration(configuration);
            this.name(configuration.name());
            this.$modal.modal('show');
        }, { ignoreIfEquals: false });
    }

    startListeningToUiEvents() {

        this.$modal.on('shown.bs.modal', () =>
            this.$nameInput.focus()
        );

        this.$modal.on('hidden.bs.modal', () =>
            this.name('')
        );
    }

    stopListeningToUiEvents() {

        this.$modal.off();
    }

    canRename() {

        const editedName = this.name(),
            configurationNameObs = this.configuration().name;

        return editedName && configurationNameObs && editedName !== configurationNameObs();
    }

    submit() {

        this.$submitButton.button('loading');

        this.configuration().rename(this.name())
            .catch(() => {})
            .then(() => {
                this.$submitButton.button('reset');
                this.$modal.modal('hide');
                this.configuration({});
            });
    }

};
