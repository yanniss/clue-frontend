import './ConfigurationRename.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './ConfigurationRename.vm';
import view from './ConfigurationRename.view.html';

koh.registerComponent('component-configuration-rename', vm, view);
