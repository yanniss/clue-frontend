import './Overview.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './Overview.vm';
import view from './Overview.view.html';

koh.registerComponent('component-overview', vm, view);
