import * as d3 from 'd3';
import Api from 'app/classes/Api.static';
import Base from 'core/classes/vm/Base.vm';
import ResizeObserver from 'resize-observer-polyfill';
import { CTX_PAGE } from 'core/classes/events/contexts';
import { ANALYSIS_ACTIVATED, FILE_RAW_OPEN } from 'app/classes/eventTypes';

/**
 *
 */
export default class VisTreemap extends Base {

    /**
     * @param {HTMLElement} element
     * @param {Bundle} bundle
     */
    constructor(element, { bundle }) {

        super(element);

        this.bundleId = bundle.id;
        this.analysisId = null;

        this.resizeObserver = null;
        this.viewport = { width: 0, height: 0 };

        this.data = null;

        this.rootNode = null;
        this.focusedNode = null;
        this.g = null;
        this.treemap = null;

        this.startListeningToPageEvents();
        this.startListeningToUiEvents();
        this.redraw();
    }

    dispose() {

        this.resizeObserver && this.resizeObserver.disconnect();
        super.dispose();
    }

    resetZoom() {

        this.rootNode && zoomTo(this, this.rootNode);
    }

    zoomOut() {

        this.focusedNode && this.focusedNode.parent && zoomTo(this, this.focusedNode.parent);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    startListeningToPageEvents() {

        this.on(CTX_PAGE, ANALYSIS_ACTIVATED, analysis => {
            this.analysisId = analysis && analysis.id;
            this.redraw();
        });
    }

    startListeningToUiEvents() {

        this.addEventListener('contextmenu', e => {
            e.preventDefault();
            this.zoomOut();
        });
    }

    redraw() {

        setupData(this).then(() => setupView(this));
    }

};

function setupData(vm) {

    return Api.fetchPackageHierarchy(vm.bundleId, vm.analysisId)
        .then(results => {
            vm.data = results;
        });
}

function setupView(vm) {

    d3.select(vm.element).select('svg').remove();
    const svg = d3.select(vm.element).append('svg:svg');

    // svg.style('background', color(-1));

    vm.resizeObserver = new ResizeObserver((entries, observer) => {
        let { width, height } = entries.values().next().value.contentRect;
        width  = parseInt(width);
        height = parseInt(height);

        // The following check 'w && h' is required in the case that the overview tab
        // is hidden (i.e. another tab is active). In that case, it would trie to
        // redraw the graph in a non-existent dom element.
        width && height && redraw(vm, svg, width, height);
    });

    vm.resizeObserver.observe(vm.element);
}

function redraw(vm, svg, width, height) {

    // Remove the <svg>'s previous contents before redraw them.
    svg.selectAll('*').remove();
    svg.attr('width',  width);
    svg.attr('height', height);

    const color = d3.scaleOrdinal(d3.schemeCategory10);

    let g = svg.append('g');

    let treemap = d3.treemap().size([width, height]).padding(2);

    // Build hierarchy.
    let root = d3.hierarchy(vm.data)
        .sum(d => d.size)
        .sort((a, b) => b.value - a.value);

    // Calculate position and size for each Node.
    let nodes = treemap(root).descendants();
    nodes.shift(); // remove root node

    let rects = g.selectAll('rect');    // Select all current rects.
    rects.data(nodes)
        .enter()
        .append('rect')                 // Create a rect for this Node.
        .attr('class', 'node')
        .attr('x', node => node.x0)
        .attr('y', node => node.y0)
        .attr('width', node => node.x1 - node.x0)
        .attr('height', node => node.y1 - node.y0)
        .style('fill', (node, i) => {
            if(node.depth <= 1) {
                // choose different color from palette.
                node._color = color(i);
            }
            else {
                // take parent's color and modify it a bit e.g. its brightness.
                node._color = d3.color(node.parent._color).brighter(.35);
            }
            return node._color;
        })
        .on('click', function(node, index, nodes) {
            if(isAncestor(vm.focusedNode, node)) {
                // Find the ancestor of 'd' that is child of focused node; or 'd' itself if it is child of focused node.
                let prefix  = vm.focusedNode.data.name ? vm.focusedNode.data.name + '\\.' : '';
                let pattern = new RegExp(`^${prefix}[^\\.]+$`);
                while(!node.data.name.match(pattern)) {
                    node = node.parent;
                }
            }

            if(!node.children) { // class node
                Api.fetchTypeInfo(vm.bundleId, vm.analysisId, node.data.name).then(type => {
                    vm.notify(CTX_PAGE, FILE_RAW_OPEN, {
                        filepath: type.sourceFileName,
                        line: type.position.startLine,
                        column: type.position.startColumn,
                    });
                })
            }
            else if(vm.focusedNode !== node) { // package node
                zoomTo(vm, node);
            }
        });

    let texts = g.selectAll('text');
    texts.data(nodes)
        .enter()
        .append('text')
        .attr('x', node => node.x0 + (node.x1 - node.x0)/2)
        .attr('y', node => node.y0 + (node.y1 - node.y0)/2)
        .attr('class', 'label')
        .style('display', 'none')
        .text(d => { let n = d.data.name.split('.'); return n[n.length-1]; });

    vm.rootNode = root;
    vm.rects = rects;
    vm.texts = texts;
    vm.g = g;
    vm.viewport.width = width;
    vm.viewport.height = height;
    vm.nodes = nodes;

    zoomTo(vm, root);
}

function isAncestor(node1, node2) {

    return node1 && node2 && (node1.data.name === '' || node2.data.name.startsWith(node1.data.name + '.'));
}

function zoomTo(vm, node) {

    let w      = vm.viewport.width,
        h      = vm.viewport.height,
        scaleX = w/(node.x1 - node.x0),
        scaleY = h/(node.y1 - node.y0),
        dx     = -node.x0 * scaleX,
        dy     = -node.y0 * scaleY;

    vm.g.transition()
        .ease(d3.easeCubic)
        .duration(750)
        .on('start', () => {
            vm.g.selectAll('text').style('display', 'none');
        })
        .on('end', () => {
            vm.g.selectAll('text').style('display', d => d.parent === node ? 'inline' : 'none')
                /*
                .attr('transform', d => {
                    if(d.parent === node) {
                        console.log(d.x0, d.x1);
                    }

                    let ddx = (1/scaleX)*(d.x1 - d.x0)/2;
                    let ddy = 0;
                    return d.parent === node ? `scale(${1/scaleX},${1/scaleY}) translate(${ddx},${ddy})` : '';
                });*/
        })
        .attr('transform', `translate(${dx},${dy}) scale(${scaleX},${scaleY})`);

    vm.focusedNode = node;
}
