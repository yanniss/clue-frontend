import './VisTreemap.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './VisTreemap.vm';
import view from './VisTreemap.view.html';

koh.registerComponent('component-vis-treemap', vm, view);
