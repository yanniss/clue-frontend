import * as d3 from 'd3';
import Api from 'app/classes/Api.static';
import Base from 'core/classes/vm/Base.vm';
import ResizeObserver from 'resize-observer-polyfill';
import { CTX_PAGE } from 'core/classes/events/contexts';
import { ANALYSIS_ACTIVATED, FILE_RAW_OPEN } from 'app/classes/eventTypes';

/**
 *
 */
export default class VisCirclePacking extends Base {

    /**
     * @param {HTMLElement} element
     * @param {Bundle} bundle
     */
    constructor(element, { bundle }) {

        super(element);

        this.bundleId = bundle.id;
        this.analysisId = null;

        this.resizeObserver = null;

        this.rootNode = null;
        this.focusedNode = null;
        this.circle = null;
        this.text = null;
        this.all = null;
        this.view = null;
        this.diameter = null;

        this.startListeningToPageEvents();
        this.startListeningToUiEvents();
        this.redraw();
    }

    dispose() {

        this.resizeObserver && this.resizeObserver.disconnect();
        super.dispose();
    }

    resetZoom() {

        this.rootNode && zoom(this, this.rootNode);
    }

    zoomOut() {

        this.focusedNode && this.focusedNode.parent && zoom(this, this.focusedNode.parent);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    startListeningToPageEvents() {

        this.on(CTX_PAGE, ANALYSIS_ACTIVATED, analysis => {
            this.analysisId = analysis && analysis.id;
            this.redraw();
        });
    }

    startListeningToUiEvents() {

        this.addEventListener('contextmenu', e => {
            e.preventDefault();
            this.zoomOut();
        });
    }

    redraw() {

        setupData(this).then(() => setupView(this));
    }

};

function setupData(vm) {

    return Api.fetchPackageHierarchy(vm.bundleId, vm.analysisId)
        .then(results => {
            vm.data = results;
        });
}

function setupView(vm) {

    d3.select(vm.element).select('svg').remove();
    const svg = d3.select(vm.element).append('svg:svg');

    vm.resizeObserver = new ResizeObserver((entries, observer) => {
        let { width, height } = entries.values().next().value.contentRect;
        width = parseInt(width);
        height = parseInt(height);

        // The following check 'w && h' is required in the case that the overview tab
        // is hidden (i.e. another tab is active). In that case, it would trie to
        // redraw the graph in a non-existent dom element.
        width && height && redraw(vm, svg, width, height);
    });

    vm.resizeObserver.observe(vm.element);
}

function redraw(vm, svg, width, height) {

    // Remove the <svg>'s previous contents before redraw them.
    svg.selectAll('*').remove();
    svg.attr('width',  width);
    svg.attr('height', height);

    let g = svg.append('g')
        .attr('transform', `translate(${width/2},${height/2})`);

    let pack = d3.pack()
        .size([width, height])
        .padding(2);

    let color = d3.scaleLinear()
        .domain([-1, 5])
        .range(['hsl(152,80%,80%)', 'hsl(228,30%,40%)'])
        .interpolate(d3.interpolateHcl);

    let root = d3.hierarchy(vm.data)
        .sum(d => d.size)
        .sort((a, b) => b.value - a.value);

    let nodes = pack(root).descendants();
    nodes.shift();  // remove root node.

    let circle = g.selectAll('circle')
        .data(nodes)
        .enter()
        .append('circle')
        .attr('class', d => d.parent ? d.children ? 'node' : 'node node--leaf' : 'node node--root')
        .style('fill', d => d.children ? color(d.depth) : null)
        .on('click', d => {
            if(isAncestor(vm.focusedNode, d)) {
                // Find the ancestor of 'd' that is child of focused node; or 'd' itself if it is child of focused node.
                let prefix  = vm.focusedNode.data.name ? vm.focusedNode.data.name + '\\.' : '';
                let pattern = new RegExp(`^${prefix}[^\\.]+$`);
                while(!d.data.name.match(pattern)) {
                    d = d.parent;
                }
            }

            if(!d.children) { // class node
                Api.fetchTypeInfo(vm.bundleId, vm.analysisId, d.data.name).then(type => {
                    vm.notify(CTX_PAGE, FILE_RAW_OPEN, {
                        filepath: type.sourceFileName,
                        line: type.position.startLine,
                        column: type.position.startColumn,
                    });
                })
            }
            else if(!same(vm.focusedNode, d)) { // package node
                zoom(vm, d);
            }
        });

    let text = g.selectAll('text')
        .data(nodes)
        .enter()
        .append('text')
        .attr('class', 'label')
        .style('fill-opacity', d => same(d.parent, root) ? 1 : 0)
        .style('display', d => same(d.parent, root) ? 'inline' : 'none')
        .text(d => { let n = d.data.name.split('.'); return n[n.length-1]; });

    let all = g.selectAll('circle,text');

    vm.rootNode = root;
    vm.focusedNode = root;
    vm.circle = circle;
    vm.text = text;
    vm.all = all;
    vm.diameter = Math.min(width, height);

    zoomTo(vm, [root.x, root.y, root.r * 2]);
}

function isAncestor(node1, node2) {

    return node1 && node2 && (node1.data.name === '' || node2.data.name.startsWith(node1.data.name + '.'));
}

function zoom(vm, node) {

    const focus = vm.focusedNode = node;

    const transition = d3.transition()
        .duration(750)
        .tween('zoom', d => {
            let i = d3.interpolateZoom(vm.view, [focus.x, focus.y, focus.r * 2]);
            return t => { zoomTo(vm, i(t)); };
        });

    transition.selectAll('text')
        .filter(                function(d) { return same(d.parent, focus) || this.style.display === 'inline'; })
        .style('fill-opacity',  function(d) { return same(d.parent, focus) ? 1 : 0; })
        .on('start',            function(d) { if (same(d.parent, focus)) this.style.display = 'inline'; })
        .on('end',              function(d) { if (!same(d.parent, focus)) this.style.display = 'none'; });
}

function zoomTo(vm, v) {

    const k = vm.diameter / v[2];

    vm.view = v;
    vm.all.attr('transform', d => `translate(${(d.x - v[0]) * k},${(d.y - v[1]) * k})`);
    vm.circle.attr('r', d => d.r * k);
}

function same(node1, node2) {

    return node1 && node2 && node1.data.name === node2.data.name;
}
