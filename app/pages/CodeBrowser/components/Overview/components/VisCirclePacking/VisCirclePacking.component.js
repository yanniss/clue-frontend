import './VisCirclePacking.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './VisCirclePacking.vm';
import view from './VisCirclePacking.view.html';

koh.registerComponent('component-vis-circle-packing', vm, view);
