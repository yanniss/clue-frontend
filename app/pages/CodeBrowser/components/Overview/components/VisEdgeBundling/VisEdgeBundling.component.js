import './VisEdgeBundling.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './VisEdgeBundling.vm';
import view from './VisEdgeBundling.view.html';

koh.registerComponent('component-vis-edge-bundling', vm, view);
