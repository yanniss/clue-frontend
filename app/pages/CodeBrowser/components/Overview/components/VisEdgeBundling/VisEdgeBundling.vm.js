import $ from 'jquery';
import * as d3 from 'd3';
import Api from 'app/classes/Api.static';
import Base from 'core/classes/vm/Base.vm';
import ResizeObserver from 'resize-observer-polyfill';
import {CTX_PAGE} from "core/classes/events/contexts";
import {ANALYSIS_ACTIVATED} from "app/classes/eventTypes";

/**
 *
 */
export default class VisEdgeBundling extends Base {

    /**
     * @param {HTMLElement} element
     * @param {Bundle} bundle
     */
    constructor(element, { bundle }) {

        super(element);

        this.bundleId = bundle.id;
        this.analysisId = null;

        this.resizeObserver = null;
        this.viewport = { width: 0, height: 0 };

        this.data = null;

        this.g = null;

        this.startListeningToPageEvents();
        this.redraw();
    }

    dispose() {

        this.resizeObserver && this.resizeObserver.disconnect();
        super.dispose();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    startListeningToPageEvents() {

        this.on(CTX_PAGE, ANALYSIS_ACTIVATED, analysis => {
            this.analysisId = analysis && analysis.id;
            this.redraw();
        });
    }

    redraw() {

        setupData(this, this.bundleId, this.analysisId).then(() => setupView(this));
    }

};

function setupData(vm, bundleId, analysisId) {

    return Api.fetchPackageGraph(bundleId, analysisId)
        .then(results => {
            vm.data = results;
        });
}

function setupView(vm) {

    d3.select(vm.element).select('svg').remove();
    const svg = d3.select(vm.element).append('svg:svg');

    vm.resizeObserver = new ResizeObserver((entries, observer) => {
        let { width, height } = entries.values().next().value.contentRect;
        width  = parseInt(width);
        height = parseInt(height);

        let diameter = Math.min(width, height),
            radius = diameter / 2;

        // Remove the <svg>'s previous contents before redraw them.
        svg.selectAll('*').remove();
        vm.g = svg.attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + width/2 + "," + height/2 + ")");

        vm.viewport.width  = width;
        vm.viewport.height = height;

        // The following check 'w && h' is required in the case that the overview tab
        // is hidden (i.e. another tab is active). In that case, it would trie to
        // redraw the graph in a non-existent dom element.
        width && height && redraw(vm);
    });

    vm.resizeObserver.observe(vm.element);
}

function redraw(vm) {

    let diameter = Math.min(vm.viewport.width, vm.viewport.height),
        radius = diameter / 2,
        innerRadius = radius - 120;

    let cluster = d3.cluster()
        .size([360, innerRadius]);

    let line = d3.radialLine()
        .curve(d3.curveBundle.beta(0.85))
        .radius(function(d) { return d.y; })
        .angle(function(d) { return d.x / 180 * Math.PI; });

    let link = vm.g.append("g").selectAll(".link"),
        node = vm.g.append("g").selectAll(".node");

    let root = packageHierarchy2(vm.data)
        .sum(function(d) { return d.size; });

    //root >>> Node: { parent:null, value:0, depth:x, height:y, data:..., children:[...] }

    cluster(root);  // >>> Adds { x, y }

    link = link
        .data(packageImports(root.leaves()))
        .enter().append("path")
        .each(function(d) { d.source = d[0], d.target = d[d.length - 1]; })
        .attr("class", "link")
        .attr("d", line);

    node = node
        .data(root.leaves())
        .enter().append("text")
        .attr("class", "node")
        .attr("dy", "0.31em")
        .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + (d.y + 8) + ",0)" + (d.x < 180 ? "" : "rotate(180)"); })
        .attr("text-anchor", function(d) { return d.x < 180 ? "start" : "end"; })
        .text(function(d) { return d.data.key; })
        .on("mouseover", mouseovered)
        .on("mouseout", mouseouted);

    function mouseovered(d) {
        node
            .each(function(n) { n.target = n.source = false; });

        link
            .classed("link--target", function(l) { if (l.target === d) return l.source.source = true; })
            .classed("link--source", function(l) { if (l.source === d) return l.target.target = true; })
            .filter(function(l) { return l.target === d || l.source === d; })
            .raise();

        node
            .classed("node--target", function(n) { return n.target; })
            .classed("node--source", function(n) { return n.source; });
    }

    function mouseouted(d) {
        link
            .classed("link--target", false)
            .classed("link--source", false);

        node
            .classed("node--target", false)
            .classed("node--source", false);
    }

    function packageHierarchy2(packages) {
        let nodes = {};
        let root = {
            name: "",
            children: [],
        };

        function addNode(name, imports) {
            let node = nodes[name];
            if(!node) {
                node = {
                    name: name,
                    parent: root,
                    key: name,
                    imports: imports,
                };
                nodes[name] = node;
                root.children.push(node);
            }
            else if(!node.imports && imports) {
                // This is in case it was firstly inserted as an import and then found in "packages".
                node.imports = imports;
            }
        }

        packages.forEach(p => {
            addNode(p.name, p.imports);
            p.imports && p.imports.forEach(i => { addNode(i); });
        });

        return d3.hierarchy(root);
    }

    // Return a list of imports for the given array of nodes.
    function packageImports(nodes) {
        let map = {},
            imports = [];

        // Compute a map from name to node.
        nodes.forEach(d => { map[d.data.name] = d; });

        // For each import, construct a link from the source to target node.
        nodes.forEach(d => {
            d.data.imports && d.data.imports.forEach(i => {
                imports.push(map[d.data.name].path(map[i]));
            });
        });

        return imports;
    }
}
