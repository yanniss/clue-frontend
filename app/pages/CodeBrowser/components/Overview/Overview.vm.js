import './components/VisTreemap/VisTreemap.component';
import './components/VisCirclePacking/VisCirclePacking.component';
import './components/VisEdgeBundling/VisEdgeBundling.component';
import $ from 'jquery';
import Api from 'app/classes/Api.static';
import Base from 'core/classes/vm/Base.vm';
import Bundle from 'app/classes/Bundle';
import Selection from 'codebrowser/classes/Selection';
import ko from 'knockout';
import thumbTreemap from './images/thumbnail-treemap.png';
import thumbCPack from './images/thumbnail-circle-packing.png';
import thumbHEB from './images/thumbnail-hier-edge-bundling.png';
import { ANALYSIS_ACTIVATED, SELECTION_NEW } from 'app/classes/eventTypes';
import { CTX_PAGE } from 'core/classes/events/contexts';

const visualizations = [
    {
        name: 'Dependencies between packages<br>Inferred from call graph edges',
        thumbnail: thumbHEB,
        alt: 'Hierarchical edge bundling thumbnail',
        component: 'component-vis-edge-bundling',
    },
    {
        name: 'Bundle hierarchy (Treemap)<br>Sizes are based on number of methods',
        thumbnail: thumbTreemap,
        alt: 'Treemap thumbnail',
        component: 'component-vis-treemap',
    },
    {
        name: 'Bundle hierarchy (Circle Packing)<br>Sizes are based on number of methods',
        thumbnail: thumbCPack,
        alt: 'Circle Packing thumbnail',
        component: 'component-vis-circle-packing',
    },
];

/**
 *
 */
export default class Overview extends Base {

    bundle;
    activeAnalysis = ko.observable();
    queries = ko.observableArray();
    visualizations = visualizations;
    activeVisualization = ko.observable(visualizations[0]);

    /**
     * @param {HTMLElement} element
     * @param {Bundle} bundle
     */
    constructor(element, { bundle }) {

        super(element);

        this.bundle = bundle;

        Api.fetchQueries(this.bundle.id)
            .then(this.queries);

        this.startListeningToPageEvents();
    }

    toggleVisualizationPanelSize = (self, e) => {

        const $button = $(e.target),
            $topPanels = $(this.element).find('.list-panels');

        if($button.hasClass('glyphicon-resize-full') || !$button.hasClass('glyphicon-resize-small')) {
            // Enlarge panel
            $button.addClass('glyphicon-resize-small');
            $button.removeClass('glyphicon-resize-full');
            $topPanels.addClass('hide');
        }
        else {
            // make it smaller
            $button.addClass('glyphicon-resize-full');
            $button.removeClass('glyphicon-resize-small');
            $topPanels.removeClass('hide');
        }
    };

    selectVisualization = visualization => {

        if(this.activeVisualization() !== visualization) {
            this.activeVisualization(visualization);
        }
    };

    executeQuery = query => {

        const bundleId = this.bundle.id,
            analysisId = this.activeAnalysis && this.activeAnalysis.id;

        this.notify(CTX_PAGE, SELECTION_NEW, new Selection(bundleId, analysisId, { kind: 'Query', ...query }));
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    startListeningToPageEvents() {

        this.on(CTX_PAGE, ANALYSIS_ACTIVATED, analysis => {
            const analysisId = analysis && analysis.id;

            Api.fetchQueries(this.bundle.id, analysisId)
                .then(this.queries);

            this.activeAnalysis(analysis);
        });
    }

};
