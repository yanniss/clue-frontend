import Tree from '../trees/Tree/Tree.vm';
import ConfigurationNode from './classes/ConfigurationNode';
import ko from 'knockout';
import { CTX_PAGE } from 'core/classes/events/contexts';
import {
    CONFIGURATION_OPEN_CREATION_FORM,
    CONFIGURATION_OPEN,
    CONFIGURATION_CREATED,
    CONFIGURATION_RENAMED,
    CONFIGURATION_DELETED,
} from 'codebrowser/classes/configuration/Configuration.events';
import 'core/components/Menu/Menu.component';

const typesOptions = {
    ...ConfigurationNode.typesOptions,
};

/**
 *
 */
export default class Configurations extends Tree {

    constructor(element, { bundle }) {

        super(element, bundle, {
            treeName: 'Configurations',
            stateKeyName: 'Configurations',
            jstreeOptions: {
                core: { check_callback: true },
                plugins: ['html_text'],
                types: typesOptions,
            },
        });

        this.bundle = bundle;

        this.setLoadDataFn(this.loadRootChildren);
        this.setUiEventHandlers();
        this.draw();
        this.startListeningToPageEvents();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bundle;
    configurationManager = this.requirePropertyInAncestors('configurationManager');
    rightClickedNode = ko.observable({});

    startListeningToPageEvents() {

        ////////////////////////////////////////////////////////////////

        this.on(CTX_PAGE, CONFIGURATION_CREATED, configuration => {

            this.insertNodeFor(configuration);
        });

        ////////////////////////////////////////////////////////////////

        this.on(CTX_PAGE, CONFIGURATION_RENAMED, configuration => {

            this.renameNodeFor(configuration);

        }, { ignoreIfEquals: false });

        ////////////////////////////////////////////////////////////////

        this.on(CTX_PAGE, CONFIGURATION_DELETED, configurationId => {

            this.deleteNodeFor(configurationId);
        });
    }

    setUiEventHandlers() {

        super.setUiEventHandlers('click', [{
            nodeTypes: ['configuration'],
            handler: (e, node) => {
                this.notify(CTX_PAGE, CONFIGURATION_OPEN, node.configuration);
            },
        }]);

        super.setUiEventHandlers('contextmenu', [{
            nodeTypes: ['configuration'],
            handler: (e, node) => {
                this.rightClickedNode(node);
                this.children.menu.open(e);
            },
        }]);
    }

    loadRootChildren = (node, setChildrenFn) => {

        this.configurationManager.getConfigurations()
            .then(configurations =>
                setChildrenFn(configurations.map(configuration => new ConfigurationNode(this, configuration)))
            )
            .catch(() =>
                setChildrenFn(false)
            );
    };

    createConfiguration() {

        this.notify(CTX_PAGE, CONFIGURATION_OPEN_CREATION_FORM, this.bundle);
    }

    insertNodeFor(configuration) {

        this._jstree.create_node('#', new ConfigurationNode(this, configuration));
    }

    renameNodeFor(configuration) {

        this._jstree.rename_node(`${this._nodeIdPrefix}/${configuration.id}`, configuration.name());
    }

    deleteNodeFor(configurationId) {

        const node = this._jstree.get_node(`${this._nodeIdPrefix}/${configurationId}`);

        node.original.disposeSubscription();
        this._jstree.delete_node(node);
    }

};
