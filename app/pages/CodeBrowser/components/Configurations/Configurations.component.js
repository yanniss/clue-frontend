import './Configurations.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './Configurations.view.html';
import vm from './Configurations.vm';

koh.registerComponent('component-configurations', vm, view);
