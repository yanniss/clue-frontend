import Node from '../../trees/Tree/classes/Node';
import './ConfigurationNode.scss';

/**
 *
 */
export default class ConfigurationNode extends Node {

    static typesOptions = {
        configuration: {
            icon: 'configuration-node fa fa-circle',
            comparator: (a,b) => b.configuration.createdDate - a.configuration.createdDate,
        },
    };

    constructor(tree, configuration) {

        super(`${tree._nodeIdPrefix}/${configuration.id}`, 'configuration', ConfigurationNode.getNodeName(configuration),
            false, false,
            { class: '' },
            {
                title: `Created:\n${configuration.createdDate}\nModified:\n${configuration.modifiedDate}`,
                class: configuration.activated() ? 'activated' : '',
            },
        );

        this.configuration = configuration;
        this.tree = tree;

        let subscription;
        this.disposeSubscription = () => subscription && subscription.dispose();

        const setComputeds = () => {
            if(!tree._jstree || !tree._jstree.get_node(this.id)) {
                setTimeout(setComputeds, 250);
                return;
            }
            // This is the Base view model's "computed" method. Therefore, the following subscription will be disposed
            // automatically when the tree component is disposed.
            subscription = tree.computed(() => this.updateNodeIcon(configuration.activated()));
        };
        setComputeds();
    }

    updateNodeIcon(activated) {

        this.li_attr.class = activated ? 'activated' : '';

        const jstree = this.tree._jstree,
            node = jstree.get_node(this.id);

        node.li_attr.class = this.li_attr.class;
        jstree.redraw_node(this.id);
    }

    static getNodeName(configuration) {

        return `${configuration.name()} <span class="modified">${configuration.modified}</span>`;
    }

};
