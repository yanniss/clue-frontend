import Tab from './Tab';

/**
 *
 */
export default class OverviewTab extends Tab {

    constructor() {

        super('Overview', 'Statistics, Visualizations, Queries', false);
    }

    toString() {

        return 'overview tab';
    }

};
