
let counter = 0;

/**
 *
 */
export default class Tab {

    domId;
    name;
    tooltip;
    closable;

    /**
     *
     * @param {string} name
     * @param {string} tooltip
     * @param {boolean} closable
     */
    constructor(name = '', tooltip = '', closable = true) {

        this.domId = `codebrowser-tab-${counter++}`;
        this.name = name;
        this.tooltip = tooltip;
        this.closable = !!closable;
    }

    dispose() {

    }

};
