import Tab from './Tab';
import ko from 'knockout';
/**
 *
 */
export default class PinnedConfigurationTab extends Tab {

    constructor() {

        super('<b>Configuration</b>', 'Active configuration', false);

        this.configurationTab = ko.observable({});
    }

    toString() {

        return `pinned_configuration_tab`;
    }

};
