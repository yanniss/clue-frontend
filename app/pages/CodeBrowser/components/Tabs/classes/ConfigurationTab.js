import Tab from './Tab';
import ko from 'knockout';

/**
 *
 */
export default class ConfigurationTab extends Tab {

    constructor(configuration) {

        super(
            configuration.name,
            ko.computed(() => `Current configuration: ${configuration.name()}\n\n${configuration.modifiedDate()}`),
            false,
        );

        this.configuration = configuration;
    }

    toString() {

        return `configuration_id:${this.configuration.id}`;
    }

    dispose() {

        this.tooltip.dispose();
        super.dispose();
    }

};
