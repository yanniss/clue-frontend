import './Tabs.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './Tabs.view.html';
import vm from './Tabs.vm';

koh.registerComponent('component-tabs', vm, view);
