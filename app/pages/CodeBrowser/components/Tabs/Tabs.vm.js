import Base from 'core/classes/vm/Base.vm';
import AnalysisTab from './classes/AnalysisTab';
import ConfigurationTab from './classes/ConfigurationTab';
import PinnedConfigurationTab from './classes/PinnedConfigurationTab';
import File from 'codebrowser/classes/File';
import FileTab from './classes/FileTab';
import OverviewTab from './classes/OverviewTab';
import dom from 'core/classes/Dom.static';
import ko from 'knockout';
import { isPositiveNatural } from 'core/classes/NumberHelpers';
import { CTX_PAGE, CTX_DESCENDANTS } from 'core/classes/events/contexts';
import { FILE_RAW_OPEN, FILE_SAVE_STATE } from 'app/classes/eventTypes';
import { CONFIGURATION_OPEN, CONFIGURATION_DELETED, CONFIGURATION_RENAMED } from 'codebrowser/classes/configuration/Configuration.events';
import { ANALYSIS_OPEN, ANALYSIS_DELETED } from 'codebrowser/classes/analysis/Analysis.events';
import 'core/bindings/midclick';
import 'codebrowser/components/Overview/Overview.component';
import 'codebrowser/components/Editor/Editor.component';
import 'codebrowser/components/Analysis/Analysis.component';
import 'codebrowser/components/Configuration/Configuration.component';

/**
 *
 */
export default class Tabs extends Base {

    /**
     * @param {HTMLElement} element
     * @param {Bundle} bundle
     */
    constructor(element, { bundle }) {

        super(element);

        this.bundle = bundle;
    }

    dispose() {

        const tabs = this.tabs();
        for(let i = 0, n = tabs.length; i < n; i++) {
            tabs[i].dispose();
        }

        super.dispose();
    }

    /**
     * @public
     * @param {string} [artifactId]
     * @param {string} filepath
     * @param {number} [line]
     * @param {number} [column]
     */
    openFile(artifactId, filepath, line, column) {

        const fileId = File.createId(this.bundle.id, artifactId, filepath);
        let tab = this.tabs().find(tab => tab.file && tab.file.toString() === fileId);

        if(tab) {
            if(isPositiveNatural(line)) {
                tab.line = line;
                tab.column = isPositiveNatural(column) ? column : 1;
            }
        }
        else {
            const file = new File(this.bundle.id, artifactId, filepath, this.getPubsub(CTX_PAGE));
            tab = new FileTab(file, line, column);
            this.tabs.push(tab);
        }

        this.selectTab(tab);
    }

    /**
     * @public
     * @param {File} file
     * @param {number} [line]
     * @param {number} [column]
     * @param {number} [scrollTop]
     * @param {number} [scrollLeft]
     */
    saveState(file, line, column, scrollTop, scrollLeft) {

        if(!(file instanceof File)) {
            throw new Error(`Tabs.saveState: file must be of type File: ${file}`);
        }

        const tab = this.tabs().find(tab => tab.file === file);
        // If tab does not exist, it is not necessarily an error, since the tab might have just been removed.
        if(tab) {
            tab.line = line;
            tab.column = column;
            tab.scrollTop = scrollTop;
            tab.scrollLeft = scrollLeft;
        }
    }

    /**
     * @public
     * @param {*} configuration
     */
    openConfiguration(configuration) {

        let tab = this.configurationTabs().find(tab => tab.configuration.id === configuration.id);

        if(!tab) {
            tab = new ConfigurationTab(configuration);
            this.configurationTabs.push(tab);
            //this.tabs.push(tab);
        }

        this.pinnedConfigurationTab(tab);
        this.selectTab(tab);
    }

    /**
     * @public
     * @param {*} analysis
     */
    openAnalysis(analysis) {

        let tab = this.analysisTabs().find(tab => tab.analysis.id === analysis.id);

        if(!tab) {
            tab = new AnalysisTab(analysis);
            this.analysisTabs.push(tab);
            this.tabs.push(tab);
        }

        this.selectTab(tab);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    tabs = ko.observableArray([]);
    analysisTabs = ko.observableArray();
    configurationTabs = ko.observableArray();
    pinnedConfigurationTab = ko.observable();
    activeTab = ko.observable();
    //activeConfiguration = ko.observable();

    selectTab = tab => {

        this.activateTab(tab);

        if(tab instanceof FileTab) {
            this.children.editor.openFile(tab.file, tab.line, tab.column, tab.scrollTop, tab.scrollLeft);
        }
    };

    closeTab = (tab, event) => {

        if(!tab.closable) { return; }

        // Remove tab.
        if(tab instanceof ConfigurationTab) {
            this.configurationTabs.splice(this.configurationTabs.indexOf(tab), 1);
        }
        else if(tab instanceof AnalysisTab) {
            this.analysisTabs.splice(this.analysisTabs.indexOf(tab), 1);
        }
        let index = this.tabs.indexOf(tab);
        this.tabs.splice(index, 1);

        // Select another tab after removing the current one.
        if(tab === this.activeTab()) {
            const tabs = this.tabs();

            if(index < tabs.length) {
                this.selectTab(tabs[index]);
            }
            else if(index > 0) {
                this.selectTab(tabs[index - 1]);
            }
            else {
                this.selectTab(this.pinnedConfigurationTab());
            }
        }

        tab.dispose();

        // Stop event bubbling for the case where the tab was closed by clicking the "close" button. If bubbling was
        // allowed, then a "click" event would fire for the tab itself, which would result to a "selectTab" call for
        // the just removed tab.
        event && event.stopPropagation();
    };

    isActiveTabOverviewTab() {

        return this.activeTab() instanceof OverviewTab;
    }

    isActiveTabFileTab() {

        return this.activeTab() instanceof FileTab;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view model members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bundle;
    elementNavTabs;

    /**
     * @override
     */
    initAfterBinding() {

        this.elementNavTabs = this.element.getElementsByClassName('nav-tabs')[1];

        this.selectTab();
        this.startListeningToEditorEvents();
        this.startListeningToPageEvents();
    }

    startListeningToEditorEvents() {

        this.on(CTX_DESCENDANTS, FILE_SAVE_STATE, ({ file, line, column, scrollTop, scrollLeft }) =>
            this.saveState(file, line, column, scrollTop, scrollLeft)
        );
    }

    startListeningToPageEvents() {

        this.on(CTX_PAGE, FILE_RAW_OPEN, ({ artifactId, filepath, line, column }) => {
            this.openFile(artifactId, filepath, line, column);
        }, { ignoreIfEquals: false });

        this.on(CTX_PAGE, CONFIGURATION_OPEN, configuration => {
            this.openConfiguration(configuration);
            //this.activeConfiguration(configuration);
        }, { ignoreIfEquals: false });

        /*
        this.on(CTX_PAGE, CONFIGURATION_RENAMED, configuration => {
            let tab = this.configurationTabs().find(tab => tab.configuration.id === configuration.id);
            tab.name(configuration.name);
        }, { ignoreIfEquals: false });
        */

        /*
        this.on(CTX_PAGE, CONFIGURATION_DELETED, configurationId => {
            let tab = this.configurationTabs().find(tab => tab.configuration.id === configurationId);
            tab && this.closeTab(tab);
        }, { ignoreIfEquals: false });
        */

        this.on(CTX_PAGE, ANALYSIS_OPEN, analysis => {
            this.openAnalysis(analysis);
        }, { ignoreIfEquals: false });

        this.on(CTX_PAGE, ANALYSIS_DELETED, analysisId => {
            let tab = this.analysisTabs().find(tab => tab.analysis.id === analysisId);
            tab && this.closeTab(tab);
        }, { ignoreIfEquals: false });
    }

    activateTab = (tab = null) => {

        this.activeTab(tab);
        tab && this.scrollToTab(tab);
    };

    scrollToTab(tab) {

        if(tab instanceof ConfigurationTab) { return; }

        const tabsContainer = this.elementNavTabs,
            tabElement = document.getElementById(tab.domId),
            rangeBegin = tabsContainer.scrollLeft,
            rangeEnd = rangeBegin + tabsContainer.offsetWidth,
            tabBegin = tabElement.offsetLeft - tabsContainer.offsetLeft,
            tabEnd = tabBegin + dom.width(tabElement);

        if(tabBegin < rangeBegin) {
            tabsContainer.scrollLeft = tabBegin;
        }
        else if(tabEnd > rangeEnd) {
            tabsContainer.scrollLeft = tabsContainer.scrollLeft + tabEnd - rangeEnd;
        }
    }

};
