import Node from './Node';

/**
 *
 */
export default class DependenciesNode extends Node {

    static typesOptions = {
        dependencies: {
            icon: 'dependencies fa fa-cubes',
            sorting_weight: 40,
        },
    };

    constructor(tree) {

        super(`${tree._nodeIdPrefix}/dependencies`, 'dependencies', 'Dependencies', true, false);
    }

};
