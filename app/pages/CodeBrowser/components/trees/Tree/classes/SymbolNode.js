import Node from './Node';
import Tree from '../Tree.vm';

/**
 *
 */
export default class SymbolNode extends Node {

    constructor(parent, symbol, type, text, hasChildren, isOpen, a_attr) {

        super(
            SymbolNode.createSymbolNodeId(parent, symbol.doopId),
            type, text, hasChildren, isOpen, a_attr,
        );

        this.doopId   = symbol.doopId;
        this.filepath = symbol.sourceFileName;
        this.position = symbol.position;
        this.symbol   = symbol;
    }

    static createSymbolNodeId(parent, doopId) {

        return `${parent instanceof Tree ? parent._nodeIdPrefix : parent.id}/${doopId}`;
    }

};
