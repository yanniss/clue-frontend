import SymbolNode from './SymbolNode';
import './TypeNode.scss';

/**
 *
 */
export default class TypeNode extends SymbolNode {

    static typesOptions = {
        class: {
            icon: 'class-node fa fa-square',
            sorting_weight: 200,
        },
        interface: {
            icon: 'interface-node fa fa-square-o',
            sorting_weight: 200,
        },
        enum: {
            icon: 'enum-node fa fa-square',
            sorting_weight: 200,
        },
    };

    constructor(tree, type, hasChildren, isOpen, a_attr, namePostfix = '') {

        super(
            tree,
            type,
            type.isEnum && 'enum' || type.isInterface && 'interface' || 'class',
            (type.name || '<anonymous class>') + (namePostfix ? ' ' : '') + namePostfix,
            hasChildren,
            isOpen,
            a_attr,
        );
    }

};
