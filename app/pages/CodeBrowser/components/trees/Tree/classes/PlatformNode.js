import Node from './Node';

/**
 *
 */
export default class PlatformNode extends Node {

    static typesOptions = {
        platform: {
            icon: 'platform fa fa-university',
            sorting_weight: 50,
        },
    };

    constructor(tree) {

        super(`${tree._nodeIdPrefix}/platform`, 'platform', 'Platform', true, false);
    }

};
