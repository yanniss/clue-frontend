import Node from './Node';

/**
 *
 */
export default class ApplicationNode extends Node {

    static typesOptions = {
        application: {
            icon: 'application fa fa-heartbeat',
            sorting_weight: 30,
        },
    };

    constructor(tree) {

        super(`${tree._nodeIdPrefix}/application`, 'application', 'Application', true, true);
    }

};
