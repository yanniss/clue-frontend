
/**
 *
 */
export default class Node {

    constructor(id, type, text, hasChildren, isOpen, a_attr, li_attr) {

        this.id = id;
        this.type = type;
        this.text = text;
        this.children = !!hasChildren;
        this.state = { opened: !!isOpen };
        this.a_attr = a_attr;
        this.li_attr = li_attr;
    }

};
