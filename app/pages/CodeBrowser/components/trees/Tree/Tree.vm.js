import 'jstree/dist/themes/default/style.min.css';
import 'jstree';
import './jstree.span_nodes';
import './jstree.html_text';
import $ from 'jquery';
import Base from 'core/classes/vm/Base.vm';
import { debounce } from 'core/classes/FunctionHelpers';

const options = {
    core: {
        animation: 50,
        dblclick_toggle: false,
        themes: {
            dots: false,
            icons: true,
        },
        force_text: true,
        data: [],
        worker: false,
    },
    plugins: [
        'search',
        'sort',
        'types',
        'wholerow',
        'span_nodes',
    ],
    search: {
        show_only_matches: true,
    },
    sort: function(a, b) {
        const types = this.settings.types,
            n1 = this.get_node(a).original,
            n2 = this.get_node(b).original,
            w1 = types[n1.type].sorting_weight,
            w2 = types[n2.type].sorting_weight,
            cmp = types[n1.type].comparator;

        return (cmp && n1.type === n2.type) ? cmp(n1, n2) : (w1 - w2 || n1.text.localeCompare(n2.text));
    },
};

/**
 *
 */
export default class Tree extends Base {

    constructor(element, bundle, {
        treeName,
        stateKeyName = false,
        jstreeOptions = {},
    } = {
        stateKeyName: false,
        jstreeOptions: {},
    }) {

        super(element);

        this._nodeIdPrefix = treeName;
        this._options = $.extend(
            true,
            { state: { key: `${stateKeyName}:${bundle.id}` } },
            options,
            jstreeOptions,
        );
        if(stateKeyName) { this._options.plugins.push('state'); }
        this._viewReady = null;
        this._jstree = null;
        this._pendingUiHandlerBinders = [];
        this._bundle = bundle;

        element.classList.add('component-tree');

        // Decide which will be the element for drawing the jstree.
        //
        // Search for any descendant elements with class "tree" and use the first one found; if no such element was
        // found, then use the component's element itself, i.e. `this.element`. If the derived class is not satisfied by
        // this selection method, then it should simply set manually the element it wants directly to `this._treeNode`.
        this._treeNode = element.getElementsByClassName('tree')[0] || element;
        if(this._treeNode === element) {
            element.classList.add('tree');
        }

        if(stateKeyName) {
            // Save scrolling state after scrolling has finished. jstree does not listen to scroll events, it stores the
            // scrolling state only when a change has occurred in the tree.
            this._treeNode.addEventListener('scroll', debounce(() => this._jstree.save_state(), 1000));
        }
    }

    dispose() {

        if(this._viewReady) {
            this._viewReady.then(() => this._jstree.element.off().jstree('destroy'));
        }
        else if(this._jstree) {
            this._jstree.element.off().jstree('destroy');
        }

        super.dispose();
    }

    /**
     *
     * @param loadChildrenFn    function(node, setChildrenFn)
     *      where "node" must be an object of a type that inherits "JstreeNode",
     *      and "setChildrenFn" the jstree callback that accepts an array of objects from which it creates the actual
     *      jstree nodes.
     */
    setLoadDataFn(loadChildrenFn) {

        this._options.core.data = (node, setChildrenFn) => {
            this._jstree && this._jstree.disable_node(node);
            loadChildrenFn(node.original, setChildrenFn);
        };
    }

    /**
     *
     * @param eventType         The event type, e.g. 'click', 'contextmenu', etc.
     * @param handlersPerTypes  An array of objects:
     *      {
     *          nodeTypes: ['application','type',...],
     *          handler: (e, node) => {}
     *      }
     */
    setUiEventHandlers(eventType, handlersPerTypes) {

        let handlerPerType = handlersPerTypes.reduce((acc, handlerPerTypes) => {
            handlerPerTypes.nodeTypes.forEach(nodeType => {
                acc[nodeType] = handlerPerTypes.handler;
            });
            return acc;
        }, {});

        let binder = () => {
            if(eventType === 'click') {
                this._jstree.element.on(eventType, '.jstree-anchor', e => {
                    let node = this._jstree.get_node(e.target),
                        handler = handlerPerType[node.type];

                    if(handler) {
                        handler(e.originalEvent, node.original);
                    }
                    else if(!node.state.loading) {
                        // Do not queue clicks. In case of successful loading they are ignored,
                        // but in case of failed loading they get queued and fired one after the other.
                        this._jstree.toggle_node(node);
                    }
                });
            }
            else if(eventType === 'contextmenu') {
                this._jstree.element.on(eventType, '.jstree-node', e => {

                    let node = this._jstree.get_node(e.target),
                        handler = handlerPerType[node.type];

                    if(handler) {
                        this._jstree.deselect_all();
                        this._jstree.select_node(node);
                        handler(e.originalEvent, node.original);
                    }
                });
            }
        };

        this._jstree ? binder() : this._pendingUiHandlerBinders.push(binder);
    }

    /**
     * Should be called once by the derived class in order to create the tree.
     *
     * @returns {null|*}    The promise of successful tree creation.
     */
    draw() {

        this._viewReady = new Promise((resolve, reject) => {
            $(this._treeNode).jstree(this._options).on('ready.jstree', (e, data) => {
                resolve(data.instance);
            }).on('load_node.jstree', (e, data) => {
                data.instance.enable_node(data.node);
            });
        }).then(jstree => {
            this._jstree = jstree;
            this._pendingUiHandlerBinders.forEach(binder => binder());
        });

        // Return the promise in case the derived class needs to use it.
        return this._viewReady;
    }

    refresh() {

        this._jstree && this._jstree.refresh(false);
    }

};
