
/**
 * ### Custom 'span_nodes' plugin
 *
 * This plugin uses spans instead of anchors for tree nodes.
 * The goal is to avoid the appearance of the browser's status bar when the mouse hovers over a tree node.
 */
/*globals jQuery, define, exports, require, document */
(function(factory) {
    "use strict";
    if(typeof define === 'function' && define.amd) {
        define('jstree.span_nodes', ['jquery','jstree'], factory);
    }
    else if(typeof exports === 'object') {
        factory(require('jquery'), require('jstree'));
    }
    else {
        factory(jQuery, jQuery.jstree);
    }
}(function($, jstree, undefined) {
    "use strict";

    if($.jstree.plugins.span_nodes) { return; }

    $.jstree.plugins.span_nodes = function(options, parent) {

        this._create_prototype_node = function() {

            const node = document.createElement('li');
            node.setAttribute('role', 'treeitem');

            const i1 = document.createElement('i');
            i1.className = 'jstree-icon jstree-ocl';
            i1.setAttribute('role', 'presentation');
            node.appendChild(i1);

            const span = document.createElement('span');
            span.className = 'jstree-anchor';
            span.setAttribute('tabindex','-1');
            span.style.cursor = 'pointer';
            node.appendChild(span);

            const i2 = document.createElement('i');
            i2.className = 'jstree-icon jstree-themeicon';
            i2.setAttribute('role', 'presentation');
            span.appendChild(i2);

            return node;
        };
    };

}));
