
/**
 * ### Custom 'html_text' plugin
 *
 * This plugin allows to give HTML as a node's text.
 */
/*globals jQuery, define, exports, require, document */
(function(factory) {
    "use strict";
    if(typeof define === 'function' && define.amd) {
        define('jstree.html_text', ['jquery','jstree'], factory);
    }
    else if(typeof exports === 'object') {
        factory(require('jquery'), require('jstree'));
    }
    else {
        factory(jQuery, jQuery.jstree);
    }
}(function($, jstree, undefined) {
    "use strict";

    if($.jstree.plugins.html_text) { return; }

    $.jstree.plugins.html_text = function(options, parent) {

        this.redraw_node = function(obj, deep, callback, force_draw) {

            const element = parent.redraw_node.apply(this, arguments);

            if(!element) return element;

            const anchor = element.getElementsByClassName('jstree-anchor')[0],
                textNode = anchor.childNodes[1],
                span = document.createElement('span');

            span.classList.add('jstree-text');
            span.innerHTML = textNode.textContent;
            textNode.remove();
            anchor.appendChild(span);

            return element;
        };
    };

}));
