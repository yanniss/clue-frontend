import './PackageTree.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './PackageTree.vm';

koh.registerComponent('component-package-tree', vm);
