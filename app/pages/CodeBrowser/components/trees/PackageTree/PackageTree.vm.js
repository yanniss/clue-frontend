import Api from 'app/classes/Api.static';
import Tree from '../Tree/Tree.vm';
import ApplicationNode from '../Tree/classes/ApplicationNode';
import DependenciesNode from '../Tree/classes/DependeciesNode';
import PlatformNode from '../Tree/classes/PlatformNode';
import TypeNode from '../Tree/classes/TypeNode';
import PackageNode from './classes/PackageNode';
import { CTX_PAGE } from 'core/classes/events/contexts';
import { FILE_RAW_OPEN } from 'app/classes/eventTypes';

const typesOptions = {
    ...ApplicationNode.typesOptions,
    ...DependenciesNode.typesOptions,
    ...PlatformNode.typesOptions,
    ...TypeNode.typesOptions,
    ...PackageNode.typesOptions,
};

/**
 *
 */
export default class PackageTree extends Tree {

    constructor(element, { bundle }) {

        super(element, bundle, {
            treeName: 'PackageTree',
            stateKeyName: 'PackageTree',
            jstreeOptions: { types: typesOptions },
        });

        this._bundle = bundle;

        this.setLoadDataFn(this._loadNodeChildren.bind(this));
        this.setUiEventHandlers('click', [{
            nodeTypes: ['class', 'interface', 'enum'],
            handler: (e, node) => this.notify(CTX_PAGE, FILE_RAW_OPEN, {
                bundleId: bundle.id,
                filepath: node.filepath,
                line: node.position.startLine,
                column: node.position.startColumn,
            }),
        }]);
        this.draw();
    }

    dispose() {

        super.dispose();
    }

    _loadNodeChildren(node, setChildrenFn) {

        if(!node) {
            this._loadRootChildren(setChildrenFn);
        }
        else if(['application', 'dependencies', 'platform', 'package'].includes(node.type)) {
            this._loadPackageChildren(node, setChildrenFn);
        }
        else {
            setChildrenFn([]);
        }
    }

    _loadRootChildren(setChildrenFn) {

        setChildrenFn([
            new ApplicationNode(this),
            new DependenciesNode(this),
            new PlatformNode(this),
        ]);
    }

    /**
     *
     * @param parentNode        The node for which to set children nodes. Must be of type { application, dependencies, platform, package }.
     * @param setChildrenFn
     * @private
     */
    _loadPackageChildren(parentNode, setChildrenFn) {

        let context, packge;

        switch(parentNode.type) {
            case 'application':
                context = 'application';
                packge  = '';
                break;
            case 'dependencies':
                context = 'dependencies';
                packge  = '';
                break;
            case 'platform':
                context = 'platform';
                packge  = '';
                break;
            default /*'package'*/:
                context = parentNode.context;
                packge  = parentNode.packageId;
                break;
        }

        Api.fetchPackage(this._bundle.id, context, packge)
            .then(contents => {
                const hasExactlyOneChild = contents.packages.length + contents.types.length === 1;

                const packages = contents.packages.map(packge =>
                    new PackageNode(parentNode, context, packge, hasExactlyOneChild)
                );
                const types = contents.types.map(type =>
                    new TypeNode(parentNode, type, false, false)
                );

                setChildrenFn([...packages, ...types]);
            })
            .catch(() => {
                setChildrenFn(false);
            });
    }

};
