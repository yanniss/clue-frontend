import Node from '../../Tree/classes/Node';
import './PackageNode.scss';

/**
 *
 */
export default class PackageNode extends Node {

    static typesOptions = {
        package: {
            icon: 'package fa fa-folder-o',
            sorting_weight: 100,
        },
    };

    constructor(parentNode, context, name, isOpen) {

        super(`${parentNode.id}/${name}`, 'package', name, true, isOpen);

        this.context   = context;
        this.packageId = parentNode instanceof PackageNode ? `${parentNode.packageId}.${name}` : name;
    }

};
