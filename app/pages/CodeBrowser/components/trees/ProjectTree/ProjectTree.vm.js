import Api from 'app/classes/Api.static';
import Tree from '../Tree/Tree.vm';
import ApplicationNode from '../Tree/classes/ApplicationNode';
import DependenciesNode from '../Tree/classes/DependeciesNode';
import PlatformNode from '../Tree/classes/PlatformNode';
import TypeNode from '../Tree/classes/TypeNode';
import ArtifactNode from './classes/ArtifactNode';
import DirectoryNode from './classes/DirectoryNode';
import FileNode from './classes/FileNode';
import { CTX_PAGE } from 'core/classes/events/contexts';
import { FILE_RAW_OPEN } from 'app/classes/eventTypes';

const typesOptions = {
    ...ApplicationNode.typesOptions,
    ...DependenciesNode.typesOptions,
    ...PlatformNode.typesOptions,
    ...TypeNode.typesOptions,
    ...ArtifactNode.typesOptions,
    ...DirectoryNode.typesOptions,
    ...FileNode.typesOptions,
};

/**
 *
 */
export default class ProjectTree extends Tree {

    bundle;

    constructor(element, { bundle }) {

        super(element, bundle, {
            treeName: 'ProjectTree',
            stateKeyName: 'ProjectTree',
            jstreeOptions: {
                core: { check_callback: true },
                types: typesOptions,
            },
        });

        this.bundle = bundle;

        this.setLoadDataFn(this._loadNodeChildren.bind(this));

        this.setUiEventHandlers('click', [
            {
                nodeTypes: ['class', 'interface', 'enum'],
                handler: (e, node) => this.notify(CTX_PAGE, FILE_RAW_OPEN, {
                    bundleId: bundle.id,
                    filepath: node.filepath,
                    line: node.position.startLine,
                    column: node.position.startColumn,
                }),
            },
            {
                nodeTypes: ['file'],
                handler: (e, node) => this.notify(CTX_PAGE, FILE_RAW_OPEN, {
                    bundleId: bundle.id,
                    artifactId: node.artifact,
                    filepath: node.filepath,
                }),
            },
        ]);

        this.draw();
    }

    _loadNodeChildren(node, setChildrenFn) {

        if(!node) {
            this._loadRootChildren(setChildrenFn);
        }
        else if(['application', 'dependencies', 'platform'].includes(node.type)) {
            this._loadContextChildren(node, setChildrenFn);
        }
        else if(['artifact', 'directory'].includes(node.type)) {
            this._loadDirectoryChildren(node, setChildrenFn);
        }
        else {
            setChildrenFn([]);
        }
    }

    _loadRootChildren(setChildrenFn) {

        setChildrenFn([
            new ApplicationNode(this),
            new DependenciesNode(this),
            new PlatformNode(this),
        ]);
    }

    /**
     *
     * @param parentNode        The node for which to set children nodes. Must be of type { application, dependencies, platform }.
     * @param setChildrenFn
     * @private
     */
    _loadContextChildren(parentNode, setChildrenFn) {

        Api.fetchArtifacts(this.bundle.id)
            .then(artifacts => {
                switch(parentNode.type) {
                    case 'application':  artifacts = artifacts.application; break;
                    case 'dependencies': artifacts = artifacts.dependency;  break;
                    default:             artifacts = artifacts.platform;    break;
                }
                let hasExactlyOneChild = artifacts.length === 1;

                setChildrenFn(artifacts.map(artifactName =>
                    new ArtifactNode(parentNode, artifactName, hasExactlyOneChild)
                ));
            })
            .catch(() => {
                setChildrenFn(false);
            });
    }

    /**
     *
     * @param parentNode        The node for which to set children nodes. Must be of type { artifact, directory }.
     * @param setChildrenFn
     * @private
     */
    _loadDirectoryChildren(parentNode, setChildrenFn) {

        Api.fetchFile(this.bundle.id, parentNode.artifact, parentNode.filepath)
            .then(contents => {
                const hasExactlyOneChild = contents.directories.length + contents.types.length + contents.simpleFiles.length === 1;

                const directories = contents.directories.map(directory =>
                    new DirectoryNode(parentNode, directory.name, hasExactlyOneChild)
                );
                const types = contents.types.map(type =>
                    new TypeNode(parentNode, type, false, false)
                );
                const simpleFiles = contents.simpleFiles.map(file =>
                    new FileNode(parentNode, file.name, file.sourceFileName)
                );
                setChildrenFn([...directories, ...types, ...simpleFiles]);
            })
            .catch(() => {
                setChildrenFn(false);
            });
    }

};
