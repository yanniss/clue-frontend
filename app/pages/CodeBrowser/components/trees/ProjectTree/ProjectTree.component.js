import './ProjectTree.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './ProjectTree.vm';

koh.registerComponent('component-project-tree', vm);
