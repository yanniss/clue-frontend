import Node from '../../Tree/classes/Node';
import './FileNode.scss';

/**
 *
 */
export default class DirectoryNode extends Node {

    static typesOptions = {
        file: {
            icon: 'file-node fa fa-file-o',
            sorting_weight: 200,
        },
    };

    constructor(parentNode, name, isOpen) {

        super(`${parentNode.id}/${name}`, 'file', name, true, isOpen);

        this.artifact = parentNode.artifact;
        this.filepath = `${parentNode.filepath && `${parentNode.filepath}/`}` + name;
    }

};
