import Node from '../../Tree/classes/Node';

/**
 *
 */
export default class ArtifactNode extends Node {

    static typesOptions = {
        artifact: {
            icon: 'artifact fa fa-cube',
            sorting_weight: 70,
        },
    };

    constructor(parentNode, name, isOpen) {

        super(`${parentNode.id}/${name}`, 'artifact', name, true, isOpen);

        this.artifact = name;
        this.filepath = '';
    }

};
