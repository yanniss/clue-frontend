import Node from '../../Tree/classes/Node';

/**
 *
 */
export default class DirectoryNode extends Node {

    static typesOptions = {
        directory: {
            icon: 'directory fa fa-folder-o',
            sorting_weight: 100,
        },
    };

    constructor(parentNode, name, isOpen) {

        super(`${parentNode.id}/${name}`, 'directory', name, true, isOpen);

        this.artifact = parentNode.artifact;
        this.filepath = `${parentNode.filepath && `${parentNode.filepath}/`}` + name;
    }

};
