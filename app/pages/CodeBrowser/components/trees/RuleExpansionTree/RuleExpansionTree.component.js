import './RuleExpansionTree.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './RuleExpansionTree.vm';

koh.registerComponent('component-rule-expansion-tree', vm);
