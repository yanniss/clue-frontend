import Api from 'app/classes/Api.static';
import Tree from '../Tree/Tree.vm';
import PackageNode from '../PackageTree/classes/PackageNode';
import TypeNode from '../Tree/classes/TypeNode';
import MethodNode from '../FileStructureTree/classes/MethodNode';
import { CTX_PAGE } from 'core/classes/events/contexts';
import { FILE_RAW_OPEN } from 'app/classes/eventTypes';

const typesOptions = {
    ...PackageNode.typesOptions,
    ...TypeNode.typesOptions,
    ...MethodNode.typesOptions,
};

/**
 *
 */
export default class RuleExpansionTree extends Tree {

    constructor(element, { bundleId, configurationName, ruleId }) {

        super(element, { id: bundleId }, {
            treeName: 'RuleExpansionTree',
            jstreeOptions: {
                plugins: ['html_text'],
                types: typesOptions,
            },
        });

        this.bundleId = bundleId;
        this.configurationName = configurationName;
        this.ruleId = ruleId;

        this.setLoadDataFn(this._loadNodeChildren.bind(this));
        this.setUiEventHandlers('click', [{
            nodeTypes: ['method'],
            handler: (e, node) => this.notify(CTX_PAGE, FILE_RAW_OPEN, {
                bundleId: bundleId,
                filepath: node.filepath,
                line: node.position.startLine,
                column: node.position.startColumn,
            }),
        }]);
        this.draw();
    }

    _loadNodeChildren(node, setChildrenFn) {

        let packge, clazz, createNode;

        const getNodeNamePostfix = count =>
            `<span class="count" title="Number of matching methods">(${count})</span>`;

        if(!node) {
            createNode = packge => {
                const nodeName = `${packge.name} ${getNodeNamePostfix(packge.matchingMethods)}`;
                const packageNode = new PackageNode(this, null, nodeName);
                packageNode.name = packge.name;
                return packageNode;
            }
        }
        else if(node.type === 'package') {
            packge = node.name;

            createNode = type => new TypeNode(this, type, true, false, null, getNodeNamePostfix(type.matchingMethods));
        }
        else if(['class', 'interface', 'enum'].includes(node.type)) {
            packge = node.symbol.packageName;
            clazz = node.symbol.name;

            createNode = method => new MethodNode(this, {
                ...method,
                name: method.methodName,
            }, true);
        }
        else {
            setChildrenFn([]);
            return;
        }

        Api.fetchRuleContents(this.bundleId, this.configurationName, this.ruleId, packge, clazz)
            .then(children => {
                setChildrenFn(children.map(child => createNode(child)));
            })
            .catch(error => {
                console.log(error);
                setChildrenFn(false)
            });
    }

};
