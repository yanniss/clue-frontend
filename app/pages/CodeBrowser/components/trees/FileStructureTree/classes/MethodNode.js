import SymbolNode from '../../Tree/classes/SymbolNode';
import str from 'core/classes/StringHelper.static';
import './MethodNode.scss';

/**
 *
 */
export default class MethodNode extends SymbolNode {

    static typesOptions = {
        method: {
            icon: 'method-node fa fa-circle',
            sorting_weight: 210,
        },
    };

    constructor(tree, method, escapeName = false) {

        let cssClass = '';
        method.analysisDirective && (cssClass += ` directive-analysis-${method.analysisDirective.type}`);
        method.bundleDirective && (cssClass += ` directive-user-${method.bundleDirective.type}`);

        let methodName      = (method.name === '<init>' && method.sourceFileName.endsWith('.java') ? parentNode.text : method.name),
            params          = method.paramTypes.join(', '),
            paramsShortened = params.substr(0, 30 - methodName.length),
            text            = `${methodName} (${paramsShortened}${params.length !== paramsShortened.length ? ' ...' : ''}): ${method.returnType}`,
            a_attr          = {
                title: `${methodName} (${params}) : ${method.returnType}`,
                class: cssClass,
            };

        super(tree, method, 'method', escapeName ? str.escapeHtml(text) : text, true, true, a_attr);
    }

};
