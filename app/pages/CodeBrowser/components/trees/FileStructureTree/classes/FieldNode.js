import SymbolNode from '../../Tree/classes/SymbolNode';
import './FieldNode.scss';

/**
 *
 */
export default class FieldNode extends SymbolNode {

    static typesOptions = {
        field: {
            icon: 'field-node fa fa-circle',
            sorting_weight: 210,
        },
    };

    constructor(tree, field) {

        super(tree, field, 'field', `${field.name}: ${field.type}`, false, false, { title: field.doopId });
    }

};
