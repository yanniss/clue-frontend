import './FileStructureTree.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './FileStructureTree.vm';

koh.registerComponent('component-file-structure-tree', vm);
