import Tree from '../Tree/Tree.vm';
import SymbolNode from '../Tree/classes/SymbolNode';
import TypeNode from '../Tree/classes/TypeNode';
import FieldNode from './classes/FieldNode';
import MethodNode from './classes/MethodNode';
import ko from 'knockout';
import { CTX_PAGE } from 'core/classes/events/contexts';
import {
    ANALYSIS_ACTIVATED,
    DIRECTIVE_DELETE,
    DIRECTIVE_KEEP_METHOD,
    DIRECTIVE_MENU_OPEN, DIRECTIVE_REMOVE_METHOD,
    FILE_RAW_OPEN
} from 'app/classes/eventTypes';
import { CONFIGURATION_ACTIVATED } from 'codebrowser/classes/configuration/Configuration.events';
import PromiseQueue from 'core/classes/PromiseQueue';

const typesOptions = {
    ...TypeNode.typesOptions,
    ...FieldNode.typesOptions,
    ...MethodNode.typesOptions,
};

/**
 *
 */
export default class FileStructureTree extends Tree {

    requests = new PromiseQueue();
    _searchText = ko.observable('');
    _data = FileStructureTree._organizeSymbols([], [], [], {}, {});
    directives = {
        bundle: {},
        analysis: {},
    };

    constructor(element, { bundle, searchText }) {

        super(element, bundle, {
            treeName: 'FileStructureTree',
            stateKeyName: 'FileStructureTree',
            jstreeOptions: { types: typesOptions },
        });

        this._searchText = searchText;

        this.setLoadDataFn(this._loadNodeChildren.bind(this));
        this.setUiEventHandlers('click', [{
            nodeTypes: ['class', 'interface', 'enum', 'field', 'method'],
            handler: (e, node) => this.notify(CTX_PAGE, FILE_RAW_OPEN, {
                filepath: node.filepath,
                line: node.position.startLine,
                column: node.position.startColumn,
            }),
        }]);
        this.setUiEventHandlers('contextmenu', [{
            nodeTypes: ['method'],
            handler: (e, node) => this.notify(CTX_PAGE, DIRECTIVE_MENU_OPEN, {
                method: {
                    ...node.symbol,
                    bundleDirective: this.directives.bundle[node.symbol.doopId],
                    analysisDirective: this.directives.analysis[node.symbol.doopId],
                },
                event: e,
            }),
        }]);
        this.draw();
        this._setupKoSubscriptions();
        this.startListeningToPageEvents();
    }

    /**
     * @public
     * @param {File|undefined} file
     */
    openFile(file) {

        let promise;

        if(file && file.hasMetadata()) {
            promise = Promise.all([
                file.types.get(),
                file.fields.get(),
                file.methods.get(),
                file.analysisDirectives.get(),
                file.bundleDirectives.get(),
            ]);
        }
        else {
            promise = new Promise(resolve => resolve([[], [], [], {}, {}]));
        }

        this.requests.push(promise, ([types, fields, methods, analysisDirectives, bundleDirectives]) => {
            this.directives.bundle = bundleDirectives;
            this.directives.analysis = analysisDirectives;

            this._searchText('');
            this._data = FileStructureTree._organizeSymbols(types, fields, methods, analysisDirectives, bundleDirectives);
            this.refresh();
        });

        this.file = file;
    }

    _loadNodeChildren(node, setChildrenFn) {

        if(!node) {
            this._loadRootChildren(setChildrenFn);
        }
        else {
            this._loadSymbolChildren(node, setChildrenFn);
        }
    }

    _loadRootChildren(setChildrenFn) {

        setChildrenFn(this._data.topLevelTypes.map(type => {
            let a_attr = { title: type.doopId, style: type.name ? '' : 'color: #888' };
            return new TypeNode(this, type, true, true, a_attr);
        }));
    }

    _loadSymbolChildren(parentNode, setChildrenFn) {

        setChildrenFn(this._data.membersPerSymbol[parentNode.doopId].map(symbol => {
            switch(symbol.kind) {
                case 'Class':
                    let a_attr = { title: symbol.doopId, style: symbol.name ? '' : 'color: #888' };
                    return new TypeNode(this, symbol, true, true, a_attr);
                case 'Field':
                    return new FieldNode(this, symbol);
                case 'Method':
                    return new MethodNode(this, symbol);
            }
        }));
    }

    _setupKoSubscriptions() {

        this.subscribe(this._searchText, searchText => {
            this._jstree.search(searchText);
        });
    }

    static _organizeSymbols(classes, fields, methods, analysisDirectives, bundleDirectives) {

        let membersPerSymbol = {},
            topLevelTypes    = [],
            pendingSymbols   = [];

        classes.forEach(type => {
            membersPerSymbol[type.doopId] = [];

            if(!type.declaringSymbolDoopId) {
                topLevelTypes.push(type);
            }
            else if(membersPerSymbol.hasOwnProperty(type.declaringSymbolDoopId)) {
                membersPerSymbol[type.declaringSymbolDoopId].push(type);
            }
            else {
                pendingSymbols.push(type);
            }
        });
        fields.forEach(field => {
            membersPerSymbol[field.doopId] = [];

            if(membersPerSymbol.hasOwnProperty(field.declaringClassDoopId)) {
                membersPerSymbol[field.declaringClassDoopId].push(field);
            }
            else {
                pendingSymbols.push(field);
            }
        });
        methods.forEach(method => {
            membersPerSymbol[method.doopId] = [];
            method = {
                ...method,
                analysisDirective: analysisDirectives[method.doopId],
                bundleDirective: bundleDirectives[method.doopId],
            };

            if(membersPerSymbol.hasOwnProperty(method.declaringClassDoopId)) {
                membersPerSymbol[method.declaringClassDoopId].push(method);
            }
            else {
                pendingSymbols.push(method);
            }
        });

        pendingSymbols.forEach(symbol => {
            let encSymbolDoopId = symbol.declaringSymbolDoopId || symbol.declaringClassDoopId;
            if(membersPerSymbol.hasOwnProperty(encSymbolDoopId)) {
                membersPerSymbol[encSymbolDoopId].push(symbol);
            }
        });

        return { membersPerSymbol: membersPerSymbol, topLevelTypes: topLevelTypes };
    }

    startListeningToPageEvents() {

        this.on(CTX_PAGE, ANALYSIS_ACTIVATED, () => {
            this.openFile(this.file);
        });

        this.on(CTX_PAGE, CONFIGURATION_ACTIVATED, () => {
            this.openFile(this.file);
        });

        const getElement = doopId => document.getElementById(`${SymbolNode.createSymbolNodeId(this, doopId)}_anchor`);

        this.on(CTX_PAGE, DIRECTIVE_KEEP_METHOD, method => {
            const element = getElement(method.doopId);
            if(!element) { return; }
            const classList = element.classList;
            classList.add('directive-user-KEEP');
            classList.remove('directive-user-REMOVE');
            this.directives.bundle[method.doopId] = method.bundleDirective;
        }, { ignoreIfEquals: false });

        this.on(CTX_PAGE, DIRECTIVE_REMOVE_METHOD, method => {
            const element = getElement(method.doopId);
            if(!element) { return; }
            const classList = element.classList;
            classList.add('directive-user-REMOVE');
            classList.remove('directive-user-KEEP');
            this.directives.bundle[method.doopId] = method.bundleDirective;
        }, { ignoreIfEquals: false });

        this.on(CTX_PAGE, DIRECTIVE_DELETE, method => {
            const element = getElement(method.doopId);
            if(!element) { return; }
            const classList = element.classList;
            classList.remove('directive-user-REMOVE');
            classList.remove('directive-user-KEEP');
            delete this.directives.bundle[method.doopId];
        }, { ignoreIfEquals: false });
    }

    /*
    static _updateTitleWithUserOptimizationDirective(element, directive) {

        const prefix = '* User optimization directive: ';
        const regex = new RegExp(`\\${prefix}(KEEP|REMOVE)`);
        const prevTitle = element.title;
        let newTitle;

        if(directive) {
            newTitle = prevTitle.replace(regex, `${prefix}${directive}`);
            if(prevTitle.length === newTitle.length) {
                newTitle += `${prefix}${directive}`;
            }
        }
        else {
            newTitle = prevTitle.replace(regex, '');
        }

        element.title = newTitle;
    }
    */

};
