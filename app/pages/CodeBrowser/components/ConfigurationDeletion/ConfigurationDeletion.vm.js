import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import $ from 'jquery';
import Api from 'app/classes/Api.static';
import Notifier from 'app/classes/Notifier.static';
import { CTX_PAGE } from 'core/classes/events/contexts';
import { CONFIGURATION_OPEN_DELETION_VERIFICATION, CONFIGURATION_DELETED } from 'codebrowser/classes/configuration/Configuration.events';

/**
 *
 */
export default class ConfigurationDeletion extends Base {

    constructor(element) {

        super(element);

        this.startListeningToPageEvents();
    }

    dispose() {

        this.$modal.removeClass('fade');
        this.$modal.modal('hide');
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    configuration = ko.observable({});
    $modal = $(this.element.getElementsByClassName('modal')[0]);
    $submitButton = $(this.element.querySelector('button[type="submit"]'));

    startListeningToPageEvents() {

        this.on(CTX_PAGE, CONFIGURATION_OPEN_DELETION_VERIFICATION, configuration => {
            this.configuration(configuration);
            this.$modal.modal('show');
        }, { ignoreIfEquals: false });
    }

    submit() {

        this.$submitButton.button('loading');

        this.configuration().delete()
            .catch(() => {})
            .then(() => {
                this.$submitButton.button('reset');
                this.$modal.modal('hide');
                this.configuration({});
            });
    }

};
