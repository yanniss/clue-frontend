import './ConfigurationDeletion.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './ConfigurationDeletion.vm';
import view from './ConfigurationDeletion.view.html';

koh.registerComponent('component-configuration-deletion', vm, view);
