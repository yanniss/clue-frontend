import './DirectiveMenu.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './DirectiveMenu.vm';
import view from './DirectiveMenu.view.html';

koh.registerComponent('component-opt-dir-menu', vm, view);
