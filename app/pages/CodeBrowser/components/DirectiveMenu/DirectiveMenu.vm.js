import $ from 'jquery';
import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import { CTX_PAGE } from 'core/classes/events/contexts';
import { DIRECTIVE_DELETE, DIRECTIVE_MENU_OPEN } from 'app/classes/eventTypes';
import { CONFIGURATION_ACTIVATED } from 'codebrowser/classes/configuration/Configuration.events';
import Api from 'app/classes/Api.static';
import Notifier from 'app/classes/Notifier.static';

/**
 *
 */
export default class DirectiveMenu extends Base {

    /**
     * @public
     * @param {HTMLElement} element
     * @param {Bundle} bundle
     */
    constructor(element, { bundle }) {

        super(element);

        this.bundle = bundle;

        this.startListeningToUiEvents();
        this.startListeningToPageEvents();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    hasDirective() {

        return !!this.method().bundleDirective;
    }

    canKeepMethod() {

        return !!this.activeConfiguration && (!this.hasDirective() || this.method.peek().bundleDirective.type === 'REMOVE');
    }

    keepMethod() {

        if(!this.canKeepMethod()) {
            Notifier.warning('You must first select a configuration from <strong>Bundle configurations</strong>');
            return;
        }

        const method = this.method();
        let promise;

        if(this.hasDirective()) {
            promise = Api.editOptimizationDirective(this.bundle.id, method.bundleDirective.id, 'KEEP');
        }
        else {
            /*
            promise = Api.addOptimizationDirective(this.bundle.id, $(`
                <form enctype="application/x-www-form-urlencoded">
                    <input type="text" name="doopId" value="${method.doopId}">
                    <input type="text" name="typeOfElement" value="Method">
                    <input type="text" name="typeOfDirective" value="KEEP">
                    <input type="text" name="set" value="${this.activeConfiguration}">
                </form>
            `));
             */
            promise = Api.addRuleFromDirective(this.bundle.id, this.activeConfiguration, method.doopId, "KEEP");
        }

        promise.then(directive => {
            console.log(directive)
            /*
            method.bundleDirective = new Directive(directive);
            this.notify(CTX_PAGE, DIRECTIVE_KEEP_METHOD, method);
            */
        });
    }

    canRemoveMethod() {

        return !!this.activeConfiguration && (!this.hasDirective() || this.method.peek().bundleDirective.type === 'KEEP');
    }

    removeMethod() {

        if(!this.canRemoveMethod()) {
            Notifier.warning('You must first select a configuration from <strong>Bundle configurations</strong>');
            return;
        }

        const method = this.method();
        let promise;

        if(this.hasDirective()) {
            promise = Api.editOptimizationDirective(this.bundle.id, method.bundleDirective.id, 'REMOVE');
        }
        else {
            /*
            promise = Api.addOptimizationDirective(this.bundle.id, $(`
                <form enctype="application/x-www-form-urlencoded">
                    <input type="text" name="doopId" value="${method.doopId}">
                    <input type="text" name="typeOfElement" value="Method">
                    <input type="text" name="typeOfDirective" value="REMOVE">
                    <input type="text" name="set" value="${this.activeConfiguration}">
                </form>
            `));
            */
            promise = Api.addRuleFromDirective(this.bundle.id, this.activeConfiguration, method.doopId, "REMOVE");
        }

        promise.then(directive => {
            /*
            method.bundleDirective = new Directive(directive);
            this.notify(CTX_PAGE, DIRECTIVE_REMOVE_METHOD, method);
            */
            console.log(directive);
        });
    }

    deleteDirective() {

        if(!this.hasDirective()) {
            Notifier.warning('You must first select a configuration from <strong>Bundle configurations</strong>');
            return;
        }

        const method = this.method();

        Api.deleteDirective(this.bundle.id, method.bundleDirective.id).then(() => {
            method.bundleDirective = null;
            this.notify(CTX_PAGE, DIRECTIVE_DELETE, method);
        });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view model members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $element = $(this.element).css({ position: 'absolute', top: 0, left: 0 });
    $menu = this.$element.find('.dropdown-menu').css({ display: 'block', zIndex: -9999 });
    bundle;
    activeConfiguration = null;
    method = ko.observable();
    directive = ko.observable();

    /**
     * @private
     */
    startListeningToUiEvents() {

        this.addEventListener('click', e => {
            // Checking if the click was done with the left button is required for Firefox, since
            // right-clicking/contextmenu in Firefox triggers both "click" and "contextmenu" events.
            e.button === 0 && this.close();
        }, { capture: true }, document);

        this.addEventListener('contextmenu', e => {
            !this._deny_close && this.close();
        }, { capture: true }, document);
    }

    /**
     * @private
     */
    startListeningToPageEvents() {

        this.on(CTX_PAGE, DIRECTIVE_MENU_OPEN, ({ method, event }) => {
            this.method(method);
            this.open(this.getMenuPosition(event));
        });

        this.on(CTX_PAGE, CONFIGURATION_ACTIVATED, configuration => {
            this.activeConfiguration = configuration;
        })
    }

    /**
     * @private
     */
    getMenuPosition(event) {

        const $win = $(window),
            ww = $win.width(),
            wh = $win.height(),
            mw = this.$menu.outerWidth(true),
            mh = this.$menu.outerHeight(true),
            left = Math.max(5, Math.min(ww - mw - 5, event.clientX)),
            top = Math.max(5, Math.min(wh - mh - 5, event.clientY));

        return [ left, top ];
    }

    /**
     * @private
     */
    open([ left, top ]) {

        this.$element.css({ top: `${top}px`, left: `${left}px` });
        this.$menu.css({ zIndex: 9999 });

        /* This "_deny_close" flag helps fix buggy behaviour in Windows both FF and Chrome, where contextmenu clicks
           in <optimization-directive-list> do not open the menu, because the event triggers firstly for the element
           and then for document ... This should be a temp fix ...
         */
        this._deny_close = true;
        setTimeout(() => { this._deny_close = false; });
    }

    /**
     * @private
     */
    close() {

        this.$menu.css({ zIndex: -9999 });
    }

};
