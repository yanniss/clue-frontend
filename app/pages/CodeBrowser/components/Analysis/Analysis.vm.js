import Base from 'core/classes/vm/Base.vm';
import str from 'core/classes/StringHelper.static';
import $ from 'jquery';
import './components/ResourceMonitor/ResourceMonitor.component';
import 'codebrowser/components/Directives/Directives.component';

/**
 *
 */
export default class Analysis extends Base {

    constructor(element, { bundle, analysis }) {

        super(element);

        this.bundle = bundle;
        this.analysis = analysis;

        this.isAnalysisFinishedOnLoad = analysis.isFinished();

        this.$directivesTab.on('show.bs.tab', event =>
            analysis.isFinished() ? this.$directivesTab.off() : event.preventDefault()
        );
    }

    dispose() {

        this.$directivesTab.off();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bundle;
    analysis;
    uniqueIDs = {
        directivesTab: str.createUnique(),
        infoTab: str.createUnique(),
    };
    $directivesTab = $(this.element).find('a[aria-controls="analysis-directives"]');
    isAnalysisFinishedOnLoad;

};
