import './ResourceMonitor.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './ResourceMonitor.view.html';
import vm from './ResourceMonitor.vm';

koh.registerComponent('component-resource-monitor', vm, view);
