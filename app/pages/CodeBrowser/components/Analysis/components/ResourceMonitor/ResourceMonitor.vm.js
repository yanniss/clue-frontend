import Base from 'core/classes/vm/Base.vm';

/**
 *
 */
export default class ResourceMonitor extends Base {

    constructor(element, { analysis }) {

        super(element);

        this.analysis = analysis;
    }

};
