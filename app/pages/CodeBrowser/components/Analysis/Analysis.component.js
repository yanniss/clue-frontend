import './Analysis.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './Analysis.vm';
import view from './Analysis.view.html';

koh.registerComponent('component-analysis', vm, view);
