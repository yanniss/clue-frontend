import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import Api from 'app/classes/Api.static';
import Analysis from 'codebrowser/classes/analysis/Analysis';
import PromiseQueue from 'core/classes/PromiseQueue';
import TextFilter from './classes/TextFilter';
import { CTX_PAGE, CTX_DESCENDANTS } from 'core/classes/events/contexts';
import { DIRECTIVE_MENU_OPEN, FILE_RAW_OPEN } from 'app/classes/eventTypes';
import 'core/components/Pagination/Pagination.component';
import { PAGINATION_PAGE_CHANGED } from 'core/components/Pagination/Pagination.events';
import 'core/components/Dropdown/Dropdown.component';
import { DROPDOWN_OPTION_CHANGED } from 'core/components/Dropdown/Dropdown.events';
import 'core/bindings/numberFormatted';
import { FACET_ENABLED_OPTIONS_CHANGED } from 'core/components/Facet/Facet.events';
import 'core/components/Facet/Facet.component';
import 'core/bindings/rclick';
import Configuration from 'codebrowser/classes/configuration/Configuration';

const FACET_CATEGORIES_FOR_ANALYSIS = {
    package: {
        label: 'Package',
        description: '?'
    },
    class: {
        label: 'Class',
        description: '?'
    }
};

const FACET_CATEGORIES_FOR_CONFIGURATION = {
    directiveType: {
        label: 'Type of directive',
        description: '?'
    },
    originType: {
        label:'Type of origin',
        description: '?'
    },
    origin: {
        label:'Origin',
        description: '?'
    },
    ruleId: {
        label: 'Rules',
        description:'?'
    },
    package: {
        label: 'Package',
        description: '?'
    },
    class: {
        label: 'Class',
        description: '?'
    },
};

/**
 *
 */
export default class Directives extends Base {

    /**
     * @param {HTMLElement} element
     * @param {Bundle} bundle
     * @param {Configuration|Analysis} source
     */
    constructor(element, { bundle, source }) {

        super(element);

        this.bundle = bundle;
        this.source = source;

        this.startListeningToChildrenEvents();
        this.startListeningToPageEvents();
    }

    resetWithRule(rule) {

        this.children.facet.clearEnabledOptions();
        this.children.facet.enableCategoryOption('ruleId', rule.id);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    sortByOptions = [
        { text: 'Package', value: 'packageName|ASC', order: 'asc', type: 'alpha' },
        { text: 'Package', value: 'packageName|DESC', order: 'desc', type: 'alpha' },
        null,
        { text: 'Class', value: 'className|ASC', order: 'asc', type: 'alpha' },
        { text: 'Class', value: 'className|DESC', order: 'desc', type: 'alpha' },
        null,
        { text: 'Method', value: 'methodName|ASC', order: 'asc', type: 'alpha' },
        { text: 'Method', value: 'methodName|DESC', order: 'desc', type: 'alpha' },
    ];

    elem_filterToggler = this.element.getElementsByClassName('fa-filter')[0];
    elementFilters = this.element.getElementsByClassName('filters')[0];

    bundle;

    textFilter = new TextFilter();
    facetEnabledOptions = ko.observable({});
    hasAppliedFilters = this.computed(() =>
        !!this.textFilter.text() || Object.keys(this.facetEnabledOptions()).length > 0
    );

    count = ko.observable(0);
    directives = ko.observableArray();

    source;
    page = ko.observable(1);
    sortBy = ko.observable();
    loadFacets = false;

    requests = new PromiseQueue();

    startListeningToChildrenEvents() {

        this.on(CTX_DESCENDANTS, PAGINATION_PAGE_CHANGED, page => {

            this.page(Math.max(parseInt(page), 1));
        });

        this.on(CTX_DESCENDANTS, DROPDOWN_OPTION_CHANGED, sortBy => {

            this.sortBy(sortBy.value);
        });

        this.on(CTX_DESCENDANTS, FACET_ENABLED_OPTIONS_CHANGED, enabledOptions => {

            this.facetEnabledOptions(enabledOptions);
        });
    }

    startListeningToPageEvents() {

        // if(this.source instanceof Analysis) { return; }

        // if current directive set is from this configuration, then reload directives.
    }

    initAfterBinding() {

        this.subscribe(this.page, page => {

            this.children.pagination.goto(page);
            const loadFacets = !!this.loadFacets;
            this.loadFacets = false;
            this.loadDirectives(loadFacets);
        });

        this.subscribe(this.sortBy, sortBy => {

            this.forceResetToPageOne();
        });

        this.computed(() => {

            this.textFilter.text();
            this.textFilter.lookupFields();
            this.facetEnabledOptions();

            this.forceResetToPageOne(true);

        }).extend({ deferred: true });
    }

    forceResetToPageOne(loadFacets = false) {

        this.loadFacets = !!loadFacets;

        this.page.peek() === 1 ? this.page.valueHasMutated() : this.page(1);
    }

    toggleFilters() {

        this.elem_filterToggler.classList.toggle('open');
        this.elementFilters.classList.toggle('hide');
    }

    clearFilters() {

        this.children.facet.clearEnabledOptions();
        this.textFilter.clear();
        this.facetEnabledOptions({});
    }

    openMethod = (directive, event) => {

        this.notify(CTX_PAGE, FILE_RAW_OPEN, {
            bundleId: this.bundle.id,
            filepath: directive.filepath,
            line: directive.line,
            column: directive.column,
        });
    };

    openMenu = (directive, e) => {

        if(this.source instanceof Analysis) { return; }

        this.notify(CTX_PAGE, DIRECTIVE_MENU_OPEN, {
            method: {
                ...directive,
                bundleDirective: directive,
            },
            event: e,
        });
    };

    convertFacetEnabledOptionsToApiOptions() {

        return Object.entries(this.facetEnabledOptions.peek()).reduce((acc, [ facetName, enabledOptions ]) => ({
            ...acc,
            [facetName]: Array.from(Object.keys(enabledOptions)),
        }), {});
    }

    convertTextFilterOptionsToApiOptions() {

        return {
            textType: this.textFilter.textType,
            text: this.textFilter.text(),
            textField: this.textFilter.lookupFields(),
        };
    }

    loadDirectives(fetchFacets = false) {

        const options = {
            source: this.source,
            page: Math.max(this.page.peek(), 1),
            sortBy: this.sortBy.peek() || this.sortByOptions[0].value,
            resultsPerPage: 15,
            facets: fetchFacets,
            textFilters: this.convertTextFilterOptionsToApiOptions(),
            facetOptions: this.convertFacetEnabledOptionsToApiOptions(),
        };

        this.requests.push(Api.fetchDirectives(this.bundle.id, options), ({ hits, directives, facets }) => {
            this.count(hits);
            this.directives(directives);
            this.children.pagination.setTotal(Math.ceil(hits/options.resultsPerPage));

            if(fetchFacets) {
                const activeCategoriesInfo = this.source instanceof Analysis
                    ? FACET_CATEGORIES_FOR_ANALYSIS
                    : FACET_CATEGORIES_FOR_CONFIGURATION;

                this.children.facet.update(facets, activeCategoriesInfo);
            }
        });
    }

};
