import ko from 'knockout';

/**
 *
 */
export default class TextFilter {

    //user input
    text = ko.observable();

    //type of input
    textType = 'W'; //Wildcard (also supported: R-Regex,FT-FullText, but not tested)

    //where to look it up
    lookupFields = ko.observableArray(['package']); //allowed values: 'package', 'class', 'method'

    clear() {
        this.text(null);
        this.lookupFields(['package']);
    }

};
