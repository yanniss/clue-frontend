import './Directives.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './Directives.vm';
import view from './Directives.view.html';

koh.registerComponent('component-directives', vm, view);
