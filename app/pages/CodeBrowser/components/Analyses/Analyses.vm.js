import Tree from '../trees/Tree/Tree.vm';
import ko from 'knockout';
import Api from 'app/classes/Api.static';
import AnalysisNode from './classes/AnalysisNode';
import { CTX_PAGE } from 'core/classes/events/contexts';
import {
    ANALYSIS_OPEN_CREATION_FORM,
    ANALYSIS_OPEN,
    ANALYSIS_CREATED,
    ANALYSIS_DELETED,
} from 'codebrowser/classes/analysis/Analysis.events';
import 'core/components/Menu/Menu.component';

const typesOptions = {
    ...AnalysisNode.typesOptions,
};

/**
 *
 */
export default class Analyses extends Tree {

    constructor(element, { bundle }) {

        super(element, bundle, {
            treeName: 'Analyses',
            stateKeyName: 'Analyses',
            jstreeOptions: {
                core: { check_callback: true },
                plugins: ['html_text'],
                types: typesOptions,
            },
        });

        this.bundle = bundle;

        this.setLoadDataFn(this.loadRootChildren);
        this.setUiEventHandlers();
        this.draw();
        this.startListeningToPageEvents();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bundle;
    analysisManager = this.requirePropertyInAncestors('analysisManager');
    rightClickedNode = ko.observable({});

    startListeningToPageEvents() {

        this.on(CTX_PAGE, ANALYSIS_CREATED, analysis =>

            this.insertNodeFor(analysis)
        );

        this.on(CTX_PAGE, ANALYSIS_DELETED, analysisId =>

            this.deleteNodeFor(analysisId)
        );
    }

    setUiEventHandlers() {

        super.setUiEventHandlers('click', [{
            nodeTypes: ['analysis'],
            handler: (e, node) => {
                this.notify(CTX_PAGE, ANALYSIS_OPEN, node.analysis);
            },
        }]);

        super.setUiEventHandlers('contextmenu', [{
            nodeTypes: ['analysis'],
            handler: (e, node) => {
                this.rightClickedNode(node);
                this.children.menu.open(e);
            },
        }]);
    }

    loadRootChildren = (node, setChildrenFn) => {


        this.analysisManager.getAnalyses()
            .then(analyses =>
                setChildrenFn(analyses.map(analysis => new AnalysisNode(this, analysis)))
            )
            .catch((error) => {console.log(error)
                setChildrenFn(false)}
            );
    };

    createAnalysis() {

        this.notify(CTX_PAGE, ANALYSIS_OPEN_CREATION_FORM, this.bundle);
    }

    insertNodeFor(analysis) {

        this._jstree.create_node('#', new AnalysisNode(this, analysis));
    }

    deleteNodeFor(analysisId) {

        const node = this._jstree.get_node(`${this._nodeIdPrefix}/${analysisId}`);

        node.original.disposeSubscription();
        this._jstree.delete_node(node);
    }

};
