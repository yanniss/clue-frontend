import Node from '../../trees/Tree/classes/Node';
import './AnalysisNode.scss';

/**
 *
 */
export default class AnalysisNode extends Node {

    static typesOptions = {
        analysis: {
            icon: 'analysis-node fa fa-minus',
            comparator: (a,b) => b.analysis.createdDate - a.analysis.createdDate,
        },
    };

    constructor(tree, analysis) {

        super(`${tree._nodeIdPrefix}/${analysis.id}`, 'analysis', analysis.name, false, false,
            { class: '' },
            { title: `Created:\n${analysis.createdDate}` },
        );

        this.analysis = analysis;
        this.tree = tree;

        let subscription;
        this.disposeSubscription = () => subscription && subscription.dispose();

        const setComputeds = () => {
            if(!tree._jstree || !tree._jstree.get_node(this.id)) {
                setTimeout(setComputeds, 250);
                return;
            }
            // This is the Base view model's "computed" method. Therefore, the following subscriptions will be disposed
            // automatically when the tree component is disposed.
            subscription = tree.computed(() => this.updateNodeText(analysis.state()));
        };
        setComputeds();
    }

    updateNodeIcon(activated) {

        this.li_attr.class = activated ? 'activated' : '';

        const jstree = this.tree._jstree,
            node = jstree.get_node(this.id);

        node.li_attr.class = this.li_attr.class;
        jstree.redraw_node(this.id);
    }

    updateNodeText(state) {

        let postfixText = state;
        if(this.analysis.isFinished(true)) {
            postfixText = this.analysis.created;
        }
        else {
            postfixText = state;
        }

        this.text = `${this.analysis.name} <span class="state ${state}">${postfixText}</span>`;

        const jstree = this.tree._jstree,
            node = jstree.get_node(this.id);

        node.text = this.text;
        jstree.redraw_node(this.id);
    }

};
