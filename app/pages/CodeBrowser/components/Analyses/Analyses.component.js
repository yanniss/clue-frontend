import './Analyses.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './Analyses.view.html';
import vm from './Analyses.vm';

koh.registerComponent('component-analyses', vm, view);
