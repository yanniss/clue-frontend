import './SymbolSearch.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './SymbolSearch.view.html';
import vm from './SymbolSearch.vm';

koh.registerComponent('component-symbol-search', vm, view);
