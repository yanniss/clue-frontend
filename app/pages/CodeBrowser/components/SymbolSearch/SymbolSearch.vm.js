import 'app/components/SearchBox/SearchBox.component';
import Api from 'app/classes/Api.static';
import Base from 'core/classes/vm/Base.vm';
import PromiseQueue from 'core/classes/PromiseQueue';
import ko from 'knockout';
import { CTX_PAGE } from 'core/classes/events/contexts';
import { FILE_RAW_OPEN } from 'app/classes/eventTypes';

const SYMBOL_TYPES = [ 'Class', 'Field', 'Method' ];

/**
 *
 */
export default class SymbolSearch extends Base {

    requests = new PromiseQueue();

    searchText = ko.observable('').extend({ rateLimit: { timeout: 250, method: 'notifyWhenChangesStop' }, notify: 'always'});

    // {{ Class: ko.observable, Field: ko.observable, Method: ko.observable }}
    searchFor = SYMBOL_TYPES.reduce((acc, type) => { acc[type] = ko.observable(true); return acc; }, {});

    activeSymbolTypes = this.computed(() => SYMBOL_TYPES.filter(type => this.searchFor[type]()));

    results = ko.observableArray();

    /**
     * @param element
     * @param bundle
     */
    constructor(element, { bundle }) {

        super(element);

        this.bundleId = bundle.id;

        this.resultsDropdownClassList = element.querySelector('.results').classList;

        this.subscribe(this.searchText, this.loadSearchResults);
        this.subscribe(this.activeSymbolTypes, this.loadSearchResults);

        this.startListeningToUiEvents();
    }

    stopBubbling(self, event) {

        event.stopPropagation();

        return true;
    }

    openSymbol = (filepath, line, column) => {

        /*
        this.notify(CTX_PAGE, FILE_RAW_OPEN, {
            filepath: filepath,
            line: line,
            column: column,
        });
        */
    };

    startListeningToUiEvents() {

        const button = this.element.getElementsByTagName('button')[0],
            span = button.children[0];

        this.addEventListener('click', e => {
            if([ button, span ].includes(e.target)) { return; }
            this.resultsDropdownClassList.add('hide');
        }, {}, document);
    }

    loadSearchResults = () => {

        const searchText = this.searchText(),
            activeSymbolTypes = this.activeSymbolTypes();

        if(searchText.trim().length === 0 || activeSymbolTypes.length === 0) {
            this.resultsDropdownClassList.add('hide');
            return;
        }

        Api.fetchSymbolsThatStartWith(this.bundleId, searchText, activeSymbolTypes);
        const promise = new Promise(resolve => {
            setTimeout(() => {
                const items = [];
                for(let i = 0; i < 30; i++) items.push('' + Math.random());
                resolve(items);
            }, 350);
        });

        this.requests.push(promise, results => {
            this.results(results);
            this.resultsDropdownClassList.remove('hide');
        });
    };

};
