/**
 * todo: scroll gets messed up when switching between iGroups. Keep scroll state for each iGroup.
 *
 * todo: sync actions; e.g. click on "<init>" of java.lang.Object. While fetching the
 *       data click on another iGroup. Wait for the initial load to finish aaand ...
 */

import 'core/bindings/rclick';
import 'knockout-delegated-events';
import Api from 'app/classes/Api.static';
import Base from 'core/classes/vm/Base.vm';
import Field from 'codebrowser/classes/programElement/Field';
import Mousetrap from 'mousetrap';
import SelectionData from 'codebrowser/classes/Selection';
import Query  from 'codebrowser/classes/programElement/Query';
import Value from 'codebrowser/classes/programElement/Value';
import ko from 'knockout';
import { CTX_PAGE } from 'core/classes/events/contexts';
import {
    FILE_RAW_OPEN,
    SELECTION_NEW,
    SELECTION_ACTIVATED, ANALYSIS_ACTIVATED,
} from 'app/classes/eventTypes';
import uri from "core/classes/UriFragment.static";
import { isPositiveNatural } from "core/classes/NumberHelpers";

/**
 *
 */
export default class Selection extends Base {

    /**
     * @param {HTMLElement} element
     * @param {Bundle} bundle
     */
    constructor(element, { bundle }) {

        super(element);

        this.bundle = bundle;

        this.startListeningToPageEvents();
        this.startListeningToUiEvents();
    }

    dispose() {

        this.stopListeningToUiEvents();

        super.dispose();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    activeSelection = ko.observable();

    isPropActive = prop => {

        const activeProp = this.activeSelection.peek().activeProp();

        return activeProp && prop.name === activeProp.prop.name;
    };

    isPropFunctional = prop => {

        return this.activeSelection.peek().programElement.isFunctional(prop);
    };

    activateProp = prop => {

        const selection = this.activeSelection();

        if(!selection.programElement.isFunctional(prop)) { return; }

        selection.activateProp(prop);
    };

    openProgramElement = programElement => {

        let bundleId = this.bundle.id,
            analysisId = this.activeAnalysis && this.activeAnalysis.id,
            filepath = programElement.filepath || programElement.sourceFileName,
            selection = this.activeSelection();

        /*
        if(this.progElemWrapper.programElement instanceof Value) {
            clickedThing =
        }
        */

        if(programElement instanceof Query) {
            // nop
        }
        else if(programElement === selection.programElement) {
            this.notify(CTX_PAGE, FILE_RAW_OPEN, {
                filepath: filepath,
                line: programElement.position.startLine,
                column: programElement.position.startColumn,
            });
        }
        else if(!programElement.sourceFileName || !programElement.position) {
            // nop
        }
        else if(!programElement.kind) {
            Api.fetchProgramElement(
                bundleId,
                analysisId,
                programElement.sourceFileName,
                programElement.position.startLine,
                programElement.position.startColumn
            ).then(data => {
                // todo: the following "data[0]" is a hack! MUST implement it in a better way!
                data[0] && this.notify(CTX_PAGE, SELECTION_NEW, new SelectionData(bundleId, analysisId, data[0]));
            });
        }
        else if(selection.programElement instanceof Value && programElement.kind === 'Field') {
            this.notify(CTX_PAGE, SELECTION_NEW, new SelectionData(
                bundleId,
                analysisId,
                new Field(bundleId, analysisId, programElement, selection.programElement.id),
            ));
        }
        else {
            this.notify(CTX_PAGE, SELECTION_NEW, new SelectionData(bundleId, analysisId, programElement));
        }

        // if view only in source, e.g. "Shift" key, then notify (codebrowser) to open file
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view model members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bundle;
    activeAnalysis;

    startListeningToPageEvents() {

        this.on(CTX_PAGE, SELECTION_ACTIVATED, selection => {
            this.activeSelection(selection);
            Selection.updateUri(selection);

            if(selection) {
                const { filepath, position } = selection.programElement;

                this.notify(CTX_PAGE, FILE_RAW_OPEN, {
                    filepath: filepath,
                    line: parseInt(position.startLine),
                    column: parseInt(position.startColumn),
                });
            }
        });

        this.on(CTX_PAGE, ANALYSIS_ACTIVATED, analysis => {
            this.activeAnalysis = analysis;
        });
    }

    startListeningToUiEvents() {

        Mousetrap.bind('g', () => {
            let selection = this.activeSelection(),
                value = selection && selection.values && selection.values[0];

            value && this.openProgramElement(value);
        });
    }

    stopListeningToUiEvents() {

        Mousetrap.unbind(['g']);
    }

    initAfterBinding() {

        const { file, line, column } = uri.getQueryParams();
        if(!file) { return; }

        this.notify(CTX_PAGE, FILE_RAW_OPEN, {
            filepath: file,
            line: parseInt(line),
            column: parseInt(column),
        });

        const bundleId = this.bundle.id,
            analysisId = this.activeAnalysis && this.activeAnalysis.id;

        Api.fetchProgramElement(bundleId, analysisId, file, line, column)
            .then(syntacticElements => {
                const syntacticElement = syntacticElements[0];
                if(!syntacticElement) { return; }
                this.notify(CTX_PAGE, SELECTION_NEW, new SelectionData(bundleId, analysisId, syntacticElement));
            });
    }

    static updateUri(selection) {

        if(!selection || selection instanceof Query) {
            uri.removeQueryParams('file', 'line', 'column');
        }
        else {
            const programElement = selection.programElement,
                params = { add: {}, remove: {} },
                filepath = programElement.filepath,
                { startLine, startColumn } = programElement.position;

            params[filepath ? 'add' : 'remove'].file = filepath;
            params[isPositiveNatural(startLine) ? 'add' : 'remove'].line = startLine;
            params[isPositiveNatural(startColumn) ? 'add' : 'remove'].column = startColumn;

            uri.updateQueryParams(params.add, params.remove);
        }
    }

};
