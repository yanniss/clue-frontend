import './Selection.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './Selection.view.html';
import vm from './Selection.vm';

koh.registerComponent('component-selection', vm, view);
