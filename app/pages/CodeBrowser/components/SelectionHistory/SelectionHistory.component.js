import './SelectionHistory.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './SelectionHistory.view.html';
import vm from './SelectionHistory.vm';

koh.registerComponent('component-selection-history', vm, view);
