/**
 * todo: add menu on right click on history entries.
 * todo: enhance history auto scrolling on entry activation.
 */

import 'core/bindings/resizable';
import 'codebrowser/components/Selection/Selection.component';
import $ from 'jquery';
import Api from 'app/classes/Api.static';
import Base from 'core/classes/vm/Base.vm';
import Mousetrap from 'mousetrap';
import Selection from 'codebrowser/classes/Selection';
import ko from 'knockout';
import { CTX_PAGE } from 'core/classes/events/contexts';
import {
    ANALYSIS_ACTIVATED,
    SELECTION_NEW,
    SELECTION_ACTIVATED,
} from 'app/classes/eventTypes';

/**
 *
 */
export default class SelectionHistory extends Base {

    /**
     * @param {HTMLElement} element
     */
    constructor(element) {

        super(element);

        this.setView();
        this.startListeningToStateChanges();
        this.startListeningToUiEvents();
        this.startListeningToPageEvents();
    }

    dispose() {

        this.stopListeningToUiEvents();

        super.dispose();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    history = ko.observableArray();
    hasNextEntry;
    hasPreviousEntry;

    activate = (historyEntry, e) => {

        const index = parseInt((e.currentTarget || e.delegateTarget).dataset.index);

        this.activeIndex(index);
    };

    activatePrevious = () => {

        const prevIndex = this.activeIndex() - 1;

        if(prevIndex >= 0) {
            this.activeIndex(prevIndex);
        }
    };

    activateNext = () => {

        const nextIndex = this.activeIndex() + 1;

        if(nextIndex < this.history().length) {
            this.activeIndex(nextIndex);
        }
    };

    clear = () => {

        this.popFromHistory(this.history().length);
    };

    pop = () => {

        this.popFromHistory(1);
    };

    popUntilFocused = () => {

        this.popFromHistory(this.history().length - this.activeIndex() - 1);
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view model members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bundle;
    activeAnalysis;
    domEntryContainer;
    activeIndex = ko.observable(-1).extend({ notify: 'always' });
    activeEntry = ko.observable();
    entriesInHistory = {};

    setView() {

        const $e = $('.entries');

        this.domEntryContainer = {
            outer: $e[0],
            inner: $e.find('.fcc')[0],
        };
    }

    startListeningToUiEvents() {

        Mousetrap.bind('w', this.activatePrevious);
        Mousetrap.bind(['a', 's', 'd'], this.activateNext);
        Mousetrap.bind('c', this.clear);
        Mousetrap.bind('p', this.pop);
    }

    stopListeningToUiEvents() {

        Mousetrap.unbind(['w', 'a', 's', 'd', 'c', 'p']);
    }

    startListeningToPageEvents() {

        this.on(CTX_PAGE, SELECTION_NEW, selection => {

            const analysisId = this.activeAnalysis && this.activeAnalysis.id;

            if(selection.analysisId === analysisId) {
                this.pushToHistory(selection);
            }
            else {
                const { bundleId, filepath, position } = selection.programElement;

                Api.fetchProgramElement(bundleId, analysisId, filepath, position.startLine, position.startColumn)
                    .then(syntacticElements => {
                        const kind = selection.programElement.kind,
                            syntacticElement = syntacticElements.find(syntacticElement =>
                                new Selection(bundleId, analysisId, syntacticElement).programElement.kind === kind
                            );
                        if(!syntacticElement) { return; }
                        this.pushToHistory(new Selection(bundleId, analysisId, syntacticElement));
                    });
            }
        });

        this.on(CTX_PAGE, ANALYSIS_ACTIVATED, analysis => {

            this.activeAnalysis = analysis;

            const entry = this.getLastHistoryEntry();
            if(!entry) { return; }

            this.clear();

            const { bundleId, filepath, position } = entry.programElement,
                analysisId = analysis && analysis.id;

            Api.fetchProgramElement(bundleId, analysisId, filepath, position.startLine, position.startColumn)
                .then(syntacticElements => {
                    const kind = entry.programElement.kind,
                        syntacticElement = syntacticElements.find(syntacticElement =>
                            new Selection(bundleId, analysisId, syntacticElement).programElement.kind === kind
                        );
                    if(!syntacticElement) { return; }
                    this.pushToHistory(new Selection(bundleId, analysisId, syntacticElement));
                });
        });
    }

    startListeningToStateChanges() {

        this.subscribe(this.history, history => {
            history.length === 0 && this.notify(CTX_PAGE, SELECTION_ACTIVATED, null);
        });

        let prevIndex = -1;

        this.subscribe(this.activeIndex, index => {
            const inner = this.domEntryContainer.inner,
                outer = this.domEntryContainer.outer,
                nodes = inner.children,
                length = this.history().length;

            if(prevIndex >= 0 && prevIndex < length) {
                nodes[prevIndex].classList.remove('active');
            }
            if(index >= 0 && index < length) {
                nodes[index].classList.add('active');
                outer.scrollTop = nodes[index].offsetTop - nodes[0].offsetTop - $(this.domEntryContainer.outer).height();
            }
            prevIndex = index;

            // New active entry may be "undefined", which is a valid value for "activeEntry".
            this.activeEntry(this.history()[index]);
        });

        let prevEntry = null;

        this.subscribe(this.activeEntry, entry => {
            let nodes = this.domEntryContainer.inner.children,
                previous = this.entriesInHistory[prevEntry],
                current = this.entriesInHistory[entry];

            previous && previous.indices.forEach(index => {
                nodes[index].classList.remove('same');
            });
            current && current.indices.forEach(index => {
                nodes[index].classList.add('same');
            });
            prevEntry = entry;

            this.notify(CTX_PAGE, SELECTION_ACTIVATED, entry);
        });

        this.hasNextEntry = this.computed(() =>
            this.activeIndex() < this.history().length - 1
        ).extend({ deferred: true });

        this.hasPreviousEntry = this.computed(() => this.activeIndex() > 0);
    }

    /**
     * Adds the given selection in history.
     *
     * @param selection
     */
    pushToHistory(selection) {

        this.popUntilFocused();

        let programElement = selection.programElement,
            programElementId = programElement.toString(),
            length = this.history().length;

        if(!this.entriesInHistory[programElementId]) {
            // The program element does not appear in history yet.
            this.entriesInHistory[programElementId] = {
                progElem: selection.programElement,
                indices: [length]
            };
            this.history.push(selection);
        }
        else if(this.isProgramElementOnTop(selection.programElement)) {
            // The program element appears in history's last entry.
            this.getLastHistoryEntry().copyActivePropFrom(selection);
            length -= 1;
        }
        else {
            // The program element appears in history.
            selection.programElement = this.entriesInHistory[programElementId].progElem;
            this.entriesInHistory[programElementId].indices.push(length);
            this.history.push(selection);
        }

        this.activeIndex(length);
    }

    /**
     * Removes the last (most recent) N entries from history.
     *
     * @param {number} count - Number of entries to pop
     */
    popFromHistory(count) {

        let length = this.history().length,
            removedEntries = this.history.splice(length - count);

        for(let i = 0, n = removedEntries.length; i < n; i++) {
            const entries = this.entriesInHistory,
                id = removedEntries[i].programElement.toString();

            entries[id].indices.pop();
            entries[id].indices.length === 0 && (delete entries[id]);
        }

        length = length - removedEntries.length;

        this.activeIndex() >= length && this.activeIndex(length - 1);
    }

    isProgramElementOnTop(programElement) {

        let history = this.history(),
            top = history[history.length - 1];

        return top && top.programElement.toString() === programElement.toString();
    }

    getLastHistoryEntry() {

        let history = this.history();

        return history[history.length - 1];
    }

    getFirstHistoryEntry() {

        return this.history()[0];
    }

};
