import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import Api from 'app/classes/Api.static';
import $ from 'jquery';
import {CTX_PAGE} from 'core/classes/events/contexts';
import {CONFIGURATION_ACTIVATED} from 'codebrowser/classes/configuration/Configuration.events';

/**
 *
 */
export default class ProguardWizardStep3 extends Base {

    title = 'Select configuration';
    nextButtonTitle = 'Import rules';
    pendingNextButtonTitle = 'Importing rules ...';
    ready = ko.observable(true);

    constructor(element, { bundle }) {

        super(element);

        this.bundleId = bundle.id;

        this.clear();

        this.on(CTX_PAGE, CONFIGURATION_ACTIVATED, configuration => {

            this.selectedConfiguration(configuration);
        });
    }

    clear() {

        this.origin('');
        this.text('');
        this.rules([]);
    }

    receive({ origin, text, rules }) {

        this.origin(origin);
        this.text(text);
        this.rules(rules);
    }

    deliver() {

        return this.selectedConfiguration().addRules(this.origin(), this.rules());
    }

    ////////////////////////////////////////////////////////////

    bundleId;
    configurations = this.requirePropertyInAncestors('configurationManager').getConfigurationsAsObservableArray();
    selectedConfiguration = ko.observable();

    origin = ko.observable();
    text = ko.observable();
    rules = ko.observableArray();

};
