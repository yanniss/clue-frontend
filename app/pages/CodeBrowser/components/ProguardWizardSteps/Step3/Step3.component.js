import './Step3.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './Step3.view.html';
import vm from './Step3.vm';

koh.registerComponent('component-proguard-wizard-step3', vm, view);
