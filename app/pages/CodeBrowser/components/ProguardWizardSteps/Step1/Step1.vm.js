import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import Api from 'app/classes/Api.static';
import $ from 'jquery';

/**
 *
 */
export default class ProguardWizardStep1 extends Base {

    title = 'Upload proguard file';
    nextButtonTitle = 'Upload';
    pendingNextButtonTitle = 'Uploading ...';
    ready = ko.observable();

    constructor(element) {

        super(element);

        this.addEventListener('change', () => {
            const match = this.input.value.match(/[^/\\]+$/);
            match && this.fileName(match[0]);
            this.ready(true);
        }, {}, this.input);

        this.clear();
    }

    clear() {

        this.ready(false);
        this.fileName('<span class="text-muted">No file chosen</span>');
        this.$form[0].reset();
    }

    receive() {

        this.clear();
    }

    deliver() {

        return Api.parseProguardFile(this.$form);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $form = $(this.element.getElementsByTagName('form')[0]);
    input = this.element.getElementsByTagName('input')[0];
    fileName = ko.observable();

    selectFile() {

        this.input.click();
    }

};
