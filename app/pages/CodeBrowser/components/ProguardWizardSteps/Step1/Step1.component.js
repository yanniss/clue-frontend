import './Step1.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './Step1.view.html';
import vm from './Step1.vm';

koh.registerComponent('component-proguard-wizard-step1', vm, view);
