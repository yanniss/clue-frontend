import './Step2.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './Step2.view.html';
import vm from './Step2.vm';

koh.registerComponent('component-proguard-wizard-step2', vm, view);
