import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';

/**
 *
 */
export default class ProguardWizardStep2 extends Base {

    title = 'Approve parsed rules';
    nextButtonTitle = 'Continue';
    pendingNextButtonTitle = 'Continue';
    ready = ko.observable(true);

    constructor(element) {

        super(element);

        this.clear();
    }

    clear() {

        this.origin('');
        this.text('');
        this.rules([]);
    }

    receive({ origin, text, rules }) {

        this.origin(origin);
        this.text(text);
        this.rules(rules.map(rule => ({
            rule: rule,
            selected: ko.observable(true),
        })));
    }

    deliver() {

        return Promise.resolve({
            origin: this.origin(),
            text: this.text(),
            rules: this.rules().filter(rule => rule.selected()).map(rule => rule.rule),
        });
    }

    ////////////////////////////////////////////////////////////

    origin = ko.observable();
    text = ko.observable();
    rules = ko.observableArray();

};
