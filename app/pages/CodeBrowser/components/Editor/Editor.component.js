import './Editor.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './Editor.vm';
import view from './Editor.view.html';

koh.registerComponent('component-editor', vm, view);
