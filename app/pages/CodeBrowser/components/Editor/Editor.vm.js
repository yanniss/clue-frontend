/**
 * Editor encapsulates "ace" usage and other editor-relevant operations.
 *
 * ToDo on "scrollbar markers":
 * > Implement scrollbar marker for cursor position.
 * > Check scrollbar markers height and "top" placement relative to scrollbar
 *   Always keep the relation of scrollbar and scrollbar markers consistent.
 * > Manage various scrollbar widths on different browsers.
 * > Take into account possible scroll arrows, e.g. in Windows/Firefox.
 * > Use custom scrollbars, instead of native or not-cross-browser-CSS.
 *
 * todo: check: setFileSession is executed twice for same file (at least on file open from url on load).
 * todo: on "window resize" the markers disappear.
 */

import Ace from 'brace';
import 'core/bindings/resizable';
import 'brace/mode/java';
import 'brace/mode/xml';
import 'brace/ext/searchbox';
import '../trees/FileStructureTree/FileStructureTree.component';
import $ from 'jquery';
import Api from 'app/classes/Api.static';
import Base from 'core/classes/vm/Base.vm';
import File from 'codebrowser/classes/File';
import Bundle from 'app/classes/Bundle';
import Selection from 'codebrowser/classes/Selection';
import PromiseQueue from 'core/classes/PromiseQueue';
import ko from 'knockout';
import { isNatural, isPositiveNatural } from 'core/classes/NumberHelpers';
import str from 'core/classes/StringHelper.static';
import {
    CTX_PAGE,
    CTX_ASCENDANTS
} from 'core/classes/events/contexts';
import {
    FILE_SAVE_STATE,
    SELECTION_ACTIVATED,
    ANALYSIS_ACTIVATED,
    SELECTION_NEW,
    RESIZE_X,
    DIRECTIVE_KEEP_METHOD,
    DIRECTIVE_REMOVE_METHOD,
    DIRECTIVE_DELETE, DIRECTIVE_MENU_OPEN
} from 'app/classes/eventTypes';
import { CONFIGURATION_ACTIVATED } from 'codebrowser/classes/configuration/Configuration.events';

const Range = Ace.acequire('ace/range').Range;

/**
 *
 */
export default class Editor extends Base {

    /**
     *
     * @param {HTMLElement} element
     * @param {Bundle} bundle
     */
    constructor(element, { bundle }) {

        super(element);

        this.bundle = bundle;

        for(const name of Object.keys(this.lsKeys)) {
            this.lsKeys[name] += bundle.id;
        }
        this.isFileStructureOpen(localStorage.getItem(this.lsKeys.fstreeVisibility) === 'true');

        this.setView();
        this.startListeningToUiEvents();
        this.startListeningToPageEvents();
        this.startListeningToStateEvents();
    }

    dispose() {

        this.stopListeningToUiEvents();

        super.dispose();
    }

    /**
     *
     * @param {File} file
     * @param {number} line - line number to set the cursor to. Possible values: [1,oo+).
     * @param {number} column - column number to set the cursor to. Possible values: [1,oo+).
     * @param {number} [scrollTop] - pixels to scroll from the top.
     * @param {number} [scrollLeft] - pixels to scroll from the left.
     */
    openFile(file, line, column, scrollTop, scrollLeft) {

        if(!(file instanceof File)) {
            throw new Error(`Editor.openFile: file must be of type File: ${file}`);
        }

        if(file === this.activeFile) {
            this.setCursor(line, column, !!(scrollTop || scrollLeft));
            this.setScroll(scrollTop, scrollLeft);

            return;
        }

        if(this.activeFile) {
            this.saveCurrentState();
        }

        this.state('loading');

        this.requests.push(file.content.get(), content => {
            const mode = file.getLanguage();
            this.editor.session.setMode(mode ? `ace/mode/${mode}` : null);

            this.activeFile = file;
            this.filename(file.path);

            this.clearMarkers();
            this.showContent(content);
            this.showSelectionMarkers();
            this.setCursor(line, column, !!(scrollTop || scrollLeft));
            this.setScroll(scrollTop, scrollLeft);

            this.state('ready');
        }, () => {
            this.state('error');
        });

        if(file.hasMetadata()) {
            this.children.structure.openFile(file);

            const promise = Promise.all([ file.bundleDirectives.get(), file.analysisDirectives.get() ]);

            this.requests.push(promise, ([ bundleDirectives, analysisDirectives ]) => {

                this.activeFileBundleDirectives = bundleDirectives;
                this.activeFileAnalysisDirectives = analysisDirectives;
                this.showDirectiveMarkers();

            }, null, false, false);

            this.requests.push(Promise.resolve(), () => {

                //this.activeSelection && this.showSelection(this.activeSelection, true);

            }, null, false, false);

            this.fileHasMetadata(true);
        }
        else {
            this.fileHasMetadata(false);
        }
    }

    showSelection(selection) {

        this.activeSelection = selection;
        this.activePropSubscription && this.activePropSubscription.dispose();
        this.activePropSubscription = null;

        this.clearSelectionMarkers();
        this.showSelectionMarkers();

        if(!selection) { return; }

        this.activePropSubscription = this.subscribe(selection.activeProp, () => {
            this.clearSelectionMarkers();
            this.showSelectionMarkers();
        });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    lsKeys = { fstreeVisibility: `CodeBrowser.fstree:`, fstreeSize: `CodeBrowser.sizes:` };
    state = ko.observable();
    filename = ko.observable('');
    cursorPosition = ko.observable();
    fstreeSearchText = ko.observable('').extend({ rateLimit: { timeout: 250, method: 'notifyWhenChangesStop' }});
    fileHasMetadata = ko.observable(false);
    isFileStructureOpen = ko.observable(false);

    selectProp = (prop, e) => {

        let $context = ko.contextFor(e.target),
            selection = $context.$parents[1];

        if(!selection.programElement.isFunctional(prop)) { return; }

        selection.activateProp(prop);
        this.notify(CTX_PAGE, SELECTION_NEW, selection);
    };

    toggleFileStructureVisibility = () => {

        const newValue = !this.isFileStructureOpen();

        this.isFileStructureOpen(newValue);
        localStorage.setItem(this.lsKeys.fstreeVisibility, ''+newValue);
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view model members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bundle;
    activeFile;
    activeSelection;
    activePropSubscription;
    activeAnalysis;
    requests = new PromiseQueue();
    selections = ko.observableArray();
    editor;
    markers = {
        cursor: null,
        selection: [],
        directives: {},
    };

    /**
     * Initializes and configures the Ace editor, creates the scrollbar markers area, and sets view-related properties
     * for the view model.
     */
    setView() {

        const element = this.element.getElementsByClassName('editor')[0],
            $menu = $(this.element).find('.menu'),
            editor = Ace.edit(element);

        editor.setOptions({
            highlightActiveLine: false,
            fontSize: '11pt',
            fontFamily: 'Cousine, Consolas',
            fixedWidthGutter: true,
            mode: 'ace/mode/java',
        });
        editor.setReadOnly(true);
        editor.container.style.lineHeight = '1.3';
        editor.renderer.setScrollMargin(5, 200, 0, 0);
        editor.setShowPrintMargin(false);
        // editor.setTheme('ace/theme/solarized_dark');

        // Remove ACE warning message in console.
        editor.$blockScrolling = Infinity;

        // Disable keyboard popup when clicked on mobile devices.
        element.getElementsByTagName('textarea')[0].setAttribute('readonly', true);

        // Add "mousetrap" class to enable handling of otherwise "global" keystrokes when the editor is focused.
        $('.ace_text-input').addClass('mousetrap');

        this.editor = editor;
        this.$menu = $menu;

        this.markers.cursor = new Marker(this, {
            startLine: 1,
            startColumn: 1,
            endLine: 1,
            endColumn :1.
        }, 'cursor-line-marker', { type: 'fullLine' });
    }

    startListeningToPageEvents() {

        this.on(CTX_PAGE, SELECTION_ACTIVATED, selection => {
            this.showSelection(selection);
        }, { ignoreIfEquals: false });

        this.on(CTX_PAGE, ANALYSIS_ACTIVATED, analysis => {
            this.activeAnalysis = analysis;

            this.activeFile && this.activeFile.analysisDirectives.get().then(analysisDirectives => {
                this.activeFileAnalysisDirectives = analysisDirectives;
                this.clearDirectiveMarkers();
                this.showDirectiveMarkers();
            });
        });

        this.on(CTX_PAGE, CONFIGURATION_ACTIVATED, () => {
            this.activeFile && this.activeFile.bundleDirectives.get().then(bundleDirectives => {
                this.activeFileBundleDirectives = bundleDirectives;
                this.clearDirectiveMarkers();
                this.showDirectiveMarkers();
            });
        });

        this.on(CTX_PAGE, RESIZE_X, () => {
            this.resize();
        }, { ignoreIfEquals: false });

        this.on(CTX_PAGE, DIRECTIVE_KEEP_METHOD, method => {
            this.activeFileBundleDirectives[method.doopId] = method.bundleDirective;
            this.addDirectiveMarker(method.doopId, method.position, 'USER', 'KEEP');
        }, { ignoreIfEquals: false });

        this.on(CTX_PAGE, DIRECTIVE_REMOVE_METHOD, method => {
            this.activeFileBundleDirectives[method.doopId] = method.bundleDirective;
            this.addDirectiveMarker(method.doopId, method.position, 'USER', 'REMOVE');
        }, { ignoreIfEquals: false });

        this.on(CTX_PAGE, DIRECTIVE_DELETE, method => {
            delete this.activeFileBundleDirectives[method.doopId];
            this.removeDirectiveMarker(method.doopId, method.position);
        }, { ignoreIfEquals: false });
    }

    startListeningToUiEvents() {

        this.addEventListener('keyup', e => {
            if(e.which === 13 && !e.target.className.includes('ace_search_field')) { // "Enter/Return" key
                this.triggerCursor();
            }
            else if(e.which === 27) { // "Esc" key
                this.$menu.addClass('hide');
            }
        }, {}, this.element.getElementsByClassName('editor')[0]);

        this.editor.selection.on('changeCursor', () => {
            const position = this.getCursor();

            this.cursorPosition(position);
            this.markers.cursor.move({ startLine: position.line });
        });

        this.addEventListener('dblclick', e => {
            this.triggerCursor();
        }, null, this.element.querySelector('.ace_scroller'));

        this.addEventListener('contextmenu', e => {
            this.$menu.addClass('hide');
        }, { capture: true }, document);

        this.addEventListener('contextmenu', e => {
            e.preventDefault();
            if(!this.activeFile.hasMetadata()) { return; }

            const bundleId = this.bundle.id,
                analysisId = this.activeAnalysis && this.activeAnalysis.id,
                filepath = this.activeFile.path,
                { line, column } = this.getCursor();

            Api.fetchProgramElement(bundleId, analysisId, filepath, line, column, true)
                .then(syntacticElements => {
                    if(syntacticElements.length === 0) { return; }

                    if(e.ctrlKey) {
                        this.selections(syntacticElements.map(syntacticElement =>
                            new Selection(bundleId, analysisId, syntacticElement)
                        ));
                        const coords = Editor.getContextMenuCoords(this.$menu, e);
                        this.$menu.css({ left: coords.x, top:  coords.y });
                        this.$menu.removeClass('hide');
                    }
                    else {
                        const method = syntacticElements.find(syntacticElement => syntacticElement.kind === 'Method');
                        if(method) {
                            this.notify(CTX_PAGE, DIRECTIVE_MENU_OPEN, {
                                method: {
                                    ...method,
                                    analysisDirective: (this.activeFileAnalysisDirectives || {})[method.doopId],
                                    bundleDirective: (this.activeFileBundleDirectives || {})[method.doopId],
                                },
                                event: e,
                            });
                        }
                    }
                });
        }, null, this.element.querySelector('.ace_text-input'));

        this.addEventListener('click', e => {
            e.button === 0 && this.$menu.addClass('hide');
        }, { capture: true }, document);

        this.editor.on('dblclick', e => {
            // Stops Ace from auto moving cursor to nearest identifier.
            e.stop();
        });
    }

    stopListeningToUiEvents() {

        this.editor.off();
        this.editor.selection.off();
    }

    startListeningToStateEvents() {

        this.subscribe(this.isFileStructureOpen, () => {
            this.resize();
        });
    }

    initAfterBinding() {

        setTimeout(() => {
            this.resize();
        }, 500);
    }

    saveCurrentState() {

        const { line, column } = this.getCursor(),
            { top, left } = this.getScroll();

        this.notify(CTX_ASCENDANTS, FILE_SAVE_STATE, {
            file: this.activeFile,
            line: line,
            column: column,
            scrollTop: top,
            scrollLeft: left,
        });
    }

    /**
     * @override
     */
    onHidden() {

        this.saveCurrentState();
    }

    /**
     * @param {number} line - line number to set the cursor to. Possible values: [1,oo+).
     * @param {number} column - column number to set the cursor to. Possible values: [1,oo+).
     * @param {boolean} scroll - also scroll to the new cursor position.
     */
    setCursor(line, column, scroll = true) {

        if(isPositiveNatural(line)) {
            if(isPositiveNatural(column)) {
                this.editor.gotoLine(line, column - 1, scroll);
            }
            else {
                this.editor.gotoLine(line, 0, scroll);
            }
        }
    }

    /**
     * @returns {{line: number, column: number}}
     */
    getCursor() {

        const cursor = this.editor.getCursorPosition();

        return {
            line: cursor.row + 1,
            column: cursor.column + 1,
        };
    }

    /**
     * @param {number} top - pixels from the top.
     * @param {number} left - pixels from the left.
     */
    setScroll(top, left) {

        if(isNatural(top)) {
            this.editor.renderer.scrollToY(top);
        }
        if(isNatural(left)) {
            this.editor.renderer.scrollToX(left);
        }
    }

    /**
     * @returns {{top: number, left: number}}
     */
    getScroll() {

        const renderer = this.editor.renderer;

        return {
            top: renderer.getScrollTop(),
            left: renderer.getScrollLeft(),
        };
    }

    /**
     * @param {string} content
     */
    showContent(content) {

        this.editor.session.setValue(content);
    }

    triggerCursor() {

        const file = this.activeFile;

        if(!file.hasMetadata()) { return; }

        const { line, column } = this.getCursor(),
            bundleId = this.bundle.id,
            analysisId = this.activeAnalysis && this.activeAnalysis.id,
            promise = Api.fetchProgramElement(bundleId, analysisId, file.path, line, column);

        this.requests.push(promise, syntacticElements => {
            const syntacticElement = syntacticElements[0];
            if(!syntacticElement) { return; }

            this.notify(CTX_PAGE, SELECTION_NEW, new Selection(bundleId, analysisId, syntacticElement));
        }, null, false, false);
    }

    showMarkers() {

        this.showSelectionMarkers();
        this.showDirectiveMarkers();
    }

    clearMarkers() {

        this.clearSelectionMarkers();
        this.clearDirectiveMarkers();
    }

    showSelectionMarkers() {

        const file = this.activeFile;
        if(!file) { return; }

        const selection = this.activeSelection;
        if(!selection) { return; }

        let markers = this.markers.selection;

        const programElement = selection.programElement;
        if(programElement.filepath === file.path) {
            markers.push(new Marker(this, programElement.position, 'selected-element-marker', { type: 'text' }));
        }

        const prop = selection.activeProp();
        if(!prop) { return; }

        const values = prop.values.groupedByFile[file.path] || [];
        for(let i = 0, n = values.length; i < n; i++) {
            const value = values[i];
            if(!value.sourceFileName || !value.position) { continue; }
            markers.push(new Marker(this, value.position, 'possible-value-marker', { type: 'text' }));
        }
    }

    clearSelectionMarkers() {

        this.markers.selection.forEach(marker => marker.dispose());
        this.markers.selection = [];
    }

    showDirectiveMarkers() {

        Object.values(this.activeFileAnalysisDirectives || {}).forEach(directive => {
            this.addDirectiveMarker(directive.doopId, directive.position, directive.origin, directive.type);
        });

        Object.values(this.activeFileBundleDirectives || {}).forEach(directive => {
            this.addDirectiveMarker(directive.doopId, directive.position, directive.origin, directive.type);
        });
    }

    clearDirectiveMarkers() {

        Object.values(this.markers.directives).forEach(marker => marker.dispose());
        this.markers.directives = {};
    }

    addDirectiveMarker(methodDoopId, methodPosition, directiveOrigin, directiveType) {

        const markers = this.markers.directives;
        let markerCss = '';

        if(markers.hasOwnProperty(methodDoopId)) {
            const marker = markers[methodDoopId];
            markerCss = marker.css.replace(new RegExp(`directive-${directiveOrigin}-(KEEP|REMOVE)`), '');
            marker.dispose();
        }

        markerCss += ` directive-${directiveOrigin}-${directiveType}`;
        markers[methodDoopId] = new Marker(this, methodPosition, markerCss, { type: 'text' });
    }

    removeDirectiveMarker(methodDoopId, methodPosition) {

        const markers = this.markers.directives;

        if(markers[methodDoopId]) {
            const marker = markers[methodDoopId],
                markerCss = marker.css.replace(new RegExp(`directive-USER-(KEEP|REMOVE)`), '');

            marker.dispose();

            if(str.isEmpty(markerCss, true)) {
                delete markers[methodDoopId];
            }
            else {
                markers[methodDoopId] = new Marker(this, methodPosition, markerCss, { type: 'text' });
            }
        }
    }

    getMethodDirectives(methodDoopId) {

        const markers = this.markers.directives,
            marker = markers[methodDoopId];

        if(marker) {
            const css = marker.css;

            return {
                bundle: css.contains('directive-USER-KEEP') && 'KEEP' || css.contains('directive-USER-REMOVE') && 'REMOVE' || null,
                analysis: css.contains('directive-ANALYSIS-KEEP') && 'KEEP' || css.contains('directive-ANALYSIS-REMOVE') && 'REMOVE' || null,
            };
        }
        else {
            return { bundle: null, analysis: null };
        }
    }

    resize = () => {

        this.editor.resize(true);
    };

    /**
     * Returns an object: { x: <int>, y: <int> }
     *
     * @param $menu
     * @param event
     * @returns {{x: *, y: *}}
     */
    static getContextMenuCoords($menu, event) {

        const x = event.clientX,
            y = event.clientY + 10,
            $win = $(window),
            w = $win.width(),
            h = $win.height(),
            sl = $win.scrollLeft(),
            st = $win.scrollTop(),
            mw = $menu.outerWidth(true),
            mh = $menu.outerHeight(true);

        return {
            x: Math.min(x + sl, w - mw - 15),
            y: Math.min(y + st, h - mh - 15),
        };
    }

};

/**
 *
 */
class Marker {

    constructor(vm, range, css = '', aceOptions = {}) {

        css = css + ' ' + (aceOptions.css || '');

        this.session = vm.editor.session;
        this.css = css;
        this.type = aceOptions.type;

        this.createAceMarker(range);
    }

    dispose() {

        this.removeAceMarker();
    }

    move(range) {

        this.removeAceMarker();
        this.createAceMarker(range);
    }

    createAceMarker(range) {

        const startLine = range.startLine - 1 || 0,
            startColumn = range.startColumn - 1 || 0,
            endLine = range.endLine - 1 || startLine,
            endColumn = range.endColumn - 1 || Infinity;

        this.aceMarkerId = this.session.addMarker(
            new Range(startLine, startColumn, endLine, endColumn),
            this.css,
            this.type,
        );
    }

    removeAceMarker() {

        this.aceMarkerId && this.session.removeMarker(this.aceMarkerId);
        this.aceMarkerId = null;
    }

}
