import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import $ from 'jquery';
import Api from 'app/classes/Api.static';
import Notifier from 'app/classes/Notifier.static';
import { CTX_PAGE } from 'core/classes/events/contexts';
import { CONFIGURATION_OPEN_ANALYSIS_IMPORT_PANEL } from 'codebrowser/classes/configuration/Configuration.events';

/**
 *
 */
export default class ConfigurationImportAnalysis extends Base {

    constructor(element) {

        super(element);

        this.startListeningToPageEvents();

        const analyses = this.requirePropertyInAncestors('analysisManager').getAnalysesAsObservableArray();
        this.finishedAnalyses = this.computed(() => {
            return analyses().filter(analysis => analysis.isFinished());
        }, { deferred: true });
    }

    dispose() {

        this.$modal.removeClass('fade');
        this.$modal.modal('hide');
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    configuration = ko.observable({});
    $modal = $(this.element.getElementsByClassName('modal')[0]);
    $submitButton = $(this.element.querySelector('button[type="submit"]'));
    selectedAnalysis = ko.observable();

    startListeningToPageEvents() {

        this.on(CTX_PAGE, CONFIGURATION_OPEN_ANALYSIS_IMPORT_PANEL, configuration => {
            this.configuration(configuration);
            this.selectedAnalysis(null);
            this.$modal.modal('show');
        }, { ignoreIfEquals: false });
    }

    submit() {

        const configuration = this.configuration(),
            bundleId = configuration.bundleId,
            analysis = this.selectedAnalysis();

        this.$submitButton.button('loading');

        Api.importAnalysis(bundleId, configuration, analysis.id)
            .then(() => {
                Notifier.success(`Analysis imported successfully: ${analysis.name}`);
                //this.notify(CTX_PAGE, CONFIGURATION_DELETED, configuration.id);
            })
            .catch(() =>
                Notifier.error(`Failed to import analysis "${analysis.id}" to configuration "${configuration.name()}"`)
            )
            .then(() => {
                this.$submitButton.button('reset');
                this.$modal.modal('hide');
                this.configuration({});
            });
    }

};
