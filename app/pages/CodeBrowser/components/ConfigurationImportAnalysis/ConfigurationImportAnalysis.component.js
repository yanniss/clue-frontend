import './ConfigurationImportAnalysis.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './ConfigurationImportAnalysis.vm';
import view from './ConfigurationImportAnalysis.view.html';

koh.registerComponent('component-configuration-import-analysis', vm, view);
