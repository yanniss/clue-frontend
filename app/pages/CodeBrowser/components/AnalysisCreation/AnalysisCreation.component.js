import './AnalysisCreation.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './AnalysisCreation.vm';
import view from './AnalysisCreation.view.html';

koh.registerComponent('component-analysis-creation', vm, view);
