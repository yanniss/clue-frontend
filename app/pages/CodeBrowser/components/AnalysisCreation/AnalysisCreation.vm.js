import 'app/components/FormOption/FormOption.component';
import 'core/bindings/stemplate';
import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import $ from 'jquery';
import Api from 'app/classes/Api.static';
import { CTX_PAGE } from 'core/classes/events/contexts';
import { CONFIGURATION_OPEN_ANALYSIS_FORM } from 'codebrowser/classes/configuration/Configuration.events';

/**
 * todo: add better notification messages based on which step the analysis creation failed.
 */
export default class AnalysisCreation extends Base {

    constructor(element) {

        super(element);

        this.startListeningToPageEvents();
        this.loadOptions();
    }

    dispose() {

        this.$modal.removeClass('fade');
        this.$modal.modal('hide');
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    configurationManager = this.requirePropertyInAncestors('configurationManager');
    //analysisManager = this.requirePropertyInAncestors('analysisManager');
    configurations = this.configurationManager.getConfigurationsAsObservableArray();
    activeConfigurationName = ko.observable();

    //bundle = ko.observable({});
    configuration = ko.observable({});
    options = ko.observable();
    $modal = $(this.element.getElementsByClassName('modal')[0]);
    $form = $(this.element.getElementsByTagName('form')[0]);
    $submitButton = $(this.element.querySelector('button[type="submit"]'));

    startListeningToPageEvents() {

        this.on(CTX_PAGE, CONFIGURATION_OPEN_ANALYSIS_FORM, configuration => {
            this.configuration(configuration);
            this.$modal.modal('show');
        }, { ignoreIfEquals: false });
    }

    loadOptions() {

        Api.fetchAnalysisOptions()
            .then(options => this.options(options.reduce((groups, option) => {
                const group = groups.find(optionGroup => optionGroup.description === option.group);

                if(group) {
                    group.options.push(option);
                }
                else {
                    groups.push({
                        description: option.group,
                        options: [ option ],
                    });
                }

                return groups;
            }, [/*{
                description: null,
                options: [{
                    id: 'set',
                    description: "Use this configuration's keep rules as entry points.",
                    name: 'Provide entry points',
                    isMandatory: false,
                    isFile: false,
                    isBoolean: false,
                    fileTypes: [],
                    validValues: this.configurations,
                    defaultValue: this.activeConfigurationName,
                    valuePropertyName: 'name',
                    caption: 'No entry points',
                }],
            }*/])));
    }

    getDescription(option) {

        return `${option.description || 'General'} options`;
    }

    submit() {

        this.$submitButton.button('loading');

        this.configuration().runAnalysis(this.$form)
            .catch(() => {})
            .then(() => {
                this.$submitButton.button('reset');
                this.$modal.modal('hide');
            });
    }

};
