import $ from 'jquery';
import Base from 'core/classes/vm/Base.vm';

/**
 *
 */
export default class BundleInfo extends Base {

    bundle;
    $modal = $(this.element).find('.modal');

    constructor(element, { bundle, open }) {

        super(element);

        this.bundle = bundle;

        this.subscribe(open, () => { this.open(); });
    }

    dispose() {

        this.close();
        super.dispose();
    }

    open() {

        this.$modal.modal('show');
    }

    close() {

        this.$modal.modal('hide');
    }

};
