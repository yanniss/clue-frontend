import './BundleInfo.style.scss'
import koh from 'core/classes/KnockoutHelper.static';
import view from './BundleInfo.view.html';
import vm from './BundleInfo.vm';

koh.registerComponent('component-bundle-info', vm, view);
