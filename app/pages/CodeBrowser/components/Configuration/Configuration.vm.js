import Base from 'core/classes/vm/Base.vm';
import str from 'core/classes/StringHelper.static';
import ko from 'knockout';
import $ from 'jquery';
import './components/Rules/Rules.component';
import 'codebrowser/components/Directives/Directives.component';
import { CTX_DESCENDANTS } from 'core/classes/events/contexts';
import { RULE_SHOW_DIRECTIVES } from './Configuration.events';
import '../Analysis/components/ResourceMonitor/ResourceMonitor.component';

/**
 *
 */
export default class Configuration extends Base {

    constructor(element, { bundle, configuration }) {

        super(element);

        this.bundle = bundle;
        this.configuration = configuration;

        this.startListeningToChildrenEvents();
        this.setMessages();
        this.setAnalyzeButtonHighlighting();
    }

    dispose() {

        this.$tabs.off();

        super.dispose();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bundle;
    configuration;
    uniqueIDs = {
        rulesTab: str.createUnique(),
        deadCodeTab: str.createUnique(),
        effectivesTab: str.createUnique(),
    };
    $tabs = $('.userDefinedRulesTab, .deadCodeTab, .directivesTab');
    highlightAnalyzeButton;
    messages;

    initAfterBinding() {

        $(this.element.querySelector(`[data-target="#${this.uniqueIDs.rulesTab}"]`)).tab('show');
    }

    startListeningToChildrenEvents() {

        this.on(CTX_DESCENDANTS, RULE_SHOW_DIRECTIVES, rule => {

            this.children.directives.resetWithRule(rule);
            const tab = this.element.querySelector(`[data-target="#${this.uniqueIDs.effectivesTab}"]`);
            $(tab).tab('show');
        });
    }

    setMessages() {

        this.messages = this.computed(() => {

            const configuration = this.configuration,
                analysis = configuration.analysis();
            let messageVisible = true,
                highlightAction = false,
                deadCodeMessage,
                effectiveDirectivesMessage;

            if(!analysis) {
                highlightAction = true;
                deadCodeMessage = "Please analyze to view Clue's dead code removal suggestions.";
                effectiveDirectivesMessage = "Please analyze to view Clue's effective keep & remove directives.";
            }
            else if(analysis.isRunning()) {
                deadCodeMessage = effectiveDirectivesMessage = 'Please wait for the analysis to complete...';
            }
            else if(configuration.dirty()) {
                highlightAction = true;
                deadCodeMessage = 'Please re-analyze to view updated dead code removal suggestions.';
                effectiveDirectivesMessage = 'Please re-analyze to view updated effective keep & remove directives.';
            }
            else {
                messageVisible = false;
            }

            return {
                highlightAction: highlightAction,
                messageVisible: messageVisible,
                deadCodeMessage: deadCodeMessage,
                effectiveDirectivesMessage: effectiveDirectivesMessage,
            };
        });
    }

    setAnalyzeButtonHighlighting() {

        const isRelevantTabActive = ko.observable(false);

        this.$tabs.on('show.bs.tab', e => {

            const b = [`#${this.uniqueIDs.deadCodeTab}`, `#${this.uniqueIDs.effectivesTab}`]
                .includes(e.currentTarget.dataset.target);

            isRelevantTabActive(b);
        });

        this.highlightAnalyzeButton = this.computed(() => {

            const highlight = this.messages().highlightAction && isRelevantTabActive();

            return highlight ? ' highlight ' : '';
        });
    }

};
