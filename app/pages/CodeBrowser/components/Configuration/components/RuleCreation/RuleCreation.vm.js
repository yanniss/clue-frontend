import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import Ace from 'brace';
import 'brace/mode/java';
import { copyToClipboard } from 'core/classes/clipboard';

/**
 *
 */
export default class RuleVM extends Base {

    hasPendingOperation;

    constructor(element, { bundle, configuration, closable }) {

        super(element);

        this.bundle = bundle;
        this.configuration = configuration;
        this.closable = closable;

        this.createEditorInstance();

        this.ruleBodyEditor.on('change', (action, editor) =>
            this.hasRuleBody(editor.getValue() !== '')
        );

        this.hasPendingOperation = ko.computed(() =>
            this.hasPendingCreateRequest()
        );
    }

    dispose() {

        this.ruleBodyEditor.off();
        super.dispose();
    }

    show() {

        this.ruleBodyEditor.setValue('', -1);
        super.show();
        this.ruleBodyEditor.focus();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bundle;
    configuration;
    closable;

    ruleBodyEditor;
    editableComment = ko.observable('');
    hasRuleBody = ko.observable(false);
    hasPendingCreateRequest = ko.observable(false);

    createEditorInstance() {

        const element = this.element.getElementsByClassName('ace')[0],
            editor = Ace.edit(element);

        editor.setOptions({
            highlightActiveLine: true,
            fontSize: '11pt',
            fontFamily: 'Cousine, Consolas',
            fixedWidthGutter: true,
            mode: 'ace/mode/java',
        });
        editor.container.style.lineHeight = '1.5';
        editor.renderer.setScrollMargin(2, 10, 0, 10);
        editor.setShowPrintMargin(false);

        // Remove ACE warning message in console.
        editor.$blockScrolling = Infinity;

        this.ruleBodyEditor = editor;
    }

    copyCommentToClipboard() {

        copyToClipboard(this.editableComment());
    }

    copyRuleBodyToClipboard() {

        copyToClipboard(this.ruleBodyEditor.getValue());
    }

    create() {

        const ruleBody = this.ruleBodyEditor.getValue(),
            comment = this.editableComment();

        this.hasPendingCreateRequest(true);

        this.configuration.addRule(ruleBody, comment)
            .then(() => {
                this.ruleBodyEditor.setValue('', -1);
                // Reasoning behind the following "setTimeout":
                //  There is at least one case where the code still appears even though the editor value is set to empty
                //  string. Reproduction steps:
                //  1) Load page with no rules; therefore rule creation panel appears.
                //  2) Create a rule; rule creation panel closes automatically.
                //  3) Open rule and delete it; after successful deletion the rule creation panel re-appears.
                //  4) The rule body is still there, until you click the editor.
                setTimeout(() => this.hide());
            })
            .finally(() =>
                this.hasPendingCreateRequest(false)
            );
    }

};
