import './RuleCreation.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './RuleCreation.vm';
import view from './RuleCreation.view.html';

koh.registerComponent('component-rule-creation', vm, view);
