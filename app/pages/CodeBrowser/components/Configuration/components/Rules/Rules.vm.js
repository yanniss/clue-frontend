import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import Api from 'app/classes/Api.static';
import PromiseQueue from 'core/classes/PromiseQueue';
import {RULE_CREATED, RULE_DELETED, RULE_SHOW_DIRECTIVES, RULE_CLOSED, RULE_EDITTED} from '../../Configuration.events';
import { assert } from 'core/classes/Assert';
import { CTX_PAGE, CTX_ASCENDANTS, CTX_DESCENDANTS } from 'core/classes/events/contexts';
import 'core/components/Facet/Facet.component';
import { FACET_ENABLED_OPTIONS_CHANGED } from 'core/components/Facet/Facet.events';
import 'core/components/Pagination/Pagination.component';
import { PAGINATION_PAGE_CHANGED } from 'core/components/Pagination/Pagination.events';
import 'core/components/Dropdown/Dropdown.component';
import { DROPDOWN_OPTION_CHANGED } from 'core/components/Dropdown/Dropdown.events';
import '../RuleCreation/RuleCreation.component';
import '../Rule/Rule.component';
import 'core/bindings/numberFormatted';

const FACET_CATEGORIES = {
    ANALYSIS: {
        origin: {
            label: 'Origin',
            description: '?',
        },
    },
    USER: {
        ruleType: {
            label: 'Type of rule',
            description: '?',
        },
        origin: {
            label: 'Origin',
            description: '?',
        },
        package: {
            label: 'Package',
            description: '?',
        },
        class: {
            label: 'Class',
            description: '?',
        },
    },
};

/**
 *
 */
export default class Rules extends Base {

    /**
     * @param {HTMLElement} element
     * @param {Bundle} bundle
     * @param {Configuration} configuration
     * @param {"ANALYSIS"|"USER"} originType
     */
    constructor(element, { bundle, configuration, originType }) {

        super(element);

        assert(['USER', 'ANALYSIS'].includes(originType),
            `Rules.vm: originType must be either "USER" or "ANALYSIS"`);

        this.bundle = bundle;
        this.configuration = configuration;
        this.originType = originType;
        this.facetCategoriesToShow = FACET_CATEGORIES[originType];
        this.resultsPerPage = originType === 'USER' ? 10000 : 15;
        this.editable = originType === 'USER';

        this.startListeningToChildrenEvents();
        this.startListeningToPageEvents();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bundle;
    configuration;
    originType;
    requests = new PromiseQueue();
    facetEnabledOptions = ko.observable({});
    editable;

    sortByOptions = [
        { text: 'Matching methods', value: 'matchingMethods|DESC', order: 'desc', type: 'quantity' },
        { text: 'Matching methods', value: 'matchingMethods|ASC', order: 'asc', type: 'quantity' },
        null,
        { text: 'Newer first', value: 'userIndex|DESC', order: 'desc', type: null },
        { text: 'Older first', value: 'userIndex|ASC', order: 'asc', type: null },
    ];

    elem_filterToggler = this.element.getElementsByClassName('fa-filter')[0];
    elementFilters = this.element.getElementsByClassName('filters')[0];
    hasAppliedFilters = this.computed(() => Object.keys(this.facetEnabledOptions()).length > 0);

    count = ko.observable(0);
    rules = ko.observableArray();
    page = ko.observable(1);
    resultsPerPage;
    sortBy = ko.observable();
    loadFacets = false;

    selectedRuleNode = null;

    // 'rulesFetched' is needed for keeping panel hidden before actually knowing the rule quantity.
    rulesFetched = ko.observable(false);
    panelFullSize = {
        margin: 0,
        height: '100%',
        boxShadow: 'none',
    };
    panelSmallSize = {
        margin: '5px 0 0',
        height: '65%',
        boxShadow: '0 0 10px rgba(0, 0, 0, 0.2)',
    };

    startListeningToChildrenEvents() {

        this.on(CTX_DESCENDANTS, PAGINATION_PAGE_CHANGED, page => {

            this.page(Math.max(parseInt(page), 1));
        });

        this.on(CTX_DESCENDANTS, DROPDOWN_OPTION_CHANGED, sortBy => {

            this.sortBy(sortBy.value);
        });

        this.on(CTX_DESCENDANTS, FACET_ENABLED_OPTIONS_CHANGED, enabledOptions => {

            this.facetEnabledOptions(enabledOptions);
        });

        this.on(CTX_DESCENDANTS, RULE_CLOSED, () => {

            this.clearSelectedRule();

        }, { ignoreIfEquals: false });


        const reloadRulesAndFacets = () => {
            this.loadFacets = true;
            this.forceResetToPageOne();
        };

        this.on(CTX_PAGE, RULE_CREATED, reloadRulesAndFacets);
        this.on(CTX_PAGE, RULE_DELETED, reloadRulesAndFacets);
        this.on(CTX_PAGE, RULE_EDITTED, reloadRulesAndFacets);
    }

    startListeningToPageEvents() {

        // listen to configuration_directive_changed event:
        // - if current directive set is from this configuration, then reload directives.
    }

    initAfterBinding() {

        this.subscribe(this.page, page => {

            this.children.pagination.goto(page);
            const loadFacets = !!this.loadFacets;
            this.loadFacets = false;
            this.loadRules(loadFacets);
        });

        this.subscribe(this.sortBy, sortBy => {

            this.forceResetToPageOne();
        });

        this.computed(() => {

            this.facetEnabledOptions();
            this.loadFacets = true;
            this.forceResetToPageOne();

        }).extend({ deferred: true });
    }

    forceResetToPageOne() {

        this.page.peek() === 1 ? this.page.valueHasMutated() : this.page(1);
    }

    toggleFilters() {

        this.elem_filterToggler.classList.toggle('open');
        this.elementFilters.classList.toggle('hide');
    }

    clearFilters() {

        this.children.facet.clearEnabledOptions();
        this.facetEnabledOptions({});
    }

    clearSelectedRule() {

        this.selectedRuleNode && this.selectedRuleNode.classList.remove('selected');
        this.selectedRuleNode = null;
    }

    openRule = (rule, event) => {

        this.clearSelectedRule();
        let node = event.target;
        while(!node.classList.contains('list-group-item')) {
            node = node.parentNode;
        }
        node.classList.add('selected');
        this.selectedRuleNode = node;

        this.children.ruleCreator.hide();
        this.children.activeRule.show(rule);
    };

    openDirectivesWithRule = (rule, event) => {

        event.stopPropagation();

        this.notify(CTX_ASCENDANTS, RULE_SHOW_DIRECTIVES, rule);
    };

    addRule() {

        this.children.activeRule.hide();
        this.children.ruleCreator.show();
    }

    convertFacetEnabledOptionsToApiOptions() {

        return Object.entries(this.facetEnabledOptions.peek()).reduce((acc, [ facetName, enabledOptions ]) => ({
            ...acc,
            [facetName]: Array.from(Object.keys(enabledOptions)),
        }), {
            originType: this.originType,
        });
    }

    loadRules(fetchFacets = false) {

        const options = {
            page: Math.max(this.page.peek(), 1),
            sortBy: this.sortBy.peek() || this.sortByOptions[0].value,
            resultsPerPage: this.resultsPerPage,
            facets: fetchFacets,
            facetOptions: this.convertFacetEnabledOptionsToApiOptions(),
        };

        this.requests.push(Api.fetchRules(this.bundle.id, this.configuration.name(), options), ({ hits, rules, facets }) => {
            this.count(hits);
            this.rules(rules);
            this.children.pagination.setTotal(Math.ceil(hits/options.resultsPerPage));

            fetchFacets && this.children.facet.update(facets, this.facetCategoriesToShow);

            this.rulesFetched(true);
        });
    }

};
