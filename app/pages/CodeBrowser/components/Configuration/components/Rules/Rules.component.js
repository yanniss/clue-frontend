import './Rules.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './Rules.vm';
import view from './Rules.view.html';

koh.registerComponent('component-rules', vm, view);
