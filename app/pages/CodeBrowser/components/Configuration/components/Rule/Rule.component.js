import './Rule.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './Rule.vm';
import view from './Rule.view.html';

koh.registerComponent('component-rule', vm, view);
