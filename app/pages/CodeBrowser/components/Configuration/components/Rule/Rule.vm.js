import Rule from 'codebrowser/classes/Rule';
import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import Ace from 'brace';
import 'brace/mode/java';
import { copyToClipboard } from 'core/classes/clipboard';
import { CTX_ASCENDANTS } from 'core/classes/events/contexts';
import { RULE_CLOSED, RULE_DELETED } from 'codebrowser/components/Configuration/Configuration.events';
import 'codebrowser/components/trees/RuleExpansionTree/RuleExpansionTree.component';

const dummyRule = new Rule({});

/**
 *
 */
export default class RuleVM extends Base {

    constructor(element, { bundle, configuration, editable = false }) {

        super(element);

        this.bundle = bundle;
        this.configuration = configuration;
        this.editable = editable;

        this.createEditorInstance();

        this.subscribe(this.editableComment, comment =>
            this.commentModified(this.rule().comment() !== comment)
        );

        this.ruleBodyEditor.on('change', (action, editor) =>
            this.ruleBodyModified(this.rule().ruleBody() !== editor.getValue())
        );

        this.canEdit = this.computed(() => {
            const canEdit = this.editable && this.configuration.canModifyRuleSet();
            this.ruleBodyEditor.setReadOnly(!canEdit);
            return canEdit;
        }, { deferred: true });
    }

    dispose() {

        this.ruleBodyEditor.off();
        super.dispose();
    }

    show(rule) {

        this.setRule(rule);
        super.show();
        this.ruleBodyEditor.focus();
    }

    hide = () => {

        super.hide();
        this.setRule(dummyRule);
        this.notify(CTX_ASCENDANTS, RULE_CLOSED, this.rule());
    };

    setRule(rule) {

        if(rule === this.rule()) { return; }

        this.rule(rule);
        this.editableComment(rule.comment());
        this.ruleBodyEditor.setValue(rule.ruleBody(), -1);
        this.ruleBodyEditor.resize(true);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    bundle;
    configuration;
    editable;

    rule = ko.observable(dummyRule);
    ruleBodyEditor;
    editableComment = ko.observable('');
    ruleBodyModified = ko.observable(false);
    commentModified = ko.observable(false);
    modified = this.computed(() => this.ruleBodyModified() || this.commentModified());

    hasPendingSaveRequest = ko.observable(false);

    createEditorInstance() {

        const element = this.element.getElementsByClassName('ace')[0],
            editor = Ace.edit(element);

        editor.setOptions({
            highlightActiveLine: true,
            fontSize: '11pt',
            fontFamily: 'Cousine, Consolas',
            fixedWidthGutter: true,
            mode: 'ace/mode/java',
        });
        editor.container.style.lineHeight = '1.5';
        editor.renderer.setScrollMargin(2, 10, 0, 10);
        editor.setShowPrintMargin(false);

        // Remove ACE warning message in console.
        editor.$blockScrolling = Infinity;

        this.ruleBodyEditor = editor;
    }

    copyCommentToClipboard() {

        copyToClipboard(this.editableComment());
    }

    copyRuleBodyToClipboard() {

        copyToClipboard(this.ruleBodyEditor.getValue());
    }

    deleteRule() {

        this.configuration.deleteRule(this.rule())
            .then(ruleId => {
                if(ruleId !== this.rule().id) { return; }
                this.hide();
            })
            .catch(() => {});
    }

    reset() {

        const rule = this.rule();

        this.ruleBodyEditor.setValue(rule.ruleBody(), -1);
        this.editableComment(rule.comment());
    }

    save() {

        const editedRuleBody = this.ruleBodyModified() ? this.ruleBodyEditor.getValue() : null,
            editedComment = this.editableComment();

        this.hasPendingSaveRequest(true);

        this.configuration.editRule(this.rule(), editedRuleBody, editedComment)
            .then(rule => {
                if(rule !== this.rule()) { return; }
                this.ruleBodyEditor.setValue(rule.ruleBody(), -1);
                this.editableComment(rule.comment());
                this.ruleBodyModified(false);
                this.commentModified(false);
            })
            .catch(() => {})
            .then(() =>
                this.hasPendingSaveRequest(false)
            );
    }

};
