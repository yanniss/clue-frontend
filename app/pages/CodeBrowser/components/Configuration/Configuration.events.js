import str from 'core/classes/StringHelper.static';

export const RULE_CREATED = str.createUnique();
export const RULE_DELETED = str.createUnique();
export const RULE_SHOW_DIRECTIVES = str.createUnique();
export const RULE_CLOSED = str.createUnique();
export const RULE_EDITTED = str.createUnique();

export const RULE_OPEN = str.createUnique();
