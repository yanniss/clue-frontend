import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import $ from 'jquery';
import Api from 'app/classes/Api.static';
import Notifier from 'app/classes/Notifier.static';
import { CTX_PAGE } from 'core/classes/events/contexts';
import { ANALYSIS_OPEN_DELETION_VERIFICATION, ANALYSIS_DELETED } from 'codebrowser/classes/analysis/Analysis.events';

/**
 *
 */
export default class AnalysisDeletion extends Base {

    constructor(element) {

        super(element);

        this.startListeningToPageEvents();
    }

    dispose() {

        this.$modal.removeClass('fade');
        this.$modal.modal('hide');
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    analysis = ko.observable({});
    $modal = $(this.element.getElementsByClassName('modal')[0]);
    $submitButton = $(this.element.querySelector('button[type="submit"]'));

    startListeningToPageEvents() {

        this.on(CTX_PAGE, ANALYSIS_OPEN_DELETION_VERIFICATION, analysis => {
            this.analysis(analysis);
            this.$modal.modal('show');
        }, { ignoreIfEquals: false });
    }

    submit() {

        const { bundleId, id, name } = this.analysis();

        this.$submitButton.button('loading');

        Api.deleteAnalysis(bundleId, id)
            .then(() => {
                this.notify(CTX_PAGE, ANALYSIS_DELETED, id);
                Notifier.success(`Analysis deleted successfully: ${name}`);
            })
            .catch(() => {
                Notifier.error(`Failed to delete analysis: ${name}`);
            })
            .then(() => {
                this.$submitButton.button('reset');
                this.$modal.modal('hide');
                this.analysis({});
            });
    }

};
