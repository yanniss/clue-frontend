import './AnalysisDeletion.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './AnalysisDeletion.vm';
import view from './AnalysisDeletion.view.html';

koh.registerComponent('component-analysis-deletion', vm, view);
