import './CodeBrowser.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './CodeBrowser.vm';
import view from './CodeBrowser.view.html';

koh.registerComponent('component-codebrowser', vm, view);
