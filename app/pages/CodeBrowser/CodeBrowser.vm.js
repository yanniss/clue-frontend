/**
 * Difference between "tabs" and "programElements" containers and active handling.
 *  would the subscription style on programElements fit on tabs too?
 *
 * todo: move subscriptions to separate method and call it when analysis is loaded and is valid to use.
 *
 * todo: remove Ace from dependencies (then editor's ace/range will crash)
 *
 * todo: URL may contain a file and a position to trigger on component initialization. What if in this position lie
 *       more than one program elements? Currently, it will open the one considered default, which is problematic...
 *
 * todo: IMPORTANT!!! bug when switching editor tabs/files
 *  e.g. "new" @ http://localhost:8081/clue/index.html#/analyses/Signal-Android-context-insensitive-plusplus-20171108_21.13.26/codebrowser?file=org/thoughtcrime/securesms/TransportOptionsAdapter.java&line=51&column=15
 *  e.g. does not keep scroll state
 */

import Page from 'core/classes/vm/Page.vm';
import $ from 'jquery';
import ko from 'knockout';
import navbar from './CodeBrowser.view.navbar.html';
import ConfigurationManager from './classes/configuration/ConfigurationManager';
import AnalysisManager from './classes/analysis/AnalysisManager';
import Api from 'app/classes/Api.static';
import Notifier from 'app/classes/Notifier.static';
import { CTX_PAGE } from 'core/classes/events/contexts';
import { SELECTION_ACTIVATED, RESIZE_X } from 'app/classes/eventTypes';

import 'core/bindings/midclick';
import 'core/bindings/resizable';
import 'app/components/Navbar/Navbar.component';
import 'app/components/SearchBox/SearchBox.component';
import './components/BundleInfo/BundleInfo.component';
import './components/SelectionHistory/SelectionHistory.component';
import './components/SymbolSearch/SymbolSearch.component';
import './components/trees/ProjectTree/ProjectTree.component';
import './components/trees/PackageTree/PackageTree.component';
import './components/DirectiveMenu/DirectiveMenu.component';
import './components/Tabs/Tabs.component';

import './components/Configurations/Configurations.component';
import './components/ConfigurationCreation/ConfigurationCreation.component';
import './components/ConfigurationDeletion/ConfigurationDeletion.component';
import './components/ConfigurationRename/ConfigurationRename.component';
import {
    CONFIGURATION_ACTIVATED,
    CONFIGURATION_OPEN,
    CONFIGURATION_OPEN_CREATION_FORM
} from 'codebrowser/classes/configuration/Configuration.events';

import './components/Analyses/Analyses.component';
import './components/AnalysisCreation/AnalysisCreation.component';
import './components/AnalysisDeletion/AnalysisDeletion.component';

import 'core/components/Wizard/Wizard.component';
import './components/ProguardWizardSteps/Step1/Step1.component';
import './components/ProguardWizardSteps/Step2/Step2.component';
import './components/ProguardWizardSteps/Step3/Step3.component';

/**
 *
 */
export default class CodeBrowser extends Page {

    app = ko.contextFor(this.element).$root;

    /**
     * @public
     * @param element
     * @param projectId
     * @param bundleId
     */
    constructor(element, { projectId, bundleId }) {

        super(element, [ projectId, bundleId ]);

        for(const name of Object.keys(this.lsKeys)) {
            this.lsKeys[name] += bundleId();
        }

        const pagePubsub = this.getPubsub(CTX_PAGE);
        this.configurationManager = new ConfigurationManager(bundleId(), pagePubsub);
        //this.analysisManager = new AnalysisManager(bundleId(), pagePubsub);

        Promise.all([
            Api.fetchProject(projectId()),
            Api.fetchBundle(bundleId()),
            this.configurationManager.getConfigurations(),
        ])
            .then(([ project, bundle ]) => {
                this.project(project);
                this.bundle(bundle);
                this.activeTree(localStorage.getItem(this.lsKeys.activeTree) || 'projectTree');
                this.activeBottomLeftTab(localStorage.getItem(this.lsKeys.activeBottomLeftTab) || 'configurationsTab');
                this._setupView();
                this._setupUiEventHandlers();
                this._setupKoSubscriptions();
            });
    }

    dispose() {

        //this.analysisManager.dispose();
        this.configurationManager.dispose();
        this._cleanView();
        super.dispose();
    }

    openBundleInfoModal = (_, event) => {

        this.openModal.bundleInfo(Math.random());
    };

    openProjectTree = (_, event) => {

        this.activeTree('projectTree');
    };

    openPackageTree = (_, event) => {

        this.activeTree('packageTree');
    };

    openConfigurationsTab = (_, event) => {

        this.activeBottomLeftTab('configurationsTab');
    };

    openAnalysesTab = (_, event) => {

        this.activeBottomLeftTab('analysesTab');
    };

    onPanelsResizing = () => {

        this.notify(CTX_PAGE, RESIZE_X);
    };

    activateConfiguration = configuration => {

        this.configurationManager.activateConfiguration(configuration);
    };

    createConfiguration = () => {

        this.notify(CTX_PAGE, CONFIGURATION_OPEN_CREATION_FORM, this.bundle());
    };

    openProguardWizard = () => {

        this.children.proguardWizard.show();
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Private members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    navbarContent = navbar;

    lsKeys = {
        activeTree: 'CodeBrowser.activeTree:',
        activeBottomLeftTab: 'CodeBrowser.activeBottomLeftTab:',
        sizes: 'CodeBrowser.sizes:',
        collapsed: 'CodeBrowser.sidebarCollapsed:',
    };

    project = ko.observable();
    bundle = ko.observable();

    openModal = {
        bundleInfo: ko.observable().extend({ notify: 'always' }),
    };

    $treeSidebar = null;
    activeTree = ko.observable();
    isRightSidebarVisible = ko.observable(false);
    activeBottomLeftTab = ko.observable();

    configurationManager;
    //analysisManager;
    activeConfiguration = ko.observable();

    _setupView() {

        $(document).addClass('unscrollable');
        $(document.body).addClass('unscrollable');

        this.$treeSidebar = $('#sidebar-contents');
        if(localStorage.getItem(this.lsKeys.collapsed) === 'true') {
            this.$treeSidebar.addClass('collapsed');
        }
    }

    _cleanView() {

        Notifier.clear();
        $(document).removeClass('unscrollable');
        $(document.body).removeClass('unscrollable');
    }

    _setupUiEventHandlers() {

        this.on(CTX_PAGE, SELECTION_ACTIVATED, selection => this.isRightSidebarVisible(!!selection));

        this.on(CTX_PAGE, CONFIGURATION_ACTIVATED, this.activeConfiguration);
    }

    _setupKoSubscriptions() {

        this.subscribe(this.activeTree, treeName =>
            localStorage.setItem(this.lsKeys.activeTree, treeName)
        );

        this.subscribe(this.activeBottomLeftTab, activeBottomLeftTab =>
            localStorage.setItem(this.lsKeys.activeBottomLeftTab, activeBottomLeftTab)
        );
    }

};
