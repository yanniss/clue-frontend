import './NotFound/NotFound.component';
import Route from 'core/classes/Route';

const ALLOW_ALL = 0;
const UNAUTHENTICATED = 1;
const AUTHENTICATED = 2;
const ADMIN = 3;

/**
 *
 */
export default {
    pages: [
        new Route(
            'dev/test',
            'component-test',
            () => import('./_Test/_Test.component'),
            //ALLOW_ALL,
        ),
        new Route(
            ['', 'projects'],
            'component-projects',
            () => import('./Projects/Projects.component'),
            //ALLOW_ALL,
        ),
        new Route(
            'projects/{projectId}',
            'component-project',
            () => import('./Project/Project.component'),
            //ALLOW_ALL,
        ),
        new Route(
            'projects/{projectId}/bundles/{bundleId}',
            'component-codebrowser',
            () => import('./CodeBrowser/CodeBrowser.component'),
            //ALLOW_ALL,
        ),
        new Route(
            'login',
            'login',
            () => import('./Login/Login.component'),
            //UNAUTHENTICATED,
        ),
        new Route(
            'change-password',
            'password-change',
            () => import('./PasswordChange/PasswordChange.component'),
            //AUTHENTICATED,
        ),
        new Route(
            ['admin', 'admin/users'],
            'user-list',
            () => import('./UserList/UserList.component'),
            //ADMIN,
        ),
        new Route(
            'admin/users/create',
            'user-creation',
            () => import('./UserCreation/UserCreation.component'),
            //ADMIN,
        ),
        new Route(
            'admin/users/{userId}/change-password',
            'user-password-change',
            () => import('./UserPasswordChange/UserPasswordChange.component'),
            //ADMIN,
        ),
    ],
    notfound: new Route(
        '<<irrelevant>>',
        'not-found',
        undefined,
        //ALLOW_ALL,
    ),
};
