import 'app/components/Navbar/Navbar.component';
import $ from 'jquery';
import Api from 'app/classes/Api.static';
import Notifier from 'app/classes/Notifier.static';
import Page from 'core/classes/vm/Page.vm';
import ko from 'knockout';
import navbar from './UserCreation.view.navbar.html';

/**
 *
 */
export default class UserCreation extends Page {

    navbarContent = navbar;

    username = ko.observable('');
    usernameHasBeenBlurred = ko.observable(false); // If it has been focused at least once in the past.
    usernameBlurred = () => { this.usernameHasBeenBlurred(true); };

    password = ko.observable('');
    passwordRepeat = ko.observable('');
    passwordRepeatHasBeenFocused = ko.observable(false); // If it has been focused at least once.
    passwordRepeatFocused = () => { this.passwordRepeatHasBeenFocused(true); };

    submit(form) {

        const $form = $(form),
            $btns = $form.find('button[type=submit]');

        // Disable submit buttons
        $btns.each((index, btn) => { $(btn).button('loading'); });

        Api.createUser($form)
            .then(userId => {
                Notifier.success(`Created user ${userId}. Please wait ...`);
                setTimeout(() => {
                    location.hash = '/admin/users';
                }, 1000);
            })
            .catch(error => {
                Notifier.error(error);
            })
            .finally(() => {
                // Enable submit buttons
                $btns.each((index, btn) => { $(btn).button('reset'); });
            });

        return false;
    }

};
