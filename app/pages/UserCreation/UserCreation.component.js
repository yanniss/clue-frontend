import './UserCreation.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './UserCreation.vm';
import view from './UserCreation.view.html';

koh.registerComponent('user-creation', vm, view);
