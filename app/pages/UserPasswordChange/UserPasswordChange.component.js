import './UserPasswordChange.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './UserPasswordChange.vm';
import view from './UserPasswordChange.view.html';

koh.registerComponent('user-password-change', vm, view);
