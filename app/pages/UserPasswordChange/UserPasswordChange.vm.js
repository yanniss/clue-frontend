import 'app/components/Navbar/Navbar.component';
import $ from 'jquery';
import Api from 'app/classes/Api.static';
import Notifier from 'app/classes/Notifier.static';
import Page from 'core/classes/vm/Page.vm';
import ko from 'knockout';
import navbar from './UserPasswordChange.view.navbar.html';

/**
 *
 */
export default class UserPasswordChange extends Page {

    navbarContent = navbar;

    username = ko.observable();
    password = ko.observable('');
    passwordRepeat = ko.observable('');
    passwordRepeatHasBeenFocused = ko.observable(false); // If it has been focused at least once.
    passwordRepeatFocused = () => { this.passwordRepeatHasBeenFocused(true); };

    constructor(element, {
        id,
    }) {

        super(element);

        this.username(id);
    }

    submit(form) {

        const btn = $(form).find('button[type=submit].btn-success');

        btn.button('loading');

        Api.updateUserPassword(this.username(), null, this.password())
            .then(() => {
                Notifier.success(`Changed password of user "..." successfully. Please wait ...`);
                setTimeout(() => {
                    location.hash = '/admin/';
                }, 2000);
            })
            .finally(() => {
                setTimeout(() => {
                    btn.button('reset');
                }, 750);
            });

        return false;
    }

};
