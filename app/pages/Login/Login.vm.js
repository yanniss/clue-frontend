import $ from 'jquery';
import Api from 'app/classes/Api.static';
import Page from 'core/classes/vm/Page.vm';

/**
 *
 */
export default class Login extends Page {

    submit(form) {

        let $form = $(form),
            username = $form.find('[name="username"]').val(),
            password = $form.find('[name="password"]').val();

        Api.login(username, password)
            .then(() => {
                window.location.hash = '';
            })
            .catch(() => {
                alert('LOGIN FAILED');
            });
    }

};
