import './Login.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './Login.view.html';
import vm from './Login.vm';

koh.registerComponent('login', vm, view);
