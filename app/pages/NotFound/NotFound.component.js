import './NotFound.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './NotFound.view.html';
import vm from './NotFound.vm';

koh.registerComponent('not-found', vm, view);
