import Page from 'core/classes/vm/Page.vm';

/**
 *
 */
export default class NotFound extends Page {

    supportsHistoryBack = !!window.history.back;

    goBack() {

        window.history.back();
    }

    goHome() {

        window.location.hash = '/';
    }

};
