import Api from 'app/classes/Api.static';
import Page from 'core/classes/vm/Page.vm';
import PromiseQueue from 'core/classes/PromiseQueue';
import ko from 'knockout';
import navbar from './Project.view.navbar.html';
import { CTX_DESCENDANTS } from 'core/classes/events/contexts';
import { BUNDLE_NEW, BUNDLE_DELETE } from 'app/classes/eventTypes';
import 'app/components/Navbar/Navbar.component';
import './components/BundleCreation/BundleCreation.component';
import 'app/components/BundleDeletion/BundleDeletion.component';

/**
 *
 */
export default class Bundles extends Page {

    /**
     * @param {HTMLElement} element
     * @param {ko.observable} projectId
     */
    constructor(element, { projectId }) {

        super(element, projectId);

        Api.fetchProject(projectId())
            .then(this.project)
            .then(() => {
                this.startListeningToChildren();
            });

        this.loadBundles(projectId);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    navbarContent = navbar;
    project = ko.observable();
    bundles = ko.observableArray();

    getBundleUrl = bundle => {

        return `#/projects/${encodeURIComponent(this.project().id)}/bundles/${encodeURIComponent(bundle.id)}`;
    };

    getBundleArtfactsAsTitle = project => {

        return `Application artifacts:\n${project.appArtifacts.map(input => '\n' + input.name).join('')}\n\n` +
            `Dependency artifacts:\n${project.depArtifacts.map(input => '\n' + input.name ).join('')}`;
    };

    getArtifactsToString(artifacts) {

        return artifacts.map(artifact => artifact.name).join(', ');
    }

    openBundleCreationModal = () => {

        this.children.bundleCreationModal.show(this.project());
    };

    openBundleDeletionModal = bundle => {

        this.children.bundleDeletionModal.show(bundle);
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view model members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    requests = new PromiseQueue();

    startListeningToChildren() {

        this.on(CTX_DESCENDANTS, BUNDLE_NEW, bundleId => {
            const projectId = encodeURIComponent(this.project().id);

            location.hash = `/projects/${projectId}/bundles/${encodeURIComponent(bundleId)}`;
        });

        this.on(CTX_DESCENDANTS, BUNDLE_DELETE, () => {
            this.loadBundles();
        });
    }

    loadBundles(projectId = this.project().id) {

        const promise = Api.fetchBundles(projectId, { _start: 0, _count: 10000, _sort: 'created|DESC', text: '' });

        this.requests.push(promise, this.bundles);
    }

};
