import 'app/components/FormOption/FormOption.component';
import $ from 'jquery';
import Notifier from 'app/classes/Notifier.static';
import Api from 'app/classes/Api.static';
import Base from 'core/classes/vm/Base.vm';
import ko from 'knockout';
import { CTX_ASCENDANTS } from 'core/classes/events/contexts';
import { BUNDLE_NEW } from 'app/classes/eventTypes';

/**
 *
 */
export default class BundleCreation extends Base {

    $modal = $(this.element).find('.modal');
    $form;
    $submit;
    project = ko.observable();
    options = ko.observableArray();
    bundleId;

    /**
     * @param {HTMLElement} element
     */
    constructor(element) {

        super(element);

        this.$modal.on('hidden.bs.modal', () => {
            this.bundleId && this.notify(CTX_ASCENDANTS, BUNDLE_NEW, this.bundleId);
        });

        Api.fetchBundleOptions()
            .then(this.options);
    }

    dispose() {

        this.$modal.off();
        super.dispose();
    }

    /**
     * @public
     * @override
     * @param {Project} project
     */
    show(project) {

        this.project(project);
        this.$form = this.$modal.find('form');
        this.$submit = this.$form.find('button[type=submit]');
        this.$modal.modal('show');
    }

    /**
     * @public
     * @override
     */
    hide() {

        this.$modal.modal('hide');
    }

    /**
     * @private
     */
    submit() {

        this.$submit.button('loading');

        Api.createBundle(this.project().id, this.$form)
            .then(bundleId => {
                this.bundleId = bundleId;
                this.hide();
                Notifier.success(`Bundle successfully created.`);
            })
            .catch(() => Notifier.error(`Failed to create bundle.`))
            .finally(() => {
                this.$submit.button('reset');
            });
    }

};
