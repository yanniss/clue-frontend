import './BundleCreation.style.scss'
import koh from 'core/classes/KnockoutHelper.static';
import view from './BundleCreation.view.html';
import vm from './BundleCreation.vm';

koh.registerComponent('component-bundle-creation', vm, view);
