import Page from 'core/classes/vm/Page.vm';
import 'core/components/Menu/Menu.component';
import 'core/bindings/midclick';
import 'core/bindings/rclick';
import 'core/components/Pagination/Pagination.component';

/**
 *
 */
export default class Test extends Page {

    initAfterBinding() {

        this.initTestMenuContextmenu();
    }

    randomizeTotalPages() {
        this.children.pages.setTotal(Math.abs(Math.random() * 100));
    }

    set = page => {
        this.children.pages.goto(page);
    };

    /*************************************************************
     * Test menu on click and contextmenu
     */

    openMenuOnClick(_, event) {

        this.children.menu.open(event);

        return true;
    }

    initTestMenuContextmenu() {

        const element = document.getElementById('test-menu-contextmenu');

        this.addEventListener('contextmenu', event => {
            this.children.menu.open(event);
            event.preventDefault();
        }, {}, element);
    }

    option1(data, e) {
        console.log('option1');
        data.close();
    }
    option2(data, e) {
        console.log('option2');
    }
    option3(data, e) {
        console.log('option3');
    }

    foo() {
        console.log(this.bar, ...arguments);
    }
    bar = 123;

};
