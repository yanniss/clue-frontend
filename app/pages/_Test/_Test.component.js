import './_Test.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import view from './_Test.view.html';
import vm from './_Test.vm';

koh.registerComponent('component-test', vm, view);
