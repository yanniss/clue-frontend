import 'app/components/Navbar/Navbar.component';
import 'app/components/ProjectDeletion/ProjectDeletion.component';
import './components/ProjectCreation/ProjectCreation.component';
import Api from 'app/classes/Api.static';
import Page from 'core/classes/vm/Page.vm';
import PromiseQueue from 'core/classes/PromiseQueue';
import ko from 'knockout';
import navbar from './Projects.view.navbar.html';
import { CTX_DESCENDANTS } from 'core/classes/events/contexts';
import { PROJECT_NEW, PROJECT_DELETE } from 'app/classes/eventTypes';
import { sleep } from 'core/classes/Async';

/**
 *
 */
export default class Projects extends Page {

    /**
     * @param {HTMLElement} element
     */
    constructor(element) {

        super(element);

        this.loadProjects();
        this.startListeningToChildren();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    navbarContent = navbar;
    projects = ko.observableArray();

    getProjectUrl = project => {

        return `#/projects/${encodeURIComponent(project.id)}`;
    };

    openProjectCreationModal = () => {

        this.children.projectCreationModal.show();
    };

    openProjectDeletionModal = project => {

        this.children.projectDeletionModal.show(project);
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // view model members
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    requests = new PromiseQueue();

    startListeningToChildren() {

        this.on(CTX_DESCENDANTS, PROJECT_NEW, project => {
            const projectId = encodeURIComponent(project.id);

            location.hash = `/projects/${projectId}`;
        });

        this.on(CTX_DESCENDANTS, PROJECT_DELETE, () => {
            this.loadProjects();
        });
    }

    loadProjects() {

        const promise = Api.fetchProjects({ _start: 0, _count: 10000, _sort: 'created|DESC', text: '' });

        this.requests.push(promise, this.projects);
    }

    /*
    loadProjects(tries = 4) {

        if(tries === 0) { return; }

        const promise = new Promise(async (resolve, reject) => {
            await sleep(.25);
            Api.fetchProjects({ _start: 0, _count: 10000, _sort: 'created|DESC', text: '' })
                .then((...args) => resolve.apply(null, args))
                .catch((...args) => reject.apply(null, args));
        });

        this.requests.push(promise, projects => {
            if(this.equals(projects)) {
                this.loadProjects(tries-1);
            }
            else {
                this.projects(projects);
            }
        });
    }
    */

    /**
     * Compares local state to "projects", which should be the state fetched from server.
     *
     * @param {array<Project>} projects
     */
    /*
    equals(projects) {

        const localProjects = this.projects(),
            n1 = localProjects.length,
            n2 = projects.length;

        if(n1 !== n2) { return false; }

        for(let i = 0; i < n1; i++) {
            if(projects[i].id !== localProjects[i].id) {
                return false;
            }
        }

        return true;
    }
    */

};
