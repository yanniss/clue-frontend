import './ProjectCreation.style.scss'
import koh from 'core/classes/KnockoutHelper.static';
import view from './ProjectCreation.view.html';
import vm from './ProjectCreation.vm';

koh.registerComponent('component-project-creation', vm, view);
