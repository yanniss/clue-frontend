import 'app/components/FormOption/FormOption.component';
import $ from 'jquery';
import Notifier from 'app/classes/Notifier.static';
import Api from 'app/classes/Api.static';
import Base from 'core/classes/vm/Base.vm';
import { CTX_ASCENDANTS } from 'core/classes/events/contexts';
import { PROJECT_NEW } from 'app/classes/eventTypes';

/**
 *
 */
export default class ProjectCreation extends Base {

    options = [
        {
            id: 'name',
            description: '',
            name: 'Project name',
            isMandatory: true,
            isFile: false,
            isBoolean: false,
            fileTypes: [],
            validValues: [],
            defaultValue: null,
        },
    ];
    $modal = $(this.element).find('.modal');
    $form = this.$modal.find('form');
    project;

    /**
     * @param {HTMLElement} element
     */
    constructor(element) {

        super(element);

        this.$modal.on('hidden.bs.modal', () => {
            this.project && this.notify(CTX_ASCENDANTS, PROJECT_NEW, this.project);
        });
    }

    dispose() {

        this.$modal.off();
        super.dispose();
    }

    /**
     * @public
     * @override
     */
    show() {

        this.$modal.modal('show');
    }

    /**
     * @public
     * @override
     */
    hide() {

        this.$modal.modal('hide');
    }

    /**
     * @private
     */
    submit() {

        Api.createProject(this.$form)
            .then(project => {
                this.project = project;
                this.hide();
                Notifier.success(`Project successfully created.`);
            })
            .catch(() => Notifier.error(`Failed to create project.`));
    }

};
