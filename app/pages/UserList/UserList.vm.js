import 'app/components/Navbar/Navbar.component';
import 'app/components/UserDeletion/UserDeletion.component';
import Api from 'app/classes/Api.static';
import Page from 'core/classes/vm/Page.vm';
import ko from 'knockout';
import navbar from './UserList.view.navbar.html';

/**
 *
 */
export default class UserList extends Page {

    navbarContent = navbar;
    users = ko.observableArray();
    openDeleteUserModal = ko.observable();

    constructor(element) {

        super(element);

        this.loadUsers();
    }

    loadUsers() {

        Api.fetchUsers()
            .then(data => {
                this.users(data.sort((a, b) => a.localeCompare(b)));
            });
    }

};
