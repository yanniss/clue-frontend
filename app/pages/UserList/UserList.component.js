import './UserList.style.scss';
import koh from 'core/classes/KnockoutHelper.static';
import vm from './UserList.vm';
import view from './UserList.view.html';

koh.registerComponent('user-list', vm, view);
