import 'bootstrap.style';
import 'bootstrap.logic';
import 'fontawesome';

import 'core/styles/concrete.scss';
import 'core/styles/bootstrap3.ext.scss';
import 'app/styles/main.css';

import Api from './classes/Api.static';
import Cookies from 'js-cookie';
import SPA from 'core/classes/vm/SPA.vm';
import routes from 'app/pages/routes';

/**
 *
 */
export default class App extends SPA {

    constructor(element) {

        super(element, routes);

        Api.fetchUserInfo();
    }

    getUsername() {

        const username = Cookies.get('username');

        return username && username !== 'demo' ? username : 'guest';
    }

    isUserAuthenticated() {

        return this.getUsername() !== 'guest';
    }

    isUserAdmin() {

        return this.getUsername() === 'admin';
    }

    /*
    userHasRoleFrom(roles) {
        if(roles.indexOf('ALL') !== -1) {
            return true;
        }
        let user = localStorage.getItem('user');
        if(user) {
            user = JSON.parse(user);
            for(let i = 0, n = roles.length; i < n; i++) {
                if(user.roles.indexOf(roles[i])) {
                    return true;
                }
            }
        }
        return false;
    }
    */

};
