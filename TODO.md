
introjs:
* Clue can remove dead code from your bundle.

server:
* when analysis finishes successfully, "dirty" is updated with a delay.
* when i stop analysis, "state" is updated with a delay.

* Στο Eff. directives tab, να μη φαίνονται τα rule ids, αλλά το rule body (με κάποιο τρόπο)
* Στον code browser, να τα δείχνουμε όλα: τα seeds (matching methods), τα analysis-generated directives  αλλά και τα
  effective directives. Πιο λογικό είναι να φαίνονται μόνο το 1ο και το 3ο, μάλλον, αλλά ας τα δείξουμε όλα και βλέπουμε.

* Add symbol search in codebrowser

* Refactor modals
* Base.vm: add notifyPage as a shortcut of notify(CTX_PAGE); same for on(CTX_PAGE).
* Make all actions events; Notifier should be a singleton listening to these events instead of being used from each component.

* Major TODO: components should lazily update.

* Can SPA/Router be not the top component?

* Load codebrowser with file in the URL, but not line and column.
Requests the server to answer for file/undefined/undefined.

* Right-click in editor > select a prop > active selection opens
but not with the selected prop

* Call overridden methods at base class constructor.
e.g. startListeningToPageEvents

* When opening the codebrowser for the first time for a bundle,
  have "optimize.clue" preselected.

* Suspend event handlers when component is hidden (optionally). When
  becomes visible, run the handlers of events fired while hidden, each
  once with the latest value for that event.


BUGS:
* Renaming configuration, does not update the tab's name if it's open.
* Open configuration tab probably stays open after deleting configuration.

* Disable "Delete directive" for directives of analysis configuration.
* Support "Keep/Remove method" for directives of analysis configuration,
  but always apply it in regard to current active bundle configuration.
