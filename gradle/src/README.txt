
### Contents of published archive

clue-frontend-X.Y.Z.tar
+-- clue-frontend.min.zip
+-- clue-frontend.zip
+-- README.txt

### "clue-frontend.min.zip" & "clue-frontend.zip"

Both zips contain the same clue-frontend version.

"clue-frontend.min.zip" should be used in clue-server production. The code is
uglified, there are no source maps and app settings are set to production
environment (e.g. logging level equals to ERROR).

"clue-frontend.zip" can be used in clue-server development. The code is again
uglified but in this case source maps are included. App settings are set to
development environment (e.g. logging level equals to DEBUG).
