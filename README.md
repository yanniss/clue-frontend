
# Table of contents

* [Setup system for development](#markdown-header-setup-system-for-development)
* [Develop](#markdown-header-develop)
* [Publish and Release](#markdown-header-publish-and-release)
* [Configure IDE](#markdown-header-configure-ide)




# Setup system for development

1. **Node.js** and **npm** are required in order to work on the project.
To install Node.js and npm in your system, follow the instructions in the following link:

    [Installing Node.js via package manager](https://nodejs.org/en/download/package-manager/)
(Node.js installation includes the node package manager npm).

2. Clone project: `git clone https://ppath@bitbucket.org/yanniss/clue-frontend.git`

3. Inside the project directory run `npm install` to fetch and install all the
required packages from the npm registry.




# Develop

1. Run **clue-server** on port **API_PORT**, used in next step. ([clue-server guide](https://bitbucket.org/yanniss/clue-server/overview))

2. Run frontend server:

        npm run start -- --port=FRONTEND_PORT --apiPort=API_PORT

    where FRONTEND_PORT and API_PORT have default values **3000** and **13000**
respectively, so you may instead simply run:

        npm run start   # is the same as the command below:
        npm run start -- --port=3000 --apiPort=13000

    [ssh local port forwarding may come in handy in the case of API_PORT](
https://bitbucket.org/yanniss/clue-server/overview#markdown-header-forwarding-to-a-different-machine).

    The above command(s) start a local server that:

    * Serves the web app's front end files on `localhost:<FRONTEND_PORT>`.
    * Serves a treemap visualization of modules on `localhost:8888` .
    * Auto reloads the page when a file changes.
    * Offers hot reloading functionality (*under constuction ...*)




# Publish and Release

Publishing and releasing can be achieved through the following commands:

    1) npm run publish:local    # build & publish in maven local
    2) npm run publish:remote   # build & publish in artifactory
    3) npm run release          # build & release in artifactory

The above commands execute npm scripts defined in `/package.json`. All three
of them include two steps:

1. Building the project for two environments, development and production.
The built project files reside in `/build/development/` and `/build/production/`
respectively. This step is handled by **npm & webpack**.

    Note that in this context, *development* and *production* refer to the appropriate
    configuration and transformation of project files in order to be used by `clue-server`.
    Therefore, it should not be confused with possible builds targeting the development
    of the project itself.

2. Archiving the contents of the two directories mentioned in 'step 1' in a zip
archive each, adding them in a .tar archive and publishing/releasing it in
artifactory (a third file is also included in the .tar archive, `/gradle/src/README.txt`,
describing the contents of the archive). This step is handled by **gradle**.

You can run the above steps separately:

    # step 1
    npm run build:dev
    npm run build:prod
    # step 2
    cd gradle
    ./gradlew {publishToMavenLocal|publish|release}

##### More on building for publish/release

There are build configurations for two target environments, development and production.
Both contain minified/uglified code, but only the development build contains source maps.
Each build has its own configuration targeting the specific environment. For example, such
configurations are the logging level and the routes.




# Configure IDE

### Disable saving changes automatically

Running `npm start` runs webpack-dev-server which watches the project app
files for changes and reloads the page each time it finds a change. If your IDE
has "automatic save changes" functionality enabled, the page will reload each
time the IDE auto saves a file.
Therefore, it is suggested to disable this functionality and manually save
changes whenever you want to load the changes in the browser.

##### WebStorm/IntelliJ disable auto save

In WebStorm/IntelliJ you can do that by disabling the following:

* Save files on frame deactivation.
* Save files automatically if application is idle for N sec.

Webpack documentation suggests to also disable:

* Use "safe write" (save changes to a temporary file first).

You may then want to enable the indication of unsaved file with changes:

* Mark modified tabs with asterisk.

### Webpack configuration and code indexing

IDEs generally take import/export statements into account when indexing code.
Ideally they should also consider webpack configuration since it may define
path aliases, e.g.

    // Webpack configuration section
    ...
    resolve: {
        alias: {
            classes: path.join(__dirname, 'public/classes')
        }
    }

    // Usage in app js code; "App" must be the exported entity from "public/classes/App"
    import App from 'classes/App';

##### WebStorm/IntelliJ webpack.config

By default, both WebStorm and IntelliJ will try to read the `webpack.config.js` in the
project root directory as the webpack configuration file. If you want to provide one
with another name or from another location, you should declare it through the IDE's
settings.
Note that the IDE extracts any useful information from the webpack configuration
file by executing it. Therefore, it is recommended to have default values for any
webpack config input argument (e.g. --env.min, --env.production, etc).
